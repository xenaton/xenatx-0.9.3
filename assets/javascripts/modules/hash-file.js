// *********
// HASH FILE
// *********

function shasum(checksumTrigger, checksumResult) {
    var oFile = document.getElementById(checksumTrigger).files[0];

    var hashSHA512 = CryptoJS.algo.SHA512.create();

    var read = 0;
    var unit = 1024 * 1024;
    var blob;
    var reader = new FileReader();
    reader.readAsArrayBuffer(oFile.slice(read, read + unit));
    
    reader.onload = function(e) {
        var bytes = CryptoJS.lib.WordArray.create(e.target.result);
        hashSHA512.update(bytes);

        read += unit;
        if (read < oFile.size) {
            blob = oFile.slice(read, read + unit);
            reader.readAsArrayBuffer(blob);
        } else {
            var hash = hashSHA512.finalize();
            document.getElementById(checksumResult).innerHTML = '<p class="alert alert-info"><i class="bi bi-hash"></i> <strong>' + i18next.t('various.hashName') + ' SHA-512</strong> :<br> ' + hash.toString(CryptoJS.enc.Hex).toUpperCase() +'</p>'
        }
    }
}

// use on two tabs
(function() {

    "use strict";
    
    // tab security of code
    document.getElementById('checksumZip').addEventListener('change', function(e) {
        e.preventDefault();
        
        var checksumResult = document.getElementById('checkSumZipResult');
        checksumResult.innerHTML = '';

        shasum('checksumZip','checkSumZipResult');
    })

    // tab hash
    document.getElementById('checksumFile').addEventListener('change', function(e) {
        e.preventDefault();
        
        var checksumResult = document.getElementById('checkSumFileResult');
        checksumResult.innerHTML = '';
        
        shasum('checksumFile','checkSumFileResult');

        removeDisabled('cancelHash'); // managed in hash.js
    })
})();
