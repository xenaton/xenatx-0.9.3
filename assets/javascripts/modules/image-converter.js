// ************************************************************
// IMAGE to TEXT base 64 and TEXT base 64 to IMAGE CONVERTERS
// ************************************************************

(function() {

    "use strict";

    let base64String = "";

    function imageUploaded() {

        var fileConverter = document.getElementById("imageToTextTransformFile").files[0];
        var readerTransform = new FileReader();
        
        readerTransform.onload = function () {
            base64String = readerTransform.result.replace("data:", "")
                .replace(/^.+,/, "");
    
        var imageBase64String = base64String;
            read(imageBase64String);
        }
        if(fileConverter) {
            readerTransform.readAsDataURL(fileConverter);
        }
    }
    
    function read(imageBase64String) {
        var texta = document.createElement("textarea");
        texta.setAttribute('id', 'texta');
        texta.setAttribute('class', 'form-control');
        texta.setAttribute('rows', '8');

        var textaParent = document.getElementById("textaLocation");
        textaParent.innerHTML = ''; // empty the eventual precedent textarea
        textaParent.appendChild(texta);
        // fill textarea
        var textarea = document.getElementById("texta");
        textarea.value = imageBase64String;
        
        // add copy button
        var copyImageConverter = document.createElement('button'); // is a node
        copyImageConverter.className = "btn btn-sm btn-outline-primary btn__copy";
        copyImageConverter.setAttribute("id", "copyImageConverter");
        copyImageConverter.setAttribute("data-clipboard-target", "#texta");
        copyImageConverter.innerHTML = '<i class="bi bi-clipboard"></i> ' + i18next.t('various.copy');
        textaParent.appendChild(copyImageConverter);

        // no other way to add-duplicate "copy clipboard" here because nodes are appended later.
        var copyImageConverter = document.getElementById('copyImageConverter');
        if(copyImageConverter){
            copyImageConverter.addEventListener('click', function(e){
                copyImageConverter.onclick = function() {
                    const target = copyImageConverter.previousElementSibling;

                    // Init clipboard -- offical documentation: https://clipboardjs.com/
                    var clipboard = new ClipboardJS(copyImageConverter, {
                        target: target,
                        text: function() {
                            return target.value;
                        }
                    });

                    // Success action handler
                    clipboard.on('success', function(e) {
                        const currentLabel = copyImageConverter.innerHTML;

                        // Exit label update when already in progress
                        if(copyImageConverter.innerHTML === i18next.t('various.copied')){
                            return;
                        }

                        // Update button label
                        copyImageConverter.innerHTML = i18next.t('various.copied');

                        // Revert button label after 3 seconds
                        setTimeout(function(){
                            copyImageConverter.innerHTML = currentLabel;
                        }, 3000)
                    })
                }
            })
        }
    }

    // Transform image to text
    // -----------------------
    var imageToTextConverter = document.getElementById('transformBtn');
    if(imageToTextConverter){
        imageToTextConverter.addEventListener('click', function(e){
            e.preventDefault();
            imageUploaded();
            removeDisabled('cancelImageToTextConverter');
        })
    }

    // Cancel Image to Text conversion
    var cancelImageToTextConverter = document.getElementById('cancelImageToTextConverter');
    if(cancelImageToTextConverter){
        cancelImageToTextConverter.addEventListener('click', function(e){
            e.preventDefault();
            var errorConvertToImage = document.getElementById("errorConvertToImage");
            errorConvertToImage.innerHTML = '';
           
            var textaLocation = document.getElementById("textaLocation");
            textaLocation.innerHTML = '';

            var imageToTextTransformFile = document.getElementById("imageToTextTransformFile");
            imageToTextTransformFile.value = '';

            setDisabled('cancelImageToTextConverter');
        })
    }

    function base64ToImage() {
        var image = document.createElement("img"),
            imageParent = document.getElementById("imageLocation"),
            imageBase64String = document.getElementById("texta2").value;

        imageParent.innerHTML = ''; // empty the eventual precedent image
        image.id = "insertedImage";
        image.className = "insertedImage";
        image.src = 'data:image/png;base64,' + imageBase64String;
        imageParent.appendChild(image);
        removeDisabled('cancelTextToImageConverter');
    }

    // Transform text to image
    // -----------------------
    var textToImageConverter = document.getElementById('base64ToImageBtn');
    if(textToImageConverter){
        textToImageConverter.addEventListener('click', function(e){
            e.preventDefault();

            var errorMessage = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.indicateBase64Content') + '</p>';

            var errorConvertToImage = document.getElementById("errorConvertToImage");
            errorConvertToImage.innerHTML = '';
       
            var imageBase64String = document.getElementById("texta2").value;
            if(imageBase64String) {
                if(!isBase64(imageBase64String)) {
                    errorConvertToImage.innerHTML = errorMessage;
                    return;
                }
            }
            else {
                errorConvertToImage.innerHTML = errorMessage;
                return;
            }
            base64ToImage();
            removeDisabled('cancelTextToImageConverter');
        })
    }

    // Cancel Text to Image conversion
    var cancelTextToImageConverter = document.getElementById('cancelTextToImageConverter');
    if(cancelTextToImageConverter){
        cancelTextToImageConverter.addEventListener('click', function(e){
            e.preventDefault();
            var errorConvertToImage = document.getElementById("errorConvertToImage");
            errorConvertToImage.innerHTML = '';
           
            var texta2 = document.getElementById("texta2");
            texta2.value = '';

            var imageLocation = document.getElementById("imageLocation");
            imageLocation.innerHTML = '';

            setDisabled('cancelTextToImageConverter');
        })
    }
})();
