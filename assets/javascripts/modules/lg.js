var resources = {
    fr: {
        translation: {
            "error": {
                "otp" : {
                    "trainingRandomOtpQuantityField" : {
                        "minimum" : "Merci d'indiquer une longueur minimum de 10 chiffres.",
                        "maximum" : "Merci d'indiquer une longueur maximum de 250 chiffres.",
                        "empty" : "Merci d'indiquer la longueur de l'OTP à créer.",
                        "NaN" : "Merci d'indiquer un nombre comme longueur d'OTP à créer.",
                        "moduloChecker" : "Merci d'indiquer une longueur qui est un multiple de 5."
                    }
                },

                "symIndicateBinaryKey" : `Merci d'indiquer une clé de 256 bits.`,
                "symIndicateSaltShort" : `Merci d'indiquer le sel de votre clé.`,
                "symIndicatePasswordShort" : `Merci d'indiquer votre mot de passe ou phrase secrète.`,
                "symProblemReconstitutionSubtitle" : `Erreur de reconstitution de clé : `,
                "symProblemReconstitution" : `Un problème est survenu lors de la tentative de récupératon de la clé symétrique !`,
                "symProblemCipherDecrytpSubtitle" : `Erreur de déchiffrement symétrique : `,
                "symProblemCipherSubtitle" : `Erreur de chiffrement symétrique : `,
                "symIndicateYourHexKey" : `Merci d'indiquer une clé symétrique au format hexadécimal et faisant 64 caractères (256 bits).`,
                "symIndicateYourContentToEncrypt" : `Merci d'indiquer votre contenu textuel à chiffrer.`,
                "symIndicateYourContentToDecrypt" : `Merci d'indiquer votre contenu textuel à déchiffrer.`,
                "symIndicateYourKey" : `Merci d'indiquer votre clé symétrique.`,
                "symIndicateSalt" : `Merci d'indiquer un sel après l'avoir généré sur l'onglet "Entropie du sel".`,
                "symProblemKeyGenerationSubtitle" : `Erreur de génération de clé symétrique : `,
                "symProblemKeyGeneration" : `Un problème est survenu lors de la génération de clé !`,
                "symIndicatePassword" : `Merci d'indiquer un mot de passe ou phrase secrète d'au moins 40 caractères pour créer votre clé symétrique.`,
                "indicateBase64Content" : `Merci d'insérer un contenu textuel encodé en base64.`,
                "convertToHexMultiQRC": `Ce contenu contient des caractères spéciaux non transportables tels quels par les QR Codes, veuillez convertir au préalable en hexadécimal tout ou partie de ce contenu dans l'onglet Convertisseurs.`,
                "asymPasswordEmpty": "Le mot de passe de votre clé privée doit être indiqué dans l\'onglet Clés.",
                "privateKeyEmpty": "La clé privée doit être indiquée dans l\'onglet Clés.",
                "publicKeyEmpty": "La clé publique doit être indiquée dans l\'onglet Clés.",
                "asymMessageToCryptEmpty": "Merci d\'indiquer un message à chiffrer !",
                "asymMessageToUncryptEmpty": "Merci d\'indiquer un message à déchiffrer !",
                "asymPasswordMinLength": "Merci d'indiquer un mot de passe d\'au moins 16 caractères pour protéger votre clé privée.",
                "publicKeyInfoEmpty": "Merci d'indiquer une clé publique.",
                "revocationPubKeyfieldEmpty": "Merci d'indiquer une clé publique à révoquer.",
                "revocationCertificateFieldEmpty": "Merci d'indiquer le certificat de révocation.",
                "hashWellCalculated": "L\'empreinte SHA-512 indiquée ci-dessous correspond bien à celle calculée !",
                "hashNotCorresponding": "Empreinte SHA-512 non correspondante !",
                "hexaToUTF8Empty": "Merci d\'indiquer un contenu hexadécimal à convertir au format texte UTF8 !",
                "notInHexa": "Ce contenu n\'est pas au format hexadécimal. Merci de relire votre saisie !",
                "textToHexaEmpty": "Merci d\'indiquer un contenu textuel à convertir au format hexadécimal !",
                "hexaToTextEmpty": "Merci d\'indiquer un contenu hexadécimal à convertir au format texte !",
                "textUpperOnlyEmpty": 'Merci d\'indiquer un contenu textuel en MAJUSCULES avec uniquement des chiffres et lettres latines et le seul signe point [.], ou convertissez votre contenu au format hexadécimal avec le bouton "Convertir avant transmission" !',
                "OTPUpperOnlyEmpty": "Merci d'indiquer un contenu textuel uniquement en MAJUSCULES avec que des chiffres et lettres latines et les seuls signes suivants . : ' + - = ? [] ()",
                "moduloBrackets": "Merci de vérifier que vous avez bien fermé les crochets [] indiquant l'usage de chiffres. Leur nombre est actuellement impair.",
                "moduloParenthesis": "Merci de vérifier que vous avez bien fermé les parenthèses () dans vos formules. Leur nombre est actuellement impair.",
                "textCipheredEmpty": 'Merci d\'indiquer un contenu textuel, chiffré de préférence pour la sécurité.',
                "textToMorseEmpty": 'Merci de remplir le champ avec un contenu textuel, chiffré de préférence pour la sécurité.',
                "textOrHexaEmpty": 'Merci d\'indiquer un contenu ou de le convertir en hexadécimal au préalable avec le bouton "Convertir avant transmission".',
                "transmissionProblem": 'Un problème est survenu lors de la transmission !',
                "transmitMessageBeforeHash": 'Merci de transmettre le message avant de transmettre son empreinte.',
                "hashKO": "Les empreintes ne sont pas équivalentes !",
                "contentToQRCodeEmpty": "Merci d'indiquer un contenu à transformer en QR Code.",
                "numberOfQRCodesToReadEmpty": "Merci d'indiquer un nombre, en chiffres, de QR Codes à lire.",
                "QRCodeEmpty": "Merci d'indiquer un contenu de QR Code.",
                "QRCodesEmpty": "Merci d'indiquer un contenu dans tous les champs de QR Code.",
                "tooBigForQRCode1": "La taille des informations est trop importante pour un transfert par QR Code. Elle excède ",
                "tooBigForQRCode2": " caractères. Réduisez votre message ou coupez-le en deux.",
                "passwordPrivKeyEmpty": "Le mot de passe de votre clé privée doit être indiqué.",
                "textToSignEmpty": "Merci d\'indiquer un contenu textuel à signer.",
                "privKeyEmpty": "Clé privée absente ou incorrecte.",
                "signInfoMissing": "Des informations importantes sont manquantes ou incorrectes, merci de vérifier.",
                "signatureKO": 'ATTENTION - La signature de ce contenu textuel n\'est pas valide. La clé publique indiquée n\'est peut-être pas la soeur "jumelle" de la clé privée ayant servi à la signature. Autre raison : si dans les détails suivants il est indiqué [ Signed digest did not match ] cela signifie que le contenu textuel ne correspond pas à l\'original. Il peut avoir été falsifié ou vous avez mal recopié l\'original. Détails : ',
                "xorDifferentLengths": 'Grave échec lors de la tentative d\'XOR de flux binaires de différentes longueurs ! ',
                "OTPDecryptSubtitle": 'Déchiffrement (OTP) : ',
                "OTPEmpty": 'Merci d\'indiquer une clé numérique (masque à usage unique - OTP).',
                "OTPNumericEmpty": 'Merci d\'indiquer une clé numérique (masque à usage unique - OTP).',
                "OTPNumericTextEmpty": 'Merci d\'indiquer un contenu textuel en clair.',
                "OTPNumericNotMultiple5Block": 'Merci d\'indiquer une clé numérique composée d\'un nombre de chiffres dont la longueur totale est multiple de 5. Veuillez relire les explications pour plus de précisions.',
                "OTPIdentifiersNotNumber": 'Merci d\'indiquer des identifiants sous forme de chiffres exclusivement. Prenez le temps de relire les explications.',
                "OTPIdentifiersEmpty": 'Merci d\'indiquer des identifiants.',
                "OTPIdentifiersTooShort": 'Merci d\'indiquer un identifiant d\'au moins 5 chiffres.',
                "OTPIdentifiersNoMatch": 'Pas de correspondance.',
                "OTPIdentifiersOneMatch": '(cet identifiant de message correspond à l\'identifiant d\'une de vos clés OTP)',
                "OTPIdentifiersManyMatch": '(ces identifiants de messages séparés par une virgule correspondent à des identifiants de vos clés OTP)',
                "clearMessageEmpty": 'Merci d\'indiquer un message en clair.',
                "clearCodeEmpty": 'Merci d\'indiquer un code en clair.',
                "encryptedCodeEmpty": 'Merci d\'indiquer un contenu chiffré à déchiffrer.',
                "clearCodeNotNumber": 'Merci d\'indiquer un code en clair sous forme de chiffres exclusivement. Prenez le temps de relire les explications, la sécurité de vos communications est en jeu.',
                "encryptedCodeNotNumber": 'Merci d\'indiquer un contenu chiffré sous forme de chiffres exclusivement. Prenez le temps de relire les explications.',
                "OTPNumericNotNumber": 'Merci d\'indiquer un code en clair et une clé numérique sous forme de chiffres exclusivement. Prenez le temps de relire les explications.',
                "textToCryptAndOTPIdentical": 'L\'OTP et le message à chiffrer ne doivent en aucun cas être identiques. Merci de relire votre saisie !',
                "clearCodeAndOTPIdentical": 'Le code en clair et la clé OTP numérique ne doivent en aucun cas être identiques. Merci de relire votre saisie !',
                "encryptedCodeAndOTPIdentical": 'Le contenu chiffré et la clé numérique (masque - OTP) ne doivent en aucun cas être identiques. Merci de relire votre saisie !',
                "textToDecryptAndOTPIdentical": 'L\'OTP et le message à déchiffrer ne doivent en aucun cas être identiques. Merci de relire votre saisie !',
                "OTPLongEnough": 'Merci d\'indiquer une clé numérique (masque à usage unique - OTP) aussi longue que le message en clair à chiffrer.',
                "OTPNumericLongEnough": 'Merci d\'indiquer une clé numérique (masque à usage unique - OTP) aussi longue que le code en clair à chiffrer.',
                "OTPNumericEncryptedCodeLongEnough": 'Merci d\'indiquer une clé numérique (masque à usage unique - OTP) aussi longue que le contenu chiffré à déchiffrer.',
                "textToDecryptEmpty": 'Merci d\'indiquer un message à déchiffrer.',
                "convertTextToHexa": "Merci d'indiquer un contenu textuel à convertir au format hexadécimal !",
                "bracketsMissing": `Il manque des crochets [] pour indiquer l'utilisation de chiffres.`,
                "bracketsMissingForSpace": `Vous ne pouvez pas utiliser d'espace dans un contenu figuratif comprenant des chiffres ou signes.`,
                "doubleSpaces": "Vous ne pouvez pas utiliser deux espaces à la suite. Sauvegardez votre contenu en le sélectionnant puis en le copiant et finalement appuyez sur Annuler pour relire et ajuster votre saisie après le caractère ",
                "steganographyHideEmpty" : "Merci d\'indiquer un contenu à dissimuler.",
                "steganographyRevealEmpty" : "Merci d\'indiquer une image pour en révéler le contenu dissimulé.",
                "steganographyFileEmpty" : "Merci d\'indiquer une image.",
                "steganographyFileNotImage" : "Merci d\'indiquer un fichier de type image.",
                "steganographyhidingCapacity" : "caractères dissimulables."
            },

            "success": {
                "revocationCertificate": "Cette clé publique a bien été révoquée. Chargez cette clé révoquée qui  inclue désormais les informations de révocation et déposez-la dans votre dépôt de clés."
            },

            "various": {
                "downloadIllustration" : "Télécharger en PDF cette illustration",
                "keyRevocationStatusYes" : "Clé révoquée ! N'utilisez plus cette clé pour chiffrer. Vous pouvez juste l'utiliser pour vérifier des signatures d'anciens messages chiffrés.",
                "keyRevocationStatusNo" : "Clé valide (non révoquée)",
                "copied" : "Copié !",
                "onlyOneQRCodeGenerated": `Un seul QR Code a été généré pour transporter ce contenu textuel.`,
                "manyQRCodesGenerated": ` QR Codes ont été générés pour transporter ce contenu textuel.`,
                "madeIn" : "Développé en France",
                "blockOf4": "Présentation par bloc de 4 caractères",
                "indicateChainToHash": "Merci d\'indiquer une chaîne de caractères pour en calculer l\'empreinte.",
                "hashName": "Empreinte",
                "hashOK": "Les empreintes sont bien équivalentes. La transmission s\'est déroulée correctement.'",
                "indicateHashToCompare": "Merci d\'indiquer deux empreintes à comparer.",
                "signatureOK": 'Signature vérifiée et valide. Ce contenu textuel a bien été signé par la clé privée soeur "jumelle" de cette clé publique dont l\'identifiant est',
                "signatureVerification": 'Vérification de signature : ',
                "copy": 'Copier',
                "validate": 'Valider',
                "cancel": 'Annuler',
                "encrypt": 'Chiffrer',
                "decrypt": 'Déchiffrer',
                "chars" : "caract.",
                "backToTop": "Retour en haut"
            },

            "meta": {
                "title": "XENATON - XENATX - Application autonome",
            },

            "nav": {
                "dashboard": "Tableau de bord",
                "asymCipher": "Chiffr. Asym.",
                "signature": "Signature",
                "symCipher": "Chiffr. Sym.",
                "otpCipher": "OTP",
                "tools": "Outils"
            },

            "dashboard": {
                "title": "Tableau de bord",
                "subtitle": "",

                "features" : {
                    "title" : "Fonctionnalités",
                    "introduction": `<p>              
                        Le XENATX fonctionne comme un site web, dans un navigateur, mais sans besoin d'être connecté à Internet. Le XENATX est donc une application autonome utilisable avec les trois systèmes d'exploitation Windows, MacOS, Linux. Quelques restrictions d'usage avec iOS sur iphone&nbsp;: le son ne fonctionnera pas et l'application gratuite de type Files, embarquant une liseuse HTML, peut être nécessaire en complément. Pour Android, cela dépend des versions et modèles mais globalement la plupart des fonctionnalités sont opérationnelles.
                        </p>
                        <p>
                        Pour générer des clés, chiffrer ou déchiffrer, le XENATX doit être utilisé de préférence sur un équipement dit OFF, donc non connecté et jamais re-connecté.
                    </p>`,

                    "menuCipherAsym": "Chiffrement asymétrique et génération de clés",
                    "menuCipherAsymKeyInfo": "Informations et révocation de clé publique",
                    "menuSignature": "Signature et vérification de signature",
                    "menuCipherSym": "Chiffrement symétrique et génération d'une clé",
                    "menuOTP": "Chiffrement par masque à usage unique - OTP",
                    "menuQRCode": "QR Code - Transfert optique sécurisé",
                    "menuTAM": "TAM - Morse - Transfert sonore sécurisé",
                    "menuHash": "Calcul d'empreinte - Hash",
                    "menuConverterHexa": "Convertisseurs - Hexadécimal et Image",
                    "menuCodeSecurity": "Sécurité du code",
                    "menuXenaton": "XENATON",
                    "menuSteganography": "Stéganographie",

                    "paragraph01": `<p>
                        Rendez-vous dans l'onglet <i class="bi bi-file-code"></i> Sécurité du code pour vérifier le code du XENATX en suivant les consignes indiquées. Vous pouvez également en savoir plus sur nos services dans l'onglet <i class="bi bi-patch-question"></i> XENATON.
                    </p>
                    `
                },

                "codeSecurity": {
                    "title": "Sécurité du code",
                    "quickCheck" : `
                        <p class="h3">
                            Vérification rapide
                        </p>
                        <p>
                            Vous pouvez procéder ci-dessous au calcul rapide de l'empreinte SHA-512 du dossier zippé xenatx-*.zip que vous avez téléchargé. Comparez le résultat avec l'empreinte fournie sur le site <a href="https://gitlab.com/xenaton/xenatx/-/wikis/version" target="_blanck">https://gitlab.com/xenaton/xenatx/-/wikis/version</a> et sur notre canal Telegram <a href="https://t.me/xenaton_official" target="_blanck">https://t.me/xenaton_official</a>
                        </p>
                    `,
                    "calculateHashField" : `Calcul de l'empreinte SHA-512 du dossier .zip`,
                    "paragraph" : `
                        Doublez le contrôle ci-dessus, en utilisant par exemple un logiciel libre tel que HashCheck (https://code.kliu.org/hashcheck/) pour Windows. 
                        <br>Avec MacOS et Linux, vous pouvez simplement taper dans un terminal la ligne de commande suivante&nbsp;: shasum -a 512 ~/Desktop/xenatx-*.zip en présumant que vous avez placé le dossier zippé sur votre "Bureau" après téléchargement. Pensez à remplacer l'étoile (*) par la bonne version du XENATX.
                        <hr>
                        Il existe aussi de nombreux outils en ligne vous permettant de faire facilement ce calcul d'empreinte tel que https://www.tools4noobs.com/online_tools/hash/ ou https://md5file.com/calculator à titre d'exemples.
                    `,

                    "general" : {
                        "subtitle": "Généralités",
                        "text" : `<p>       
                                        Tout utilisateur du XENATX est en droit, voire en devoir, de se demander ce qui garantit la sécurité et l'innocuité du code.
                                    </p>
                                    <p>  
                                        De notre point de vue, la réponse la plus valable est que les parties critiques du code proviennent de bibliothèques Open Source. Nous les utilisons sans les modifier et nous vous indiquons comment le vérifier.
                                    </p>
                                    <p>  
                                        Cependant, même si nous vous garantissons que vous pouvez avoir confiance, nous préconisons pour votre sécurité de... ne pas nous faire confiance... <br>Autrement dit, procédez toujours à la vérification du code du XENATX.
                                        <br>Pourquoi ? Parce que même si nous n'avons pas mis de porte dérobée, vous ne devez pas nous croire sur parole. Par ailleurs, des attaques sophistiquées ont permis la modification de code à la volée, parfois même uniquement pour certains utilisateurs en fonction de leur "fingerprint" (empreinte multi-factorielle de leur équipement informatique). 
                                    </p>
                                    <p>
                                        Le code du XENATX peut donc être intercepté et modifié par un tiers, au vol en quelque sorte, durant le chemin entre notre serveur et votre équipement, lors du téléchargement. Tout est possible quand vous êtes relié à Internet.
                                    </p>
                                    </p>
                                        Cette application est destinée à fonctionner, de préférence, sur un équipement hors ligne. Ainsi, s'il y avait une porte dérobée dans le XENATX, ce qui n'est pas le cas mais pourrait l'être par voie détournée comme vu ci-dessus, elle ne pourrait pas être exploitée, car aucun lien filaire ou par ondes n'existe avec votre équipement faisant fonctionner le XENATX. 
                                        <br>Nous nommerons tout équipement hors ligne avec le qualificatif "OFF" pour le distinguer d'un équipement "ON", connecté à Internet. 
                                        <br>Un OFF n'a donc pas de wifi, pas de bluetooth et pas de liaison filaire ethernet. Il n'y a donc pas de possibilité de fuite de données sensibles comme une clé privée, un message en clair avant chiffrement, un mot de passe, etc.
                                    </p>
                                    <p>
                                        Après téléchargement, il vous restera à vérifier que les bibliothèques open source, n'ont pas été modifiées par nous ou "à la volée", comme indiqué précédemment, pour affaiblir par exemple leur qualité cryptographique. Vous calculerez l'empreinte globale du XENATX (vu au premier paragraphe) pour la comparer à celle que nous indiquons sur le site <a href="https://gitlab.com/xenaton/xenatx/-/wikis/version" target="_blank">https://gitlab.com/xenaton/xenatx/-/wikis/version</a>, sur Telegram <a href="https://t.me/xenaton_official" target="_blank">https://t.me/xenaton_official</a>, sur Twitter, Mastodon et sur notre site (liens sur les icônes bleues en pied de page). Idéalement vous vérifierez également les empreintes de chaque bibliothèque open source. Nous expliquons comment faire dans le paragraphe ci-dessous "Bibliothèques Open Source".
                                    </p>
                                    <p>
                                        Vous l'aurez compris, nous préconisons, avec force, l'usage du XENATX sur un équipement OFF (comme le XENATOFF en cours de développement). Les QR Codes ou le TAM (Morse) servent alors au transfert sécurisé sans contact de données, depuis votre équipement OFF vers votre équipement ON. Vous pouvez ainsi transférer tous types de données comme des messages chiffrés, des clés publiques, des signatures, etc.
                                    </p>
                                    <p>
                                        Ces voies sécurisées vous évite l'utilisation d'une clé USB pour le transfert de données d'un équipement à un autre. Sans parler du wifi ou bluetooth dont la circulation double sens des données est invérifiable pour le grand public, voire pour tout le monde (cf. l'histoire de Crypto AG rappelée dans l'onglet XENATON).
                                        <br>Une clé USB peut facilement être infectée par un malware (via la faille badUSB entre autres) se cachant dans le firmware. Le malware pourrait alors se propager d'un équipement à un autre sans que vous puissiez vous en rendre compte. Il risquerait ensuite au prochain transfert vers votre équipement ON de faire fuiter vos données sensibles stockées sur votre équipement OFF. Tous les efforts de protection par chiffrement seraient anéantis.
                                    </p>
                                    <p>
                                        La conception de cette suite d'outils de Cyber Sécurité Personnelle intégrant dès l'origine un fonctionnement mi ON mi OFF, avec transferts sans contact, vous garantit le concept de "Privacy by design", sans besoin d'être un expert en sécurisation d'équipement informatique. 
                                    </p>
                                    <p>
                                        Le couplage du XENATX avec les services en ligne XenaTeam, XenaTrust et XenaTraining n'est pas indispensable mais ces services sont complémentaires. 
                                        <br>Si vous faites partie d'associations de défense des libertés, vous pouvez peut-être accéder à ces services gratuitement, sinon demander à votre association de nous contacter pour un partenariat.
                                    </p>       
                                    <p>
                                        Dans le cas de l'utilisation de notre service XenaTrust, les messages chiffrés déposés dans la DLB (Dead Letter Box - boîte aux lettres morte) sont indéchiffrables par nos serveurs. En effet, votre clé privée servant au déchiffrement reste toujours sur votre équipement ON, voire mieux, sur votre équipement OFF, ou encore mieux sur une clé USB ou microSD, elle-même chiffrée, et exclusivement utilisée sur votre équipement OFF quand cela est nécessaire.
                                    </p>
                                    <p>
                                        Nous ne stockons donc jamais vos clés privées et n'essayons pas d'y accéder. Physiquement, nous ne le pourrions même pas si vous utilisez un équipement OFF. Idem pour les données de types clé symétrique, mot de passe et sel utilisées à la section <i class="bi bi-file-earmark-lock"></i> Chiffrement symétrique.
                                    </p>
                                    <p>
                                        Vous constaterez qu'aucune fonction JavaScript AJAX n'est là pour rapatrier discrètement des données sensibles vers un quelconque serveur <i>(rappel&nbsp;: si vous utilisez un équipement OFF une fuite n'est physiquement même pas possible)</i>. 
                                        <br>Vous devez bien sûr avoir quelques compétences en informatique pour le vérifier. Le code est néanmoins relativement simple. Le travail difficile étant fait par les bibliothèques open source co-créées et maintenues par des dizaines de développeurs.
                                    </p>
                                    `
                    },

                    "license" : {
                        "subtitle" : "Licence",
                        "text" : `<p>
                                    La licence est maintenue "propriétaire" mais l'usage est gratuit et sans limitation.
                                </p>
                                <p>    
                                    Le code est ouvert et volontairement non obfusqué, donc parfaitement vérifiable et améliorable. 
                                    <br>Nous indiquons d'ailleurs la méthodologie détaillée pour cette vérification et encourageons à la faire. Rendez-vous pour cela sur l'onglet <i class="bi bi-file-code"></i> Sécurité du code de cette présente section.
                                </p>
                                <p>
                                    Nous attirons votre attention sur le fait qu'aucune garantie n'est fournie par XENATON quant à l'usage du XENATX qui se fait sous la seule reponsabilité de l'utilisateur. 
                                    <br>Des informations complémentaires sur ce sujet sont indiquées dans le fichier README.txt à la racine du dossier XENATX téléchargé. 
                                </p>`
                    }, 

                    "resilience" : {
                        "subtitle" : "Résilience",
                        "text" : `<p>
                                    Concernant nos services en ligne, si nous devions "faire faillite", a minima le XENATX restera utilisable. Il s'utilise de façon autonome sur votre équipement informatique.
                                </p>
                                <p>
                                    Par ailleurs toutes les notions apprises, grâce aux explications dans le XENATX et à notre programme de formation XenaTraining en ligne, resteront pour vous des acquis valables et réutilisables partout. Ces connaissances concernent des protocoles ouverts et des techniques et connaissances logiques.
                                </p>
                                <p>
                                    Les clés de chiffrement et les signatures créées avec le XENATX, traduisant la lente construction d'un réseau de confiance, resteront pleinement valides, gratuites et réutilisables facilement dans d'autres systèmes. Ces clés et signatures reposant également sur des protocoles et des standards ouverts et éprouvés.
                                </p>`     
                    },

                    "library" : {
                        "subtitle" : "Bibliothèques Open Source",
                        "text" : `<p>
                                    Nous avons listé toutes les bibliothèques JavaScript utilisées. Vous pouvez les retrouver dans le dossier assets/javascripts/libs dans le dossier source XENATX que vous avez dézippé. Vous pouvez également vérifier le contenu de nos codes JS dans assets/javascripts/modules. Nos codes JS ne sont volontairement pas minifiés, ni obfusqués. Nous indiquons les empreintes sur https://gitlab.com/xenaton/xenatx/-/wikis/version pour que vous puissiez vérifier que rien n'a été altéré par un MITM (Man In The Middle) ou autres techniques.
                                </p>
                                <p>    
                                    Utilisez l'onglet <i class="bi bi-hash"></i> Empreinte - Hash pour calculer les empreintes SHA-512 et les comparer entre elles. Par sécurité, doublez la vérification des empreintes de la façon indiquée plus haut dans l'encadré. 
                                    <br>Merci de bien vérifier deux fois avant de penser à nous contacter si les empreintes diffèrent pour l'une ou l'autre des bibliothèques ci-dessous. Il suffit d'un commentaire laissé ou d'un retour à la ligne en trop pour rendre les empreintes différentes.
                                </p>
                                <div class="alert alert-info">
                                    Nous avons implémenté et agrémenté de nos propres codes les bibliothèques suivantes pour créer un système complet et original par son concept. Cependant le premier des mérites revient bien aux créateurs et contributeurs de ces bibliothèques, et, en cascade, à tous ceux qui les ont inspirés. Pour certaines, ces bibliothèques constituent des briques essentielles du XENATX.
                                    <hr>
                                    <i class="bi bi-hand-thumbs-up"></i> Remerciements particuliers à davidshimjs Sgnamin Shim (qrcode.js),  Fabian Kurz (jscwlib.js - JS Morse Code Library), Joshua M. David (OTP functions), Stephen C. Philips (Morse Pro), Julian Fietkau (Mozaic visual hash), Peter Eigenschink (Steganography.js) et Guillaume (langue et correction de bug).
                                </div>
                                <p>
                                    <strong>Audio-decoder-adaptive - Décodage morse</strong>
                                    <br>Source publique : https://morsecode.world/js/audio-decoder-adaptive.96da976088f5fb7c78e537b303a0ebee.js 
                                    <br>Si la chaine suivante entre crochets du lien ci-dessus change [ 96da976088f5fb7c78e537b303a0ebee ].js, copiez et collez juste le contenu https://morsecode.world/js/audio-decoder-adaptive.*.js et collez-le localement en remplacement. 
                                    <br>Les différences ne devraient pas être grandes et vous êtes ainsi garantis d'utiliser une source publique non "manipulée". La bibliothèque complète est ici, pour étude : https://github.com/scp93ch/morse-pro
                                    <br>Appel en pied de page du fichier index.html : assets/javascripts/libs/audio-decoder-adaptive-2.0.min.js
                                </p>
                                <p>
                                    <strong>Bootstrap</strong>
                                    <br>Source publique : https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.0/js/bootstrap.min.js
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/bootstrap-5.2.0.min.js
                                </p>
                                <p>
                                    <strong>Chroma</strong>
                                    <br>Source publique : https://raw.githubusercontent.com/gka/chroma.js/main/chroma.min.js
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/chroma-2.4.2.min.js
                                </p>
                                <p>
                                    <strong>Clipboard</strong>
                                    <br>Source publique : https://raw.githubusercontent.com/zenorocha/clipboard.js/master/dist/clipboard.min.js
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/clipboard-2.0.10.min.js
                                </p>
                                <p>
                                    <strong>Crypto-js</strong>
                                    <br>Source publique : https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.1.1/crypto-js.min.js
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/crypto-js-4.1.1.min.js
                                </p>
                                <p>
                                    <strong>Forge</strong>
                                    <br>Source publique : https://cdnjs.cloudflare.com/ajax/libs/forge/1.3.1/forge.min.js
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/forge-1.3.1.min.js
                                </p>
                                <p>
                                    <strong>i18next</strong>
                                    <br>Source publique : https://cdnjs.cloudflare.com/ajax/libs/i18next/21.8.14/i18next.min.js
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/i18next-21.8.14.min.js
                                </p>
                                <p>
                                    <strong>jQuery</strong>
                                    <br>Source publique : https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/jquery-2.2.4.min.js
                                </p>
                                <p>
                                    <strong>Js-cookie</strong>
                                    <br>Source publique : https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js (https://github.com/js-cookie/js-cookie/releases/tag/v2.1.4)
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/js-cookie-2.1.4.min.js
                                </p>
                                <p>
                                    <strong>Jscwlib - Encodage morse</strong>
                                    <br>ATTENTION ! Nous avons opéré quelques changements mineurs sur cette bibliothèque. Lisez le fichier README.txt à la racine du dossier du XENATX pour voir les changements et calculer au mieux l'empreinte pour vérification.
                                    <br>Source publique : https://git.fkurz.net/dj1yfk/jscwlib/raw/branch/master/src/jscwlib.js (https://git.fkurz.net/dj1yfk/jscwlib/
                                    <br>Appel dans le head du fichier index.html&nbsp;: assets/javascripts/libs/jscwlib.js
                                </p>
                                <p>
                                    <strong>Modernizr</strong>
                                    <br>Source publique : https://modernizr.com/download/?touchevents-setclasses-shiv (custom build)
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/modernizr-3.6.0.min.js
                                </p>
                                <p>
                                    <strong>MosaicVisualHash - Empreinte visuelle</strong>
                                    <br>Source publique : https://raw.githubusercontent.com/jfietkau/Mosaic-Visual-Hash/master/mosaicVisualHash.js
                                    <br>Appel dans le head du fichier index.html&nbsp;: assets/javascripts/libs/mosaicVisualHash-1.0.1.js
                                </p>
                                <p>
                                    <strong>Openpgp</strong>
                                    <br>Source publique : https://unpkg.com/openpgp@5.3.1/dist/openpgp.min.js
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/openpgpjs-5.3.1.min.js
                                </p>
                                <p>
                                    <strong>Qrcode</strong>
                                    <br>Source publique : https://cdn.jsdelivr.net/gh/davidshimjs/qrcodejs@gh-pages/qrcode.min.js
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/qrcode.min.js
                                </p>
                                <p>
                                    <strong>Underscore</strong>
                                    <br>Source publique : https://cdn.jsdelivr.net/npm/underscore@1.13.6/underscore-umd-min.js
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/underscore-1.13.6.min.js
                                </p>
                                <p>
                                    <strong>Steganography</strong>
                                    <br>Source publique : https://raw.githubusercontent.com/petereigenschink/steganography.js/master/build/steganography.min.js
                                    <br>Appel dans le head du fichier index.html : assets/javascripts/libs/steganography-1.0.3.min.js
                                </p>`          
                    },

                    "jscodes" : {
                        "subtitle" : "Codes JavaScript",
                        "text" : `
                            <p>
                                Consultez le site <a href="https://gitlab.com/xenaton/xenatx/-/wikis/version" target="_blanck">https://gitlab.com/xenaton/xenatx/-/wikis/version</a> pour comparer tous les fichiers des codes internes JS suivants&nbsp;:
                            </p>
                            <p>
                                <strong>Asym. Cipher</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/asym-cipher.js
                            </p>
                            <p>
                                <strong>Asym. Generate Keys</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/asym-generate-keys.js
                            </p>
                            <p>
                                <strong>Asym. Info Key</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/asym-info-key.js
                            </p>
                            <p>
                                <strong>Asym. Revocation Certificate</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/asym-revcert.js
                            </p>
                            <p>
                                <strong>Core</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/core.js
                            </p>
                            <p>
                                <strong>Hash File</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/hash-file.js
                            </p>
                            <p>
                                <strong>Hash</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/hash.js
                            </p>
                            <p>
                                <strong>Hex converter</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/hex-converter.js
                            </p>
                            <p>
                                <strong>i18n</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/i18n.js
                            </p>
                            <p>
                                <strong>Image converter</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/image-converter.js
                            </p>
                            <p>
                                <strong>Lg</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/lg.js
                            </p>
                            <p>
                                <strong>Morse</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/morse.js
                            </p>
                            <p>
                                <strong>OTP Numérique</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/otp-numeric.js
                            </p>
                            <p>
                                <strong>QR Code</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/qrcode.js
                            </p>
                            <p>
                                <strong>Signature</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/signature.js
                            </p>
                            <p>
                                <strong>Stéganographie</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/steganography-image.js
                            </p>
                            <p>
                                <strong>Sym. cipher file</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/sym-cipher-file.js
                            </p>
                            <p>
                                <strong>Sym. cipher</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/sym-cipher.js
                            </p>
                            <p>
                                <strong>Sym. entropy</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/sym-entropy.js
                            </p>
                            <p>
                                <strong>Various functions</strong>
                                <br>Appel en bas du fichier index.html : assets/javascripts/modules/various-functions.js                               
                            </p>
                        `        
                    },

                    "conclusion" : {
                        "subtitle" : "Conclusion",
                        "text" : `
                            <p>
                                Nous vous remercions de votre confiance, non aveugle comme il se doit. Soyez certains que nous avons le respect de la vie privée et de la souveraineté personnelle chevillé au corps. Découvrez-en plus sur les services de XENATON dans l'onglet <i class="bi bi-patch-question"></i> XENATON.
                            </p>
                            <p>
                                Pour augmenter encore la sécurité fournie par le XENATX et pallier de potentielles fragilités du chiffrement (manque d'entropie des clés, IA, timing attack, calculateur quantique, etc.), plusieurs solutions s'offrent à vous et la liste n'est pas exhaustive.
                            </p>
                            <p>
                                Pour plus d'entropie, les clés peuvent être générées par des CSPRNG en ligne de commande sur des systèmes Linux/Unix. Des générateurs d'entropie additionnels peuvent aussi aider. Le plus courant est lié aux mouvements de la souris, comme celui que nous utilisons dans la section "Chiffrement sym." et l'onglet "Entropie du sel". Le but est de tendre vers le TRNG (True Random Number Generator). 
                                <br>Des logiciels open source comme Veracrypt ont ce type de solution dans leur "Keyfile generator". Par ailleurs, leur fonctionnalité de volume caché est aussi à découvrir. La Stéganographie sous toutes ses formes a également son importance. 
                            </p>
                            <p>
                                Vous pouvez également améliorer la résistance de vos contenus chiffrés par l'utilisation de masques à usage unique (OTP - One-Time-Pad) pour les élements les plus sensibles de vos messages avant "sur-chiffrement classique". Cela nécessite néanmoins l'échange préalable de répertoires d'OTP, bien générés avec un maximum d'entropie. 
                                <br>Pour augmenter l'entropie, tant pour l'AES-256 que pour les masques à usage unique, nous indiquons des solutions de création de clés à base de dés de précision... Lent mais fiable. Cela restera encore longtemps la solution la plus accessible à tous et la plus sûre.
                                <br>Cela peut en valoir la peine dans certaines situations. Nous approfondissons ces sujets dans notre programme XenaTraining mais vous en avez déjà un très bon aperçu dans la rubrique "Explications" de la section "OTP - Masque à usage unique".
                            </p>
                            <p>
                                Dans tous les cas, nous vous conseillons de vous méfier grandement des failles matérielles et logicielles de vos appareils connectés en permanence à Internet... Pensez à utiliser le XENATX, ou un autre logiciel de chiffrement, sur un équipement OFF définitivement hors ligne et faites la liaison avec vos appareils ON grâce aux voies de transfert sécurisé offertes par le XENATX. Faire autrement est illusoire en terme de sécurité réelle.
                            </p>
                            <p>
                                Merci de votre intérêt, bonne utilisation éclairée et bonne reprise d'autonomie.
                            </p>
                        `        
                    },

                },

                "xenaton" : {
                    "titleOrigin" : "Origine et objectif",
                    "originParagraph01" : `
                        <p>
                            XENATON est la contraction de Xenophon et de Platon en hommage à ces deux sages, disciples directs de Socrate. 
                            <br>Sous cette bannière, nous accompagnons les organisations responsables (entreprise, association, collectif, etc.) dans leurs besoins en Cyber Sécurité Personnelle pour leurs collaborateurs ou membres.
                        </p>
                        <p>
                            Vous utilisez actuellement le XENATX, notre application autonome gratuite pour tous. Cette application se couple idéalement avec la suite de services en ligne développée par XENATON. 
                        <p>
                    `,
                    "titlePresentation" : "Présentation succincte de nos services",

                    "titleXenaTrust" : "XenaTrust",
                    "xenaTrustParagraph01" : `
                        <p>
                            Application web en mode SaaS (Software as a Service) offrant un dépôt de messages chiffrés dans une boite aux lettres morte numérique et un service décentralisé et anonyme d’établissement et de propagation de la confiance grâce aux signatures cryptographiques auto-gérées. Le fonctionnement se situe à mi-chemin entre l'Autorité de Certification (PKI) et le réseau de confiance décentralisé. XenaTrust est couplé au XENATX bien sûr et souvent couplé à XenaTraining et XenaTeam.
                        </p>
                    `,

                    "titleXenaTraining" : "XenaTraining",
                    "xenaTrainingParagraph01" : `
                        <p>
                            Application web en mode SaaS favorisant l’apprentissage et la mise à jour des connaissances et savoir-faire essentiels en matière de PCS (Personal Cyber Security). XenaTraining est souvent couplé à XenaTeam, XenaTrust et au XenaTx bien sûr. 
                        </p>
                    `,

                    "titleXenaTeam" : "XenaTeam",
                    "xenaTeamParagraph01" : `
                        <p>
                            Application web en mode SaaS pour la coordination sécurisée d’une équipe&nbsp;: espace sécurisé, gestion de comptes anonymes, dépôts de messages chiffrés dans une boite aux lettres morte numérique, messages en multi-diffusion, service XenatHIC de messages HIC (Hidden In the Crowd), etc. 
                            <br>XenaTeam est automatiquement couplé à XenaTrust, XenaTraining et au XenaTx bien sûr.
                        </p>
                    `,

                    "titleXenatOFF" : "XenatOFF",
                    "xenatOFFParagraph01" : `
                        <p>
                            Equipement informatique portatif de taille réduite dit OFF. Le principe d’un équipement OFF est qu’il est non connecté et jamais re-connecté. Il est sans carte wifi, sans carte bluetooth et avec ses ports USB et ethernet « obstrués ». Une sorte de boite noire.
                            <br>Le XenatOFF est pré-équipé d'une distribution linux complétée d'outils essentiels dont bien sûr le XENATX. Il délivre le plein potentiel de la suite d'outils XenaTx - XenaTrust - XenaTraining - XenaTeam.
                        </p>
                        <p>
                            Cet équipement informatique peut s'utiliser en toute autonomie juste avec un XENATX, ou un autre logiciel de chiffrement, sans lien nécessaire avec nos services en ligne.
                        </p>
                        <p>
                            Pour nos collectifs partenaires, nous indiquons des solutions pour créer un XenatOFF à budget réduit. <br>Si vous n'êtes pas encore partenaire ou client, contactez-nous via xenaton.com
                        </p>
                        <p>
                            Des idées d'équivalents à bas coût existent, telle une simple tour informatique, équipée du XENATX gratuit, et dont les cartes wifi et bluetooth ont été physiquement retirées. Vous pouvez donc dès maintenant créer votre chaîne matérielle et logicielle sécurisée à un budget très limité.
                        </p>
                    `,

                    "titleXenaTx" : "XENATX",
                    "xenaTxParagraph01" : `
                        <p>
                            Présente application gratuite autonome sous forme de simple site web en JavaScript et HTML utilisable sur tous systèmes d'exploitation tels MacOS, Windows, Linux et même Android et iOS avec quelques limitations pour ces deux derniers.
                            <br><i class="text-muted small">Une version plus adaptée aux smartphones est envisagée afin de pouvoir y activer l'ensemble des fonctionnalités.</i>
                        </p>
                        <p>
                            Le XENATX couvre de nombreuses fonctionnalités essentielles&nbsp;: 
                            <ul>
                                <li>Chiffrement asymétrique (OpenPGP.js utilisé également par Proton Mail).</li>
                                <li>Signature cryptographique.</li>
                                <li>Chiffrement symétrique AES-256 en mode CBC avec IV aléatoire.</li>
                                <li>Chiffrement par masque à usage unique (One-Time-Pad) et HIC</li>
                                <li>Calcul d'empreinte (hash)</li>
                                <li>Transmission sécurisée par QR Code.</li>
                                <li>Transmission sécurisée par Transmetteur Acoustique en Morse pour des données chiffrées transmissibles notamment par CiBi/PMR (émetteur radio portatif).</li>
                                <li>Stéganographie.</li>
                            </ul>
                        </p>
                        <p>
                            L’approche sans contact (QR Code et Son) du XENATX est essentielle. Elle évite la contamination ou la fuite de données par les moyens courant de transfert de données (wifi, bluetooth, ethernet, USB, etc.) vers, ou depuis, un XenatOFF ou équipement équivalent.
                        </p>
                        <p>
                            L’équipement OFF est la seule technique offrant une sécurité optimale en prémunissant contre les failles matérielles et autres portes dérobées logicielles présentes sur quasiment tous les équipements (failles « zero day » notamment).
                            <br>La fuite de données, ou l'insertion malicieuse de données, par Internet ou par connectique devient physiquement impossible <em>(hors cas extrême d'attaque par captation à distance du champ électromagnétique de l'équipement : vous trouverez plus d'informations sur ces attaques avec le mot-clé TEMPEST)</em>. 
                        </p>
                        <p>    
                            Les seules « voies » autorisées et vérifiables (QR code et Son) permettent ainsi l’import ou l’export sécurisés de précieuses données tels des messages chiffrés, des clés publiques, des signatures et même des images compressées, etc.
                        </p>
                        <p>
                            Le XenaTx peut s’utiliser seul mais il se couple de façon optimale avec nos trois services en ligne XenaTrust, XenaTraining, XenaTeam et bien sûr notre XenatOFF.
                            </p>
                            <p>
                            Cette approche originale et transparente nous place loin des promesses marketing oblitérant volontairement toutes les failles précitées.
                            </p>
                            <p>
                            Ce sujet des failles "zero day" et autres vous parait peut-être pointu. Si vous souhaitez approfondir pour mieux appréhender notre propos, cherchez du côté de l’IME (Intel Management Engine) et AMD st où les failles/portes dérobées (back door) pourraient être directement dans les processeurs…
                        </p>
                        <p>
                            Vous doutez encore&nbsp;? Cherchez alors du côté de l’entreprise suisse Crypto AG (Hagelin) <a href="https://youtu.be/SWFlA248spU" target="_blank">https://youtu.be/SWFlA248spU</a>
                            <br>Leurs très chers produits de qualité suisse étaient destinés, entre autres, aux ambassades du monde entier. Or ces produits étaient « backdoorés » depuis des dizaines d’années par un grand service à 3 lettres... La faillite complète de Crypto AG n'est intervenue que récemment en 2020 suite à ces révélations.
                            <br>Autre faille plus « grand public » intéressante car répandue&nbsp;: la faille BadUSB touchant les périphériques USB (clé, souris, clavier, etc.)
                        </p>
                        <p>
                            Avec ce très rapide tour d’horizon, vous devriez être convaincu que le chiffrement, ayant déjà ses potentielles faiblesses, n'a qu'un intérêt limité sans sécurisation par mise hors ligne complète et permanente de l'équipement sur lequel il s'effectue.
                        </p>
                    `,

                    "titleRH" : "Ressources humaines",
                    "RHParagraph01" : `
                        <p>
                            Nous recherchons régulièrement des collaborateurs de grande sagesse, à l’ego maitrisé, conscients de l’équilibre subtil et ancestral entre les trois groupes suivants&nbsp;: les « Producteurs », les « Sophistes-Nettoyeurs » et les « Philosophes-Gardiens ». Nuances et imbrications naturelles mises à part. Selon les savoirs issus de la grèce antique et notre propre grille d'analyse.
                            <br>Cette compréhension nous assure que nos collaborateurs auront l'approche optimale des composantes et problématiques d’une situation et d'un environnement. En somme, ils adopteront une attitude humaniste mais réaliste.
                        </p>
                        <p>
                            Etre conscient des raisons de la prédominance des Sophistes sur le marché actuel nous parait essentiel. Le marché étant déséquilibré, il favorise l'existence et le développement des sophistes. Leur rôle de testeurs de sagacité, de « nettoyeurs » sans empathie, s’en trouve mieux compris, sans en justifier les excès. 
                            <br>Les termes de "producteurs d’images et d'apparences ou de producteurs de prodiges dans les discours", utilisés par Platon dans la conclusion de son ouvrage Le Sophiste, trouvent aujourd’hui, plus que jamais, leur parfaite illustration.
                        </p>
                        <p>
                            A nous tous d'assainir la situation par des services centrés sur l'équilibre, l'équité, la lucidité et la souveraineté.
                        </p>
                    `,

                    "titleTrainers" : "Réseau de formateurs",
                    "trainersParagraph01" : `
                        <p>
                            Nous développons notre réseau de formateurs indépendants pour l'accompagnement de nos clients. Nous les formons gratuitement à nos solutions informatiques tout en affinant leur vision stratégique des équilibres dans les systèmes d'organisation.
                        </p>
                        <p>
                            Toute forme de déni, comme sans exclusive l’angélisme, ne peut et ne doit pas trouver d'écho chez nos collaborateurs. L'approche auprès de nos clients est humaniste mais pragmatique.
                            <br>Montesquieu, et ses pairs avant lui, nous a rappelé que "Tout Homme, ou groupe, entreprise, organisation, etc., qui a du pouvoir est porté à en abuser. Il faut donc que par la disposition des choses le pouvoir arrête le pouvoir." ...
                        </p>
                        <p>
                            De fait, XENATON se tient loin de l'angélisme béat. La Cyropédie de Xenophon, le Sophiste de Platon, l’Art de la guerre de Sun Tzu, la Servitude volontaire de la Boétie et les Discours sur la première décade de Tite-Live de Machiavel figurent parmi les ouvrages que nous recommandons à nos collaborateurs afin que l'équilibre réaliste soit leur quête, sans refuge dans un monde fantasmé. L’ancrage dans le réel est primordial.
                        </p>
                        <p>
                            Comme toute entreprise tournée vers l’enseignement, cher aux philosophes, et non vers la prédation, chère aux sophistes, nous concentrons nos efforts sur la formation et le développement de la sagacité et de la responsabilité de nos usagers et collaborateurs dans un esprit vertueux. 
                        </p>
                        <p>
                            Nous encourageons nos formateurs et nos clients à diffuser nos savoirs et techniques pour qu’ils soient les acteurs de l’assainissement de leur environnement professionnel, voire personnel.
                        </p>
                        <p>
                            Dans cet esprit, le XENATX est gratuit pour tous, sans restriction.
                        </p>
                    `,

                    "titlePartnerships" : "Partenariats",
                    "partnershipsParagraph01" : `
                        <p>
                            Tous nos services en ligne sont accessibles gracieusement pour bon nombre de collectifs, de droit ou de fait, oeuvrant à la défense des libertés individuelles et collectives, après cooptation, étude et mise en place d'un partenariat.
                        </p>
                        <p>
                            Le respect de l’Homme et de sa souveraineté personnelle - incluant sa vie privée et le secret de ses communications - est à la source de XENATON.
                        </p>
                        <p>
                            L'hyper grille centralisée de surveillance et de contrôle, supprimant toute autonomie, donc toute vie, est une impasse. Toute concentration excessive, en tout domaine, crée un dangereux point unique de fragilité. Un non sens en terme de résilience et un sophisme niant le vivant, ses écosystèmes et sa nécessaire biodiversité.
                        </p>
                        <p>
                            Si vous vous reconnaissez dans ces valeurs d’entreprise, nous serions honorés d’évoluer à vos côtés dans la poursuite de ces objectifs communs. N'hésitez pas à nous contacter sur xenaton.com
                        </p>
                    `,
                
                }
            },

            "menus": {
                "menuCipherAsym": "Chiffrement asymétrique",
                "menuSign": "Signature cryptographique",
                "menuCipherSym": "Chiffrement symétrique",
                "menuCipherOTP": "OTP - Masque à usage unique",
                "menuTools": "Outils - Transfert sécurisé",
            },

            "cipherAsym": {
                "introduction": "Le chiffrement asymétrique, donc à clés publique et privée, permet d'éviter l'échange d'une clé secrète au préalable.",
                "menuExplanations": "Explications",
                "menuCreateKeys": "Créer une paire de clés",
                "menuKeys": "Clés",
                "menuEncrypt": "Chiffrement",
                "menuDecrypt": "Déchiffrement",
                "menuKeyInfo": "Info. sur une clé publique",
                "menuRevocationKey": "Révocation d'une clé publique",

                "explanations": {
                    "title" : "Explications",
                    "paragraph01" : `
                        <p>
                            <strong>La particularité exceptionnelle du chiffrement asymétrique est d'éviter l'échange secret, à l'avance, d'une clé commune de chiffrement et déchiffrement entre correspondants comme dans le cas du chiffrement symétrique</strong> (voir section "Chiffrement symétrique" plus bas).
                        </p>
                        <p>Ce type de chiffrement fut inventé seulement dans les années 1970. Pour l'historique complet, cherchez avec le mot-clé RSA (Rivest-Shamir-Adleman).</p>
                        
                        <p>
                            Dans l'échange d'un message chiffré asymétriquement entre deux personnes (ou deux machines), les deux personnes ont chacune une paire de clés&nbsp;:
                            <ul>
                                <li>Une clé publique, divulgable publiquement, donc connue de tous, permettant de chiffrer mais pas de déchiffrer <em>(du moins pas dans cet objectif de chiffrement-déchiffrement d'un message)</em></li>
                                <li>Une clé privée, parfois appelée "clé secrète", pour déchiffrer et signer. <u>Cette clé privée est strictement personnelle</u>.</li>
                            </ul>
                        </p>
                        <p>
                            Il n'y a pas de possibilité de recréer la clé privée à partir de la clé publique. La clé publique peut donc être connue de tous, notamment d'un attaquant.
                        </p>
                        <p>
                            Le chiffrement asymétrique assure trois fonctions essentielles&nbsp;:
                            <ul>
                                <li>la confidentialité (secret du message), </li>
                                <li>l'intégrité (non altération du message), </li>
                                <li>l'authenticité (preuve que l'auteur du message est bien celui qui prétend l'être).</li>
                            </ul>
                        </p>
                        <p>
                            Pour ces deux derniers points, l'intégrité et l'authenticité, un mécanisme de signature est intégré au processus. Ce mécanisme fait appel au chiffrement et à une fonction de hachage établissant une empreinte. Une signature d'un message est une empreinte, de ce message, chiffrée avec la clé privée de l'auteur du message.
                        </p>
                        <p class="alert alert-warning">
                            <i class="bi bi-exclamation-octagon"></i> Le XENATX permet de chiffrer asymétriquement des textes mais pas un fichier complet. 
                            <br>Si vous souhaitez malgré tout envoyer un fichier chiffré sans avoir au préalable échangé de clé symétrique avec votre correspondant, chiffrez-le symétriquement en AES-256 (section "Chiffrement symétrique" puis onglet "Chiffrement - Déchiffrement de fichier") et indiquez la clé de chiffrement symétrique dans un texte chiffré asymétriquement. 
                            <br>Exemple de texte à indiquer sous votre message général avant chiffrement asymétrique&nbsp;: 
                            <br>Fichier en pièce jointe "test.png.enc" chiffré symétriquement avec cette clé 55BCCF0E4BA8426C779D62A534EA183EED3FEEAB4C08615712593E646D737EBF
                        </p>
                        <p class="alert alert-info">
                            <i class="bi bi-exclamation-octagon"></i> Le XENATX fournit des outils essentiels pour le chiffrement mais il ne peut pas se substituer à un programme complet de formation en cryptographie. Vous trouverez de nombreuses autres explications et références dans notre programme d'apprentissage en ligne XenaTraining (gratuit pour les collectifs partenaires).
                            <br>Nous vous conseillons déjà cette vidéo composée de bons schémas pour bien comprendre le chiffrement asymétrique https://youtu.be/AQDCe585Lnc (activez le sous-titrage en français si besoin). Ce site explique également bien le principe&nbsp;: https://wikilibriste.fr/fr/intermediaire/chiffrement
                        </p>`
                },

                "creatingAKeyPair": {
                    "title" : "Créer une paire de clés",
                    "paragraph01": `Créez votre paire de clés publique et privée (ECC-25519) puis&nbsp;:
                    <ul>
                        <li>Stockez-les en sécurité dans un gestionnaire de mot de passe comme Keepass placé sur une clé USB ou microSD.</li>
                        <li>Stockez-y également votre mot de passe de clé privée, si possible en ne stockant pas tout le mot de passe et en retenant une partie.</li>
                        <li>Stockez le certificat de révocation dans un fichier Keepass différent et sur un autre support, si possible.</li>
                        <li>Les informations non critiques comme l'identifiant de clé publique peuvent être retrouvées dans l'onglet "Info. sur une clé publique".</li>
                    </ul>`,

                    "paragraph02": `Pour atteindre un bon niveau de sécurité, votre paire de clés doit être générée sur un équipement dit "OFF" qui n'est pas, et ne sera plus jamais connecté à Internet.
                    <br>Cet équipement OFF doit avoir les caractéristiques suivantes&nbsp;: ni carte wifi, ni carte bluetooth, ni carte wifi-4G externe, ni câble ethernet branché, ni ports USB accessibles à des tiers, etc. En somme, aucune capacité de connexion par ondes ou par connectique.
                    <br>Seuls un clavier et une souris filaires, plus jamais reconnectés à un autre équipement, peuvent être utilisés&nbsp;: les prises USB ont en effet un firmware facilement infectable permettant des fuites de données. Votre clé privée ou autres données sensibles pourraient être récupérées plus facilement que vous ne le pensez.`,
                    "passphraseLabel": `Mot de passe protégeant votre clé privée`,
                    "passphraseHelp": `16 caractères au minimum, mais plutôt 40 et plus, dont des spéciaux et bien sûr des chiffres, majuscules et minuscules. Attention, il n'y a aucun moyen de retrouver votre mot de passe si vous l'oubliez.`,

                    "keyPairIntroduction": "Pour créer une clé publique anonyme, nous vous conseillons de laisser ces nom et email factices ci-dessous. Le fait qu'il y ait un nom et un email augmente la compatibilité avec certains dépôts publics de clés.",
                    "keyPairName": "Nom",
                    "keyPairEmail": "Email",

                    "keyValidity": {
                        "label" : "Durée de validité",
                        "infinity" : "Infinie",
                        "oneWeek" : "Une semaine",
                        "14Days" : "14 jours",
                        "30Days" : "30 jours",
                        "oneYear" : "1 an",
                        "twoYears" : "2 ans",
                        "fiveYears" : "5 ans",
                        "tenYears" : "10 ans"
                    },

                    "keyPairPubKey": "Nouvelle clé publique",
                    "keyPairPubKeyCopy": "Copier",
                    "keyPairPrivKey": "Nouvelle clé privée",
                    "keyPairPrivKeyCopy": "Copier",
                    "keyPairRevCert": "Certificat de révocation",
                    "keyPairRevCertCopy": "Copier",
                    "keyPairID": "Identifiant de la clé publique",
                    "keyPairIDHelp": "Une fois l'identifiant généré, notez-le. Vous pourrez toutefois le retrouver dans l'onglet",
                    "keyPairIDInfoKey": "Info. sur une clé publique.",
                    "keyPairCreateYourKeys": "Créer vos clés",
                    "keyPairCancel": "Annuler",
                },

                "keys": {
                    "title" : "Clés",
                    "introduction": `Déposez ci-dessous votre clé privée et la clé publique pour pouvoir chiffrer, déchiffrer, signer ou vérifier une signature.
                    <br>Vous devez avoir votre propre paire de clés ou la générer dans l'onglet`,
                    "createKeysTab": "Créer une paire de clés",
                    "password": "Mot de passe protégeant votre clé privée",
                    "pubKey": "Clé publique",
                    "pubKeyHelp": `Pour un chiffrement, indiquez la clé publique du destinataire (ou votre propre clé publique pour un message chiffré auto-adressé). Pour déchiffrer, indiquez celle de l'expéditeur pour vérifier sa signature (ou votre propre clé pour vérifier votre propre signature).`,
                    "privKeyHelp": `Votre clé privée est nécessaire pour chiffrer (signature) et déchiffrer. Une clé publique est normalement suffisante pour chiffrer. Cependant une signature est automatiquement insérée au moment du chiffrement. Or une signature ne se fait qu'avec votre clé privée.`,
                    "pubKeyLoad": `Chargez la clé publique depuis un fichier.`,
                    "privKey": "Ma clé privée",
                    "privKeyLoad": `Chargez votre clé privée depuis un fichier.`
                },

                "encrypt": {
                    "title": "Chiffrement asym.",
                    "subtitle": "Signature incluse",
                    "introduction": `<p>
                                        Au préalable, indiquez les clés dans l'onglet <i class="bi bi-key-fill"></i> Clés.
                                    </p>

                                    <p class="alert alert-warning">
                                        <i class="bi bi-exclamation-octagon"></i> Une clé publique est normalement suffisante pour chiffrer un contenu textuel. Cependant une signature est automatiquement insérée au moment du chiffrement. Or une signature ne se fait qu'avec votre clé privée. C'est la raison pour laquelle vous devez également indiquer votre clé privée dans l'onglet "Clés".
                                    </p>`,
                    "contentToCrypt": "Contenu textuel à chiffrer",
                    "contentDecrypted": "Contenu textuel après chiffrement",
                },

                "decrypt": {
                    "title": "Déchiffrement asym.",
                    "subtitle": `Vérification de signature incluse`,
                    "introduction": `<p>
                                        Au préalable, indiquez les clés dans l'onglet <i class="bi bi-key-fill"></i> Clés.
                                    </p>
                                    <p class="alert alert-warning">
                                        <i class="bi bi-exclamation-octagon"></i> Une clé privée est normalement suffisante pour déchiffrer un contenu textuel. Cependant une signature a été automatiquement insérée au moment du chiffrement. Or une signature ne se vérifie qu'avec la clé publique du signataire, donc celle de la personne qui vous a envoyé ce contenu chiffré ou la vôtre si c'est un contenu auto-adressé. C'est la raison pour laquelle vous devez aussi indiquer une clé publique dans l'onglet "Clés" pour déchiffrer ce contenu.
                                    </p>`,
                    "contentToDecrypt": "Contenu textuel à déchiffrer",
                    "contentAfterBeingDecrypted": "Contenu textuel après déchiffrement",
                    "verifySignature": "Vérification de la signature",

                },

                "infoKey": {
                    "title": "Informations sur une clé publique",
                    "introduction": `Indiquez une clé publique et découvrez les informations de cette clé.`,
                    "pubKey": "Clé publique",
                    "infoKeyField": "Informations sur la clé",
                    "discover": "Découvrir"
                },

                "revocationKey": {
                    "title": "Révocation d'une clé publique",
                    "introduction": `
                    <p>
                        Indiquez la clé publique à révoquer et le certificat de révocation que vous aviez précieusement conservé.
                    </p>
                    <p>
                        Vous obtiendrez une nouvelle clé publique révoquée conservant le même identifiant mais intégrant les informations de révocation. Chargez-la sur vos dépôts de clés publics ou privés en remplacement de l'ancienne version pour informer vos correspondants qu'ils ne doivent plus l'utiliser. Si jamais il le faisait encore pour chiffrer un message, ils seraient empêchés et une indication apparaitrait indiquant que cette clé publique est révoquée. Ils pourront néanmoins continuer à utiliser cette version révoquée pour vérifier votre signature d'anciens messages ou documents.
                    </p>`,
                    "pubKeyfield": "Clé publique à révoquer",
                    "revocationCertificateField": "Certificat de révocation",
                    "newPubKeyRevokedField": "Clé publique révoquée",
                    "revoke": "Révoquer"
                }

            },

            "signature": {
                "header": "Etablir, propager et vérifier la confiance entre membres d'une organisation ou entre organisations.",
                "menuExplanations": "Explications",
                "menuSign": "Signer",
                "menuVerifySignature": "Vérifier une signature",
                "explanations": {
                    "title": "Explications de la signature cryptographique",
                    "introduction": `
                        <p>
                            Le but d'une signature cryptographique est de s’assurer de l’identité de l’auteur d’un document (authenticité) et que ce dernier n’a pas été modifié (intégrité). En revanche, la signature ne s'occupe pas de la confidentialité qui est l'affaire du chiffrement.
                        </p>
                        <p>
                            Il est possible de signer de multiples documents numériques (photo, contrat, message, clé publique, etc.)
                        </p>
                        <p>
                            Les signatures de clés publiques permettent d'attester des relations de confiance. Ces signatures constituent alors une chaîne ou une toile de confiance.
                        </p>
                        <p>
                            En effet, dans une clé publique il n'y a pas nécessairement d'informations de type nom, prénom, email voire numéro de sécurité sociale pouvant relier une clé publique à son véritable "propriétaire". Techniquement, ces informations peuvent bien sûr être incluses et le sont d'ailleurs parfois. Le XENATX vous le permet.
                        </p>
                        <p>
                            Cependant, quand bien même ces informations seraient intégrées à la clé publique, elles ne garantissent pas l'identité réelle de l'émetteur de cette clé sans vérification directe auprès de cette personne dont les nom, prénom, etc. ont été insérés dans cette clé publique. N'importe qui peut usurper un nom, un prénom, voire un email, et créer une clé publique avec cette identité.
                        </p>
                        <p>
                            Signer une clé publique d'un individu vous permet donc d'attester que l'individu, qui prétend être le propriétaire de cette clé, l'est bien. Vous l'aurez en effet rencontré et aurez signé sa clé publique avec votre clé privée. En signant la clé publique d'un individu, vous devenez en quelque sorte une autorité de certification, tel un tampon sur un passeport. Vous propagez ainsi la confiance.
                        </p>

                        <p class="h4">
                            Dans la pratique comment cela fonctionne-t-il ?
                        </p>
                        <p class="alert alert-info">
                            <i class="bi bi-info-circle"></i>
                            Attention, nous allons utiliser dans les paragraphes suivants les termes pubKey (public key donc clé publique), privKey (private key donc clé privée) et IDPubKey (Identifiant de clé publique).
                        </p>
                        <p>
                            Lors d'une rencontre, physique de préférence, deux membres d'une même organisation (ou d'une famille, d'un club, d'un groupe d'amis, etc.) nommés M1 et M2 décident de se signer. Concrètement, la clé privée de l'un va signer la clé publique de l'autre et vice versa. Cette opération se fait sans montrer sa clé privée à l'autre car cette dernière doit toujours rester secrète. 
                        </p>
                        <p>
                            Idéalement, chacun rapatrie, sans contact par sécurité (grâce à la génération de QR Codes dans le XENATX - Rubrique "Outils"), la clé publique de l'autre sur son propre équipement, de grande préférence non connecté, et signe enfin cette clé publique avec sa clé privée. 
                            <br>La signature effectuée est "renvoyée" à l'autre par une nouvelle génération de QR Codes.
                        </p>
                        <p>
                            Les membres M1 et M2 se prouvent ainsi réciproquement être les véritables détenteurs de leur paire respective de clés, constituée de deux clés "jumelles", la publique et la privée.
                        </p>
                        <p>
                            La signature de la pubKey1 du membre M1 réalisée avec la privKey2 du signataire M2 prouve ainsi que ce dernier connait bien le détenteur M1 de cette pubKey1. Et inversement.
                            M1 et M2 forment alors une chaine (ou toile) de confiance à deux maillons. Le lien entre ces deux maillons correspond à ces deux paires de clés signées entre elles.
                            <br>Pour un autre membre de l'organisation, il faut maintenant pouvoir faire confiance à ces deux signatures. Vous (M3) êtes un autre membre et n'étiez pas présent lors de la rencontre physique de M1 avec M2. Vous ne savez donc pas si ces signatures sont valides. Il vous faut les vérifier pour être sûr qu'il existe bien une chaine de confiance entre M1 et M2.
                        </p>
                        <p>
                            Vous (M3) allez donc récupérer la pubKey1 de M1 car vous connaissez son IDPubKey1 grâce, par exemple, à un dépôt public ou privé de clés publiques ou simplement grâce à un ami commun, à savoir M2. 
                            <br><i>XENATON fournit un dépôt de clés publiques avec son service en ligne XenaTrust mais il existe de nombreux dépôts publics tenus par exemple par des universités</i>.
                        </p>
                        <p>
                            Vous (M3) et M2 formez déjà une chaine de confiance. Or vous avez repéré le nom de M2 ou son IDPubKey2, dans la liste des signatures de M1. 
                        </p>
                        <p>
                            En copiant à la fois la pubKey1 de M1, la signature et la pubKey2 de M2 (si vous ne l'avez pas déjà étant donné que vous connaissez M2) vous pourrez vérifier cryptographiquement la validité de cette signature que votre ami M2 a effectué sur la pubKey1 de M1.
                        </p>
                        <p>
                            Le point essentiel pour comprendre est le suivant&nbsp;: seule la pubKey2, soeur jumelle de la privKey2 ayant effectué la signature de pubKey1, peut déchiffrer et donc vérifier la validité de la signature établie avec cette dite privKey2.
                        </p>
                        <p>
                            Techniquement, une signature correspond à l'empreinte SHA-256 chiffrée du document. Cette empreinte SHA-256 du document est chiffrée avec la clé privée de celui qui signe.
                        </p>
                        <p>
                            Vous trouverez de nombreuses ressources sur Internet ou sur notre plateforme d'apprentissage XenaTraining pour approfondir le fonctionnement de la signature cryptographique basée sur le chiffrement asymétrique et les fonctions mathématiques de hachage.
                        </p>
                        <p class="h4">
                            Exemple pratique
                        </p>
                        <p>
                            Voilà la signature (qui commence par -----BEGIN PGP SIGNATURE-----) de la photo ci-dessous d'une jeune fille brune en pull rouge. 
                            <br>Vous trouverez plus bas la clé publique, soeur "jumelle" de la clé privée ayant servi à effectuer cette signature. Cette clé publique vous permettra de vérifier la validité de la signature de cette photo dans l'onglet "Vérifier une signature".
                        </p>
                        <p>
                            <label class="control-label" for="signatureGirlRed">Signature de la photo de la jeune fille en rouge</label>
                            <textarea id="signatureGirlRed" class="form-control" rows="7" cols="100" type="text">-----BEGIN PGP SIGNATURE-----

wnUEARYKAAYFAmQF5GIAIQkQInG4+5oONHwWIQSy1IOvxQwveTazar4icbj7
mg40fA0RAP9WeFvOrnDxE393rDuZzfuu37nSYcfm9cyReq3SMX0btgEAm/Hb
Ub62qULmGxaDj3TAg2/hq5mhGgXnqUTDcRKmNQw=
=HIKk
-----END PGP SIGNATURE-----</textarea>
                        </p>
                        <p class="h4">Photo - Objet de la signature</p>
                        <p>
                            <img src="assets/images/signature/girl_red.jpg" alt="XENATON">
                        </p>

                        <br>
                        <p>
                            <label class="control-label" for="pubKeyGirlRed">Clé publique du signataire pour vérification</label>
                            <textarea id="pubKeyGirlRed" class="form-control" rows="14" cols="100" type="text">
-----BEGIN PGP PUBLIC KEY BLOCK-----

xjMEZAXhvxYJKwYBBAHaRw8BAQdAekpBliuUeh/zxH5LdtEZm7dL9CkZ7SaP
7c7jJZ1K8+zNG0pvaG4gRG9lIDxqb2huQGV4YW1wbGUuY29tPsKMBBAWCgAd
BQJkBeG/BAsJBwgDFQgKBBYAAgECGQECGwMCHgEAIQkQInG4+5oONHwWIQSy
1IOvxQwveTazar4icbj7mg40fMUHAQDARa54M0QPcZLhtjYHn9ULamTvg2uL
XNBJ1ampVnNfXgD/U/FCsquv34Oth2mv6dkqJ4ViXiVK91vBUIpx/0Zc6QXO
OARkBeG/EgorBgEEAZdVAQUBAQdA6lkmut2gpU48IKSSqHrq+rbXjnO8Btfn
i7/ogWxMUAoDAQgHwngEGBYIAAkFAmQF4b8CGwwAIQkQInG4+5oONHwWIQSy
1IOvxQwveTazar4icbj7mg40fClgAP9GOMLfSS4XhHgbWRwKE3FfRjOlbBhE
m3eeKhcO8Uwe/gEAkqUCUb14GdqqqEMSg4/o6ybM/hkD8kTZ7jhnx08FYQ8=
=rhKn
-----END PGP PUBLIC KEY BLOCK-----</textarea>

                    </p>
                    <p class="alert alert-info">
                        <i class="bi bi-info-circle"></i>
                        Pour procéder à la vérification de la signature à des fins pédagogiques, vous trouverez ci-après la photo transformée en format base64. En effet, une photo, comme tout fichier, n'est qu'une suite de 0 et de 1, et peut donc prendre cette forme textuelle après conversion.
                        <br>Pour tester par vous-même, vous pouvez copier tout le contenu textuel ci-dessous et reconstituer la photo en vous rendant dans la rubrique "Outils" puis dans l'onglet "Convertisseurs" et enfin dans "Format texte vers image".
                    </p>
                    <p>
                        <label class="control-label" for="girlRed">Photo convertie en contenu textuel au format base64</label>
                        <textarea class="form-control" rows="10" cols="100" id="girlRed" type="text">/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAAAAAD/4QOBaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA3LjItYzAwMCA3OS4xYjY1YTc5YjQsIDIwMjIvMDYvMTMtMjI6MDE6MDEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9IkQ3RTRCMkY4RkIzRDEwQjUwMEI3MjEzN0Q0MjNFQjBEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkFCMzRCNzlEMTgzMzExRUQ5RTU0QTNFNkEwOTFFODUwIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkFCMzRCNzlDMTgzMzExRUQ5RTU0QTNFNkEwOTFFODUwIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCAyMDIyIE1hY2ludG9zaCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjY4NmVhMjEwLTVjMGItNDAxYS1iODgzLWY5ZGM4ZDk5NTBhYSIgc3RSZWY6ZG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOjFhZTUyMTAwLWE3NzktNWM0OS1iZTZhLWUxNTdlMTBlYzJmMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uACFBZG9iZQBkwAAAAAEDABADAgMGAAAMkwAAFXYAABlg/9sAhAAbGhopHSlBJiZBQi8vL0JHPz4+P0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHAR0pKTQmND8oKD9HPzU/R0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0f/wgARCAFdAWMDASIAAhEBAxEB/8QAqgAAAgMBAQAAAAAAAAAAAAAAAAECAwQFBgEBAQEBAQEAAAAAAAAAAAAAAAECAwQFEAACAgEEAgEEAwEBAQAAAAAAARECAxAhEgQgEzEwQCIUUDIFQRVgEQABAgQEBAQGAgMBAAAAAAABABEQITECIEFREjBAYXFQgaEi8JGx0TIDwWLhghOSEgAABQIFBQAAAAAAAAAAAAAAQAERIRBwIFBgMUHwUXGhMv/aAAwDAQACEQMRAAAA5ks0pdLzBreQNjxs1vGza8TN7wyNzws2wzVmyXLF6dvNDqPlzOjHmRTqHPsNqyBrWVGpZQ1LKGlZkaI0IvVCNCoRpMwYW3ESSqI0AmMQSIsk4MnbTpHVZnptIcYonKhGkomO/NoJwlTLMpaWlIXFLLCtFhWiwrRYVotKg6MuxKOJHt0HEp7PPMrnYtb02Lhj0HGGW5mNN7zCjRSRLAhXoZmjfWRkgtspuL6boGdWqWBbKKTQzMbtJyF3mnn13MhzVoqqBMPaObSmnVXLzed1ubLgvq01MmpqV0bc2NGjNqY3W94LL+lnXHu68zj19uqXg0dzPXGXVoTC7HZPRlssnGwlVq1S1z2TzYaZW6zCU3c5svSqXh4fRY864x1SX0LDpzjXZXm5Ob1uZnXKuplua40KNluK+XXytfNqWzL2jVeyWMZxWMZKIRmlgrBefzfRYE5Mq7N83Mmt/Rx9Ll0nN2EZs1zGG8qM0U06oZuY0EugH0xGuyvNq5XW5GdcqUH0zOIotspsVZNuIv8AQcLvS64yjLEZLGMkRUhYjSibTz2DrcrWNF+C+zp9Dg9Dn07F2PRjVxB6xMiayxGoIBARax7zGFkZaOP2+Ri8NkemZEXLbOuxbMtwWeh5HazpYNta5NWPDL3DBulcY886FfKvs6N2LQcvl9/z1zHXk0azosjKas2YFm9yzj6Ma6RinZqMs7m8hKxiE1g+mUpIq5XW5eNcGF0NyDk4cpJYQrZ6HbTfjfJwdrOvN1wrOnszbIoxdDkrBZ1Zv38PcunzPq/MXNFtM9Y26cWuVqSEBYnGEX7+N2JdVkLEYytTi9ZaIxDm7+XnXKU3qQLBQlAwyTl9Xbn0Z0UaY1kNBKWMuac+quaxzvlLXY2kPNek89WAa3zu2YNK7a51kFXBm+NYS7HD6MvYsy2y6Cgudssspb66s5Ll2ZqhIlqJTQqrqYy21a5vt6sunOpQUAz13Z10YZXrOjJJSxu52yW4iWV+c9DwTCk+nKV1NpvefSuSrRVcwJJI21h1NPDuzewcgOiZHWiuDsEywkpANkc+vKuCVbxv0HR856HOlj083HS/Zwuhq74Y6l6T5Tl0GKcz0p1WJX57sed3iDDfK2yu1qevFpFG+NxQr1ZQrkUq1FRYGlxZJpjknTZIGSRZtWWa5ZKvOrPU+W6uddbHsq59OZo6PK3rrauTXefW5leCVdKyzOmymOVzSHXi2nZOyqwtsqkvQKNmsUx0KzNHTBKI3ooLQjJTzokSobkKTkkXJrXl1VS4MurHLYAvp7uP2Oe7seyNvBr71WfRxOtdYw4WQcquBv4WohvfKIkWSgLfZmmujTGlOsY9+s1xtilMbolRMKpOUqm2DchNyESZTn05V5dVsZSKkvV35dXPe583WWpMTjBZ4ms6x8Xu8PpzacNYiSjZMGRYS7rcm1aujivTapRuYRsjUBhXJTGyQSGDbAbjPi6tC8XRZQsc2urPTr7uN3MbzRvUmSN9OdKamCnAycXtcbpzrB75pSSaad9C5LYWhoxbpbLst9bbHXcyjOKRAIyjOnJSG1IbTG0wKME7aeaq53koOWfU5cJ09Q/P9LPn2VtZiIqWVcsdlHLtq6ISia4TUomrfxNVlUWyG3NbNbE4nQWDal0QuYgEZxnTkpDlGldJyKZ26/Pxxd7IRbowITBJRFNiBi2/ETO6vKmbKxUkxIRsjeSTHOIK5slXKWyVYaL82hbI3Zk6jpuuUARnGVSlGuafNdD3KKAEDAlbUmgBRNCUlMxGMxJIQxIjSJMYipFzBTTnEExKdTLp0WL1aYC7boO4kIQnCYcy/E9pAU7JAwAwabTaGmAoCQTJENIhgkxEMSKmkipCRJJIRtV51qxMxsi0nZTcurVPLeWowidCUKzn1xU+g0JGJyDBoYKxNWIVoAAQTBAAAgAIaBMRAIhiJSQkxknF3XV1crq3zUFo5V5dPPdM8Wp6QCRtNptNsABoVgKAAgRgCGgAAAAEQAAIhgk4sghhuLWztcPq3F4i+Wvl9Tku0E1Ow05ZIGpSjJtAAArEDEAADENAAAAIJoAAAQGljGUXMAZGgl0OfsroCL4//9oACAECAAEFAP5OSft1pJJJJPjPhJOj1ggQ/JPRjei1ZGj8n5rRrSfrTrtotWpIfm/LY2EhohaLxga+nBGjIIF5PWdEvB6STJJImSLzjSCPB6PV+CX1no9Z0eiX2rKr7Jk6yV+yfjX7KCNULb7SCF/9Mvuf/9oACAEDAAEFAP5KCCPt3pBBBxOL8II8Y0XixeTU6ISGPwnReS83omNEfWjXfS2skoj6m5uNiZLPkt4yT9OSXoiSR/HitY8lpBEHESgaIHq/CdJJ8FotxH4iVTij40s/rLRCI5riKurcvV/WTdWsha86Wfk19REawW+xVSyh+Fvr1xNnBVEpdq8k6tC1gf00nZrEytVXRqRKNYIRAxqR/RrV2aoqLVawQQRq0OuqGo8MVeKb+nBBBBapxIgiR6JS5gn7CNEZFFjGt39i9EZVKMfgvrPW/wDU/9oACAEBAAEFAFYVjmczmczmczmczmK4rnM5nM5lsjK5YH2IF2JFmFmk9hzPYczmczmczmczmczmch2HY5HI5HI5HIVjkcjmczmew9h7D2HsFkPYew9h7D2Fsg8jYrJCzHtFnQ89Uv2LWPdYV5FZHOD2HsPYew9h7D2HsOZzOZzOZzOZz1kkkkk5HI5HI5HI5nMqmLYs5bbIhczmhZKnJEzo7Mx2bHceQ9h7D2HsPYczmczmczmczmczmQQQQQQR5pCUuYUwSULOXEj1UITGnZ44RasDX2H65+ufrjwFsUDUECqKknqHjg4MWORYmKqRcXwyuwvg4y2tFIkKqKwhPkWTGiCCCCCCDiziyCCCCBYUelFsJbEZMZkpAkVqVocThJ6RYoLUG95bGNCqOm3GU8Zeg1BuxIUFYK2VS1lZuu/EVRVOB6ymCSvWF1UPqot1kX65fHBBAqnEdS1TLUzVKoqiCCtStRqDJsnJDOLbWEWFi67ar19r4mWqWqOrOLIE2imRlWkNSJCQqioLHvjxlaCqcS1DJiMuIvjaOLEiBosjKjKipQjSghfm+y4OQnJhwOxTALCkcEOiLYky/Xkt1JLdQv1mi1bVE0zhBTINpiaKox4iuMWMrUSEiB1LUL4pMmA9GrLGRSdhQLYrc5juVucpK2hdiztdVk6+Hm8WJJRo9WiDiPGmZuvtfHxa2HXaraFuYaS8dStTiJEEaQNDqWoevVjLI7S3WkklWUsZcqorX3qoXTx7JD0fm6ydjAX2eLINQVUmLZ13VREeTQ0RqxljtfK8EVZm+KLlbG+duqvxHqyB+CL1ldvHDTgplKWRS0vHYo9l9CCNWMsdv5RJImIRk3rb8Vipxr1v6j+ihrbt/N0VY8bRjvepgzox5EyrORJJJJJJJOrGiyO2tySRCZVlvjHT2XxTkvirCyZK41fv8Sv+ljZXNW5y0kkeRIfZxox5qXGju1dXb8ivzibRx3hMqrVMXYTFcVjkcjkciSSfCCyO2iNUIWw90vxr/nY5Ezgk73Rlx0yD698bwZbMRZwZsthdfJkKdLGivTxlHah3qTS27+Sj5Kr5JSiYHaTHnaK50LMmew9hW8iYn5WO0tmhogSEtMtoVn+PQpGO2y7PfWJ/vOwu4jHl5GKsnHa62vFW82/7MFO2kVyK6zV5UyLSjh1sRySGhLdo5NCztH7BhtySF5WO18NDRAkJFlDvabP56yjG1JfrY279TEy/RohdSH16Oqfxb47Fbn6l8i/86x/590dTr3xGRbZlD+BGO2+LYezsyd2xsenWX4oRHgyzOzuoIIEhIv8AFVNlvbF/UaLY5HgTFgSK1gfxZF6SekVGKkCUFzsqLNFWVZjtJfcsx2ORyGyTq2/FMTJE9JGzJY7F9hIggRlf44/mm9uu5otWKBKSxA0QQRpY7O7tsyrMbhvdWLEkkknWybVuK5zEySR2M2QzXnRIjXO4rVRXrU536yaoiRstaBWeW6SRcqjt0tRUyKyT0Zdwsjbtk/sITK2mrUq6a8cd+Lx5hZT2CyHsHlRkzpGTJI9xIgggg7C/Fs6CnLg2UklrQZMsmCvEyPKh5ZVstjLkdqUfB1sSSZv63XFWctaVML2fxc4wQQRoruos7R+wfsj7DY8zHds+fGCDs/1kV3R9Dse5FnBmy8TDj5FcuNHtoNOwmzjJmxtGK8iemb+vZyKBCI2q4c8lZEHE4kEEEEaLxWsEHYU1/wCLc/zcnDNJY7Uox887xf5yY+gy/Sy1f6OWf1s1U+1mxnXyex1JO/k4YolPRblGLZ43KaHUdTicTiQQR9CCCCDP/RlXvydLY8iyVZkrN+z13jMH+nahTt43RpS8lFe/+nhVXkv2rYMaoJEn+lmm1Xoiu2jXJY7bpSOpxOI6nE4nE46L6Wfd3rGllK/y88odfzzYvZThxFixZU/86w+nCdKnV6yxVpXf4M+VY65Lu9tFpVlHDiHgy8hI4nEdR1OI6nEQhISEhIgSIIG4Epv2KxX5KsrZ4r4cqy1alo7OBt3lP2QJKz63Xgs5EoLODvdn2uCI0kkTG9uUrLTiYM+yh6NDQ0NECQkJCQkJEEEEGRbJHYtLmBraeS/zbPjS3LRlh0qyqgRBZwd3M+LIExvwrYrbbDlXG+P02w5OJ8jQ0NDRAkJCQkJEEEEEF1tf8TK98iiuJyo3/wA5vjW5S6sho4kCRe6qr5Hc7FfwYlKew9EfGiZiY6bUnHbBaU0NDQ0QJCQkJCRBBBBBkaqZE7J/nbJV2eJRY/y6xTLil0yXo6ditzkmSci2VIs3Zwdn+r+cbh2e730S2tpX5o4K2My/DC+Np0Y9EJCEtI8bUh9vI2lSFhok82J0txP83+lkOpasjdqntuLJZi3ILGfdX2bIk+D/ALhx801KktXgJlXtltOLA+T3oLcY9EIQvODN11kL9J1G7Ud7Ow1C6Wb13aIHUtUdRI4HwWMvxk/tMCGI6Kk7OPhka3zW5FWY9q5NqdVjUquzY9EIQvoNpGbsotbflJZy2dTuqJkksh1I1sdrJCsSKqZwIOlfhbu0V6D+K1Me67FIrgaRW+19tHohCF5TBfsKplztlskjciZXdvTD3L4jF3aZCTkNkjukZ+yql7uzZZCIGTBj7LVRqSqKWgpnTVsFjH2Ibc1W60QhaLWUi/YrUy9tstlbJ0qyyKuNHrTPegu9dH7zLdyzLZrWG51aPg5wOxI2Ji2JFucSnOhe1cy6+Vp1crRCEIRn7HqLdzIy2e9h2b+i/qNEHHRoXwnGtLGO0ldzs14Xw2laIQhGTMqma/JyT9y/BOBEmK8Wwnb3uqw50QhGXLwVryX+tH1YI0jROBWE0zDl4vLblmqLYkQi1uKyZOTbHo/sZ0gggggjSBogjT5EtlaBzZ07XErlrdSIR2sg2N/ZQR9CCNYIII0Rj2daVyVydbifssRMLJflaf4KCCNEdO8qx6VKM9+NGySf4VHWvxu9Edu238G/BFXDraaiOzabfwb8Uda00EZXNvJfwSOnbT/mT+3ihC/gUdP5P//aAAgBAgIGPwC1M4Y1m92f/9oACAEDAgY/ALUxhnQMuIUfXqrEo3HYQVdBsIo2ezARE5DdMhJkEiCjIILPypNqsV8l3opVaf/aAAgBAQEGPwDkawqpKcK+EaCM1VOvaPmmJUypeCME5w18F7B4EopsbeBHtDzRT9IMKRp4CykFMQfAyYRqqxcDnHRKlJV4rFU5x8gmUhB1PgSg1wT2pjypxNAkmS+PVdOLL5J8k3KHHKDZlPxHEGKeE6j6J+ZnQLpx2K+J/wCfqpIIshy7pszVB8+MU6nROJhSmEz+SbmHP42r6IBPcvwuTF7V7S+GZU7gmtIMHUkxW01yTiRXuHmE9pcJjXjntjcr4qt5huMzrBiE9hcJjWLBPcWU5pwGOq23z0KfRaFTquoU6iPVMeMe2JkAgEIbc9F9g6q3cL4Y4Oq9of6fNZf+l7gbfp84EdE6nDcE44D8E4ngFb2g5tD6pmHkpO3dPbdt7zCnOPtm/kvdd6SHkjO31XtuXvLwkukSOAOCcRgEO3CcYT3MeyBQOMcE4yUFb2454jci3RAGocYRYKZ4P+luVR0T4DBovzDJv6lF9cDBOntYjQoZJrQtuqbAeydPFk3MbrZEIvIiPVbim3BSuCcEEKo+cHwHstowOnCccxtNLw3nGVULbrmB+Sm9NUCLrgDrNMLnCE6p7b82kmv9w6pxQxOpknwMU3LGIuFQXQvFLg8AERbIXTHdN+0OJ+4VOkvQ/NWkkWkt7dCfiqDpiQCA89EGck7pCoaj90zNuoNAmGURYMplNgcJxVd+VFsXR/Ue9v8AMAmFRRFxutNdRq2h+qP/ADvHS25OGJ7r33WWDWpW2yZzu+2gW4/kR6ImBuNAjcanExTapjXlHOa84MULraiaF4oU8N/6/wAsxqvdayYKQcrdeG/r9/tg22/jb6nG6dC8UKa6mvJuiOqdOF1RGWGYHyUg2Ai3zON00Nt9CnE7bqfYr+p9OTJ1QC2wNwDjPVbrZjG5XRHG0WTVtNRotl1D8OmOXITTmQT5BSTLzRPVbrfbdr91MeY/kKRnphcwIxvhF2dlzeS6cg5mtohPNPAjBIwnwiMR6kLyXTkZFMVPKDGh4ssB0M06BGkHQGpTwbjzTWrca5JkBDZ+zyKccBs8FYMc1vFRBoNogRkmTJ+LNMFMqSeDxaoU5FSwsE+PbdO0+mBltuT/AKz5fZbb5FeXDniYqWKRU1SFeNNe0prwx1R/XdXLgtUqUlM8xLGxTZhOFbcOAwqnPgTwtClibPwWaGJyn8GFwqmvCe04No8HdOt1kiqZN/tB0T4Rth/s/pA+Gt4S6B8QPhRET4UYf//Z</textarea>
                    </p>`
                },
                
                "sign": {
                    "title": "Signer",
                    "introduction": `
                        <p>
                            Indiquez ci-dessous votre mot de passe et votre clé privée avant de signer une clé publique, un contenu textuel, une empreinte de contenu, etc. 
                            <br>Seule votre clé privée peut signer.
                        </p>
                    `,
                    "passwordPrivKey": `Mot de passe protégeant votre clé privée`,
                    "yourPrivKey": "Votre clé privée",
                    "yourPrivKeyHelp": "Collez ci-dessus votre clé privée ou chargez-la depuis un fichier.",
                    "yourPrivKeyFromFile": "Votre clé privée depuis un fichier.",
                    "contentToSign": "Contenu textuel à signer",
                    "contentToSignHelp": "Collez ci-dessus votre contenu textuel à signer (texte, clé publique, empreinte, etc.)",
                },

                "verifySignature": {
                    "title": "Vérifier une signature",
                    "introduction": `
                        <p>
                            Vous devez indiquer ci-dessous la clé publique du signataire, le contenu textuel original ayant fait l'objet de la signature et la signature à vérifier.
                            <br>Rappel&nbsp;: le contenu textuel original peut-être un contenu textuel en clair ou chiffré, une empreinte de contenu, une clé publique, une empreinte de clé publique, etc.
                        </p>
                        <p class="alert alert-info">
                            <i class="bi bi-info-circle"></i>
                            C'est toujours la clé publique du signataire qui sert à vérifier sa signature effectuée avec sa clé privée. En effet seule la clé publique peut déchiffrer donc vérifier la signature effectuée avec sa soeur "jumelle" la clé privée.
                        </p>
                    `,
                    "pubKeyField": `Clé publique du signataire`,
                    "pubKeyFieldHelp": `Collez ci-dessus la clé publique du signataire ou chargez-la depuis un fichier.`,
                    "pubKeyFile": `Clé publique du signataire depuis un fichier.`,
                    "originalContent": "Contenu textuel original",
                    "originalContentHelp": "Collez ci-dessus votre contenu textuel original ayant fait l'objet de la signature (texte, clé publique, empreinte, etc.)",
                    "signatureToCheck": `Signature à vérifier`,
                    "signatureToCheckHelp": `Collez ci-dessus la signature à vérifier.`,
                    "verifyBtn": `Vérifier`
                }
            },

            "symCipher": {
                "header": "Chiffrement sûr nécessitant néanmoins un échange de clé ou un fonctionnement en combinaison avec le chiffrement asymétrique.",
                "menuExplanations": "Explications",
                "menuCreateKey": "Créer une clé",
                "menuEntropy": "Entropie du sel",
                "menuCrypt": "Chiffrement",
                "menuDecrypt": "Déchiffrement",
                "menuRecreateKey": "Reconstitution de clé",

                "explanations": {
                    "introduction": `
                        <p>
                            Remontant à plusieurs millénaires, sous différentes formes, le chiffrement dit "symétrique", permet de rendre incompréhensible une chaine de caractères ou un fichier complet grâce à une clé de sécurité ou clé de chiffrement (sorte de super mot de passe pour résumer trivialement). 
                            <br>Un message en clair devient donc un message chiffré assurant ainsi le secret durant la transmission.
                            <br>En chiffrement symétrique, la même clé sert au chiffrement et au déchiffrement du message, d'où le nom de "symétrique". Cela nécessite de se l'échanger entre correspondants au préalable. C'est là une limite importante du chiffrement symétrique car cet échange physique préalable de la clé pose des problèmes de logistique et de sécurité.
                        </p>
                        <p>
                            Il existe un autre type de chiffrement plus récent&nbsp;: le chiffrement <u>a</u>symétrique, sujet d'une section précédente, qui s'utilise souvent en complément du chiffrement symétrique.
                        </p>    
                        <p>
                            Commencez donc par créer une clé à l'aide d'un CSPRNG à l'onglet <mark><i class="bi bi-filetype-key"></i> Créer une clé</mark>. 
                            <br>Conservez le mot de passe et le sel dans un Keepass (logiciel open source de type coffre-fort) voire dans deux keepass différents, l'un pour les mots de passe, l'autre pour les sels. L'idéal est ne de ne pas conserver écrit l'entièreté du mot de passe, en en mémorisant une partie.
                            <br>La conservation du mot de passe et du Sel est essentielle. Sans ces deux informations, vous ne pourrez pas recréer la clé symétrique.
                        </p>
                        <p>
                            Sauf si la clé a été créée par une méthode manuelle, conserver directement la clé, au lieu du mot de passe et du sel, est bien sûr possible mais non recommandé. Son potentiel vol sera facilité et, pour des raisons techniques lié à la fonction PBKDF2, il est plus difficile de recréer la clé avec un seul élément comme le mot de passe ou le sel.
                        </p>
                        <p>
                            Le chiffrement symétrique utilisé est l'AES en mode CBC (Cipher Block Chaining) avec une clé de 256 bits et un vecteur d'initialisation de 128 bits unique pour chaque chiffrement. Vous pouvez générer des clés grâce à la fonction de dérivation de clé PBKDF2 avec un sel basé en partie sur des mouvements de souris. Dans tous les cas, la possibilité vous est laissée d'utiliser vos propres clés générées avec d'autres logiciels et équipements ou par lancers de dés.
                        </p>
                        <p>
                            Exemple de message à chiffrer&nbsp;: « Tout organisme vivant subit des attaques. L’Homme aura toujours sa stratégie de défense à cacher. »
                            <br>Après chiffrement avec la clé 1E5A4797BCECAEB0926FEDDE2A10F28E6D6082BE1411E90D66AA79946908D1F7 vous obtiendrez la chaîne de caractères suivante&nbsp;:
                            <br>2uZ4je6Nnfd0t6aCwhhk7w==:fj5+3D0mITDRWKgD2xVUSQRmsXFlXZ7VryYVbCQRx6fqzQtKHrt+Xtx18Su6lSdB0mAPB81C7VPWhTpsn6gYmVM/Lx9AANz8xQaJ1OpMzmYv+X0BlT9/atjTDhkqzdWujYoQgsmTXAiedbz5pyU2fQ==
                        </p>
                        <p>
                            Si vous chiffrez une nouvelle fois le même message avec la même clé, vous obtiendrez "un chiffré" différent, ce qui est normal et voulu&nbsp;:
                            <br>qB2+XhwK6AKT8lMd1JtGzA==:vG/xNCUFfhd3mXNQ7OCUuq1hWEbb9UsQkT/oT5rF7J0hpAJmmASXW4Atu680c1yQ9XWFkFgYPIbKMFKsox3a1SnM0B34XLym5A6N9I3ZCWc/Ja8ApjVdQoBH3Yow53YZD7ciVZPntwdfXsTfwr7hew==
                        </p>
                        <p>
                            Analyse de ce "chiffré"&nbsp;: les premiers caractères jusqu'au signe de ponctuation "deux-points" [ : ] constituent le Vecteur d'Initialisation (IV - Initialization Vector) simplement encodé en base64, soit cette chaîne qB2+XhwK6AKT8lMd1JtGzA==
                            <br>Après le signe "deux-points" faisant office de séparateur, vous trouvez le message réel chiffré et encodé également en base64, soit cette chaîne vG/xNCUFfhd3mXNQ7OCUuq1hWEbb9UsQkT/oT5rF7J0hpAJmmASXW4Atu680c1yQ9XWFkFgYPIbKMFKsox3a1SnM0B34XLym5A6N9I3ZCWc/Ja8ApjVdQoBH3Yow53YZD7ciVZPntwdfXsTfwr7hew==
                            <br>L'IV est nécessaire pour décoder ce message chiffré situé après les "deux-points". Pour cette raison, il est "embarqué" en étant placé avant le vrai message chiffré. L'IV change aléatoirement à chaque chiffrement entrainant aussi le changement du message chiffré à chaque chiffrement. Cette technique limite les attaques par texte choisi.
                        </p>

                        <p class="h4">Chiffrement, transfert de la clé et du contenu textuel chiffré</p>
                        <p>
                            Chiffrez votre contenu textuel avec votre clé symétrique. Re-chiffrez ce contenu déjà chiffré avec la clé publique de votre correspondant (chiffrement sym. doublé d'un chiffrement asym.). Pensez à conserver votre contenu chiffré symétriquement en lieu sûr ou mieux supprimez-le de votre équipement si vous l'avez bien en tête. Conservez éventuellement une empreinte SHA-512 du message en clair.
                        </p>
                        <p>
                            Indiquez à votre correspondant les informations suivantes par au moins trois messageries chiffrées différentes telles que Signal, Briar, Wire, Session, Olvid ou Telegram<sup>*</sup>&nbsp;:
                            <ul>
                                <li>Le code de message pour récupération dans la DLB. <span class="text-muted">Après avoir déposé le message dans la DLB (Dead Letter Box - Boite aux lettres morte) numérique du service en ligne XenaTrust (ou à mettre en place vous-même) voire dans une DLB physique.</span></li>
                                <li>La moitié du mot de passe. <span class="text-muted">L'autre moitié pourra être placée au-dessus du contenu textuel chiffré symétriquement avant re-chiffrement asymétrique.</span></li>
                                <li>Le sel coupé en deux. <span class="text-muted">Utilisez une messagerie différente pour chaque moitié du sel.</span></li>
                            </ul>
                            <i><sup>*</sup> Evitez Telegram qui n'est pas chiffré de bout en bout ou alors utilisez leurs messages ephémères. Dans tous les cas, souvenez-vous de toute façon qu'aucune de ces apps n'est vraiment sécurisée sinon nous n'aurions pas créé le XENATX...</i>
                        </p>
                        <p>
                            Votre correspondant reviendra ici reconstituer la clé symétrique à partir du sel et du mot de passe. Ensuite il récupèrera le contenu textuel à déchiffrer sur la DLB grâce au code de message. Il le déchiffrera d'abord une première fois asymétriquement avec sa clé privée puis une deuxième fois symétriquement avec la clé symétrique reconstituée.
                        </p>
                        <p class="h4">Répertoire physique de clés symétriques</p>
                        <p>
                            L'échange de la clé symétrique, en transmettant juste le sel et le mot de passe pour reconstituer ensuite la clé, peut être fait de façon relativement sécurisée en utilisant trois messageries, nous l'avons vu ci-dessus. Cependant cela comporte quand même beaucoup de risques d'interception, notamment en raison des failles zero day de votre équipement connecté, avant même la transmission via les messageries.
                        </p>
                        <p>
                            Une autre solution, plus sûre, est de créer un répertoire de clés constitué de nombreuses clés créées à l'avance, par lancers de dés idéalement, et d'échanger ce répertoire lors d'une rencontre physique, ou par un intermédiaire très fiable. Les clés sont placées sur microSD, clé USB voire imprimées sur feuilles avec QR Codes pour une récupération numérique facile. Vous pouvez aussi les écrire à la main sur de mini-feuilles de papier.
                        </p>   
                        <p>
                            Avec un répertoire de clés, seule la référence de la clé symétrique n°A2, A3, A4, etc. est à communiquer à votre correspondant qui a le double de ce répertoire. Un attaquant n'a donc aucune idée de la clé utilisée car seule sa référence transite sur le réseau, et encore pas toujours, car le plus souvent vous placerez cette référence au dessus du contenu textuel chiffré symétriquement. Le tout est ensuite re-chiffré asymétriquement.
                            <br>Autre solution&nbsp;: utilisez les clés du répertoire dans l'ordre. Une clé ne servant qu'une fois (c'est du moins le plus sûr) ou pour une période limitée. Ainsi il n'y a rien de particulier à communiquer à votre destinataire.
                        </p>
                        <p>
                            Pour sécuriser encore plus ce répertoire et pour des clés créées via le générateur de clé de type CSPRNG du XENATX, vous pourriez n'indiquer que les mots de passe à la place des clés. Vous pourriez ensuite communiquer, toujours de façon sûre, donc en mains propres idéalement, un sel commmun (ou plusieurs) stocké sur un autre support, voire sur papier avec son éventuelle traduction en QR Code pour récupération facile.
                            <br>Pour retrouver la clé de référence A3, par exemple, il suffira de la recréer via l'onglet "Reconstitution de clé" grâce au mot de passe n°A3 et le sel commun (ou si le sel n'est pas commun, avec le sel noté A3 également).
                        </p>
                        <p>
                            Pour des raisons de sécurité, la création de ces répertoires de clés doit se faire impérativement sur un équipement dit OFF, donc non connecté à Internet et jamais re-connecté.
                        </p>

                        <p class="h4">Dépôt et Code de message</p>
                        <p>   
                            Dans le cas de l'utilisation du service en ligne XenaTrust, vous déposez simplement votre message chiffré dans la DLB numérique. Le message peut aussi être déposé dans une DLB physique. Ensuite vous appliquez le moyen discret et original, convenu à l'avance dans le répertoire pour cette clé précise, afin de signaler discrètement à votre correspondant qu'un message est à relever.
                        </p>
                        <p>
                            Un code de message, convenu à l'avance comme le mot commun "train", peut aussi être utilisé à la place par exemple de X4V2Z pour être glissé plus discrètement dans la conversation afin d'indiquer qu'un message avec ce code, ou un autre indiqué dans le répertoire, doit être relevé. Le moyen le plus discret est celui que vous inventerez...
                        </p>`
                },

                "createKey": {
                    "title": "Créer une clé symétrique",
                    "introduction": `
                        <p>
                            Plusieurs solutions, détaillées ci-dessous, s'offrent à vous pour créer une clé AES de 256 bits.
                        </p>
                        <ul>
                            <li>
                                Pour créer une clé informatiquement à l'aide d'un CSPRNG (Crypto Secure Pseudo Random Number Generator), consultez ci-dessous "Création d'une clé avec un CSPRNG". ATTENTION, la méthode la plus sûre est la création par lancers de dés, avec l'avantage que sa création ne laisse aucune trace dans la mémoire de votre équipement... Des traces peuvent en effet persister même après transfert sur microSD et effacement. Donc, avec la méthode par lancers de dés, la clé générée n'a pas d'existence informatique tant que vous ne l'utilisez pas sur un équipement pour chiffrer ou déchiffrer. Son vol devient donc difficile pour un attaquant, sauf à trouver votre cache de clés "papier" chez vous ou ailleurs.
                            </li>
                            <li>
                                Pour créer une clé réellement aléatoire avec des dés (de précision), consultez ci-dessous les explications complètes "Création d'une clé avec des dés".
                            </li>
                            <li>
                                Pour convertir automatiquement votre clé du format binaire au format hexadécimal, utilisez ci-dessous le "Convertisseur binaire vers hexadécimal".
                            </li>
                            <li>
                                Pour convertir manuellement votre clé du format binaire vers le format héxadécimal, utilisez ci-dessous le "Tableau de conversion binaire vers hexadécimal".
                            </li>
                        </ul>
                    `,

                    "csprng": {
                        "title": `Création d'une clé symétrique avec un CSPRNG`,
                        "paragraph01": `Merci de créer un sel au préalable à l'onglet <strong><i class="bi bi-infinity"></i> Entropie du sel</strong>. Vous pouvez également fournir votre propre sel voire même utiliser votre propre clé. Dans tous les cas, le sel sera intégré avec votre mot de passe à la fonction de dérivation de clé PBKDF2 avec 100 000 itérations. Attention, sur un ordinateur faiblement puissant tel un Raspberry Pi 2 par exemple, la génération de la clé peut prendre plusieurs minutes...
                        <br>Bien entendu, pour une clé générée par lancers de dés par exemple, ou autre TRNG (True Random Number Generator), il n'y a pas besoin de créer un sel.`,

                        "passwordField": "Mot de passe-phrase secrète pour créer votre clé",
                        "passwordFieldHelp": "caract. - Au moins 40 caractères dont des spéciaux. Attention ce mot de passe-phrase secrète ne permettra pas de retrouver la clé symétrique si vous n'avez pas mémorisé le sel final ci-dessous également.",
                        "saltField": `Sel final`,
                        "saltFieldHelp": `Attention, notez bien ce sel que vous avez généré dans l'onglet "Entropie du sel" ou par vous-même. Il est autant indispensable que votre mot de passe-phrase secrète pour reconstituer votre clé en cas de perte.`,
                        "symKeyField": "Clé symétrique",
                        "symKeyFieldHelp": "Rappel&nbsp;: une clé AES de 256 bits correspond à 64 caractères hexadécimaux. ",
                        "createKeyBtn": "Créer la clé"
                    },

                    "keyWithDices" : {
                        "title" : `Explications de création d'une clé avec des dés`,
                        "text" : `
                            <p>
                                Vous pouvez créer une clé par lancers de dés successifs mais attention utilisez des dés équilibrés donc de précision (dés de casino ou de backgammon).
                            </p>
                            <p>
                                Vous créerez une clé binaire de 256 bits donc 256 zéros ou uns. Après l'avoir bien secoué dans le gobelet ou votre main, lancez votre dé et notez le résultat. Tout lancer qui est inférieur ou égal à 3 sera noté 0 et ce qui sera supérieur ou égal à 4 sera noté 1.
                            </p>
                            <p>
                                Pour accélérer la création d'une clé et diviser par deux le nombre de lancers nécessaires (128 au lieu de 256), vous pouvez utiliser deux dés de couleurs différentes. Lancez les deux dés en même temps et relevez les numéros toujours IMPERATIVEMENT dans le même ordre de couleurs. Donc si les deux dés sont bleu et rouge, alors choisissez un ordre de lecture et respectez le scrupuleusement. Par exemple lecture du bleu puis du rouge.
                            </p>
                            <p>
                                Exemple&nbsp;:
                                <br>1<sup>er</sup> lancer de deux dés&nbsp;: 5 pour le bleu et 2 pour le rouge donc nous noterons&nbsp;: 10
                                <br>2<sup>e</sup> lancer de deux dés&nbsp;: 6 pour le bleu et 4 pour le rouge donc nous noterons&nbsp;: 11
                                <br>Les 4 premiers bits de la clé seront donc 1011 (ces 4 bits seront traduits ultérieurement en hexadécimal par la lettre B)
                                <br>Poursuivez vos lancers jusqu'à obtenir 256 bits.
                                <br>Convertissez ensuite ces 256 bits en hexadécimal 4 bits par 4 bits en vous aidant du "Tableau de conversion binaire vers hexadécimal" ou utilisez le convertisseur automatique. L'objectif de cette conversion est de pouvoir utiliser votre clé dans les onglets Chiffrement ou Déchiffrement qui n'acceptent que des clés au format hexadécimal.
                            </p>
                            <p>
                                Rien de très compliqué mais multipliez les essais pour bien comprendre.
                            </p>
                        `
                    },
                    
                    "autoConvertBinToHex" : {
                        "title" : "Convertisseur automatique binaire vers hexadécimal",
                        "binKeyField" : "Clé au format binaire (01001..)",
                        "keyConvertedToHex" : "Clé convertie en hex",
                        "keyConvertedToHexHelp" : "Votre clé en hex sera composée de 64 caractères",
                        "convertToHexBtn" : "Convertir en hex",
                    },
                    "manualConversionBinToHex" : {
                        "title" : "Tableau de conversion binaire vers hexadécimal",
                        "introduction" : `Pour une meilleure compréhension, consultez la rubrique ci-dessus "Explications de création d'une clé avec des dés" avant d'utiliser ce tableau.`,
                        "binary" : "Binaire",
                        "hexadecimal" : "Hexadécimal",
                    } 
                },

                "entropy" : {
                    "title" : "Entropie du sel",
                    "text" : `
                        <p>
                            L'objectif de cette fonctionnalité est de créer un sel final plus aléatoire en mixant deux sels&nbsp;: un sel intermédiaire généré par les mouvements de votre souris et un autre sel, non affiché, généré par une fonction mathématique. Cette fonctionnalité est utile dans le seul cas de la création d'une clé symétrique avec un CSPRNG. Elle est inutile si vous créez votre clé symétrique avec des lancers de dés.
                        </p>
                        <p class="alert alert-info">
                            <i class="bi bi-info-circle"></i>
                            Bougez votre souris le plus aléatoirement possible dans le cadre jusqu'à ce que la barre de progression, placée sous le cadre, atteigne les 100%. Le sel intermédiaire sera créé. Cliquez alors sur "Créer le sel final" pour générer le sel final à copier dans l'onglet "Créer une clé" et la rubrique "Création d'une clé symétrique avec un CSPRNG".
                        </p>`,
                    "intermediateSaltField" : "Sel intermédiaire",
                    "finalSaltField" : "Sel final",
                    "finalSaltFieldHelp" : `Copiez ce sel final dans le champ "Sel final" à l'onglet "Créer une clé".`,
                    "createFinalSaltBtn" : "Créer le sel final"
                },

                "encryption" : {
                    "title" : "Chiffrement sym.",
                    "paragraph01" : `Merci de lire les explications au premier onglet "Explications" si vous n'êtes pas familier du chiffrement symétrique.`,
                    "symKeyField" : "Votre clé symétrique",
                    "symKeyFieldHelp" : "64 caractères hexadécimaux.",
                    "contentToEncrypt" : "Contenu textuel à chiffrer",
                    "contentEncrypted" : "Contenu textuel après chiffrement",
                },

                "decryption" : {
                    "title" : "Déchiffrement sym.",
                    "symKeyField" : "Votre clé symétrique",
                    "symKeyFieldHelp" : "64 caractères hexadécimaux.",
                    "contentToDecrypt" : "Contenu textuel à déchiffrer",
                    "contentDecrypted" : "Contenu textuel après déchiffrement",
                },

                "findBackSymKey" : {
                    "title" : "Reconstitution de clé",
                    "introduction" : `
                        <p>
                            Pour reconstituer une clé symétrique créée avec le CSPRNG, voir l'onglet "Créer une clé". Vous devez fournir le mot de passe ou phrase secrète et le sel que vous avez stockés en sécurité et peut-être en partie mémorisés. Sans ces deux éléments aucune reconstitution n'est possible.
                        </p>`,
                    "passwordField" : "Mot de passe-phrase secrète",
                    "saltField" : "Sel",
                    "backKeyField" : "Clé reconstituée",
                    "backKeyBtn" : "Reconstituer la clé"
                },

                "symFile" : {
                    "title" : "Fichier - Chiffrement-Déchiffrement",
                    "symKeyField" : "Votre clé symétrique",
                    "symKeyFieldHelp" : "64 caractères hexadécimaux.",

                    "encryption" : "Chiffrement",
                    "encryption-introduction" : `Sélectionnez un fichier et il sera chiffré automatiquement en AES-256 - Mode CBC - IV aléatoire et conversion en base64. Une fenêtre s'ouvrira et il vous sera demandé d'indiquer où enregistrer le fichier chiffré. Le suffixe .enc sera ajouté au nom original du fichier.`,
                    "fileToEncrypt" : "Fichier à chiffrer",

                    "decryption" : "Déchiffrement",
                    "decryption-introduction" : `Sélectionnez un fichier chiffré et il sera déchiffré automatiquement. Une fenêtre s'ouvrira et il vous sera demandé d'indiquer où enregistrer le fichier déchiffré.
                    <br>Le fichier doit avoir été chiffré par le XENATX en AES-256, et non par un autre logiciel, en raison des caractéristiques spécifiques de l'implémentation notamment de la méthode d'inclusion de l'IV et de la conversion en base64.`,
                    "fileToDecrypt" : "Fichier à déchiffrer",
                    "fileToDecryptFiedHelp" : "Le fichier à déchiffrer doit avoir le suffixe .enc"
                }
            },

            "otp" : {
                "introduction": `Chiffrement symétrique dit "parfait", car indécryptable, souvent utilisé en complément pour les passages sensibles d'un message.`,
                "menuExplanations": "Explications",
                "menuConverter": "Convertisseur",
                "menuEncryption": "Chiffrement numérique",
                "menuDecryption": "Déchiffrement numérique",
                "menuHic": "HIC - Hidden In the Crowd",

                "explanations" : {
                    "general" : {
                        "tabTitle" : "Généralités",
                        "title" : "Généralités sur l'OTP",
                        "text" : `
                            <p>
                                Tout au long de cette section, intitulée "OTP – Masque à usage unique", les termes suivants signifient la même chose et sont donc utilisés de manière interchangeable&nbsp;:
                                <ul>
                                    <li>Masque jetable</li>
                                    <li>Masque</li>
                                    <li>Clé ou masque à usage unique</li>
                                    <li>Clé de chiffre de Vernam</li>
                                    <li>Clé OTP</li>
                                    <li>Clé</li>
                                    <li>OTP</li>
                                    <li>Pad (clé OTP stockée sur papier)</li>
                                </ul>
                            </p>    
                            <p>
                                L'OTP (One Time Pad), ou clé OTP, est assimilable à une clé symétrique où chaque caractère de la clé ne serait utilisé qu'une fois. La longueur de la clé est au moins égale à la longueur du message.
                                <br>L'argument théorique est le suivant&nbsp;: toutes les clés étant équiprobables, alors tous les textes en clair de cette longueur sont également possibles et avec la même probabilité. Ne connaissant que le texte chiffré, il n'y a aucun moyen de distinguer, parmi ces possible textes en clair, le texte en clair original. Une analyse statistique, comme l'analyse de fréquence, est vaine.
                            </p>
                            <p>
                                L'OTP est le seul chiffrement théoriquement 100% sûr, quelle que soit la puissance informatique en face, même quantique. Par contre, à la différence du chiffrement asymétrique il faut s'échanger à l'avance des OTP. 
                                <br>Chaque OTP ne s'utilise impérativement qu'une fois. La sécurité de ces échanges physiques d'OTP et de leur stockage en attente d'utilisation est primordiale. Cela requiert une organisation rigoureuse. La sécurité réelle a ses exigences. C'est cela ou rien, et rien n'est pas une option...
                            </p>
                            <p>
                                Un autre avantage du chiffrement par OTP est qu'il se base sur la fonction "OU exclusif" (XOR). De par sa grande simplicité, cette fonction est difficile à affaiblir par un malware ou autre manipulation matérielle, sans que cela soit visible. Donc, même sur un équipement infesté, bien sûr non connecté pour éviter toute fuite de données comme l'OTP lui-même, le chiffrement se fait correctement.
                            </p>
                             <p>
                                Plus d'info.&nbsp;: <a href="https://fr.wikipedia.org/wiki/Masque_jetable" target="_blank">https://fr.wikipedia.org/wiki/Masque_jetable</a>
                            </p>
                            <p>
                                La caractéristique incassable du chiffrement par OTP repose sur la réelle entropie, donc le caractère vraiment aléatoire, de la clé OTP.
                                <br>Plus vous trouverez une source capable de générer un OTP ayant une très forte entropie, donc très aléatoire, mieux sera votre OTP. Vous pouvez donc le générer ailleurs que sur le XENATX et revenir l'utiliser ici.
                                <br>Malheureusement, aucun appareil grand public ne peut créer facilement des OTP. Vous dépendrez donc de fabricant professionnel de TRNG (True Random Number Generator). Outre la difficulté d'avoir accès à ce type de matériel, serez-vous sûr que ces matériels sont fiables et non détournés par des agences de renseignement comme le fut jusqu'en 2020 le fabricant suisse Crypto AG - Hagelin&nbsp;?
                            </p>
                            <p>
                                Nous utilisons dans le XENATX uniquement le chiffrement par OTP numérique, et non le chiffrement par OTP alphabétique également possible. La raison&nbsp;: c'est juste plus simple à mettre en oeuvre et à apprendre.
                            </p>
                            <p>
                                Comme pour les autres techniques de chiffrement, un OTP doit bien sûr n'être créé et utilisé que sur un XENATOFF, ou équivalent, pour éviter les failles logicielles et matérielles d'un équipemet connecté. Tout le chiffrement peut même se faire uniquement avec un papier et un crayon... C'est un autre des grands avantages du chiffrement par OTP.
                            </p>
                            <p>
                                Pour la très haute sécurité, c'est cette dernière solution avec papier-crayon qu'il faut privilégier. Ainsi, aucune trace n'est laissée dans une quelconque mémoire informatique.  
                                <br>Utiliser la touche "Effacer" ne suffit pas. La mémoire de votre équipement informatique pourrait être inspectée à votre insu après le vol de votre matériel, ou durant une simple visite de votre domicile, sans effraction, donc sans que vous vous en rendiez nécessairement compte.
                            </p>
                            <p>
                                Vous trouverez les explications de la génération d'un OTP avec deux dés de précision et les techniques manuelles de chiffrement-déchiffrement avec OTP dans les rubriques respectives ci-dessous <span style="color: rgb(11, 110, 253);"><i class="bi bi-bullseye"></i>&nbsp;Entropie - Création d'un OTP</span> et <span style="color: rgb(11, 110, 253);"><i class="bi bi-key-fill"></i>&nbsp;Chiffrement - Déchiffrement</span>.
                            </p>
                            `,
                    },

                    "entropy" : {
                        "title" : "Entropie - Création d'un OTP",
                        "text" : `
                            <div class="alert alert-info">
                                <p>
                                    <i class="bi bi-info-circle"></i> Trouver, voire fabriquer, un générateur de nombres réellement aléatoires (TRNG - True Random Number Generator) ou même un très bon CSPRNG (Cryptographically Secure Pseudo Random Number Generator) pour générer un OTP le plus entropique possible, n'est pas chose aisée. 
                                </p>
                                <p>
                                    En tout cas, n'utilisez jamais un OTP généré à distance même si les échanges sur le réseau sont protégés par TLS/SSL. Tous sites web, proposant des nombres soi-disant sûrs car basés sur des phénomènes quantiques aléatoires, sont donc à proscrire.
                                </p>
                                <hr>
                                <p>
                                    <i class="bi bi-dice-4"></i> Si vous n'êtes pas un expert en cryptographie, restez-en à la génération de vos OTP grâce à des lancers de dés. En terme de simplicité et d'efficacité, c'est inégalable, au prix bien sûr de la lenteur. Vous trouverez ci-dessous les explications essentielles.
                                </p>
                                <p>
                                    La génération d'un OTP prend du temps par lancers de dés. Vous ne pourrez donc pas avoir beaucoup de clés d'avance. Trop chronophage à générer. Vous ne pourrez donc échanger que des messages relativement courts. Cependant vous pouvez aussi l'utiliser comme un chiffrement complémentaire, au chiffrement AES-256 par exemple, pour certaines parties courtes mais sensibles d'un message comme un lieu de rendez-vous, des noms, des horaires, etc.
                                </p>
                                <p>
                                    Vous pourrez vérifier et compléter à partir des sites ci-dessous <i>(nous ne touchons aucune commission...)</i>&nbsp;:
                                </p>
                                <ul>
                                    <li>https://www.ciphermachinesandcryptology.com/en/onetimepad.htm</li>
                                    <li>https://dicekeys.com/ ou https://www.schneier.com/blog/archives/2020/08/dicekeys.html</li>
                                </ul>
                            </div>
                            <p class="h4">
                                Comment générer un masque (OTP) avec deux dés&nbsp;?
                            </p>
                            <p>
                                Munissez-vous idéalement des éléments suivants : 
                                <ul>
                                    <li>Des dés de précision à six faces tels que les dés de casino ou de backgammon.</li>
                                    <li>Un gobelet en cuir pour bien secouer les deux dés, sans les abimer, avant de les lancer. </li>
                                    <li>Un tapis vert pour recevoir les dés.</li>
                                    <li>La "table de dés", ci-après, pour convertir les résultats en suite de chiffres de 0 à 9.</li>
                                </ul>
                            </p>

                            <p>
                                La technique pour créer une clé, composée exclusivement de chiffres de 0 à 9 inclus, est d'attribuer une valeur à chacune des 36 combinaisons possibles avec deux dés de couleurs distinctes en tenant compte de l'ordre et de la couleur des dés (voir rubrique "Tableau de dés").
                                Pour l'exemple, nous dirons que nous utilisons un dé noir et un dé blanc, même si les dés transparents et de couleurs sont préférables car leur "bonne fabrication" est plus vérifiable.
                                <br>Nous commençerons arbitrairement la lecture toujours par le dé noir.
                                <br>Dans le tableau de dés, nous avons trois séries de valeurs entre 0 et 9. De cette façon, chaque combinaison a une probabilité de 1/30. Les 6 combinaisons restantes, commençant par un 6, n'assurent par une nouvelle série complète de 0 à 9. Du coup, tous les lancers commençant par un 6 (noir) sont simplement ignorés. Cela n'affecte pas la probabilité des autres combinaisons.
                            </p>

                            <p class="h4">Mise en pratique</p>
                            <p>
                                <strong>Premier lancer&nbsp;:</strong>
                                <br>Nous lançons une première fois les deux dés et nous obtenons le résultat suivant&nbsp;:
                                <br>3 pour le noir et 5 pour le blanc, donc nous lisons 35 (rappel&nbsp;: nous avons choisi arbitrairement de toujours commencer la lecture du nombre par le noir, qui devient donc le chiffre des dizaines).
                                <br>Dans la table des dés, le nombre 35 correspond à 6. Le premier chiffre de la clé OTP numérique sera donc 6.
                            </p>
                            <p>
                                <strong>Deuxième lancer&nbsp;:</strong>
                                <br>6 pour le noir et 2 pour le blanc. Nous ignorons ce lancer car le chiffre des dizaines, donc du dé noir, est un 6 or tous les nombres commençant par un 6 doivent être ignorés.
                            </p>
                            <p>
                                <strong>Troisième lancer&nbsp;:</strong>
                                <br>2 pour le noir et 3 pour le blanc. Donc 23 qui correspond dans la table à 8. Le deuxième chiffre de la clé OTP sera donc 8.
                                <br>Et ainsi de suite, jusqu'à atteindre la longueur de clé désirée. Rappel&nbsp;: les 5 premiers chiffres tirés ainsi aléatoirement seront l'identifiant de clé et pas encore la clé elle-même.
                            </p>
                            <p>
                                Vous trouverez les explications des techniques de mise en répertoire et stockage et de chiffrement-déchiffrement dans les rubriques respectives ci-dessous <span style="color: rgb(11, 110, 253);"><i class="bi bi-book"></i>&nbsp;Répertoire et stockage</span> et <span style="color: rgb(11, 110, 253);"><i class="bi bi-key-fill"></i>&nbsp;Chiffrement - Déchiffrement</span>.
                            </p>`,
                    },

                    "diceTable" : {
                        "title" : "Table de dés",
                        "introduction" : `
                            <p>
                                Utilisez deux dés de couleurs différentes. L'important est de les différencier facilement : lisez les explications de création d'une clé à l'aide de dés à la rubrique "Entropie - Création d'un OTP" ci-dessus. 
                                <br>Nous indiquons des dés Noir et Blanc dans le tableau ci-dessous, cependant le mieux est deux couleurs différentes et en même temps transparents pour contrôler visuellement qu'aucune "impureté" ne vient déséquilibrer ces dés de précision (casino ou backgammon)
                            </p>
                        `,
                        "image" : `<img class="code__table" src="assets/images/otp/dice_fr.png" alt="XENATON">`
                    },

                    "randomOtp" : {
                        "title" : 'Entrainement - OTP pseudo-aléatoire',
                        "alert" : `Attention ces OTP d'entrainement ne sont ABSOLUMENT PAS valables pour un usage en situation réelle. L'entropie, donc le caractère aléatoire, est trop faible&nbsp;!`,

                        "quantityField" : "Longueur de l'OTP",
                        "otpField" : "OTP",
                        "createOtpField" : "Créer l'OTP",
                    },

                    "directoryAndStorage" : {
                        "tabTitle": "Répertoire et stockage",
                        "title": "Répertoire d'OTP",
                        "subtitle": "Stocké par exemple sur carte microSD ou sur papier",
                        "paragraph01": `<p>
                                    Pour effectuer un chiffrement par OTP, nous avons besoin d'une clé, appelée une clé à usage unique ou OTP. Cette clé devant être échangée au préalable, nous allons créer d'avance plusieurs clés à s'échanger lors d'un seul échange physique par souci d'économie de rencontres souvent délicates à organiser.
                                </p>
                                <p>
                                    Un répertoire/bloc-notes (le Pad dans One-Time-Pad) peut être une seule mini-feuille, plusieurs mini-feuilles agrafées, un microfilm, etc. Dans tous les cas, un ensemble de répertoires d’OTP se compose de deux répertoires identiques (mais inversé), un répertoire appelé OUT et un autre appelé IN.
                                </p>
                                <p>
                                    Une feuille de répertoire d’OTP standard, ou clé OTP, contient environ 250 chiffres en groupes de cinq chiffres, ce qui est suffisant pour un message d'environ 180 caractères. Le premier groupe de cinq chiffres sur chaque feuille sert de référence de clé et doit être unique pour cet ensemble particulier de feuilles. Tous les chiffres de chaque feuille doivent être vraiment aléatoires (voire Entropie - Création d'un OTP). Le caractère aléatoire est un élément essentiel de la sécurité du processus de chiffrement.
                                    <br>Etant donné que toute feuille (ou clé stockée numériquement) commencée doit être détruite, il est préférable de créer des clés plus courtes d'une centaines de chiffres voire moins. La longueur dépend uniquement de votre choix personnel. 
                                    <br>Si le message est plus long que la clé, la clé suivante dans le répertoire sera utilisée (sans les 5 premiers chiffres, servant d'identifiant, de cette clé suivante) et il n'est pas nécessaire de le préciser. 
                                    <br>L'identifiant de clé suivante à utiliser peut néanmoins être indiqué à la fin de la première partie du message chiffré pour être le plus clair possible.
                                </p>
                                <p class="alert alert-danger">
                                    <i class="bi bi-exclamation-octagon"></i> Pour établir des communications bidirectionnelles, vous avez besoin de deux ensembles de répertoires différents&nbsp;: la personne A a un répertoire OUT dont la personne B a la copie IN, et la personne B a un autre répertoire OUT dont la personne A a la copie IN.
                                    <br>
                                    <br>
                                    N'utilisez jamais un seul répertoire pour communiquer dans les deux sens afin d'éviter le risque d'utilisation simultanée d’une même clé OTP dans le cas par exemple d'un besoin pour les personnes A et B d'écrire en direction l'une de l'autre au même moment. Le chiffrement des deux messages par la même clé, permettrait potentiellement le déchiffrement des deux messages interceptés, même sans connaître la clé&nbsp;!
                                </p>
                                <p>
                                    L'utilisation de plusieurs copies IN d'un répertoire est possible afin de permettre à plus d'une personne de recevoir un message, mais cette pratique est déconseillée. Les copies multiples présentent des risques de sécurité supplémentaires et ne doivent être utilisées que dans un environnement très strictement contrôlé. Cette pratique multiplie également le risque de copies non détruites d'un répertoire après déchiffrement, avec interception possible par la suite.
                                    <br>Dans tous les cas, n'utilisez jamais plusieurs copies OUT d'un répertoire, car cela entraînera inévitablement l'utilisation simultanée d’une même clé OTP, donc un déchiffrement potentiel par un attaquant sans connaître la clé.
                                </p>
                                <p>
                                    Les répertoires d'OTP seront échangés lors de rencontres physiques. Chaque répertoire sera idéalement chiffré et dupliqué sur une deuxième microSD de sauvegarde, stockée dans une "cache" différente. Attention de ne pas perdre la clé AES-256 de protection du répertoire et ne pas oublier les lieux de stockage de tous ces éléments... Organisation et rigueur...
                                </p>
                                <p class="h4">
                                    Exemple de feuillets papier de répertoire OTP
                                </p>
                                <p>
                                    De tels feuillets comprenant 120 chiffres dont 5 d'identifiant peuvent facilement être obtenus en partageant en 4 une feuille de bloc RHODIA n°12 et en utilisant un stylo-feutre à mine 0.5
                                    <br>Agrafez ensuite les feuillets ensemble et vous avez vos repertoires d'OTP "maison" prêts à être échangés. 
                                </p>
                                <p>
                                    Vous constatez ci-dessous la correspondance clé OUT avec clé IN. La première clé OUT 01 (pour écrire et chiffrer) est donc donnée au correspondant A et la clé IN 01 (pour recevoir et déchiffrer) est donnée au correspondant B.
                                </p> 
                        `,

                        "paragraph02" : `
                            <i class="bi bi-exclamation-octagon"></i> Attention, la carte microSD ayant servi à un stockage numérique d'OTP et qui aura régulièrement été insérée dans votre XenatOFF, ne doit ABSOLUMENT JAMAIS être insérée dans un autre équipement. Après utilisation de tous les OTP sur cette microSD, la carte devra être détruite (brûlée) ou éventuellement conservée en sécurité pour de nouveaux déchiffrements ultérieurs d'anciens messages (peu recommandé) 
                            <hr>
                            <i class="bi bi-exclamation-octagon"></i> Cette microSD pourrait être "rechargée" en OTP depuis "l'extérieur" via votre XenatOFF exclusivement en utilisant les voies sécurisées du XENATX.
                            <br>Néanmoins, l'idéal est qu'aucun nouvel OTP ne transite par votre XenatOFF avant utilisation effective. En effet, des traces des OTP ayant transité par votre XenatOFF au rechargement de votre microSD pourrait persister sur le disque dur voire dans la mémoire. Une récupération par un attaquant qualifié et équipé ayant physiquement accès à votre matériel serait alors possible en inspectant-copiant les mémoires de votre XenatOFF. Ce dernier étant plus difficile à cacher qu'une microSD.
                            <hr>
                            <i class="bi bi-exclamation-octagon"></i> Stockez vos répertoires d'OTP et votre XenatOFF dans des lieux différents et sûrs avec impérativement des astuces permettant de savoir si vos "caches" n'auraient pas été "visitées" sans autorisation...`
                    },

                    "encryptionDecryption" : {
                        "title" : "Chiffrement - Déchiffrement",
                        "paragraph01" : `
                            <p class="alert alert-danger">
                                <i class="bi bi-exclamation-octagon"></i> Rappel&nbsp;: <strong>un OTP ne doit JAMAIS être utilisé deux fois</strong>. A n'utiliser qu'une fois, vraiment une seule fois, d'où son nom de "masque à usage unique" ou "masque jetable".
                            </p>

                            <p class="h4">Code en clair</p>
                            <p>
                                En premier lieu, le "contenu textuel en clair" ou "message en clair" doit être converti en "code en clair" grâce à une table de codes qui permet la conversion (cf. rubrique "Tables multilingues de codes"). Il existe une table de codes pour chaque langue. Nous utiliserons la table française de codes dans les exemples suivants.
                            </p>
                            <p>
                                Dans une table de codes, chaque lettre alphabétique (ou caractère spécial) à une correspondance numérique sous forme de chiffres ou de nombres. Les lettres les plus utilisées dans la langue considérée sont assignées à des chiffres pour raccourcir le code en clair au maximum. Les autres lettres et caractères spéciaux sont traduits par un nombre à deux chiffres. 
                                <br>Des astuces et règles de construction évitent les mauvaises interprétations au moment du décodage, à savoir le passage du code en clair en message en clair, côté destinataire du message.
                            </p>
                            <p>
                                Après encodage, le code en clair est regroupé en blocs de 5 chiffres pour plus de lisibilité et éviter les erreurs. Les OTP sont eux-mêmes disposés de la sorte facilitant la saisie, le chiffrement et le déchiffrement manuels.
                            </p>
                            <p>
                                <strong>
                                    Exemple ci-dessous du passage en code en clair du message simple « EXPLICATIONS DE L'OTP » en utilisant la table française de codes <i>(voir onglet Tables multilingues de codes ci-dessous)</i> puis chiffrement par arithmétique modulaire.
                                </strong>
                            </p>`,

                        "paragraph02" : `
                            <p>
                                En pratique, il faut toujours "terminer" tout bloc de 5 "commencé". Donc, par convention, nous terminons toujours le bloc final du code en clair en ajoutant manuellement des points (91).
                                <br>Le code en clair devrait donc se terminer par 68191 au lieu de 681 comme indiqué ci-dessus. Si vous avez 4 "emplacements" à terminer cela donne 9191 (donc deux points). Si vous avez à combler trois emplacements ou un seul emplacement vous ajoutez juste 919 ou juste 9, dans le dernier cas. En voyant un 9 final, nous saurons que ce 9, placé en fin de message, signifie le démarrage de 91 donc un point. Nous l'indiquons par le signe (.) ou néant (Ø), s'il y avait déjà un point avant, au moment du décodage du code en clair en message en clair.
                            </p>
                            <p>
                                Ce code en clair final 28781 78371 16380 45997 22997 89380 68191 est désormais prêt à être chiffré avec une clé à usage unique OTP. 
                            </p>
                            <p>
                                Une fois le code chiffré (pas encore réalisé), il pourra être directement "exfiltré" de votre XenatOFF par une voie de transfert sécurisé (son ou QR Code) voire envoyé tel quel par CiBi/PMR en "jouant" le morse correspondant grâce au TAM (rubrique Outils, onglet TAM - Emett. morse). Attention, renseignez-vous sur la législation de votre pays concernant la transmission chiffrée par CiBi/PMR.
                                <br>Le résultat chiffré pourrait aussi être intégré à un contenu textuel en clair qui sera lui-même chiffré "classiquement" en chiffrement symétrique ou asymétrique, ou les deux.
                            </p>
                            
                            <p class="h4">
                                Chiffrement
                            </p>
                            <p>
                                Une fois le message en clair converti en code en clair, le chiffrement, tout comme le déchiffrement, est basé sur l'arithmétique modulaire. Le procédé est très simple à assimiler.
                            </p>
                            <p>
                                Pour chiffrer le message, nous allons écrire les chiffres du code en clair convertis en groupes de cinq chiffres et écrire juste en-dessous, bien alignés verticalement, les chiffres de l’OTP. Nous opérons ensuite une soustraction modulaire chiffre par chiffre (chiffre du haut moins chiffre du bas).
                            </p>
                            <p>
                                Avec le message en clair ci-dessus "EXPLICATIONS DE L'OTP" converti en code en clair et l'OTP 27793 33873 22989 05220 80984 29034 63759 54704 cela donne la disposition ci-dessous. 
                                <br>Attention il faut bien décaler d'un bloc car les 5 premiers chiffres de l'OTP correspondent à son identifiant (ID.) et ils ne sont donc pas utilisés pour le chiffrement.
                            </p>
                            <p>
                                <strong>Code en clair&nbsp;:</strong>
                                <br>ID.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 28781 78371 16380 45997 22997 89380 68191
                            </p>
                            <p>
                                <strong>Clé OTP&nbsp;:</strong>
                                <br>27793 33873 22989 05220 80984 29034 63759 54704
                            </p>
                            <p>
                                <strong>Résultat chiffré&nbsp;: </strong>
                                <br>27793 95918 56492 11160 65013 03963 26631 14497 (il était bien primordial d'avoir rappelé, en début de message chiffré, l'identifiant de clé utilisée afin de savoir quelle clé utiliser pour le déchiffrement)
                            </p>

                            <p>
                                Pour obtenir ce résultat chiffré, nous avons soustrait sans passer en négatif (par exemple 5 - 9 = 6 car [1]5 - 9 = 6 mais nous n’utilisons pas le [1] du chiffre à gauche, indiqué comme une sorte de retenue&nbsp;!). N'effectuez jamais une soustraction normale car cela créerait un texte chiffré biaisé et complètement non sécurisé ! Utilisez toujours l’arithmétique modulaire et utilisez les onglets chiffrement et déchiffrement avec des suites de nombres fictifs pour vous entrainer et bien comprendre le procédé.
                            </p>
                            <p>
                                Si vous souhaitez envoyer "ce chiffré" par radio, à la voix ou en morse, ou par téléphone, il est recommandé de répéter tous les groupes deux fois pour éviter les erreurs (ex. 27793 27793 95918 95918...).
                            </p>
                            <p>
                                Il peut être très utile de savoir dissimuler son message chiffré (stéganographie). Nous vous conseillons d'approfondir vos recherches grâce au mot clé "WPS" ou Words Per Sentence.
                            </p>
                            <p>
                                Pour finir, renseignez-vous si l'utilisation d'OTP est légalement autorisée dans votre pays. Tous les pays n'autorisent pas le chiffrement incassable. Il est cependant essentiel de connaître cette technique accessible à tous au cas où le légitime devrait primer sur le légal...
                            </p>
                            
                            <p class="h4">Déchiffrement</p>
                            <p>
                                Pour déchiffrer un message chiffré par OTP, vérifiez son premier groupe de 5 chiffres (qui sert de réf./identifiant de clé) par rapport au premier groupe de chaque clé de votre répertoire d’OTP pour retrouver la clé utilisée. Il doit s'agir d'une clé OTP notée "IN" en titre car si vous déchiffrez c'est que vous recevez le message (voir la rubrique "Répertoire et stockage")
                                <br>Rappel&nbsp;: ce premier groupe de 5 chiffres ne fait pas partie du message proprement dit, il ne sert que de référence de clé OTP et n'est donc pas utilisé dans le déchiffrement.
                            </p>
                            <p>
                                Écrivez les chiffres de l’OTP sous le texte chiffré reçu et additionnez le texte chiffré et l’OTP, chiffre par chiffre verticalement. Cela signifie une addition modulaire. Par exemple 9 + 6 = 5 et non 15. Aturement dit quand cela dépasse 9 nous retirons le chiffre des dizaines et conservons juste le chiffre des unités.<br>N'utilisez jamais d'addition normale&nbsp;!
                            </p>
                            <p>
                                <strong>Résultat chiffré&nbsp;: </strong>
                                <br>27793 95918 56492 11160 65013 03963 26631 14497 (il est très important de rappeler l'identifiant en début de message chiffré)
                            </p>
                            <p>
                                <strong>Clé OTP&nbsp;:</strong>
                                <br>27793 33873 22989 05220 80984 29034 63759 54704
                            </p>
                            <p>
                                <strong>Résultat du déchiffrement&nbsp;: </strong>
                                <br>ID.   28781 78371 16380 45997 22997 89380 68191
                            </p>
                            <p>
                                Vous n'avez plus qu'à retourner dans l'onglet "Convertisseur" puis "Convertisseur de code en clair vers du texte en clair" pour constater que vous retrouvez bien le message en clair "EXPLICATIONS DE L'OTP."
                            </p>
                            <p class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> Chiffrez toujours chaque nouveau message avec une nouvelle clé (stockée numériquement ou sous forme de mini-feuillet papier). Ne réutilisez JAMAIS une clé OTP&nbsp;!
                                <br>Détruisez toujours la clé OTP complète immédiatement après avoir terminé le chiffrement, même si elle contient encore des groupes de chiffres inutilisés. Idem après le déchiffrement.
                                <br>Si vous craignez ne pas vous souvenir du message que vous avez envoyé, rechiffrez-le avec un autre OTP (ou éventuellement une clé AES-256) et cachez cette nouvelle clé et le message conservé à deux emplacements sûrs et distincts.
                                <br>Un attaquant aura ainsi deux caches locales à découvrir et non plus une seule (pour trouver votre "première" clé OTP) car votre premier message chiffré aura probablement été intercepté durant sa transmission par internet, voix, morse, etc. à moins que vous l'ayez correctement dissimulé par stéganographie.
                            </p>

                            <p class="alert alert-info">
                                <i class="bi bi-info-circle"></i> Vous trouverez plus d'explications sur notre plateforme d'apprentissage. Vous pouvez également consulter ce très bon site&nbsp;: <br>https://www.ciphermachinesandcryptology.com/en/onetimepad.htm 
                            </p>
                            `
                    },

                    "codeTables" : {
                        "title" : "Tables multilingues de codes",
                        "introduction" : `Ces tables multilingues de codes servent à la conversion manuelle. Il existe également l'onglet "Convertisseur" pour procéder à la conversion automatique.`
                    },

                    "frenchCodeBook" : {
                        "title" : "Livre français de codes",
                        "text" : `
                            <p>
                                L’utilisation manuelle d’un livre de codes est possible pour réduire la taille des messages. Il suffit d'insérer directement le code précédé d'un zéro dans le message en clair à transformer en code en clair dans l'onglet "Convertisseur". Exemple pour le mot ACCEPTER, insérez 0019.
                            </p>
                            <p>
                                Le message ANNULER(073) TRANSFERT(875)S, en mettant un S à transferts car il y a plusieurs transferts que nous souhaitons annuler, devient en message en clair à écrire manuellement 0073 0875S qui sera finalement traduit automatiquement en code en clair par 00739 90875 5 que nous terminerons par 9191 pour compléter le dernier bloc. Soit au final 00739 90875 59191
                            </p>
                            <p>
                                Vous pouvez bien sûr utiliser votre propre livre de codes. Il faut juste que les codes ne dépassent pas trois caractères et uniquement des caractères décimaux.
                                <u>Rappel</u>&nbsp;: le fait que ce livre de codes soit public, donc connu, n'a aucune incidence sur l'indéchiffrabilité du message final chiffré par OTP.
                            </p>
                        `
                    },

                    "englishCodeBook" : {
                        "title" : "Livre anglais de codes",
                        "text" : `
                            <p>
                                L’utilisation manuelle d’un livre de codes est possible pour réduire la taille des messages. Il suffit d'insérer directement le code précédé d'un zéro dans le message en clair à transformer en code en clair dans l'onglet "Convertisseur". Exemple pour le mot DANGER, insérez 0244.
                            </p>
                            <p>
                                Le message DANGER (244) AIRPLANE(064)S, en mettant un S à AIRPLANES car il y a plusieurs avions représentant un danger, devient en message en clair à écrire manuellement 0244 0064S qui sera finalement traduit automatiquement en code en clair par 02449 90064 83 que nous terminerons par 919 pour compléter le dernier bloc. Soit au final 02449 90064 83919 (attention le S est traduit en 83 et non en 5 pour respecter la table anglaise de codes)
                            </p>
                            <p>
                                Vous pouvez bien sûr utiliser votre propre livre de codes. Il faut juste que les codes ne dépassent pas trois caractères et uniquement des caractères décimaux.
                                <u>Rappel</u>&nbsp;: le fait que ce livre de codes soit public, donc connu, n'a aucune incidence sur l'indéchiffrabilité du message final chiffré par OTP.
                            </p>
                        `
                    }
                },

                "converter" : {
                    "title" : "Convertisseur",
                    "paragraph01" : `
                        <p class="alert alert-info">
                            <i class="bi bi-info-circle"></i> Ce convertisseur utilise la table <strong>française</strong> de codes disponible dans l'onglet "Explications" puis la rubrique "Tables multilingues de codes".
                        </p>
                        <p>
                            Convertissez un contenu textuel en clair en code en clair pour un chiffrement par un masque numérique à usage unique (OTP). Rappel&nbsp;: votre OTP doit être généré par un TRNG fiable (générateur de nombres réellement aléatoires). Etant donné que c'est très difficile à trouver, vous devez l'avoir généré a minima par des lancers successifs de dés de précision. Seule technique réellement à disposition du "grand public" et expliquée dans l'onglet "Explications" puis "Entropie - Création d'un OTP".
                        </p>
                        <p>
                            Inversement, convertissez votre code en clair en contenu textuel en clair.
                        </p>
                        <p class="h3">
                            Convertisseur de contenu textuel en clair vers du code en clair
                        </p>

                        <p>
                            Si vous souhaitez indiquer que les caractères suivants sont des signes figuratifs ou des chiffres, formules, etc. utilisez le signe ouverture et fermeture de crochet [], et idem pour indiquer la fin de cette utilisation.
                        </p>
                        <p>
                            Ces crochets [] sont un équivalent au terme "FIG." dans la table de codes. Ces crochets seront donc bien traduits par le code 90
                            <br>Exemple pour encadrer un nombre, voilà le résultat&nbsp;: []42[]
                            <br>Ainsi le contenu textuel en clair (ou message en clair) entre guillemets "UN NOMBRE QUELCONQUE : []42[]." donnera le code en clair suivant&nbsp;: 
                            84 4 99 4 80 79 70 83 2 99 82 84 2 78 71 80 4 82 84 2 99 92 99 90 444 222 90 91
                            <br>Ce qui donne regroupés par blocs de 5 : 84499 48079 70832 99828 42787 18048 28429 99299 90444 22290 91
                        </p>
                        <p>
                            Or, conformément aux explications, il faut terminer manuellement le bloc par des points (91) et en l'occurence également par un 9 seul à la fin ce qui donne&nbsp;:
                            <br>84499 48079 70832 99828 42787 18048 28429 99299 90444 22290 91919
                        </p>
                        <p>
                            Les signes suivants ? (REQ) . ' : + - = peuvent être utilisés directement dans votre message en clair. En revanche pour les parenthèses, indiquez () pour une parenthèse ouvrante et également () pour une parenthèse fermante. Le ? remplace ici le REQ (qui signifie Requête) dans la table française de codes (98).
                        </p>
                        <p>
                            Faites des tests avec la table de codes sous les yeux pour bien comprendre. Dans tous les cas, dès que vous avez des chiffres ou nombres, comme par exemple le nombre 947, encadrez-les de [] donc cela donne []947[]
                        </p>

                        <p>
                            L’utilisation manuelle d’un livre de codes est expliquée dans la rubrique « Livre français de codes » de l'onglet "Explications". Un livre de codes permet de raccourcir les messages.
                        </p>
                    `,
                    "paragraph02" : `
                        Convertissez votre contenu textuel en clair en code en clair, en utilisant uniquement des MAJUSCULES. Chiffrez ensuite ce code en clair dans l'onglet "Chiffrement numérique".
                    `,
                    "clearMessageField" : "Contenu textuel en clair",
                    "clearCodeField" : "Code en clair",
                    "convertToClearCodeBtn" : "Convertir en code en clair",

                    "titleConvertFromClearCodeToClearMessage" : "Convertisseur de code en clair vers du contenu textuel en clair",
                    "deleteBracketsBtn" : "Supprimer les crochets",
                    "convertToClearMessageBtn" : "Convertir en message en clair",

                },

                "encryption" : {
                    "title" : "Chiffrement numérique",
                    "introduction" : `
                        <p>
                            Si vous n'êtes pas familier de l'OTP, merci de lire les explications au premier onglet "Explications".
                        </p>
                        <p class="alert alert-warning">
                            <i class="bi bi-exclamation-octagon"></i> Convertissez au préalable votre contenu textuel en clair en code en clair dans l'onglet "Convertisseur". N'oubliez pas de <strong>retirer l'identifiant</strong>, donc les 5 premiers chiffres de l'OTP, dans le champ ci-dessous Clé numérique (masque - OTP) MAIS <strong>replacez ensuite cet identifiant devant le résultat chiffré</strong> avant de transmettre ce message chiffré à votre correspondant. C'est en effet ces 5 chiffres placés devant le vrai contenu chiffré qui lui permettra de savoir quelle clé OTP utiliser pour déchiffrer.
                        </p>
                    `,
                    "clearCode" : "Code en clair",
                    "digitalKey" : "Clé numérique (masque - OTP)",
                    "encryptedResult" : "Résultat&nbsp;: contenu chiffré",
                    "deleteSpacesHelp" : `Le bouton "Supprimer les espaces" ci-dessous permet de raccourcir le message chiffré dans le cas, entre autres, d'une transmission en morse, via le "TAM" (section Outils)`,
                    "deleteSpaces" : "Supprimer les espaces"
                },

                "decryption" : {
                    "title" : "Déchiffrement numérique",
                    "introduction" : `
                        <p>
                            Si vous n'êtes pas familier de l'OTP, merci de lire les explications à l'onglet "Explications".
                        </p>
                        <p class="alert alert-warning">
                            <i class="bi bi-exclamation-octagon"></i> N'oubliez pas de <strong>retirer l'identifiant de clé OTP</strong>, donc les 5 premiers chiffres du contenu chiffré et de l'OTP, dans les deux champs ci-dessous avant de procéder au déchiffrement. C'est en effet ces 5 chiffres, placés devant le contenu chiffré reçu, qui permettent de savoir quelle clé OTP utiliser pour déchiffrer. Convertissez le résultat obtenu, à savoir votre code en clair déchiffré, en contenu textuel en clair dans l'onglet "Convertisseur".
                        </p>
                    `,
                    "encryptedContentField" : "Contenu chiffré",
                    "otpKeyField" : "Clé numérique (masque - OTP)",
                    "decryptedCCField" : "Code en clair déchiffré",
                    "decryptedCCFieldHelp" : `Rendez-vous à l'onglet "Convertisseur" pour le transformer en message en clair.`
                },

                "hic" : {
                    "title" : "HIC - Hidden In the Crowd",
                    "subtitle" : "Message OTP « caché dans la foule »",

                    "explanations" : {
                        "title" : "Explications",
                        "subtitle" : "",
                        "text" : `
                            <p>
                                Si vous n'êtes pas familier de l'OTP, merci de lire les explications à l'onglet "Explications".
                            </p>
                            <p>
                                Le HIC, terme inventé par Xenaton, désigne plus un principe qu'un système. La logique sous-jacente est connue, du moins connue de quelques « spécialistes » et amateurs de sécurité réelle... dans ce monde d'illusions...
                                <br>Cette logique est utilisée par tous les services de renseignement et armées du monde...
                            </p>
                            <p>
                                le HIC est un peu le « roi » des systèmes de communication sécurisée, au moins pour des messages courts&nbsp;: simplicité, intraçabilité et indécryptabilité garanties de l’échange… 
                            </p>
                            <p>
                                Le HIC n’aura probablement jamais d’équivalent car il repose sur des fondamentaux indépassables comme l'OTP et la stéganographie « inverse » (tout « montrer » à tout le monde plutôt que tout cacher)
                            </p>
                            <p>
                                Une fois le principe compris et un peu d'apprentissage, tout le monde peut mettre en oeuvre un système HIC, sans aucun soutien technologique. C'est l'une des plus grandes forces du HIC.
                                <br>Xenaton n’a « que » optimisé ces principes sous-jacents pour les rendre accessibles mais également adaptés à un environnement public « moderne ».
                            </p>
                            <p>
                                Xenaton vous a aussi rendu « facile » l'apprentissage avec des explications et de la documentation complète et traduite.
                            </p>
                            <p>
                                Des outils de simulation et d'accélération inédits ont bien sûr également été créés. Ces outils s'imbriquent naturellement avec nos autres systèmes novateurs telles que nos indispensables voies de transfert sécurisé par QR codes ou Transmetteur Acoustique en Morse. 
                                <br>Le tout étant, en grande partie, regroupé ici dans le XENATX, outil gratuit à votre service.
                            </p>
                            <p>
                                Une partie complémentaire, en ligne, au sein du service XenaTeam, permet à nos clients d'optimiser-accélérer-automatiser la diffusion de leurs messages HIC sur leurs multiples canaux.
                                <br>Cependant tout peut se faire « manuellement » sans ce service, comme la copie des messages HIC sur de multiples canaux tels que des groupes de messageries, des newsletters, des sites web, etc.
                            </p>
                            <p>
                                Dans la rubrique ci-dessous suivante, « Correspondance d'identifiants », vous trouverez une fonctionnalité à utiliser sur un XenatOFF. Elle vous permet d'accélérer grandement la vérification qu'un HIC vous est adressé ou non.
                            </p>
                        `,
                    },

                    "keysIdentifiers" : {
                        "title" : "Correspondance d'identifiants",
                        "introduction" : `
                            <p>
                                Cette fonctionnalité vous permet de savoir si les identifiants (5 premiers chiffres) des messages HIC, chiffrés par OTP, consultés en ligne sur des canaux partenaires, peuvent correspondre à des identifiants de clés OTP qui vous sont propres.
                                <br>Rappel&nbsp;: les cinq premiers chiffres d'un message chiffré par OTP (HIC ou non) correspondent à l'identifiant, donc également aux 5 premiers chiffres, de la clé OTP à utiliser pour le déchiffrement.
                            </p>
                            <p>
                                Indiquez vos identifiants de clés par blocs de 5, et uniquement vos identifiants, dans le champ "Identifiants de mes clés OTP", ensuite placez-vous dans le champ "Identifiants de messages OTP-HIC" et flashez le premier QR Code des identifiants des messages. 
                                <br>Ces identifiants sont systématiquement regroupés dans un premier QR Code intitulé IDs et sont également indiqués au-dessus du QR Code au cas où vous n'auriez rien pour flasher ou que vous souhaitiez resaisir les identifiants à la main sur papier ou directement dans ce deuxième champ "Identifiants de messages".
                            </p>
                            <p class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> Utilisez cette fonctionnalité uniquement sur un XenatOFF pour vous préserver du fait qu'un attaquant, "présent" dans votre équipement connecté grâce à un malware ou une faille matérielle, puisse voir que tel ou tel message HIC vous était destiné. 
                            </p>
                        `,
                        "keysIdentifiersField" : "Identifiants de mes clés OTP",
                        "messagesIdentifiersField" : "Identifiants de messages OTP-HIC",
                        "resultIdentifiersField" : "Résultat de correspondance",
                        "checkKeysIdentifiersBtn" : "Vérifier"
                    },

                    "training" : {
                        "title" : "Entrainez-vous&nbsp;!",
                        "text1" : `
                            <p>
                                Placez-vous dans le deuxième champ « Identifiants de messages » et flashez le premier QR Code ci-dessous qui transporte les identifiants des 20 HIC du jour publiés sur des sites web partenaires, des canaux de messagerie partenaires, etc. 
                            </p>
                            <p>
                                Ensuite, indiquez plusieurs identifiants que vous inventerez et qui, dans la vraie vie, correspondraient à vos identifiants de clés OTP en votre possession.
                            </p>
                            <p>
                                Exemple pour 40 clés que vous auriez, donc 40 identifiants sous forme de blocs de 5 chiffres&nbsp;: 00414 00868 01656 04876 07735 08436 10860 14258 15833 16843 22383 26126 30544 30750 33465 33504 34468 41004 45818 50844 52152 53187 53440 57368 60508 61553 61884 63547 64088 67703 76481 78007 80736 81505 82531 82578 84422 85518 88687 88715 (copiez-les tous, séparés d'un espace, dans le premier champ « Identifiants de clés » ci-dessous)
                            </p>
                            <p>
                                Si un ou plusieurs identifiants de messages du jour correspondent à un ou plusieurs identifiants de vos clés, le système vous l'indiquera.
                            </p>
                        `,
                        "text2" : ` 
                            <p>
                                Les 4 QR Codes ci-dessous transportent les messages OTP indiqués juste au-dessus. Ils sont séparés par un saut de ligne (65 caractères incluant le premier bloc de 5 chiffres servant d'identifiant). 
                            </p>
                            <p>
                                Ces 20 messages OTP-HIC au total, accompagnés de leur 4 QR codes facilitant leur récupération, servent ici d'exemple d'un envoi quotidien de 20 messages différents dupliqués sur de multiples canaux partenaires. 
                                <br>Grâce au premier QR code nommé OTP IDs qui regroupe tous les identifiants de ces 20 messages, il est plus facile de retrouver le message qui vous est destiné. Il faudra flasher, depuis un XenatOFF, le bon QR Code contenant ce message parmi les cinq autres en vue de son déchiffrement automatique. Vous pouvez aussi le recopier à la main pour un déchiffrement manuel avec un simple papier-crayon.
                            </p>
                        `
                    }
                }
            },

            "tools" : {
                "title" : "Outils",
                "header" : `Transmettre un contenu sans contact par QR Code ou Morse - Convertir - Calculer une empreinte - Stéganographie avec une image.`,
                "menuQrCode": "QR Code",
                "menuTam": "TAM - Morse",
                "menuConverter": "Convertisseurs",
                "menuHash": "Empreinte - Hash",
                "menuSteganography": "Stéganographie",

                "qrcode" : {
                    "title" : "QR Code",
                    "subtitle" : "Transfert optique sécurisé",

                    "textualAndVisualHashSubtitle": `Empreinte SHA-1 globale (textuelle et visuelle) : `,
                    "visualGlobalHashIndication": `Empreinte visuelle globale SHA-1`,
                    "finalGlobalHashVerificationText": `Empreinte textuelle et visuelle SHA-1 de vérification finale de tous les contenus de chaque QR Code ci-dessus réunis : `,
                    "hashParenthesis" : "(empreinte SHA-1)",
                    "introduction" : `
                        <p>
                            Le XENATX utilise le QR Code comme voie de transfert optique sécurisée. Aucun malware ne peut se cacher dans le QR Code. Aucune fuite de donnée importante n'est donc réalisable. Le "transport" des données contenues dans le QR Code est également unidirectionnel. Vous ne risquez donc pas une contamination dans l'autre sens.
                        </p>
                        <p>
                            Vous pourriez également utiliser le <i class="bi bi-soundwave"></i> TAM - Emett. morse dans l'onglet suivant, dans un même objectif de transfert sonore sécurisé de contenu. Les deux solutions ont leurs avantages et inconvénients. Le TAM se prête mieux à un transfert par radio (CiBi/PMR) ou en l'absence de capacité de lecture de QR Code.
                        </p>
                        <p>
                            Pour "transporter" un contenu par QR Codes vers un autre équipement, utilisez la rubrique ci-dessous <i class="bi bi-qr-code"></i> Affichage de QR Code. A l'inverse, pour "recevoir" un contenu transporté par QR Codes, utilisez la rubrique <i class="bi bi-qr-code-scan"></i> Lecture de QR Code.
                        </p>
                    `,

                    "displayQrCode" : {
                        "title" : "Affichage de QR Code",
                        "paragraph01" : `
                            <p>
                                Transmettez optiquement sans contact, via un ou plusieurs QR Codes, un message chiffré, une empreinte, une clé publique, une signature, etc.
                            </p>
                            <p>
                                Le transfert peut parfois nécessiter l'utilisation de l'onglet <i class="bi bi-arrow-clockwise"></i> Convertisseurs pour convertir au préalable le contenu au format hexadécimal. Ce format permet une meilleure préservation des caractères spéciaux et un meilleur découpage automatique en de multiples QR Codes. L'inconvénient étant que l'hexadécimal augmente le nombre de caractères donc le nombre de QR Codes à scanner ensuite.
                                <br>Faites vos propres essais.
                            </p>
                            <p>
                                La capacité de transport d'un seul QR Code est définie ci-dessous pour s'adapter aux sensibilités de détection des différents matériels. Diminuez la capacité si la détection du QR Code ne s'effectue pas correctement par votre équipement (smartphone, webcam, scanner, etc.)
                            </p>
                            <p>
                                Le contenu textuel peut être d'une taille allant jusqu'à 100 fois la capacité de transport maximale d'un seul QR Code. Le contenu sera réparti automatiquement sur de multiples QR Codes. Ils seront générés et numérotés automatiquement les uns en-dessous des autres.
                                <br>Attention le temps de génération peut être long suivant les performances de votre équipement.
                            </p>
                            <p>
                                Pour récupérer le contenu sur votre équipement de destination, utilisez de nouveau un XENATX et allez dans la section "Outils" puis onglet <i class="bi bi-qr-code"></i> QR Code et enfin dans la rubrique "Lecture de QR Codes". Indiquez alors le nombre de QR Codes à lire. Les champs de "réception" sont générés automatiquement.
                            </p>
                            <p>
                                Scannez votre premier QR Code et collez le contenu dans le premier champ. Appuyez sur "Vérifier l'empreinte". L'empreinte textuelle et visuelle générée doit correspondre à celle générée sur votre équipement affichant les QR Codes <i>(grâce à l'empreinte visuelle, le contrôle de la bonne exécution du transfert est particulièrement rapide).</i>
                            </p>
                            <p>
                                Une fois tous les QR Codes copiés un à un, appuyez sur le bouton bleu "RECONSTITUER ET VERIFIER GLOBALEMENT". Les contenus de chaque champ seront réassemblés et une empreinte textuelle et visuelle globale sera générée. Elle doit bien sûr correspondre à l'empreinte globale indiquée sur votre autre équipement "émetteur".
                            </p>
                        `,
                        "transportCapacitySelect" : `Choix de la capacité de transport (en nb de caractères)`,
                        "transportCapacitySelectHelp" : `Entre 200 et 900 caractères transportés par un seul QR Code`,

                        "contentField" : "Contenu textuel",
                        "visualHashSubtitle" : "Empreinte visuelle SHA-1 : ",
                        "contentFieldHelp" : "Indiquez par exemple un message chiffré, une empreinte, une signature, etc.",
                        "createQrCodeBtn" : "Créer le QR Code"
                    },

                    "readQrCode" : {
                        "title" : "Lecture de QR Code",
                        "visualHashTitle" : "Empreinte visuelle SHA-1",
                        "numberOfQRCodesToReadField" : "Nombre de QR Codes à lire",
                        "checkHashBtn" : `Vérifier l'empreinte`,
                        "globalVisualHash" : `Empreinte visuelle globale SHA-1`,
                        "globalReconstitutionField" : `Reconstitution du contenu transféré par QR Codes`,
                        "globalReconstitutionBtn" : `RECONSTITUER ET VERIFIER GLOBALEMENT`
                    }
                },

                "tam" : {
                    "title" : "TAM",
                    "subtitle" : "Transmetteur Acoustique en Morse",
                },

                "tamTx" : {
                    "title" : "Emetteur TAM",
                    "introduction" : `Merci de lire les consignes essentielles et les explications détaillées.`,

                    "essentialInstructions" : {
                        "title" : "Consignes essentielles",
                        "paragraph01" : `
                            <p class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> Pour transmettre un contenu textuel chiffré en asymétrique ou symétrique, convertissez-le d'abord en hexadécimal avec le bouton "Convertir avant transmission". Cela allonge le temps de transmission mais préserve les caractères spéciaux.
                            </p>
                            <p>
                                Pour un message chiffré par OTP numérique, vous pouvez le transmettre sans aucune conversion. Pour gagner un peu de temps de transmission, vous pouvez supprimer tous les espaces entre les blocs chiffrés de 5 chiffres.
                            </p>
                            <p>
                                Pour un message en clair, donc non chiffré, vous pouvez le transmettre tel quel également sans conversion. Cependant écrivez en anglais car l'apostrophe ['] comme dans la phrase [ NOUS T'EMM... AFFECTUEUSEMENT ] n'est pas convertible en morse. Les accents non plus. Seuls les caractères alphanumériques en MAJUSCULES sont autorisés ainsi que le seul signe point [.]
                                <br>Pour éviter l'anglais, convertissez en hexadécimal, avec le bouton "Convertir avant transmission", votre message français en clair comprenant des accents et autres caractères spéciaux, comme pour un contenu textuel chiffré.
                            </p>
                            <p>
                                A la fin de la transmission, transmettez l'empreinte du message pour que le destinataire puisse vérifier qu'il a reçu l'intégralité des données, sans perte ni erreur suite au décodage. Il procédera à la vérification à la rubrique "Vérification des empreintes" dans l'onget "TAM - Récept. morse".
                            </p>    
                        `,
                        "morseFreq" : "Fréquence (Hz)",
                    },

                    "detailedExplanations" : {
                        "title" : "Explications détaillées",
                        "paragraph01" : `
                            <p>
                                Dans le même esprit que le transfert par QR Codes, le TAM permet la transmission sécurisée de données entre un équipement OFF - Voir rubrique « Sécurité du code » pour les détails OFF/ON - et un équipement ON connecté à Internet qui, comme tous les équipements connectés, est vulnérable à une attaque en raison des failles logicielles et matérielles.
                            </p>
                            <p>
                                Le TAM permet donc de transmettre tout type de message, chiffré de préférence. La vérification de l’intégrité de la transmission se fait à la fois à l'oreille et visuellement par le spectrogramme. Le tout en temps réel. 
                            </p>

                            <div class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> 
                                En prime, la transmission radio (CiBi/Emetteur portatif) devient possible d'équipements OFF1 à OFF2, OFF3, OFF4, OFF<sup>N</sup>...
                                <hr>
                                Nous attirons votre attention sur le fait que la transmission de données chiffrées par radio est interdite pour les particuliers radio-amateurs dans certains pays comme la France, bien que la transmission de données chiffrées soit autorisée par Internet.
                            </div>
                            <p>
                                Le TAM est utilisable sur quasiment tous les équipements car les cartes son se trouvent pré-installées presque partout, même sur des équipements réduits comme le Raspberry Pi <i>(modèle n°2, ou n°4 en version modulaire, sans carte wifi, ni bluetooth pour plus de sécurité)</i>.
                            </p>

                            <p class="h4">Procédure en 6 étapes</p>
                            <p>
                                Description précise de la procédure pour transférer un message chiffré de l’équipement sécurisé OFF1 d’un expéditeur vers l’équipement sécurisé OFF2 du destinataire, en passant par les équipements ON1 et ON2 de l'expéditeur et du destinataire, très difficilement sécurisables car connectés.
                            </p>
                            <p class="alert alert-info">
                                <i class="bi bi-info-circle"></i> Rappel&nbsp;: un équipement est dit OFF s'il n’est jamais connecté au monde extérieur par des voies standards (ni wifi, ni bluetooth, ni USB, ni ethernet, etc.). Il n'utilise alors que les voies sécurisées par le XENATX pour communiquer avec l'extérieur. L’environnement sûr qu’il procure est lié, quasi exclusivement, à cette inaccessibilité par des voies standards.
                            </p>
                            </p>
                                Précisions&nbsp;: 4 XENATX seront installés. Un sur chaque équipement en jeu pour cette transmission. Dans le cas de la transmission radio (CiBi-PMR) qui ne mobilise que deux équipements informatiques, OFF de grande préférence, seuls deux XENATX seront donc nécessaires.
                            </p>
                            <br>
                            <h5>
                                <span class="badge bg-success">1</span>&nbsp; Chiffrement et émission depuis le OFF1 vers le ON1 de l'expéditeur
                            </h5>
                            <p>
                                L'expéditeur chiffre d'abord le message grâce au XENATX installé sur son équipement OFF1. Il utilise du chiffrement asymétrique, symétrique, par masque à usage unique ou les trois cumulés.
                                <br>Ce message chiffré est alors transformé en hexadécimal pour une transmission aisée des caractères spéciaux et une vérification visuelle et sonore simplifiée (bouton « Convertir avant transmission »). Dans le cas d'un chiffrement uniquement par masque à usage unique, la transmission se fera tel quel en morse sans conversion préalable en héxadécimal.
                                <br>Le message encodé est transmis en morse vers le ON1, par la carte son avec sortie sur haut-parleurs (bouton « Transmettre »)
                                <br>A la fin de la transmission du message, une empreinte SHA-256 du message chiffré peut également être transmise (bouton « Transmettre l’empreinte »). L'empreinte sert à vérifier sur le ON1 que la transmission s’est faite sans erreur. 
                                <br>S'il y a des erreurs, il arrive souvent que ce soit le son émis par le haut-parleur qui ne soit pas assez fort. Augmentez simplement le volume, le microphone du smartphone ou de l'ordinateur enregistrera mieux le signal et le décodage du morse se fera mieux. <br>Faites des tests de volume sur un court message. Un filtre passe-bas peut malgré tout être nécessaire avant décodage sur le ON1 (voir l'explication à l'onglet "TAM - Récept. morse" puis "Paramètres de décodage" et enfin paragraphe "Filtre passe-bas")
                            </p>
                            
                            <p class="alert alert-success">
                                <i class="bi bi-info-circle"></i> A cette étape, une alternative est possible, à savoir la transmission directe du message du OFF1 de l'expéditeur vers le OFF2 du destinataire par radio (CiBi ou émetteur portatif type PMR) <em> - Dans ce cas, rendez-vous directement à l'étape n°6 - Voir paragraphe RÉSILIENCE plus bas.</em>
                            </p>
                            <br>
                            <h5>
                                <span class="badge bg-danger">2</span>&nbsp; Réception sur le ON1
                            </h5>
                            <p>
                                L’équipement ON1 enregistre cette « partition » en morse via le microphone par une application type enregistreur vocal sur smartphone ou par exemple via le logiciel libre et gratuit Audacity sur ordinateur (https://www.audacityteam.org
                                <br>Le fichier audio de cet enregistrement du code morse est téléchargé et décodé dans le XENATX du ON1 sur l’onglet « TAM - Récepteur morse » bouton "Télécharger un fichier audio". Nous retrouvons alors le contenu hexadécimal, le même qu’avant la transmission.
                                <br>L’équipement ON1 enregistre maintenant la nouvelle « partition » en morse correspondante à l’empreinte SHA-256. Comme l'empreinte est déjà au format hexadécimal, inutile de l'encoder avant transmission.
                                <br>Une empreinte SHA-256 du message au format hexadécimal reçu sur le ON1 est calculée dans l’onglet « # Empreinte - Hash » et comparée à l’empreinte transmise. La transmission s’est bien déroulée si les deux empreintes sont concordantes. Le processus peut donc se poursuivre.
                                <br>Ce contenu hexadécimal est décodé au format texte&nbsp;: format initial suite au chiffrement sur le OFF1.
                                <br>Pour gagner 2 à 3 petites sous-étapes, il est possible de ne pas décoder l'hexadécimal et de transférer le message dans ce format avec les moyens qui vont être expliqués à l'étape suivante n°3, voire de transmettre tel quel le fichier son de l'enregistrement par messagerie ou email.
                            </p>
                            <br>
                            <h5>
                                <span class="badge bg-danger">3</span>&nbsp; Transfert par internet du ON1 au ON2
                            </h5>
                            <p>
                                Transmission du ON1 de l'expéditeur vers le ON2 du destinataire par dépôt du message chiffré dans une boite aux lettres morte numérique (service en ligne de XENATON) ou transmission par messagerie, email, etc. voire remise de main à la main ou dépôt dans une boîte aux lettre morte physique via une clé USB, microSD, papier, etc.
                            </p>
                            <br>
                            <h5><span class="badge bg-danger">4</span>&nbsp; Réception sur le ON2</h5>
                            <p>
                                Si ce message a été intercepté par un attaquant sur cet équipement ON2 (voire sur le ON1 ou même lors de la transmission morse précédente) cela n’a pas plus d’importance que d’habitude car il est chiffré.
                            </p>
                            <p>
                                Le message peut maintenant être déchiffré MAIS comme il est toujours sur une machine ON, qui peut être compromise, ce n’est pas judicieux. Nous allons donc ré-émettre en morse le message vers le OFF2, suivi éventuellement de son empreinte SHA-256.
                            </p>

                            <br>
                            <h5>
                                <span class="badge bg-danger">5</span>&nbsp; Ré-émission depuis le ON2 vers le OFF2
                            </h5>
                            <p>
                                Le destinataire ré-émet vers son équipement sécurisé OFF2 pour déchiffrement et enfin lecture.
                                <br>La suite des opérations est similaire aux indications précédentes. Rappel&nbsp;: le message chiffré est de nouveau encodé en hexadécimal sauf s’il a voyagé encodé entre ON1 et ON2, voire qu'il a transité directement sous forme de fichier son (au format .wav). Le message est alors « rejoué » acoustiquement en morse depuis le ON2 vers le OFF2.
                            </p>
                            <br>
                            <h5><span class="badge bg-success">6</span>&nbsp; Réception sur le OFF2 depuis le ON2</h5>
                            <p>
                                Enregistrement via le microphone du OFF2 dans le logiciel Audacity puis Décodage du morse - Vérification de l’empreinte - Déchiffrement.
                                <br>Déchiffrement et lecture dans cet environnement sécurisé assuré par ce OFF2. 
                                <br>Rédaction de la réponse.
                                <br>Chiffrement et encodage hexadécimal.
                                <br>Et c’est reparti pour une transmission acoustique en morse dans l’autre sens&nbsp;!
                            </p>
                            <p>
                                Le transfert par QR Codes est bien sûr plus rapide pour ces transferts de OFF1 à ON1 et de ON2 à OFF2 mais ce n’est pas toujours possible ni compatible avec la transmission radio.
                            </p>

                            <p class="h4">RÉSILIENCE</p>
                            <p>
                                La résilience pour nos clients en situation dégradée - faible couverture Internet, zone blanche, coupure internet et/ou téléphonie mobile, zone de conflit, surveillance des mobiles, etc. - est donc réelle grâce à la transmission par radio du message chiffré.
                            </p>
                            <p>
                                La transmission se fait plus facilement car directement d’un équipement OFF1 à un autre équipement OFF2.
                                <br>Il suffit de « jouer » le code morse du message chiffré depuis le haut-parleur de son OFF1 à travers sa CiBi/PMR (Emetteur portatif). 
                                <br>Côté réception, il suffit de la même manière de coller le récepteur radio au microphone du OFF2 qui enregistrera directement les DIT-DIT-DAH du morse.
                            </p>
                            <p>
                                Dans certains cas extrêmes, deux personnes distantes de quelques centaines ou milliers de mètres peuvent même communiquer un message court, toujours chiffré et toujours en morse, grâce à des signaux lumineux la nuit et par déviation de rayons du soleil par miroir le jour. Ou autres astuces tel le code sémaphore.
                            </p>
                            
                            <p class="h4">CONCLUSION</p>
                            <p>
                                L'ensemble des fonctionnalités du XENATX, dont ce TAM, était attendu par nos clients les plus exposés à travers le Monde. Ils souhaitaient une solution portative, donc adaptée à tout terrain d’opération, peu coûteuse avec un XenatOFF ou équivalent, logiciellement simple d’utilisation, avec du code vérifiable et gratuit&nbsp;!
                            </p>
                            <p>
                                Chez XENATON, nous sommes heureux d’offrir cette solution globale à ce besoin essentiel de communication sécurisée.
                            </p>
                            <p>
                                La vérité diffusée libère. Bonne utilisation éclairée !
                            </p>
                        `,
                    },

                    "morseCode" : {
                        "title" : "Code morse",
                    },

                    "emission" : {
                        "title" : "Emission",
                    },

                    "spectrogram" : "Spectrogramme",
                    "txTime" : "Temps d'émission&nbsp;:",
                    "contentField" : "Contenu textuel",
                    "contentFieldHelp" : "Caractères alphanumériques en MAJUSCULES plus le signe point [.] uniquement pour un message à passer sans conversion avant transmission.",

                    "hexEncodedContentField" : "Contenu textuel encodé en hexadécimal",
                    "hashField" : "Empreinte SHA-256 du contenu transmis",
                    "hashFieldHelp" : "Transmettez l'empreinte après la transmission du message.",

                    "convertBeforeTxBtn" : "Convertir avant transmission",
                    "originalContentBtn" : "Contenu original",
                    "morseView" : "Vue du Morse",
                    "txBtn" : "Transmettre",
                    "hashTxBtn" : "Transmettre l'empreinte",
                },

                "tamRx" : {
                    "title" : " Récepteur TAM",
                    "subtitle" : "Transmetteur Acoustique en Morse",
                    "introduction" : `
                        <p>
                            Si des erreurs de décodage surviennent alors un filtre passe-bas est probablement nécessaire. C'est toujours le cas pour une transmission radio (CiBi, etc.) - Voir l'explication à la rubrique <i class="bi bi-gear"></i> Paramètres de décodage ci-dessous et au paragraphe « Filtre passe-bas ».
                        </p>
                    `,

                    "downloadFile" : {
                        "title" : "Téléchargement de fichier audio",
                        "paragraph01" : `
                            <div class="alert alert-info">
                                <i class="bi bi-info-circle"></i> Avec le décodage par lecture de fichier, <strong>le son de l'équipement informatique peut être coupé</strong>. Cela rend le décodage du morse beaucoup plus confortable pour les oreilles.
                            </div>
                            <p>
                                Le meilleur décodage est assuré par ce principe de <strong>téléchargement de fichier audio au format WAV uniquement</strong> par rapport à l'usage direct du microphone, rendu possible à la rubrique ci-dessous. 
                                <br>Ce procédé de lecture de fichier est donc impératif pour le décodage d'un message chiffré car la moindre erreur de décodage du morse rendrait ensuite impossible le déchiffrement par clé cryptographique.
                            </p>
                        `,
                        "downloadAudioFileBtn" : "Télécharger un fichier audio",
                        "decodeBtn" : "Décoder",
                        "file" : "Fichier&nbsp;:",
                        "none" : "aucun",
                        "tooManyFiles" : "Vous avez sélectionné trop de fichiers.",
                        "selectOneFile" : "Merci de réessayer en ne sélectionnant qu'un seul fichier.",
                        "notAcceptedFile" : "Ce type de fichier n'est pas supporté.",
                        "selectAudioWav" : "Merci de réessayer en sélectionnant un fichier audio WAV.",
                    },

                    "decodingParameters" : {
                        "title" : "Paramètres de décodage",
                        "paragraph01" : `
                            <p>
                                A la rubrique "Téléchargement de fichier audio", utilisez uniquement un fichier au format WAV. Il faut donc que l'enregistrement du morse "joué" soit effectué avec le format WAV sélectionné.
                                <br>Le logiciel libre et gratuit Audacity (Mac, Windows, Linux) permet l'enregistrement sur ordinateur et l'export au format WAV.
                            </p>
                            <p>
                                Attention le dictaphone par défaut d'un iPhone ne permet pas le format WAV. Il n'autorise a priori que le format M4A. 
                                <br>Utilisez une application gratuite comme l'enregistreur vocal AVR qui permet l'enregistrement au format WAV.
                            </p>
                            <p class="h5">
                                Problème de transmission par radio (CiBi/PMR)
                            </p>
                            <p>
                                Le « souffle » des radios et autres altérations du signal perturbent le décodage du morse. Pour résoudre ce problème, passez l’enregistrement audio du morse dans le filtre passe-bas dans le logiciel Audacity avant décodage dans le XENATX à l'onglet "TAM - Récepteur morse". L'utilisation du filtre passe-bas est expliquée au paragraphe ci-dessous.
                            </p>
                            <p class="h5">
                                Filtre passe-bas
                            </p>
                            <p>
                                Pour appliquer un filtre passe-bas au fichier WAV de l'enregistrement du morse émis, suivez cette procédure dans le logiciel open source Audacity&nbsp;: double cliquez sur la piste pour sélectionner tout l'enregistrement puis menu Effets / Filtre passe-bas. Sélectionnez la fréquence à 563 Hz et le Roll-off à 6 db. 
                                <br>Enfin exportez la piste vers un nouveau fichier en allant dans le menu Fichier / Exporter / Exporter en WAV.
                            </p>
                            <p>
                                <i>Dans une prochaine version nous essaierons d'intégrer ce filtre passe-bas dans le XENATX pour qu'aucune manipulation dans Audacity ne soit nécessaire.</i>
                            </p>
                        `,
                        "freq" : "Fréquence (Hz)"
                    },

                    "microphone" : {
                        "title" : "Microphone",
                        "paragraph01" : `
                            <p class="lead">
                                L'utilisation du microphone fonctionne pour décoder en direct le morse. Cependant le décodage est moins performant que lors du téléchargement d'un fichier audio.
                            </p>
                            <p>
                                Nous préconisons donc l'étape supplémentaire d'enregistrement du morse via un enregistreur vocal sur smartphone ou via le microphone d'un ordinateur en utilisant par exemple le logiciel Audacity. Le fichier audio WAV résultant de cette captation sera ensuite téléchargé et décodé à la rubrique ci-dessus "Téléchargement de fichier audio".
                            </p>
                        `,
                        "listen" : "Ecouter",
                        "stop" : "Stop",

                        "paragraph02" : `
                            <p class="lead">
                                Vous avez dit à votre navigateur de ne pas écouter avec votre microphone.
                            </p>
                            <p>
                                Pour réactiver le microphone, vous devez modifier les paramètres de votre navigateur Web. Pour Chrome, cliquez sur l'icône de la caméra vidéo avec une croix rouge dans la barre d'adresse. Pour Firefox, cliquez sur l'icône du microphone dans la barre d'adresse. Si ce n'est pas là, cliquez sur l'icône du globe à la place.
                            </p>
                        `,
                    },

                    "hashCheck" : {
                        "title" : "Vérification d'empreintes",
                        "paragraph01" : `
                            <p>
                                Calculez dans un premier temps l'empreinte SHA-256 des données décodées du morse en vous rendant à l'onglet <i class="bi bi-hash"></i> Empreinte - Hash de la présente section <i class="bi bi-tools"></i> Outils. <span class="text-muted">Vous obtiendrez par exemple&nbsp;: 9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08</span>
                            </p>
                            <p>
                                Indiquez et comparez ensuite ci-dessous les deux empreintes, celle calculée et celle transmise en morse.
                            </p>
                        `,
                        "calculatedHashField" : "Empreinte calculée",
                        "calculatedHashFieldHelp" : "Collez ci-dessus l'empreinte SHA-256 calculée.",

                        "hashTxField" : "Empreinte transmise",
                        "hashTxFieldHelp" : "Collez ci-dessus l'empreinte SHA-256 transmise.",
                        "compareBtn" : "Comparer"
                    },

                    "displayDataFurtherDown" : `Les données décodées apparaitront un peu plus bas sous le titre "Données décodées".`,
                    "decodedDataTitle" : "Données décodées",
                    "paragraph02" : `
                        Si aucune donnée n'apparait dans le champ ci-dessus, c'est souvent en raison d'un son trop faiblement émis par le haut-parleur pendant l'enregistrement. Re-commencez l'émission et l'enregistrement en augmentant le volume. L'indication d'un enregistrement correct en terme de volume sonore est la couleur vert foncé des barres du spectrogramme, et non vert pâle, ci-dessous, durant le décodage.
                    `,
                    "deleteDataBtn" : "Effacer les données",

                    "spectrogramTitle" : "Spectrogramme",
                    "zoomInBtn" : "Zoom avant",
                    "zoomOutBtn" : "Zoom arrière",
                    "band" : "Plage"
                },

                "converters" : {
                    "title" : "Convertisseurs",
                    "browser-warning" : `Cette transformation fonctionne bien avec les navigateurs <strong>Chrome, Chromium, Opera, Brave et Vivaldi</strong> pour des images jusqu'à 2&nbsp;Mo (plus délicat pour de plus grandes tailles). Ne fonctionne pas avec Firefox, Librewolf et Safari&nbsp;!`,
                    "textToHex" : {
                        "title" : "Contenu textuel vers format hexadécimal",
                        "paragraph01" : `
                            Ce convertisseur convertit un contenu textuel, chiffré ou non, vers le format hexadécimal (HEX). Et inversement. 
                            <br>Cette fonctionnalité est utile pour un transfert plus sûr, par QR Code, des différents caractères notamment spéciaux.
                        `,
                        "contentTextFormatField" : "Contenu textuel",
                        "contentConvertedToHexField" : "Converti au format hexadécimal",
                        "convertToHexBtn" : "Convertir en HEX"
                    },

                    "hexToText" : {
                        "title" : "Format hexadécimal vers contenu textuel",
                        "contentHexFormatField" : "Contenu au format hexadécimal",
                        "contentConvertedToTextField" : "Converti en contenu textuel",
                        "convertToTextBtn" : "Convertir en texte"
                    },

                    "imageToText" : {
                        "title" : "Image vers format texte-base64",
                        "paragraph01" : `
                            <p>
                                Transformer une image (JPEG, PNG ou GIF) en format texte-base64. Limitez la taille de l'image idéalement à moins de 350&nbsp;Ko (2&nbsp;Mo max) en utilisant une forte compression dans un logiciel graphique. Utilisez l'accélération matérielle dans les options de votre navigateur. Soyez patient sur des ordinateurs peu puissants.
                                <br>Ce procédé peut être utile pour chiffrer l'image et/ou la transférer ensuite par QR Codes (attention 5&nbsp;Ko pour cet usage fait déjà environ une dizaine de QR Codes à flasher pour le transfert...)
                                <br>Pour retrouver l'image, utilisez le menu ci-dessous "Format texte-base64 vers image".
                            </p>
                        `,
                        "transformBtn" : "Transformer"
                    },

                    "textToImage" : {
                        "title" : `Format texte-base64 vers image`,
                        "paragraph01" : "Contenu au format texte-base64 transformable en image",
                        "revealImageBtn" : `Révéler l'image`
                    }
                },

                "hash" : {
                    "title" : "Empreinte - Hash",
                    "introduction" : `
                        <p class="alert alert-info">
                            <i class="bi bi-info-circle"></i>
                            Calculez les résultats des principales fonctions mathématiques d'empreintes, appelées aussi "hash", pour un fichier ou une chaîne de caractères. Nous préconisons et utilisons principalement la fonction SHA-512 (SHA = Secure Hashing Algorithm)
                        </p>
                        <p class="h4">
                            Empreinte d'un fichier
                        </p>
                        <p class="alert alert-warning">
                            <i class="bi bi-exclamation-octagon"></i>
                            Veuillez patienter plusieurs secondes pour un fichier volumineux.
                        </p>
                    `,
                    "fileToCheckHash" : "Fichier pour calcul de son empreinte",
                    "title02" : "Empreintes d'une chaîne de caractères",
                    "charsChainField" : "Chaîne de caractères",
                    "hashToCompareField" : "Empreinte pour comparaison",
                    "hashToCompareFieldHelp" : `
                        Optionnel&nbsp;: indiquez une empreinte SHA-512 pour comparaison automatique avec celle qui va être calculée à partir de la chaîne de caractères
                    `,
                    "calculate" : "Calculer"
                },

                "steganography" : {

                    "title" : "Stéganographie",
                    "subtitle" : "Dissimulation d'un contenu textuel dans une image", 
                    "introduction" : `La fonctionnalité de stéganographie suivante vous permet de :
                    <br>
                    <ul>
                        <li><strong>DISSIMULER</strong> un contenu textuel dans une image</li>
                        <li><strong>RÉVELER</strong> un contenu textuel dissimulé dans une image</li>
                    </ul>
                    <p>
                        Il est également possible de dissimuler une image dans une autre image. L'image doit être convertie au préalable en contenu textuel en base 64. Pour ce faire, rendez-vous dans l'onglet « <i class="bi bi-arrow-clockwise"></i> Convertisseurs » et utilisez la fonctionnalité <strong>IMAGE <i class="bi bi-arrow-right-short"></i> TEXTE en base 64</strong>. Nous vous conseillons de chiffrer ce contenu textuel obtenu avant dissimulation. La fonctionnalité <strong>TEXTE en base 64 <i class="bi bi-arrow-right-short"></i> IMAGE</strong> vous permettra l'opération inverse, à savoir reconstituer l'image depuis le contenu textuel, déchiffré au préalable.
                    </p>
                    `,
                    
                    "explanations" : {
                        "title" : "Explications", 
                        "text" : `
                            <div class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> Dissimulez uniquement un contenu textuel chiffré dans une image. Exemple&nbsp;: un message, une empreinte ou une autre image. Tous ces contenus devront préalablement être chiffrés asymétriquement a minima.
                                <br>La dissimulation d'un contenu textuel non chiffré est très fortement déconseillée.
                            </div>
                            <p>
                                Si vous transférez l'image via un service spécialisé de transfert ou la postez sur une messagerie, l'image pourrait subir une compression empêchant la récupération du contenu dissimulé.
                            </p>
                            <p>
                                N'utilisez que des images de type photo de paysage ou plantes et fleurs prises en mode macro. Evitez les images de journaux ou de schémas.
                                <br>Suivant le niveau de sécurité requis, méfiez-vous que l'image ne permette pas votre identification (lieu connu, ensoleillement caractérisant l'heure, reflets, etc.). 
                                <br>Utilisez un logiciel open source comme https://exiftool.org/install.html#Unix (pour utilisateurs un peu avancés) pour retirer les metadata pouvant contenir votre géolocalisation, votre modèle de caméra voire votre nom, etc.
                            </p>
                            <p>
                                Vous pouvez utiliser une banque d'images en ligne telle Unsplash mais la photo personnelle est préférable surtout prise depuis un appareil photo non connecté avec stockage sur carte microSD. Constituez-vous d'avance une banque d'images personnelle sécurisée. Cela peut inclure de ne pas avoir pris son téléphone avec soi... pour éviter tout recoupement entre votre téléphone qui aura borné sur ces lieux et la vue reconnaissable, surtout avec l'IA, et donc associable.
                            </p>
                            <p>
                                Evidemment, il faut qu'à aucun moment votre photo n'ait transité sur le réseau. Attention à iCloud ou tout envoi par internet par email ou par service de transfert de fichier avant insertion du contenu textuel à dissimuler.
                                <br>L'insertion d'un contenu textuel change l'empreinte de l'image en raison de la modification de certains bits. La comparaison avec l'empreinte initiale est impossible sans avoir la photo originale avant l'insertion. Cette impossibilité de comparer les empreintes assure une sécurité supplémentaire.
                            </p>
                        `,
                        "training" : {
                            "title": "Entrainez-vous !",
                            "text": `
                            <p>
                                Les deux images ci-dessous contiennent des contenus dissimulés. L'image de désert contient un OTP déchiffrable avec la clé 27793 33873 22989 05220 80984 29034 63759 54704. L'autre image contient une photo d'une femme en rouge ; pour reconstituer l'image vous devrez utiliser l'onglet "<i class="bi bi-arrow-clockwise"></i> Convertisseurs" et la rubrique "Format text-base64 vers image". 
                            </p>
                            <p>
                                Dans le cadre d'un usage "professionnel", une image cachée doit toujours être chiffrée avant d'être dissimulée dans une autre image avec par exemple une clé symétrique AES-256 (section "Chiffrement symétrique"). Cela évite que n'importe qui utilisant le XENATX puisse découvrir vos images dissimulées dans d'autres images.
                                <br>Un bon exercice à réaliser pour vous entrainer..
                            </p>
                            `
                        }
                    },

                    "hideOrReveal" : {
                        "title" : "Dissimuler ou révéler un contenu textuel",
                        "imageToHideTitle" : "Image pour dissimulation ou révélation d'un contenu textuel",
                        "contentToHideOrReveal" : "Contenu textuel à dissimuler ou à révéler",
                        "imageHidingContent" : "Image dissimulant le contenu textuel",
                        "btnHide" : "Dissimuler",
                        "btnReveal" : "Réveler",
                        "btnDownload" : "Télécharger"
                    }
                }
            }
        }
    },

    en: {
        translation: {
            "meta": {
                "title": "XENATON - XENATX - Autonomous application"
            },

            "error": {
                "otp" : {
                    "trainingRandomOtpQuantityField" : {
                        "minimum" : "Please indicate a minimum length of 10 digits.",
                        "maximum" : "Please indicate a maximum length of 250 digits.",
                        "empty" : "Please indicate the length of the OTP to create.",
                        "NaN" : "Please indicate a number as the length of OTP to create.",
                        "moduloChecker" : "Please indicate a length that is a multiple of 5."
                    }
                },
                "symIndicateBinaryKey" : `Please enter a 256 bit key.`,
                "symIndicateSaltShort" : `Please indicate the salt of your key.`,
                "symIndicatePasswordShort" : `Please enter your password or passphrase.`,
                "symProblemReconstitutionSubtitle" : `Key recovery error: `,
                "symProblemReconstitution" : `A problem occurred while trying to retrieve the symmetric key!`,
                "symProblemCipherDecrytpSubtitle" : `symmetrical decryption error: `,
                "symProblemCipherSubtitle" : `Symmetric cipher error: `,
                "symIndicateYourHexKey" : `Please indicate a symmetric key in hexadecimal format and 64 characters (256 bits).`,
                "symIndicateYourContentToEncrypt" : `Please indicate your textual content to be encrypted.`,
                "symIndicateYourContentToDecrypt" : `Please indicate your textual content to be decrypted.`,
                "symIndicateYourKey" : `Please enter your symmetric key.`,
                "symIndicateSalt" : `Please indicate a salt after generating it on the "Salt Entropy" tab.`,
                "symProblemKeyGenerationSubtitle" : `Symmetric key generation error: `,
                "symProblemKeyGeneration" : `A problem occurred while generating the key!`,
                "symIndicatePassword" : `Please enter a password or passphrase of at least 40 characters to create your symmetric key.`,
                "convertToHexMultiQRC": `This content contains special characters that cannot be transported as such by QR Codes, please convert all or part of this content to hexadecimal first in the Converters tab.`,
                "asymPasswordEmpty": `Your private key password must be specified in the Keys tab.`,
                "privateKeyEmpty": `The private key must be specified in the Keys tab.`,
                "publicKeyEmpty": `The public key must be indicated in the Keys tab.`,
                "asymMessageToCryptEmpty": `Please enter a message to be encrypted!`,
                "asymMessageToUncryptEmpty": `Please enter a message to decrypt!`,
                "asymPasswordMinLength": `Enter a password of at least 16 characters to protect your private key.`,
                "publicKeyInfoEmpty": `Specify a public key.`,
                "revocationPubKeyfieldEmpty": "Please enter a public key to revoke.",
                "revocationCertificateFieldEmpty": "Please indicate the revocation certificate.",
                "hashWellCalculated": `The SHA-512 hash indicated below corresponds well to that calculated!`,
                "hashNotCorresponding": `SHA-512 fingerprint mismatch!`,
                "hexaToUTF8Empty": `Please specify hexadecimal content to convert to UTF8 text format!`,
                "notInHexa": `This content is not in hexadecimal format. Please review your entry!`,
                "textToHexaEmpty": `Please indicate a textual content to convert to hexadecimal format!`,
                "hexaToTextEmpty": `Please specify hexadecimal content to convert to text format!`,
                "textUpperOnlyEmpty": `Please indicate textual content in UPPERCASE with only numbers and Latin letters and the single dot sign [.], or convert your content to hexadecimal format with the "Convert before transmission" button!`,
                "OTPUpperOnlyEmpty": `Please indicate textual content only in CAPITAL LETTERS with only numbers and Latin letters and only the following signs. : ' + - = ? [] ()`,
                "moduloBrackets": `Please check that you have closed the square brackets [] indicating the use of numbers. Their number is currently odd.`,
                "moduloParenthesis": `Please check that you have closed the parentheses () in your formulas. Their number is currently odd.`,
                "textCipheredEmpty": `Please indicate text content, preferably encrypted for security.`,
                "textToMorseEmpty": `Please fill in the field with textual content, preferably encrypted for security.`,
                "textOrHexaEmpty": `Please indicate a content or convert it to hexadecimal beforehand.`,
                "transmissionProblem": `A problem occurred during transmission!`,
                "transmitMessageBeforeHash": `Please send a message before sending your fingerprint.`,
                "hashKO": `Fingerprints are not equivalent!`,
                "contentToQRCodeEmpty": `Please indicate content to be transformed into a QR Code.`,
                "numberOfQRCodesToReadEmpty": `Please indicate a number, in figures, of QR Codes to be read.`,
                "QRCodeEmpty": `Please indicate a QR Code content.`,
                "QRCodesEmpty": `Please indicate content in all QR Code fields.`,
                "tooBigForQRCode1": `The size of the information is too large for a transfer by QR Code. She exceeds `,
                "tooBigForQRCode2": ` characters. Reduce your message or cut it in half.`,
                "passwordPrivKeyEmpty": `Your private key password must be specified.`,
                "textToSignEmpty": `Please indicate a textual content to be signed.",
                "privKeyEmpty": "Missing or incorrect private key.`,
                "signInfoMissing": `Important information is missing or incorrect, please check.`,
                "signatureKO": `WARNING - The signature of this textual content is not valid. The public key indicated may not be the "twin" of the private key used for signing. Another reason: if in the following details it is indicated [ Signed digest did not match ] it means that the textual content does not correspond to the original. It may have been tampered with or you copied the original incorrectly. Details: ',
                "xorDifferentLengths": 'Serious failure, trying to XOR bitstreams of different lengths! `,
                "OTPDecryptSubtitle": 'Decryption (OTP): ',
                "OTPEmpty": `Please indicate a digital key (one-time-pad - OTP).',
                "OTPNumericEmpty": 'Please indicate a digital key (digital one-time-pad - OTP).`,
                "OTPNumericTextEmpty": 'Please indicate plain text content to convert.',
                "OTPNumericNotMultiple5Block": `Please indicate a numeric key composed of a number of digits whose total length is a multiple of 5. Please read the explanations for more details.`,
                "OTPIdentifiersNotNumber": 'Please indicate identifiers in the form of numbers only. Take the time to reread the explanations.',
                "OTPIdentifiersEmpty": 'Please provide identifiers.',
                "OTPIdentifiersTooShort": 'Please provide an identifier of at least 5 digits.',
                "OTPIdentifiersNoMatch": 'No match.',
                "OTPIdentifiersOneMatch": '(this message identifier corresponds to the identifier of one of your OTP keys)',
                "OTPIdentifiersManyMatch": '(these message identifiers separated by a comma correspond to identifiers of your OTP keys)',
                "clearMessageEmpty": 'Please indicate a plaintext message.',
                "clearCodeEmpty": `Please enter a clear code.`,
                "encryptedCodeEmpty": `Please provide encrypted content to decrypt.`,
                "clearCodeNotNumber": `Please indicate a clear code in the form of numbers only. Take the time to reread the explanations, the security of your communications is at stake.`,
                "encryptedCodeNotNumber": `Please indicate encrypted content in the form of numbers only. Take the time to read the explanations.`,
                "OTPNumericNotNumber": `Please indicate a clear code and a numerical key in the form of numbers only. Take the time to read the explanations.`,

                "textToCryptAndOTPIdentical": `The OTP and the message to be encrypted must never be identical. Please re-read your input!`,
                "clearCodeAndOTPIdentical": `The clear code and the digital OTP key must not be identical in any way. Please review your entry!`,
                "encryptedCodeAndOTPIdentical": `The encrypted content and the digital key (mask - OTP) must not be identical in any way. Please review your entry!`,
                "textToDecryptAndOTPIdentical": `The OTP and the message to be decrypted must in no case be identical. Please review your entry!`,
                "OTPLongEnough": `Please provide a digital key (one-time-use mask - OTP) as long as the plaintext message to be encrypted.`,
                "OTPNumericLongEnough": `Please specify a numeric key (one-time-use mask - OTP) as long as the plaintext code to encrypt.`,
                "OTPNumericEncryptedCodeLongEnough": `Please specify a numeric key (one-time-use mask - OTP) as long as the encrypted content to be decrypted.`,
                "textToDecryptEmpty": `Please specify a message to decrypt.`,
                "convertTextToHexa": `Please specify textual content to convert to hexadecimal format!`,
                "bracketsMissing": `Brackets [] are missing to indicate the use of numbers.`,
                "bracketsMissingForSpace": `You cannot use spaces in figurative content including numbers or signs.`,
                "doubleSpaces": `You cannot use two spaces in a row. Save your content by selecting it then copying it and finally press Cancel to read again and adjust your input after the character `,
                "steganographyHideEmpty" : "Please indicate content to hide.",
                "steganographyRevealEmpty" : "Please provide an image to reveal hidden content.",
                "steganographyFileEmpty" : "Please provide an image.",
                "steganographyFileNotImage" : "Please indicate an image file type.",
                "steganographyhidingCapacity" : "concealable characters."
            },

            "success": {
                "revocationCertificate": "This public key has been revoked. Upload this revoked key which now includes the revocation information and deposit it in your key repository."
            },

            "various": {
                "downloadIllustration" : "Download this illustration in PDF",
                "keyRevocationStatusYes" : "Revoked key! Do not use this key for encryption anymore. You can still use it to verify signatures of old encrypted messages.",
                "keyRevocationStatusNo" : "Valid key (not revoked)",
                "copied" : "Copied!",
                "onlyOneQRCodeGenerated": `A single QR Code was generated to transport this textual content.`,
                "manyQRCodesGenerated": ` QR Codes have been generated to carry this textual content.`,
                "madeIn" : "Developed in France",
                "blockOf4": "Presentation by block of 4 characters",
                "indicateChainToHash": "Please indicate a character string to calculate the hash.",
                "hashName": "Fingerprint",
                "hashOK": "The prints are quite equivalent. The transmission was successful.'",
                "indicateHashToCompare": "Please indicate two fingerprints to compare.",
                "signatureOK": 'Signature verified and valid. This textual content has been signed by the "twin" sister private key of this public key whose identifier is',
                "signatureVerification": 'Signature Verification: ',
                "copy": 'Copy',
                "validate": 'Validate',
                "cancel": 'Cancel',
                "encrypt": 'Encrypt',
                "decrypt": 'Decrypt',
                "chars" : "chars."
            },

            "nav": {
                "dashboard": "Dashboard",
                "asymCipher": "Asym. Cipher",
                "signature": "Signature",
                "symCipher": "Sym. Cipher",
                "otpCipher": "OTP",
                "tools": "Tools"
            },

            "dashboard": {
                "title": "Dashboard",
                "subtitle": "",
                "features": {
                    "title" : "Features",
                    "introduction": `<p>
                        XENATX works like a website, in a browser, but without the need to be connected to the Internet. XENATX is therefore a standalone application that can be used with the three operating systems Windows, MacOS, Linux. Some restrictions of use with iOS on iPhone: the sound will not work and the free Files application, including an HTML reader, may be necessary in addition. For Android, it depends on the versions and models but overall most of the features are operational.
                    </p>
                    <p>
                        To generate keys, encrypt or decrypt, the XENATX should preferably be used on equipment called OFF, therefore not connected and never re-connected.
                    </p>`,

                    "menuCipherAsym": "Asymmetric cipher and generating a key pair",
                    "menuCipherAsymKeyInfo": "Public key information and revocation",
                    "menuSignature": "Signature and signature verification",
                    "menuCipherSym": "Symmetric encryption and generation of a key",
                    "menuOTP": "Encryption by One-Time-Pad",
                    "menuQRCode": "QR Code - Optical transfer",
                    "menuTAM": "TAM - Morse - Secure sound transfer",
                    "menuHash": "Fingerprint calculation - Hash",
                    "menuConverterHexa": "Converters - Hexadecimal & Image",
                    "menuCodeSecurity": "Code Security",
                    "menuXenaton": "XENATON",
                    "menuSteganography": "Steganography",

                    "paragraph01": `<p>
                    Go to the <i class="bi bi-file-code"></i> Code Security tab to check the XENATX code by following the instructions provided. You can also find out more about our services in the <i class="bi bi-patch-question"></i> XENATON tab.
                    </p>`
                },

                "codeSecurity": {
                    "title": "Code Security",
                    "quickCheck" : `
                        <p class="h3">
                            Quick check
                        </p>
                        <p>
                            Below you can quickly calculate the SHA-512 hash of the xenatx-*.zip zipped folder you downloaded. Compare the result with the fingerprint provided on our site <a href="https://xenaton.com" target="_blanck">https://xenaton.com</a>, our GitLab <a href="https ://gitlab.com/xenaton/xenatx/-/wikis/version" target="_blanck">https://gitlab.com/xenaton/xenatx/-/wikis/version</a>, our Telegram channel <a href="https://t.me/xenaton_official" target="_blanck">https://t.me/xenaton_official</a> or even on Twitter or Mastodon <a href="https://social.vivaldi.net/@xenaton" target="_blanck">https://social.vivaldi.net/@xenaton</a>
                        </p>
                    `,
                    "calculateHashField" : `Calculation of the SHA-512 hash of the .zip folder`,
                    "paragraph" : `
                        Double check above, for example using free software such as HashCheck (https://code.kliu.org/hashcheck/) on Windows.
                        <br>On Mac and Linux, you can simply type in a terminal the following command line: shasum -a 512 ~/Desktop/xenatx-*.zip assuming you placed the zipped folder on your "Desktop" after download. Remember to replace the star (*) with the correct version of XENATX.
                        <hr>
                        There are also many online tools allowing you to easily do this hash calculation such as https://www.tools4noobs.com/online_tools/hash/ or https://md5file.com/calculator for examples.
                    `,

                    "general": {
                        "subtitle": "General",
                        "text": `
                            <p>
                                Any XENATX user has the right, even the duty, to wonder what guarantees the security and innocuousness of the code.
                            </p>
                            <p>
                                From our point of view, the most valid answer is that the critical parts of the code come from Open Source libraries. We use them without modifying them and we tell you how to check it.
                            </p>
                            <p>
                                However, even if we guarantee you that you can trust, we recommend for your safety to... do not trust us... <br>In other words, always carry out the verification of the code of the XENATX.
                                <br>Why? Because even though we didn't put a backdoor, you don't have to take our word for it. In addition, sophisticated attacks have allowed the modification of code on the fly, sometimes even only for certain users according to their "fingerprint" (multi-factorial fingerprint of their computer equipment).
                            </p>
                            <p>
                                The XENATX code can therefore be intercepted and modified by a third party, on the fly in a way, during the journey between our server and your equipment, during the download. Everything is possible when you are connected to the Internet.
                            </p>
                            </p>
                                This application is intended to work preferably on offline equipment. Thus, if there was a backdoor in the XENATX, which there is not but could be through the backdoor as seen above, it could not be exploited, because no wired or airwave link exists between the Internet and your equipment running the XENATX.
                                <br>We will name any offline equipment with the qualifier "OFF" to distinguish it from "ON" equipment, connected to the Internet.
                                <br>An OFF therefore has no wifi, no bluetooth and no wired ethernet connection. There is therefore no possibility of leakage of sensitive data such as a private key, a message in plain text before encryption, a password, etc.
                            </p>
                            <p>
                                After downloading, you will have to check that the open source libraries have not been modified by us or "on the fly", as indicated above, to weaken their cryptographic quality, for example. You will calculate the global fingerprint of XENATX (seen in the first paragraph) to compare it to the one we indicate on the site <a href="https://gitlab.com/xenaton/xenatx/-/wikis/version" target= "_blank">https://gitlab.com/xenaton/xenatx/-/wikis/version</a>, on Telegram <a href="https://t.me/xenaton_official" target="_blank">https ://t.me/xenaton_official</a>, on Twitter, Mastodon and on our site (links at the footer via the icons). Ideally, you will also check the fingerprints of each open source library. We explain how to do this in the paragraph below "Open Source Libraries".
                            </p>
                            <p>
                                As you will have understood, we strongly recommend the use of XENATX on OFF equipment (such as the XENATX under development). QR Codes or TAM (Morse) are then used for the secure contactless transfer of data from your OFF equipment to your ON equipment. You can thus transfer all types of data such as encrypted messages, public keys, signatures, etc.
                            </p>
                            <p>
                                These secure channels eliminate the need for you to use a USB key to transfer data from one device to another. Not to mention wifi or bluetooth whose two-way data flow is unverifiable for the general public, even for everyone (see the history of Crypto AG recalled on the tab XENATON).
                                <br>A USB key can easily be infected by malware hiding in the firmware (via the badUSB flaw among others). The malware could then spread from one device to another without you being able to know it. The malware would then risk, on the next transfer to your ON equipment, leaking your sensitive data stored on your OFF equipment. All encryption protection efforts would be defeated.
                            </p>
                            <p>
                                The design of this suite of Personal Cyber ​​Security tools integrating from the outset a half ON half OFF operation, with contactless transfers, guarantees you the concept of "Privacy by design", without the need to be an expert in securing your data. 'computer equipment.
                            </p>
                            <p>
                                The coupling of XENATX with the online services XenaTeam, XenaTrust and XenaTraining is not essential but these services are complementary.
                                <br>If you belong to associations for the defense of freedoms, you may be able to access these services for free, otherwise ask your association to contact us for a partnership.
                            </p>
                            <p>
                                When using our XenaTrust service, encrypted messages deposited in the DLB (Dead Letter Box) are undecipherable by our servers. Indeed, your private key used for decryption always remains on your ON equipment, or even better, on your OFF equipment, or even better on a USB or microSD key, itself encrypted, and exclusively used on your OFF equipment when necessary. .
                            </p>
                            <p>
                                So we never store your private keys or try to access them. Physically we couldn't even if you use OFF equipment. Ditto for the data types OTP, symmetric key, password and salt used in the sections <i class="bi bi-file-binary"></i> Encryption by One-Time-Pad (OTP) and <i class="bi bi-file-earmark-lock"></i>Symmetric encryption.
                            </p>
                            <p>
                                You will find that no JavaScript AJAX function is there to discreetly repatriate sensitive data to any server <i>(reminder: if you are using OFF equipment, a leak is not even physically possible!)</i> .
                                <br>You must of course have some computer skills to verify this. However, the code is relatively simple. The hard work being done by open source libraries co-created and maintained by dozens of developers.
                            </p>
                        `
                    },

                    "license": {
                        "subtitle": "License",
                        "text": `<p>
                                   The license has been kept "proprietary" but the use is free and without limitation.
                                </p>
                                <p>
                                    The code is open and voluntarily not obfuscated, therefore perfectly verifiable.
                                    <br>We also indicate the detailed methodology for this verification and encourage it to be done. To do this, go to the <i class="bi bi-file-code"></i> Code Security tab of this section.
                                </p>
                                <p>
                                    We draw your attention to the fact that no guarantee is provided by XENATON regarding the use of XENATX, which is done under the sole responsibility of the user.
                                    <br>Additional information on this subject is indicated in the README.txt file at the root of the downloaded XENATX folder.
                                </p>`
                    },

                    "resilience": {
                        "subtitle": "Resilience",
                        "text": `<p>
                                    Regarding our online services, if we were to "go bankrupt": at least, the XENATX will remain usable and autonomously on your computer equipment.
                                </p>
                                <p>
                                    In addition, all the concepts learned, thanks to the explanations in XENATX and in our online training program XenaTraining, will remain valid and reusable assets for you everywhere. This knowledge concerns open protocols, techniques and logical knowledge.
                                </p>
                                <p>
                                    Encryption keys and signatures created with XENATX, reflecting the slow construction of a network of trust, will remain fully valid, free and easily reusable in other systems. These keys and signatures are also based on open and proven protocols and standards.
                                </p>`
                    },

                    "library": {
                        "subtitle": "Open Source Libraries",
                        "text": `<p>
                                    We have listed all the JavaScript libraries used. You can find them in the assets/javascripts/libs folder in the XENATX source folder that you unzipped. You can also check the contents of our JS codes in assets/javascripts/modules. Our JS codes are deliberately not minified or obfuscated. We indicate the fingerprints on https://gitlab.com/xenaton/xenatx/-/wikis/version so that you can verify that nothing has been altered by a MITM (Man In The Middle) or other techniques.
                                </p>
                                <p>
                                    Use the <i class="bi bi-hash"></i>Fingerprint - Hash tab to calculate SHA-512 hashes and compare them with each other. For security, double the verification of fingerprints as indicated above in the box.
                                    <br>Please double check before thinking of contacting us if the fingerprints differ for one or the other of the libraries below. All it takes is a left comment or an extra line break to make the fingerprints different.
                                </p>
                            
                                <div class="alert alert-info">
                                    We have implemented and embellished with our own codes the following libraries to create a complete and original system by its concept. However, the first merit goes to the creators and contributors of these libraries, and, in succession, to all those who inspired them. For some, these libraries constitute essential building blocks of XENATX.
                                    <hr>
                                    <i class="bi bi-hand-thumbs-up"></i> Special thanks to davidshimjs Sgnamin Shim (qrcode.js), Fabian Kurz (jscwlib.js - JS Morse Code Library), Joshua M. David (OTP functions), Stephen C. Philips (Morse Pro), Julian Fietkau (Mozaic visual hash), Peter Eigenschink (Steganography.js) and Guillaume (language and bug fix)
                                </div>
                                <p>
                                    <strong>Audio-decoder-adaptive - Morse decoding</strong>
                                    <br>Public Source: https://morsecode.world/js/audio-decoder-adaptive.96da976088f5fb7c78e537b303a0ebee.js 
                                    <br>If the following string in square brackets from the above link changes [ 96da976088f5fb7c78e537b303a0ebee ].js, 
                                    just copy and paste the content https://morsecode.world/js/audio-decoder-adaptive.*.js and paste it locally as a replacement.
                                    <br>The differences shouldn't be big and you're guaranteed to use a non-"manipulated" public source. The complete library is here, for study : https://github.com/scp93ch/morse-pro
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/audio-decoder-adaptive-2.0.min.js
                                </p>
                                <p>
                                    <strong>Bootstrap</strong>
                                    <br>Public Source: https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.0/js/bootstrap.min.js
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/bootstrap-5.2.0.min.js
                                </p>
                                <p>
                                    <strong>Chroma</strong>
                                    <br>Public Source: https://raw.githubusercontent.com/gka/chroma.js/main/chroma.min.js
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/chroma-2.4.2.min.js
                                </p>
                                <p>
                                    <strong>Clipboard</strong>
                                    <br>Public Source: https://raw.githubusercontent.com/zenorocha/clipboard.js/master/dist/clipboard.min.js
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/clipboard-2.0.10.min.js
                                </p>
                                <p>
                                    <strong>Crypto js</strong>
                                    <br>Public Source: https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.1.1/crypto-js.min.js
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/crypto-js-4.1.1.min.js
                                </p>
                                <p>
                                    <strong>Forge</strong>
                                    <br>Public Source: https://cdnjs.cloudflare.com/ajax/libs/forge/1.3.1/forge.min.js
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/forge-1.3.1.min.js
                                </p>
                                <p>
                                    <strong>i18next</strong>
                                    <br>Public Source: https://cdnjs.cloudflare.com/ajax/libs/i18next/21.8.14/i18next.min.js
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/i18next-21.8.14.min.js
                                </p>
                                <p>
                                    <strong>jQuery</strong>
                                    <br>Public Source: https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/jquery-2.2.4.min.js
                                </p>
                                <p>
                                    <strong>Js-cookie</strong>
                                    <br>Public Source: https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js (https://github.com/js-cookie/js-cookie/releases/tag/v2.1.4)
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/js-cookie-2.1.4.min.js
                                </p>
                                <p>
                                    <strong>Jscwlib - Morse encoding</strong>
                                    <br>ATTENTION! We have made some minor changes to this library. Read the README.txt file at the root of the XENATX folder to see the changes and calculate the best fingerprint for verification.
                                    <br>Public source: https://git.fkurz.net/dj1yfk/jscwlib/raw/branch/master/src/jscwlib.js (https://git.fkurz.net/dj1yfk/jscwlib/
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/jscwlib.js
                                </p>
                                <p>
                                    <strong>Modernizr</strong>
                                    <br>Public Source: https://modernizr.com/download/?touchevents-setclasses-shiv (custom build)
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/modernizr-3.6.0.min.js
                                </p>
                                <p>
                                    <strong>MosaicVisualHash - Visual fingerprint</strong>
                                    <br>Public Source: https://raw.githubusercontent.com/jfietkau/Mosaic-Visual-Hash/master/mosaicVisualHash.js
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/mosaicVisualHash-1.0.1.js
                                </p>
                                <p>
                                    <strong>Openpgp</strong>
                                    <br>Public Source: https://unpkg.com/openpgp@5.3.1/dist/openpgp.min.js
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/openpgpjs-5.3.1.min.js
                                </p>
                                <p>
                                    <strong>Qrcode</strong>
                                    <br>Public Source: https://cdn.jsdelivr.net/gh/davidshimjs/qrcodejs@gh-pages/qrcode.min.js
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/qrcode.min.js
                                </p>
                                <p>
                                    <strong>Underscore</strong>
                                    <br>Public Source: https://cdn.jsdelivr.net/npm/underscore@1.13.6/underscore-umd-min.js
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/underscore-1.13.6.min.js
                                </p>
                                <p>
                                    <strong>Steganography</strong>
                                    <br>Public Source: https://raw.githubusercontent.com/petereigenschink/steganography.js/master/build/steganography.min.js
                                    <br>Call in the head of the index.html file: assets/javascripts/libs/steganography-1.0.3.min.js
                                </p>`
                    },

                    "jscodes": {
                        "subtitle": "JavaScript codes",
                        "text": `<p>
                                    See <a href="https://gitlab.com/xenaton/xenatx/-/wikis/version" target="_blanck">https://gitlab.com/xenaton/xenatx/-/wikis/version</a> to compare all JS internal code files following:
                                </p>
                                <p>
                                    <strong>Asym. Cipher</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/asym-cipher.js
                                </p>
                                <p>
                                    <strong>Asym. Generate Keys</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/asym-generate-keys.js
                                </p>
                                <p>
                                    <strong>Asym. Info Key</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/asym-info-key.js
                                </p>
                                <p>
                                    <strong>Asym. Revocation Certificate</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/asym-revcert.js
                                </p>
                                <p>
                                    <strong>Core</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/core.js
                                </p>
                                <p>
                                    <strong>Hash File</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/hash-file.js
                                </p>
                                <p>
                                    <strong>Hash</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/hash.js
                                </p>
                                <p>
                                    <strong>Hex converter</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/hex-converter.js
                                </p>
                                <p>
                                    <strong>i18n</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/i18n.js
                                </p>
                                <p>
                                    <strong>Image converter</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/image-converter.js
                                </p>
                                <p>
                                    <strong>Lg</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/lg.js
                                </p>
                                <p>
                                    <strong>Morse</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/morse.js
                                </p>
                                <p>
                                    <strong>OTP numeric</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/otp-numeric.js
                                </p>
                                <p>
                                    <strong>QR Code</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/qrcode.js
                                </p>
                                <p>
                                    <strong>Signature</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/signature.js
                                </p>
                                <p>
                                    <strong>Steganography</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/steganography-image.js
                                </p>
                                <p>
                                    <strong>Sym. cipher file</strong>
                                    <br>Call at the bottom of the index.html : assets/javascripts/modules/sym-cipher-file.js
                                </p>
                                <p>
                                    <strong>Sym. cipher</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/sym-cipher.js
                                </p>
                                <p>
                                    <strong>Sym. entropy</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/sym-entropy.js
                                </p>
                                <p>
                                    <strong>Various functions</strong>
                                    <br>Call at the bottom of the index.html file: assets/javascripts/modules/various-functions.js
                                </p>`
                    },

                    "conclusion" : {
                        "subtitle" : "Conclusion",
                        "text" : `
                           <p>
                                We thank you for your confidence, not blind as it should be. Rest assured that we have privacy and personal sovereignty at heart. Find out more about XENATON's services on the tab <i class="bi bi-patch-question"></i> XENATON.
                            </p>
                            <p>
                                To further increase the security provided by XENATX and overcome potential encryption weaknesses (lack of key entropy, AI, timing attack, quantum calculator, etc.), several solutions are available to you and the list is not exhaustive.
                            </p>
                            <p>
                                For more entropy, keys can be generated by command line CSPRNGs on Linux/Unix systems. Additional entropy generators can also help. The most common is related to mouse movements, like the one we use in the "Sym encryption" section. and the "Salt Entropy" tab. The goal is to move towards the TRNG (True Random Number Generator).
                                <br>Open source software like Veracrypt have this type of solution in their "Keyfile generator". In addition, their hidden volume functionality is also to be discovered. Steganography in all its forms is also important.
                            </p>
                            <p>
                                You can also improve the resistance of your encrypted content by using one-time pads (OTP - One-Time-Pad) for the most sensitive elements of your messages before "classic over-encryption". This nevertheless requires the prior exchange of OTP directories, well generated with maximum entropy.
                                <br>To increase entropy for both AES-256 and single-use masks (OTP), we indicate precision dice-based key creation solutions... Slow but reliable. This will remain the most accessible and safest solution for a long time to come.
                                <br>Might be worth it in some situations. We go deeper into these topics in our XenaTraining program but you already have a very good overview of them in the "Explanations" section of the "Encryption by One-Time-Pad (OTP)" section.
                            </p>
                            <p>
                                In any case, we advise you to be very wary of hardware and software flaws in your devices permanently connected to the Internet... Consider using XENATX, or another encryption software, on OFF equipment that is permanently offline and make the link with your ON devices thanks to the secure transfer channels offered by the XENATX. To do otherwise is illusory in terms of real security.
                            </p>
                            <p>
                                Thank you for your interest, good enlightened use and good recovery of autonomy.
                            </p>
                        `        
                    },
                },

                "xenaton" : {
                    "titleOrigin" : "Origin and purpose",
                    "originParagraph01" : `
                        <p>
                            XENATON is the contraction of Xenophon and Platon in homage to these two sages, direct disciples of Socrates.
                            <br>Under this banner, we support responsible organizations (company, association, collective, etc.) in their Personal Cyber ​​Security needs for their employees or members.
                        </p>
                        <p>
                            You are currently using XENATX, our free-for-all standalone application. This application is ideally coupled with the suite of online services developed by XENATON.
                        <p>
                    `,
                    "titlePresentation" : "Brief presentation of our services",

                    "titleXenaTrust" : "XenaTrust",
                    "xenaTrustParagraph01" : `
                        <p>
                            Web application in SaaS (Software as a Service) mode offering the deposit of encrypted messages in a digital dead letter box and a decentralized and anonymous service for establishing and spreading trust thanks to self-managed cryptographic signatures. The operation is located halfway between the Certification Authority (PKI) and the decentralized trust network. XenaTrust is coupled with XenaTx of course and often coupled with XenaTraining and XenaTeam.
                        </p>
                    `,

                    "titleXenaTraining" : "XenaTraining",
                    "xenaTrainingParagraph01" : `
                        <p>
                            Web application in SaaS mode promoting the learning and updating of essential knowledge and know-how in the field of PCS (Personal Cyber ​​Security). XenaTraining is often coupled with XenaTrust, XenaTeam and of course XenaTx.
                        </p>
                    `,

                    "titleXenaTeam" : "XenaTeam",
                    "xenaTeamParagraph01" : `
                        <p>
                            Web application in SaaS mode for the secure coordination of a team: secure space, management of anonymous accounts, deposits of encrypted messages in a digital dead letter box, multi-cast messages, XenatHIC with HIC (Hidden In the Crowd) messages, etc. XenaTeam is coupled with XenaTrust, XenaTraining and of course XenaTx.
                        </p>
                    `,

                    "titleXenatOFF" : "XenatOFF",
                    "xenatOFFParagraph01" : `
                        <p>
                            Small portable computer equipment called OFF. The principle of OFF equipment is that it is not connected and never re-connected. It is without a wifi card, without a bluetooth card and with its “obstructed” USB and ethernet ports. A sort of black box.
                            <br>The XenatOFF is pre-equipped with a Linux distribution supplemented with essential tools including of course the XENATX. It delivers the full potential of the XenaTx - XenaTrust - XenaTraining - XenaTeam suite of tools.
                        </p>
                        <p>
                            This computer equipment can be used independently with just XENATX, or other encryption software, without any necessary link with our online services.
                        </p>
                        <p>
                            For our partner collectives, we indicate solutions to create a XenatOFF on a reduced budget. <br>If you are not yet a partner or customer, contact us via xenaton.com
                        </p>
                        <p>
                            Ideas for low-cost equivalents exist, such as a simple computer tower, equipped with free XENATX, and from which the wifi and bluetooth cards have been physically removed. You can now create your secure hardware and software chain on a very limited budget.
                        </p>
                    `,

                    "titleXenaTx" : "XENATX",
                    "xenaTxParagraph01" : `
                        <p>
                            Presents free autonomous application in the form of a simple website in JavaScript and HTML usable on all operating systems such as MacOS, Windows, Linux and even Android and iOS with some limitations for the latter two.
                            <br><i class="text-muted small">A version more adapted to smartphones is planned in order to be able to activate all the functionalities.</i>
                        </p>
                        <p>
                            The XENATX covers many essential features:
                            <ul>
                                <li>Asymmetric encryption (OpenPGP.js also used by Proton Mail).</li>
                                <li>Cryptographic signature.</li>
                                <li>AES-256 symmetric encryption in CBC mode with random IV.</li>
                                <li>One-Time-Pad Mask Encryption.</li>
                                <li>Hash calculation</li>
                                <li>Transmission secured by QR Code.</li>
                                <li>Secure transmission by Acoustic Transmitter in Morse for encrypted data that can be transmitted in particular by CiBi/PMR (portable radio transmitter).</li>
                                <li>Steganography.</li>
                            </ul>
                        </p>
                        <p>
                            The contactless approach (QR Code and Sound) of XENATX is essential. It avoids contamination or data leakage by common means of data transfer (wifi, bluetooth, ethernet, USB, etc.) to, or from, a XenatOFF or equivalent equipment.
                        </p>
                        <p>
                            OFF equipment is the only technique offering optimal security by guarding against hardware faults and other software backdoors present on almost all equipment (“zero day” faults in particular).
                            <br>The leak of data, or the malicious insertion of data, via the Internet or via connections becomes physically impossible <em>(except in the extreme case of an attack by remote capture of the electromagnetic field of the equipment: you will find more about these attacks with the TEMPEST keyword)</em>.
                        </p>
                        <p>
                            The only authorized and verifiable "paths" (QR code and Sound) thus allow the secure import or export of valuable data such as encrypted messages, public keys, signatures and even images, etc.
                        </p>
                        <p>
                            The XENATX can be used alone but it pairs optimally with our three online services XenaTeam, XenaTrust, XenaTraining and of course our future XenatOFF.
                            </p>
                            <p>
                            This original and transparent approach places us far from the marketing promises voluntarily obliterating the aforementioned flaws.
                            </p>
                            <p>
                            This subject of "zero day" vulnerabilities and others may seem sharp to you. If you want to go deeper to better understand our point, look on the side of the IME (Intel Management Engine) and AMD st where the flaws / back doors could be directly in the processors…
                        </p>
                        <p>
                            Do you still doubt? Then look for the Swiss company Crypto AG (Hagelin) <a href="https://youtu.be/SWFlA248spU" target="_blank">https://youtu.be/SWFlA248spU</a>
                            <br>Their very expensive Swiss quality products were intended, among others, for embassies around the world. However, these products had been "backdoored" for decades by a large 3-letter service... The complete bankruptcy of Crypto AG only occurred recently in 2020 following these revelations.
                            <br>Another flaw more "mainstream" interesting because widespread: the BadUSB flaw that affects USB devices (key, mouse, keyboard, etc.)
                        </p>
                        <p>
                            With this very quick overview, you should be convinced that encryption, already having its potential weaknesses, is of only limited interest without securing by completely and permanently offline the equipment on which it is carried out.
                        </p>
                    `,

                    "titleRH" : "Human ressources",
                    "RHParagraph01" : `
                        <p>
                            We regularly seek collaborators of great wisdom, with controlled egos, aware of the subtle and ancestral balance between the following three groups: the "Producers", the "Sophists-Cleaners" and the "Philosophers-Guardians". Shades and natural interweavings aside. According to the knowledge from ancient Greece and our own analysis grid.
                            <br>This understanding ensures that our employees will have the optimal approach to the components and issues of a situation and an environment. In short, they will adopt a humanist but realistic attitude.
                        </p>
                        <p>
                            Being aware of the reasons for the predominance of Sophists on the current market seems essential to us. The market being unbalanced, it favors the existence and the development of the sophists. Their role of testers of sagacity, of “cleaners” without empathy, is better understood, without justifying the excesses.
                            <br>The terms "producers of images and appearances or producers of prodigies in speeches", used by Platon in the conclusion of his work The Sophist, find today, more than ever, their perfect illustration.
                        </p>
                        <p>
                            It is up to all of us to clean up the situation through services centered on balance, fairness, lucidity and sovereignty.
                        </p>
                    `,

                    "titleTrainers" : "Network of trainers",
                    "trainersParagraph01" : `
                        <p>
                            We are developing our network of independent trainers to support our clients. We train them for free in our IT solutions while refining their strategic vision of balances in organizational systems.
                        </p>
                        <p>
                            Any form of denial, such as angelism, without exception, cannot and must not resonate with our employees. The approach to our customers is humanistic but pragmatic.
                            <br>Montesquieu, and his peers before him, reminded us that "Every man, or group, company, organization, etc., who has power is inclined to abuse it. It is therefore necessary that by the arrangement of things the power stop the power." ...
                        </p>
                        <p>
                            In fact, XENATON stays away from blissful otherworldliness. Xenophon's Cyropedia, Platon's Sophist, Sun Tzu's Art of War, Voluntary Servitude of Boétie and Machiavelli's Discourses on the First Decade of Livy are among the works we recommend to our collaborators in order to let realistic balance be their quest, without refuge in a fantasy world. Anchoring in reality is essential.
                        </p>
                        <p>
                            Like any company geared towards education, dear to philosophers, and not towards predation, dear to sophists, we focus our efforts on training and developing the sagacity and responsibility of our users and collaborators in a virtuous spirit.
                        </p>
                        <p>
                            We encourage our trainers and our customers to disseminate our knowledge and techniques so that they are the actors of the sanitation of their professional environment, even personal.
                        </p>
                        <p>
                            With that in mind, XENATX is free to everyone, with no restrictions.
                        </p>
                    `,

                    "titlePartnerships" : "Partnerships",
                    "partnershipsParagraph01" : `
                        <p>
                            All of our online services are available free of charge to a large number of collectives, de jure or de facto, working to defend individual and collective freedoms, after cooptation, study and establishment of a partnership.
                        </p>
                        <p>
                            Respect for Man and his personal sovereignty - including his private life and the secrecy of his communications - is at the source of XENATON.
                        </p>
                        <p>
                            The hyper centralized grid of surveillance and control, suppressing all autonomy, therefore all life, is a dead end. Any excessive concentration, in any area, creates a dangerous single point of weakness. Nonsense in terms of resilience and a sophism denying the living, its ecosystems and its necessary biodiversity.
                        </p>
                        <p>
                            If you recognize yourself in these corporate values, we would be honored to evolve alongside you in the pursuit of these common objectives. Do not hesitate to contact us at xenaton.com
                        </p>
                    `,
                }
            },

            "menus": {
                "menuCipherAsym": "Asymmetric cipher",
                "menuSign": "Signature",
                "menuCipherSym": "Symmetric cipher",
                "menuCipherOTP": "Encryption by One-Time-Pad (OTP)",
                "menuTools": "Tools"
            },

            "cipherAsym": {
                "introduction": "Asymmetric encryption, therefore with public and private keys, makes it possible to avoid exchanging a secret key beforehand.",
                "menuExplanations": "Explanations",
                "menuCreateKeys": "Creating a key pair",
                "menuKeys": "Keys",
                "keys": "Keys",
                "menuEncrypt": "Encryption",
                "menuDecrypt": "Decryption",
                "menuKeyInfo": "Info. about a key",
                "menuRevocationKey": "Public key revocation",
                
                "explanations": {
                    "title" : "Explanations",
                    "paragraph01" : `
                        <p>
                            <strong>The exceptional feature of asymmetric encryption is to avoid the secret exchange, in advance, of a common encryption and decryption key between correspondents as in the case of symmetric encryption</strong> (see section "Symmetric cipher" below).
                        </p>
                        <p>This type of encryption was only invented in the 1970s. For the full history, search with the keyword RSA (Rivest-Shamir-Adleman).</p>
                        
                        <p>
                            In the exchange of an asymmetrically encrypted message between two people (or two machines), the two people each have a pair of keys:
                            <ul>
                                <li>A public key, publicly divulgable, therefore known to all, allowing encryption but not decryption <em>(at least not for this purpose of encryption-decryption of a message)</em></li>
                                <li>A private key, sometimes called a "secret key", for decrypting and signing. <u>This private key is strictly personal</u>.</li>
                            </ul>
                        </p>            
                        <p>
                            There is no possibility to recreate the private key from the public key. The public key can therefore be known to everyone, in particular to an attacker.
                        </p>
                        <p>
                            Asymmetric encryption performs three essential functions:
                            <ul>
                                <li>confidentiality (message secrecy), </li>
                                <li>integrity (not altering the message), </li>
                                <li>authenticity (proof that the author of the message is who he claims to be).</li>
                            </ul>
                        </p>
                        <p>
                            For these last two points, integrity and authenticity, a signature mechanism is integrated into the process. This mechanism uses encryption and a hash function establishing a fingerprint. A signature of a message is an imprint, of this message, encrypted with the private key of the author of the message.
                        </p>

                        <p class="alert alert-warning">
                            <i class="bi bi-exclamation-octagon"></i> XENATX allows texts to be asymmetrically encrypted but not a complete file.
                            <br>If you still wish to send an encrypted file without having first exchanged a symmetric key with your correspondent, encrypt it symmetrically in AES-256 ("Symmetric encryption" section then "File - Encryption-Decryption" tab) and indicate the symmetric encryption key in asymmetrically encrypted text.
                            <br>Example of text to indicate under your main message before asymmetric encryption:
                            <br>Attachment file "test.png.enc" symmetrically encrypted with this key 55BCCF0E4BA8426C779D62A534EA183EED3FEEAB4C08615712593E646D737EBF
                        </p>
                        <p class="alert alert-info">
                            <i class="bi bi-exclamation-octagon"></i> XENATX provides essential tools for encryption but it cannot replace a comprehensive cryptography training program. You will find many other explanations and references in our online learning program XenaTraining (free for partner collectives).
                            <br>We already recommend this video composed of good diagrams to fully understand asymmetric encryption https://youtu.be/AQDCe585Lnc (activate the French subtitles if necessary). This site also explains the principle well: https://wikilibriste.fr/fr/intermediaire/chiffrement
                        </p>`
                },

                "creatingAKeyPair": {
                    "title": "Creating a key pair",
                    "paragraph01": `Create your public and private key pair (ECC-25519) then:
                    <ul>
                        <li>Store them safely in a password manager like Keepass placed on a USB or microSD key.</li>
                        <li>Also store your private key password there, if possible by not storing the whole password and withholding part of it.</li>
                        <li>Store the revocation certificate in a different Keepass file and on another medium, if possible.</li>
                        <li>Non-critical information such as the public key identifier can be found in the "Info. about a key" tab.</li>
                    </ul>`,

                    "paragraph02": `To achieve a good level of security, your key pair must be generated on a so-called "OFF" device that is not, and will never be connected to the Internet again.
                    <br>This OFF equipment must have the following characteristics&nbsp; : no wifi card, no bluetooth card, no external wifi-4G card, no ethernet cable connected, no USB ports accessible to third parties, etc. In short, no connection capacity by waves or by connectors.
                    <br>Only a wired keyboard and mouse, never reconnected to other equipment again, can be used: USB sockets indeed have easily infectable firmware allowing data leaks. Your private key or other sensitive data could be stolen more easily than you think.`,
                    "passphraseLabel": `Password protecting your private key`,
                    "passphraseHelp": `16 characters minimum, but rather 40 and more, including specials and of course numbers, upper and lower case. Please note that there is no way to recover your password if you forget it.`,

                    "keyPairIntroduction": "To create an anonymous public key, we recommend leaving this dummy name and email below. Having a name and email increases compatibility with some public key repositories.",
                    "keyPairName": "Name",
                    "keyPairEmail": "Email",

                    "keyValidity": {
                        "label" : "Validity period",
                        "infinity" : "Infinite",
                        "oneWeek" : "One week",
                        "14Days" : "14 days",
                        "30Days" : "30 days",
                        "oneYear" : "One year",
                        "twoYears" : "Two years",
                        "fiveYears" : "Five years",
                        "tenYears" : "Ten years"
                    },

                    "keyPairPubKey": "New public key",
                    "keyPairPubKeyCopy": "Copy",
                    "keyPairPrivKey": "New private key",
                    "keyPairPrivKeyCopy": "Copy",
                    "keyPairRevCert": "Revocation Certificate",
                    "keyPairRevCertCopy": "Copy",
                    "keyPairID": "Public key identifier",
                    "keyPairIDHelp": "Once the identifier is generated, write it down. You may be able to find it in the tab",
                    "keyPairIDInfoKey": "Info. about a key",
                    "keyPairCreateYourKeys": "Create your keys",
                    "keyPairCancel": "Cancel"
                },

                "keys": {
                    "title" : "Keys",
                    "introduction": `Deposit below your private key and the public key to be able to encrypt, decrypt, sign or verify a signature.
                        <br>You must have your own key pair or generate it in the tab`,
                    "password": "Password protecting your private key",
                    "pubKey": "Public key",
                    "pubKeyHelp": `For encryption, specify the recipient's public key (or your own public key for a self-addressed encrypted message). To decrypt, specify the sender's key to verify their signature (or your own key to verify your own signature).`,
                    "privKeyHelp": `Your private key is needed to encrypt (sign) and decrypt. A public key is normally sufficient to encrypt. However, a signature is automatically inserted at the time of encryption. However, a signature is only made with your private key.`,
                    "pubKeyLoad": `Load the public key from a file.`,
                    "privKey": "My private key",
                    "privKeyLoad": `Load your private key from a file.`
                },

                "encrypt": {
                    "title": "Asymmetric Encryption",
                    "subtitle": "Signature included",
                    "introduction": `<p>
                                        Beforehand, indicate the keys in the <i class="bi bi-key-fill"></i> Keys tab.
                                    </p>

                                    <p class="alert alert-warning">
                                        <i class="bi bi-exclamation-octagon"></i> A public key is normally sufficient to encrypt textual content. However, a signature is automatically inserted at the time of encryption and a signature is only made with your private key. This is why you must also indicate your private key in the "Keys" tab.
                                    </p>`,
                    "contentToCrypt": "Textual content to encrypt",
                    "contentDecrypted": "Text content after encryption",
                },

                "decrypt": {
                    "title": "Asymmetric Decryption",
                    "subtitle": `Signature verification included`,
                    "introduction": `<p>
                                        Beforehand, indicate the keys in the <i class="bi bi-key-fill"></i> Keys tab.
                                    </p>
                                    <p class="alert alert-warning">
                                        <i class="bi bi-exclamation-octagon"></i> A private key is normally sufficient to decrypt textual content. However, a signature was automatically inserted at the time of encryption, and a signature is only verified with the public key of the signatory, therefore that of the person who sent you this encrypted content or yours if it is self-addressed content. This is why you must also indicate a public key in the "Keys" tab to decrypt this content.
                                    </p>`,
                    "contentToDecrypt": "Textual content to decrypt",
                    "contentAfterBeingDecrypted": "Text content after decryption",
                    "verifySignature": "Verify Signature",
                },

                "infoKey": {
                    "title": "Information about a public key",
                    "introduction": `Specify a public key and learn the information of this key.`,
                    "pubKey": "Public key",
                    "infoKeyField": "Key Info",
                    "discover": "Discover"
                },

                "revocationKey": {
                    "title": "Revoke a public key",
                    "introduction": `
                    <p>
                        Indicate the public key to revoke and the revocation certificate that you had carefully kept.
                    </p>
                    <p>
                        You will get a new revoked public key keeping the same identifier but incorporating the revocation information. Upload it to your public or private key repositories to replace the old version to inform your correspondents that they should no longer use it. If he ever did it again to encrypt a message, they would be blocked and an indication would appear that this public key is revoked. However, they can continue to use this revoked version to verify your signature on old messages or documents.
                    </p>`,
                    "pubKeyfield": "Public key to revoke",
                    "revocationCertificateField": "Revocation certificate",
                    "newPubKeyRevokedField": "Revoked public key",
                    "revoke": "Revoke"
                }

            },

            "signature": {
                "header": "Establish, propagate and verify trust between members of an organization or between organizations.",
                "menuExplanations": "Explanations",
                "menuSign": "Sign",
                "menuVerifySignature": "Verify a signature",
                "explanations": {
                    "title": "Explanations of the cryptographic signature",
                    "introduction": `<p>
                                        The purpose of a cryptographic signature is to ensure the identity of the author of a document (authenticity) and that the latter has not been modified (integrity). On the other hand, the signature does not deal with confidentiality, which is the business of encryption.
                                    </p>
                                    <p>
                                        It is possible to sign multiple digital documents (photo, contract, message, public key, etc.)
                                    </p>
                                    <p>
                                        Public key signatures are used to certify trust relationships. These signatures then form a chain or web of trust.
                                    </p>
                                    <p>
                                        Indeed, in a public key there is not necessarily information such as surname, first name, email or even social security number that can link a public key to its real "owner". Technically, this information can of course be included and sometimes is. The XENATX makes it possible.
                                    </p>
                                    <p>
                                        However, even if this information is integrated into the public key, it does not guarantee the real identity of the issuer of this key without direct verification with this person whose name, first name, etc. have been inserted into this public key. Anyone can spoof a surname, first name, or even an email, and create a public key with that identity.
                                    </p>
                                    <p>
                                        Signing an individual's public key therefore allows you to certify that the individual, who claims to be the owner of this key, is indeed. You will indeed have met him and will have signed his public key with your private key. By signing an individual's public key, you become a certificate authority, like a stamp in a passport. This spreads trust.
                                    </p>

                                    <p class="h4">
                                        In practice how does it work?
                                    </p>
                                    <p class="alert alert-info">
                                        <i class="bi bi-info-circle"></i>
                                        Attention, in the following paragraphs we will use the terms pubKey (public key therefore public key), privKey (private key therefore private key) and IDPubKey (public key identifier).
                                    </p>
                                 <p>
                                        During a meeting, preferably physical, two members of the same organization (or of a family, a club, a group of friends, etc.) named M1 and M2 decide to sign each other. Concretely, the private key of one will sign the public key of the other and vice versa. This operation is done without showing his private key to the other because the latter must always remain secret.
                                    </p>
                                    <p>
                                        Ideally, everyone repatriates, without contact for security (thanks to the generation of QR Codes in the XENATX - "Tools" section), the public key of the other on their own equipment, preferably not connected, and finally signs this key public with its private key.
                                        <br>The signature made is "returned" to the other by a new generation of QR Codes.
                                    </p>
                                    <p>
                                        

The members M1 and M2 thus mutually prove to be the true holders of their respective pair of keys, consisting of two "twin" keys, the public and the private.
                                    </p>
                                    <p>
                                        The signature of the pubKey1 of the member M1 carried out with the privKey2 of the signatory M2 thus proves that the latter knows well the holder M1 of this pubKey1. And vice versa.
                                        M1 and M2 then form a chain (or web) of trust with two links. The link between these two links corresponds to these two pairs of keys signed between them.
                                        <br>For another member of the organization, it is now necessary to be able to trust these two signatures. You (M3) are another member and were not present during M1's physical meeting with M2. So you don't know if these signatures are valid. You need to check them to be sure that there is indeed a chain of trust between M1 and M2.
                                    </p>
                                    <p>
                                        You (M3) are therefore going to retrieve the pubKey1 of M1 because you know its IDPubKey1 thanks, for example, to a public or private deposit of public keys or simply thanks to a common friend, namely M2.
                                        <br><i>XENATON provides a repository of public keys with its online service XenaTrust but there are many public repositories maintained for example by universities</i>.
                                    </p>
                                    <p>
                                        You (M3) and M2 already form a chain of trust. However, you have spotted the name of M2 or its IDPubKey2, in the list of signatures of M1.
                                    </p>
                                    <p>
                                        By copying both the pubKey1 of M1, the signature and the pubKey2 of M2 (if you don't already have it since you know M2) you will be able to cryptographically verify the validity of this signature that your friend M2 made on the pubKey1 of M1.
                                    </p>
                                    <p>
                                        The essential point to understand is the following: only pubKey2, twin sister of privKey2 having performed the signature of pubKey1, can decipher and therefore verify the validity of the signature established with this so-called privKey2.
                                    </p>
                                    <p>
                                        Technically, a signature is the encrypted SHA-256 fingerprint of the document. This SHA-256 hash of the document is encrypted with the private key of the person signing.
                                    </p>
                                    <p>
                                        You will find many resources on the Internet or on our learning platform XenaTraining to deepen the operation of the cryptographic signature based on asymmetric encryption and mathematical hash functions.
                                    </p>
                                   <p class="h4">
                                        Practical example
                                    </p>
                                    <p>
                                        Here is the signature (which starts with -----BEGIN PGP SIGNATURE-----) of the photo below of a brunette girl in a red sweater.
                                        <br>You will find below the public key, "twin" sister of the private key used to perform this signature. This public key will allow you to verify the validity of the signature of this photo in the "Verify a signature" tab.
                                    </p>
                                    <p>
                                        <label class="control-label" for="signatureGirlRed">Signature de la photo de la jeune fille en rouge</label>
                                        <textarea id="signatureGirlRed" class="form-control" rows="7" cols="100" type="text">-----BEGIN PGP SIGNATURE-----

wnUEARYKAAYFAmQF5GIAIQkQInG4+5oONHwWIQSy1IOvxQwveTazar4icbj7
mg40fA0RAP9WeFvOrnDxE393rDuZzfuu37nSYcfm9cyReq3SMX0btgEAm/Hb
Ub62qULmGxaDj3TAg2/hq5mhGgXnqUTDcRKmNQw=
=HIKk
-----END PGP SIGNATURE-----</textarea>
                    </p>
                    <p class="h4">Photo - Object of the signature</p>
                    <p>
                        <img src="assets/images/signature/girl_red.jpg" alt="XENATON">
                    </p>

                    <br>
                    <p>
                        <label class="control-label" for="pubKeyGirlRed">Signer's public key for verification</label>
                        <textarea id="pubKeyGirlRed" class="form-control" rows="14" cols="100" type="text">-----BEGIN PGP PUBLIC KEY BLOCK-----

xjMEZAXhvxYJKwYBBAHaRw8BAQdAekpBliuUeh/zxH5LdtEZm7dL9CkZ7SaP
7c7jJZ1K8+zNG0pvaG4gRG9lIDxqb2huQGV4YW1wbGUuY29tPsKMBBAWCgAd
BQJkBeG/BAsJBwgDFQgKBBYAAgECGQECGwMCHgEAIQkQInG4+5oONHwWIQSy
1IOvxQwveTazar4icbj7mg40fMUHAQDARa54M0QPcZLhtjYHn9ULamTvg2uL
XNBJ1ampVnNfXgD/U/FCsquv34Oth2mv6dkqJ4ViXiVK91vBUIpx/0Zc6QXO
OARkBeG/EgorBgEEAZdVAQUBAQdA6lkmut2gpU48IKSSqHrq+rbXjnO8Btfn
i7/ogWxMUAoDAQgHwngEGBYIAAkFAmQF4b8CGwwAIQkQInG4+5oONHwWIQSy
1IOvxQwveTazar4icbj7mg40fClgAP9GOMLfSS4XhHgbWRwKE3FfRjOlbBhE
m3eeKhcO8Uwe/gEAkqUCUb14GdqqqEMSg4/o6ybM/hkD8kTZ7jhnx08FYQ8=
=rhKn
-----END PGP PUBLIC KEY BLOCK-----</textarea>

                </p>
                <p class="alert alert-info">
                    <i class="bi bi-info-circle"></i>
                  To proceed with the verification of the signature for educational purposes, you will find below the photo transformed into base64 format. Indeed, a photo, like any file, is only a sequence of 0s and 1s, and can therefore take this textual form after conversion.
                    <br>To test for yourself, you can copy all the textual content below and reconstruct the photo by going to the "Tools" section then to the "Converters" tab and finally to "Format text to image".
                </p>
                <p>
                    <label class="control-label" for="girlRed">Photo converted to text content in base64 format</label>
                    <textarea class="form-control" rows="10" cols="100" id="girlRed" type="text">/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAAAAAD/4QOBaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA3LjItYzAwMCA3OS4xYjY1YTc5YjQsIDIwMjIvMDYvMTMtMjI6MDE6MDEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9IkQ3RTRCMkY4RkIzRDEwQjUwMEI3MjEzN0Q0MjNFQjBEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkFCMzRCNzlEMTgzMzExRUQ5RTU0QTNFNkEwOTFFODUwIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkFCMzRCNzlDMTgzMzExRUQ5RTU0QTNFNkEwOTFFODUwIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCAyMDIyIE1hY2ludG9zaCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjY4NmVhMjEwLTVjMGItNDAxYS1iODgzLWY5ZGM4ZDk5NTBhYSIgc3RSZWY6ZG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOjFhZTUyMTAwLWE3NzktNWM0OS1iZTZhLWUxNTdlMTBlYzJmMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uACFBZG9iZQBkwAAAAAEDABADAgMGAAAMkwAAFXYAABlg/9sAhAAbGhopHSlBJiZBQi8vL0JHPz4+P0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHAR0pKTQmND8oKD9HPzU/R0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0f/wgARCAFdAWMDASIAAhEBAxEB/8QAqgAAAgMBAQAAAAAAAAAAAAAAAAECAwQFBgEBAQEBAQEAAAAAAAAAAAAAAAECAwQFEAACAgEEAgEEAwEBAQAAAAAAARECAxAhEgQgEzEwQCIUUDIFQRVgEQABAgQEBAQGAgMBAAAAAAABABEQITECIEFREjBAYXFQgaEi8JGx0TIDwWLhghOSEgAABQIFBQAAAAAAAAAAAAAAQAERIRBwIFBgMUHwUXGhMv/aAAwDAQACEQMRAAAA5ks0pdLzBreQNjxs1vGza8TN7wyNzws2wzVmyXLF6dvNDqPlzOjHmRTqHPsNqyBrWVGpZQ1LKGlZkaI0IvVCNCoRpMwYW3ESSqI0AmMQSIsk4MnbTpHVZnptIcYonKhGkomO/NoJwlTLMpaWlIXFLLCtFhWiwrRYVotKg6MuxKOJHt0HEp7PPMrnYtb02Lhj0HGGW5mNN7zCjRSRLAhXoZmjfWRkgtspuL6boGdWqWBbKKTQzMbtJyF3mnn13MhzVoqqBMPaObSmnVXLzed1ubLgvq01MmpqV0bc2NGjNqY3W94LL+lnXHu68zj19uqXg0dzPXGXVoTC7HZPRlssnGwlVq1S1z2TzYaZW6zCU3c5svSqXh4fRY864x1SX0LDpzjXZXm5Ob1uZnXKuplua40KNluK+XXytfNqWzL2jVeyWMZxWMZKIRmlgrBefzfRYE5Mq7N83Mmt/Rx9Ll0nN2EZs1zGG8qM0U06oZuY0EugH0xGuyvNq5XW5GdcqUH0zOIotspsVZNuIv8AQcLvS64yjLEZLGMkRUhYjSibTz2DrcrWNF+C+zp9Dg9Dn07F2PRjVxB6xMiayxGoIBARax7zGFkZaOP2+Ri8NkemZEXLbOuxbMtwWeh5HazpYNta5NWPDL3DBulcY886FfKvs6N2LQcvl9/z1zHXk0azosjKas2YFm9yzj6Ma6RinZqMs7m8hKxiE1g+mUpIq5XW5eNcGF0NyDk4cpJYQrZ6HbTfjfJwdrOvN1wrOnszbIoxdDkrBZ1Zv38PcunzPq/MXNFtM9Y26cWuVqSEBYnGEX7+N2JdVkLEYytTi9ZaIxDm7+XnXKU3qQLBQlAwyTl9Xbn0Z0UaY1kNBKWMuac+quaxzvlLXY2kPNek89WAa3zu2YNK7a51kFXBm+NYS7HD6MvYsy2y6Cgudssspb66s5Ll2ZqhIlqJTQqrqYy21a5vt6sunOpQUAz13Z10YZXrOjJJSxu52yW4iWV+c9DwTCk+nKV1NpvefSuSrRVcwJJI21h1NPDuzewcgOiZHWiuDsEywkpANkc+vKuCVbxv0HR856HOlj083HS/Zwuhq74Y6l6T5Tl0GKcz0p1WJX57sed3iDDfK2yu1qevFpFG+NxQr1ZQrkUq1FRYGlxZJpjknTZIGSRZtWWa5ZKvOrPU+W6uddbHsq59OZo6PK3rrauTXefW5leCVdKyzOmymOVzSHXi2nZOyqwtsqkvQKNmsUx0KzNHTBKI3ooLQjJTzokSobkKTkkXJrXl1VS4MurHLYAvp7uP2Oe7seyNvBr71WfRxOtdYw4WQcquBv4WohvfKIkWSgLfZmmujTGlOsY9+s1xtilMbolRMKpOUqm2DchNyESZTn05V5dVsZSKkvV35dXPe583WWpMTjBZ4ms6x8Xu8PpzacNYiSjZMGRYS7rcm1aujivTapRuYRsjUBhXJTGyQSGDbAbjPi6tC8XRZQsc2urPTr7uN3MbzRvUmSN9OdKamCnAycXtcbpzrB75pSSaad9C5LYWhoxbpbLst9bbHXcyjOKRAIyjOnJSG1IbTG0wKME7aeaq53koOWfU5cJ09Q/P9LPn2VtZiIqWVcsdlHLtq6ISia4TUomrfxNVlUWyG3NbNbE4nQWDal0QuYgEZxnTkpDlGldJyKZ26/Pxxd7IRbowITBJRFNiBi2/ETO6vKmbKxUkxIRsjeSTHOIK5slXKWyVYaL82hbI3Zk6jpuuUARnGVSlGuafNdD3KKAEDAlbUmgBRNCUlMxGMxJIQxIjSJMYipFzBTTnEExKdTLp0WL1aYC7boO4kIQnCYcy/E9pAU7JAwAwabTaGmAoCQTJENIhgkxEMSKmkipCRJJIRtV51qxMxsi0nZTcurVPLeWowidCUKzn1xU+g0JGJyDBoYKxNWIVoAAQTBAAAgAIaBMRAIhiJSQkxknF3XV1crq3zUFo5V5dPPdM8Wp6QCRtNptNsABoVgKAAgRgCGgAAAAEQAAIhgk4sghhuLWztcPq3F4i+Wvl9Tku0E1Ow05ZIGpSjJtAAArEDEAADENAAAAIJoAAAQGljGUXMAZGgl0OfsroCL4//9oACAECAAEFAP5OSft1pJJJJPjPhJOj1ggQ/JPRjei1ZGj8n5rRrSfrTrtotWpIfm/LY2EhohaLxga+nBGjIIF5PWdEvB6STJJImSLzjSCPB6PV+CX1no9Z0eiX2rKr7Jk6yV+yfjX7KCNULb7SCF/9Mvuf/9oACAEDAAEFAP5KCCPt3pBBBxOL8II8Y0XixeTU6ISGPwnReS83omNEfWjXfS2skoj6m5uNiZLPkt4yT9OSXoiSR/HitY8lpBEHESgaIHq/CdJJ8FotxH4iVTij40s/rLRCI5riKurcvV/WTdWsha86Wfk19REawW+xVSyh+Fvr1xNnBVEpdq8k6tC1gf00nZrEytVXRqRKNYIRAxqR/RrV2aoqLVawQQRq0OuqGo8MVeKb+nBBBBapxIgiR6JS5gn7CNEZFFjGt39i9EZVKMfgvrPW/wDU/9oACAEBAAEFAFYVjmczmczmczmczmK4rnM5nM5lsjK5YH2IF2JFmFmk9hzPYczmczmczmczmczmch2HY5HI5HI5HIVjkcjmczmew9h7D2HsFkPYew9h7D2Fsg8jYrJCzHtFnQ89Uv2LWPdYV5FZHOD2HsPYew9h7D2HsOZzOZzOZzOZz1kkkkk5HI5HI5HI5nMqmLYs5bbIhczmhZKnJEzo7Mx2bHceQ9h7D2HsPYczmczmczmczmczmQQQQQQR5pCUuYUwSULOXEj1UITGnZ44RasDX2H65+ufrjwFsUDUECqKknqHjg4MWORYmKqRcXwyuwvg4y2tFIkKqKwhPkWTGiCCCCCCDiziyCCCCBYUelFsJbEZMZkpAkVqVocThJ6RYoLUG95bGNCqOm3GU8Zeg1BuxIUFYK2VS1lZuu/EVRVOB6ymCSvWF1UPqot1kX65fHBBAqnEdS1TLUzVKoqiCCtStRqDJsnJDOLbWEWFi67ar19r4mWqWqOrOLIE2imRlWkNSJCQqioLHvjxlaCqcS1DJiMuIvjaOLEiBosjKjKipQjSghfm+y4OQnJhwOxTALCkcEOiLYky/Xkt1JLdQv1mi1bVE0zhBTINpiaKox4iuMWMrUSEiB1LUL4pMmA9GrLGRSdhQLYrc5juVucpK2hdiztdVk6+Hm8WJJRo9WiDiPGmZuvtfHxa2HXaraFuYaS8dStTiJEEaQNDqWoevVjLI7S3WkklWUsZcqorX3qoXTx7JD0fm6ydjAX2eLINQVUmLZ13VREeTQ0RqxljtfK8EVZm+KLlbG+duqvxHqyB+CL1ldvHDTgplKWRS0vHYo9l9CCNWMsdv5RJImIRk3rb8Vipxr1v6j+ihrbt/N0VY8bRjvepgzox5EyrORJJJJJJJOrGiyO2tySRCZVlvjHT2XxTkvirCyZK41fv8Sv+ljZXNW5y0kkeRIfZxox5qXGju1dXb8ivzibRx3hMqrVMXYTFcVjkcjkciSSfCCyO2iNUIWw90vxr/nY5Ezgk73Rlx0yD698bwZbMRZwZsthdfJkKdLGivTxlHah3qTS27+Sj5Kr5JSiYHaTHnaK50LMmew9hW8iYn5WO0tmhogSEtMtoVn+PQpGO2y7PfWJ/vOwu4jHl5GKsnHa62vFW82/7MFO2kVyK6zV5UyLSjh1sRySGhLdo5NCztH7BhtySF5WO18NDRAkJFlDvabP56yjG1JfrY279TEy/RohdSH16Oqfxb47Fbn6l8i/86x/590dTr3xGRbZlD+BGO2+LYezsyd2xsenWX4oRHgyzOzuoIIEhIv8AFVNlvbF/UaLY5HgTFgSK1gfxZF6SekVGKkCUFzsqLNFWVZjtJfcsx2ORyGyTq2/FMTJE9JGzJY7F9hIggRlf44/mm9uu5otWKBKSxA0QQRpY7O7tsyrMbhvdWLEkkknWybVuK5zEySR2M2QzXnRIjXO4rVRXrU536yaoiRstaBWeW6SRcqjt0tRUyKyT0Zdwsjbtk/sITK2mrUq6a8cd+Lx5hZT2CyHsHlRkzpGTJI9xIgggg7C/Fs6CnLg2UklrQZMsmCvEyPKh5ZVstjLkdqUfB1sSSZv63XFWctaVML2fxc4wQQRoruos7R+wfsj7DY8zHds+fGCDs/1kV3R9Dse5FnBmy8TDj5FcuNHtoNOwmzjJmxtGK8iemb+vZyKBCI2q4c8lZEHE4kEEEEaLxWsEHYU1/wCLc/zcnDNJY7Uox887xf5yY+gy/Sy1f6OWf1s1U+1mxnXyex1JO/k4YolPRblGLZ43KaHUdTicTiQQR9CCCCDP/RlXvydLY8iyVZkrN+z13jMH+nahTt43RpS8lFe/+nhVXkv2rYMaoJEn+lmm1Xoiu2jXJY7bpSOpxOI6nE4nE46L6Wfd3rGllK/y88odfzzYvZThxFixZU/86w+nCdKnV6yxVpXf4M+VY65Lu9tFpVlHDiHgy8hI4nEdR1OI6nEQhISEhIgSIIG4Epv2KxX5KsrZ4r4cqy1alo7OBt3lP2QJKz63Xgs5EoLODvdn2uCI0kkTG9uUrLTiYM+yh6NDQ0NECQkJCQkJEEEEGRbJHYtLmBraeS/zbPjS3LRlh0qyqgRBZwd3M+LIExvwrYrbbDlXG+P02w5OJ8jQ0NDRAkJCQkJEEEEEF1tf8TK98iiuJyo3/wA5vjW5S6sho4kCRe6qr5Hc7FfwYlKew9EfGiZiY6bUnHbBaU0NDQ0QJCQkJCRBBBBBkaqZE7J/nbJV2eJRY/y6xTLil0yXo6ditzkmSci2VIs3Zwdn+r+cbh2e730S2tpX5o4K2My/DC+Np0Y9EJCEtI8bUh9vI2lSFhok82J0txP83+lkOpasjdqntuLJZi3ILGfdX2bIk+D/ALhx801KktXgJlXtltOLA+T3oLcY9EIQvODN11kL9J1G7Ud7Ow1C6Wb13aIHUtUdRI4HwWMvxk/tMCGI6Kk7OPhka3zW5FWY9q5NqdVjUquzY9EIQvoNpGbsotbflJZy2dTuqJkksh1I1sdrJCsSKqZwIOlfhbu0V6D+K1Me67FIrgaRW+19tHohCF5TBfsKplztlskjciZXdvTD3L4jF3aZCTkNkjukZ+yql7uzZZCIGTBj7LVRqSqKWgpnTVsFjH2Ibc1W60QhaLWUi/YrUy9tstlbJ0qyyKuNHrTPegu9dH7zLdyzLZrWG51aPg5wOxI2Ji2JFucSnOhe1cy6+Vp1crRCEIRn7HqLdzIy2e9h2b+i/qNEHHRoXwnGtLGO0ldzs14Xw2laIQhGTMqma/JyT9y/BOBEmK8Wwnb3uqw50QhGXLwVryX+tH1YI0jROBWE0zDl4vLblmqLYkQi1uKyZOTbHo/sZ0gggggjSBogjT5EtlaBzZ07XErlrdSIR2sg2N/ZQR9CCNYIII0Rj2daVyVydbifssRMLJflaf4KCCNEdO8qx6VKM9+NGySf4VHWvxu9Edu238G/BFXDraaiOzabfwb8Uda00EZXNvJfwSOnbT/mT+3ihC/gUdP5P//aAAgBAgIGPwC1M4Y1m92f/9oACAEDAgY/ALUxhnQMuIUfXqrEo3HYQVdBsIo2ezARE5DdMhJkEiCjIILPypNqsV8l3opVaf/aAAgBAQEGPwDkawqpKcK+EaCM1VOvaPmmJUypeCME5w18F7B4EopsbeBHtDzRT9IMKRp4CykFMQfAyYRqqxcDnHRKlJV4rFU5x8gmUhB1PgSg1wT2pjypxNAkmS+PVdOLL5J8k3KHHKDZlPxHEGKeE6j6J+ZnQLpx2K+J/wCfqpIIshy7pszVB8+MU6nROJhSmEz+SbmHP42r6IBPcvwuTF7V7S+GZU7gmtIMHUkxW01yTiRXuHmE9pcJjXjntjcr4qt5huMzrBiE9hcJjWLBPcWU5pwGOq23z0KfRaFTquoU6iPVMeMe2JkAgEIbc9F9g6q3cL4Y4Oq9of6fNZf+l7gbfp84EdE6nDcE44D8E4ngFb2g5tD6pmHkpO3dPbdt7zCnOPtm/kvdd6SHkjO31XtuXvLwkukSOAOCcRgEO3CcYT3MeyBQOMcE4yUFb2454jci3RAGocYRYKZ4P+luVR0T4DBovzDJv6lF9cDBOntYjQoZJrQtuqbAeydPFk3MbrZEIvIiPVbim3BSuCcEEKo+cHwHstowOnCccxtNLw3nGVULbrmB+Sm9NUCLrgDrNMLnCE6p7b82kmv9w6pxQxOpknwMU3LGIuFQXQvFLg8AERbIXTHdN+0OJ+4VOkvQ/NWkkWkt7dCfiqDpiQCA89EGck7pCoaj90zNuoNAmGURYMplNgcJxVd+VFsXR/Ue9v8AMAmFRRFxutNdRq2h+qP/ADvHS25OGJ7r33WWDWpW2yZzu+2gW4/kR6ImBuNAjcanExTapjXlHOa84MULraiaF4oU8N/6/wAsxqvdayYKQcrdeG/r9/tg22/jb6nG6dC8UKa6mvJuiOqdOF1RGWGYHyUg2Ai3zON00Nt9CnE7bqfYr+p9OTJ1QC2wNwDjPVbrZjG5XRHG0WTVtNRotl1D8OmOXITTmQT5BSTLzRPVbrfbdr91MeY/kKRnphcwIxvhF2dlzeS6cg5mtohPNPAjBIwnwiMR6kLyXTkZFMVPKDGh4ssB0M06BGkHQGpTwbjzTWrca5JkBDZ+zyKccBs8FYMc1vFRBoNogRkmTJ+LNMFMqSeDxaoU5FSwsE+PbdO0+mBltuT/AKz5fZbb5FeXDniYqWKRU1SFeNNe0prwx1R/XdXLgtUqUlM8xLGxTZhOFbcOAwqnPgTwtClibPwWaGJyn8GFwqmvCe04No8HdOt1kiqZN/tB0T4Rth/s/pA+Gt4S6B8QPhRET4UYf//Z
                    </textarea>
                </p>`,
                },
                "sign": {
                    "title": "Sign",
                    "introduction": `<p>
                                        Provide your password and your private key below before signing a public key, textual content, content fingerprint, etc.
                                        <br>Only your private key can sign.
                                    </p>`,
                    "passwordPrivKey": `Password protecting your private key`,
                    "yourPrivKey": "Your private key",
                    "yourPrivKeyHelp": "Paste your private key above or load it from a file.",
                    "yourPrivKeyFromFile": "Your private key from a file.",
                    "contentToSign": "Textual content to sign",
                    "contentToSignHelp": "Paste your textual content to sign above (text, public key, fingerprint, etc.)",
                },
                "verifySignature": {
                    "title": "Verify a signature",
                    "introduction": `<p>
                                    You must indicate below the public key of the signer, the original textual content which was the subject of the signature and the signature to be verified.
                                    <br>Reminder: original textual content can be plain or encrypted textual content, content fingerprint, public key, public key fingerprint, etc.
                                    </p>
                                    <p class="alert alert-info">
                                        <i class="bi bi-info-circle"></i>
                                        It is always the signatory's public key that is used to verify his signature made with his private key. In fact, only the public key can decipher and therefore verify the signature made with its "twin" sister, the private key.
                                    </p>`,
                    "pubKeyField": `Public key of signer`,
                    "pubKeyFieldHelp": `Paste the signer's public key above or load it from a file.`,
                    "pubKeyFile": `Signer's public key from a file.`,
                    "originalContent": "Original text content",
                    "originalContentHelp": "Paste your original text content that was signed above (text, public key, fingerprint, etc.)",
                    "signatureToCheck": `Signature to check`,
                    "signatureToCheckHelp": `Paste the signature to check above.`,
                    "verifyBtn": `Verify`
                },
            },

            "symCipher": {
                "header": "Secure encryption, however, requiring key exchange or operation in combination with asymmetric encryption.",
                "menuExplanations": "Explanations",
                "menuCreateKey": "Create a key",
                "menuEntropy": "Entropy of salt",
                "menuCrypt": "Encryption",
                "menuDecrypt": "Decryption",
                "menuRecreateKey": "Key reconstruction",

                "explanations": {
                    "introduction": `<p>
                                        Going back several millennia, in different forms, so-called "symmetric" encryption makes it possible to make a string of characters or a complete file incomprehensible thanks to a security key or encryption key (kind of super password to sum up trivially).
                                        <br>A clear message therefore becomes an encrypted message, thus ensuring secrecy during transmission.
                                        <br>In symmetric encryption, the same key is used to encrypt and decrypt the message, hence the name "symmetric". This requires exchanging it between correspondents beforehand. This is a major limitation of symmetric encryption because this prior physical exchange of the key poses logistical and security problems.
                                    </p>
                                    <p>
                                        There is another newer type of encryption: symmetric encryption, the subject of a previous section, which is often used in addition to symmetric encryption.
                                    </p>
                                    <p>
                                        So start by creating a key using a CSPRNG at the <i class="bi bi-filetype-key"></i>Create Key tab.
                                        <br>Keep the password and the salt in a Keepass (vault type open source software) or even in two different keepass, one for the passwords, the other for the salts. The ideal is not to keep writing the entire password, by memorizing part of it.
                                        <br>Safeguarding Password and Salt is essential. Without these two pieces of information, you will not be able to recreate the symmetric key.
                                    </p>
                                    <p>
                                        Unless the key was created by a manual method, storing the key directly, instead of the password and salt, is of course possible but not recommended. Its potential theft will be facilitated and, for technical reasons linked to the PBKDF2 function, it is more difficult to recreate the key with a single element such as the password or the salt.
                                    </p>
                                   <p>
                                        The symmetric cipher used is AES in CBC (Cipher Block Chaining) mode with a 256-bit key and a unique 128-bit initialization vector for each cipher. You can generate keys through the PBKDF2 key derivation function with a salt based partly on mouse movements. In any case, you have the option of using your own keys generated with other software and equipment or by dice rolls.
                                    </p>
                                    <p>
                                        Example of a message to encrypt: “Every living organism is under attack. Man will always have his defense strategy to hide. »
                                        <br>After encryption with the key 1E5A4797BCECAEB0926FEDDE2A10F28E6D6082BE1411E90D66AA79946908D1F7 vous obtiendrez la chaîne de caractères suivante&nbsp;:
                                        <br>2uZ4je6Nnfd0t6aCwhhk7w==:fj5+3D0mITDRWKgD2xVUSQRmsXFlXZ7VryYVbCQRx6fqzQtKHrt+Xtx18Su6lSdB0mAPB81C7VPWhTpsn6gYmVM/Lx9AANz8xQaJ1OpMzmYv+X0BlT9/atjTDhkqzdWujYoQgsmTXAiedbz5pyU2fQ==
                                    </p>
                                   <p>
                                        If you encrypt the same message again with the same key, you will get a different "ciphertext", which is normal and intended:
                                        <br>qB2+XhwK6AKT8lMd1JtGzA==:vG/xNCUFfhd3mXNQ7OCUuq1hWEbb9UsQkT/oT5rF7J0hpAJmmASXW4Atu680c1yQ9XWFkFgYPIbKMFKsox3a1SnM0B34XLym5A6N9I3ZCWc/Ja8ApjVdQoBH3Yow53YZD7ciVZPntwdfXsTfwr7hew==
                                    </p>
                                    <p>
                                       Analysis of this "encrypted": the first characters up to the punctuation mark "colon" [: ] constitute the Initialization Vector (IV - Initialization Vector) simply encoded in base64, i.e. this string qB2+XhwK6AKT8lMd1JtGzA= =
                                        <br>After the "colon" as a separator, you find the actual message encrypted and also encoded in base64, i.e. this string vG/xNCUFfhd3mXNQ7OCUuq1hWEbb9UsQkT/oT5rF7J0hpAJmmASXW4Atu680c1yQ9XWFkFgYPIbKMFKsox3a1SnM0B34XLym5A6VQBowJapCW9I3ZZ 53YZD7ciVZPntwdfXsTfwr7hew==
                                        <br>The IV is needed to decode this encrypted message located after the "colon". For this reason, it is "embedded" by being placed before the real encrypted message. The IV changes randomly with each encryption, also causing the encrypted message to change with each encryption. This technique limits chosen text attacks.
                                    </p>

                                    <p class="h4">Encryption, transfer of key and encrypted text content</p>
                                    <p>
                                        Encrypt your text content with your symmetric key. Re-encrypt this already encrypted content with your correspondent's public key (sym. encryption coupled with asym. encryption). Remember to keep your content symmetrically encrypted in a safe place or better delete it from your equipment if you have it in mind. Optionally keep a SHA-512 hash of the plaintext message.
                                    </p>
                                    <p>
                                        Indicate to your correspondent the following information by at least three different encrypted messengers such as Signal, Briar, Wire, Session, Olvid or Telegram<sup>*</sup>&nbsp;:
                                        <ul>
                                            <li>The message code for retrieval from the DLB. <span class="text-muted">After depositing the message in the digital DLB (Dead Letter Box) of the XenaTrust online service (or to set it up yourself) or even in a physical DLB. </span></li>
                                            <li>Half the password. <span class="text-muted">The other half may be placed on top of the symmetrically encrypted text content before asymmetric re-encryption.</span></li>
                                            <li>Salt cut in half. <span class="text-muted">Use different messaging for each half of the salt.</span></li>
                                        </ul>
                                        <i><sup>*</sup> Avoid Telegram which is not end-to-end encrypted or use their ephemeral messages. In any case, remember anyway that none of these apps are really secure otherwise we wouldn't have created the XENATX...</i>
                                    </p>
                                    <p>
                                        Your correspondent will come back here to reconstitute the symmetric key from the salt and the password. Then it will retrieve the textual content to be decrypted on the DLB thanks to the message code. He will decrypt it first asymmetrically with his private key then a second time symmetrically with the reconstituted symmetric key.
                                    </p>
                                    <p class="h4">Physical directory of symmetric keys</p>
                                    <p>
                                     The exchange of the symmetric key, by transmitting just the salt and the password to then reconstitute the key, can be done in a relatively secure way by using three couriers, as we have seen above. However, this still involves a lot of risk of interception, in particular due to zero-day flaws in your connected equipment, even before transmission via messaging.
                                    </p>
                                    <p>
                                        Another, safer solution is to create a key directory consisting of many keys created in advance, ideally by dice rolls, and to exchange this directory during a physical meeting, or through a very reliable intermediary. The keys are placed on microSD, USB key or even printed on sheets with QR Codes for easy digital recovery. You can also handwrite them on mini-sheets of paper.
                                    </p>
                                    <p>
                                        With a key directory, only the symmetric key reference #A2, A3, A4, etc. is to communicate to your correspondent who has the double of this directory. An attacker therefore has no idea of ​​the key used because only its reference transits on the network, and even then not always, because most often you will place this reference above the symmetrically encrypted textual content. Everything is then re-encrypted asymmetrically.
                                        <br>Alternative solution: use directory keys in order. A key used only once (it is at least the safest) or for a limited period. So there is nothing in particular to communicate to your recipient.
                                    </p>
                                    <p>
                                        To further secure this directory and for keys created via the XENATX CSPRNG type key generator, you could only indicate the passwords instead of the keys. You could then communicate, always in a secure way, so ideally by hand, a common salt (or several) stored on another medium, or even on paper with its possible translation into a QR Code for easy retrieval.
                                        <br>To find the reference key A3, for example, it will suffice to recreate it via the "Reconstruction of key" tab using the password n°A3 and the common salt (or if the salt is not common , with salt noted as A3 as well).
                                    </p>
                                    <p>
                                        For security reasons, the creation of these key directories must imperatively be done on equipment called OFF, therefore not connected to the Internet and never re-connected.
                                    </p>

                                    <p class="h4">Deposit and Message Code</p>

                                  <p>
                                        In the case of using the XenaTrust online service, you simply drop your encrypted message into the digital DLB. The message can also be deposited in a physical DLB. Then you apply the discreet and original means, agreed in advance in the directory for this precise key, in order to discreetly signal to your correspondent that a message is to be picked up.
                                    </p>
                                    <p>
                                        A message code, agreed in advance such as the common word "train", can also be used instead of for example X4V2Z to be slipped more discreetly into the conversation to indicate that a message with this code, or another indicated in the directory, must be noted. The most discreet way is the one you invent...
                                    </p>`
                },

                "createKey": {
                    "title": "Create a symmetric key",
                    "introduction": `<p>
                                        Several solutions, detailed below, are available to you to create a 256-bit AES key.
                                    </p>
                                    <ul>
                                        <li>
                                            To create a key electronically using a CSPRNG (Crypto Secure Pseudo Random Number Generator), see "Creating a key with a CSPRNG" below. CAUTION, the safest method is creation by rolling the dice, with the advantage that its creation leaves no trace in the memory of your equipment... which can persist even after transfer to microSD and deletion. So as long as you don't use it on your equipment to encrypt or decrypt, this key has no computer existence, so its theft becomes difficult for an attacker, except to find your cache of "paper" keys at home or elsewhere.
                                        </li>
                                        <li>
                                            To create a truly random key with (precision) dice, see the full explanation "Creating a key with dice" below.
                                        </li>
                                        <li>
                                            To automatically convert your key from binary to hex format, use the "Binary to Hex Converter" below.
                                        </li>
                                        <li>
                                            To manually convert your key from binary to hexadecimal format, use the "Binary to Hexadecimal Conversion Chart" below.
                                        </li>

                                    </ul>`,
                    "csprng": {
                        "title": `Creating a symmetric key with a CSPRNG`,
                        "paragraph01": `Please create a salt beforehand in the <strong><i class="bi bi-infinity"></i> Entropy of salt</strong> tab. You can also provide your own salt or even use your own key. In any case, the salt will be integrated with your password to the PBKDF2 key derivation function with 100,000 iterations. Be careful, on a weak computer such as a Raspberry Pi 2 for example, the generation of the key can take several minutes...
                        <br>Of course, for a key generated by throwing dice for example, or another TRNG (True Random Number Generator), there is no need to create a salt.`,
                        "passwordField": "Password - passphrase to create your key",
                        "passwordFieldHelp": "charact. - At least 40 characters including specials. Beware this password-passphrase will not allow to find the symmetric key if you have not memorized the final salt also.",
                        "saltField": `final salt`,
                        "saltFieldHelp": `Attention, take note of this salt that you generated in the "Salt Entropy" tab or by yourself. It is as essential as your password-passphrase to reconstitute your key in the event of loss.`,
                        "symKeyField": "Symmetric Key",
                        "symKeyFieldHelp": "Reminder: a 256-bit AES key is 64 hex characters.",
                        "createKeyBtn": "Create Key"
                    },

                    "keyWithDices" : {
                        "title" : `Explanations of creating a key with dice`,
                        "text" : `
                            <p>
                                You can create a key by rolling the dice successively, but be careful, use dice that are balanced and therefore precision (casino or backgammon dice).
                            </p>
                            <p>
                                You will create a binary key of 256 bits so 256 zeros or ones. After shaking it well in the goblet or your hand, roll your die and note the result. Any throw that is less than or equal to 3 will be scored 0 and any throw that is greater than or equal to 4 will be scored 1.
                            </p>
                            <p>
                                To speed up the creation of a key and halve the number of throws needed (128 instead of 256), you can use two dice of different colors. Throw the two dice at the same time and raise the numbers always IMPERATIVELY in the same order of colors. So if the two dice are blue and red, then choose a reading order and respect it scrupulously. For example reading blue then red.
                            </p>
                            <p>
                                Example:
                                <br>1<sup>st</sup> roll of two dice: 5 for blue and 2 for red so we will note: 10
                                <br>2<sup>e</sup> throwing two dice: 6 for blue and 4 for red so we will note: 11
                                <br>The first 4 bits of the key will therefore be 1011 (these 4 bits will later be translated into hexadecimal by the letter B)
                                <br>Keep rolling until you get 256 bits.
                                <br>Then convert these 256 bits to 4-bit by 4-bit hexadecimal using the "Binary to hexadecimal conversion table" or use the automatic converter. The purpose of this conversion is to be able to use your key in the Encryption or Decryption tabs which only accept keys in hexadecimal format.
                            </p>
                            <p>
                                Nothing very complicated but multiply the tests to understand.
                            </p>
                            `
                    },

                    "autoConvertBinToHex" : {
                        "title" : "Automatic binary to hexadecimal converter",
                        "binKeyField" : "Key in binary format (01001..)",
                        "keyConvertedToHex" : "Key converted to hex",
                        "keyConvertedToHexHelp" : "Your hex key will consist of 64 characters",
                        "convertToHexBtn" : "Convert to hex",
                    },

                    "manualConversionBinToHex" : {
                        "title" : "Binary to Hexadecimal Conversion Table",
                        "introduction" : `For a better understanding, see the above topic "Creating a key with dice explained" before using this table.`,
                        "binary" : "Binary",
                        "hexadecimal" : "Hexadecimal",
                    }
                },

                "entropy" : {
                    "title" : "Entropy of salt",
                    "text" : `
                        <p>
                            The purpose of this feature is to create a more random final salt by mixing two salts: an intermediate salt generated by your mouse movements and another salt, not displayed, generated by a mathematical function. This feature is useful only when creating a symmetric key with a CSPRNG. It is useless if you create your symmetric key with dice rolls.
                        </p>
                        <p class="alert alert-info">
                            <i class="bi bi-info-circle"></i>
                            Move your mouse as randomly as possible in the frame until the progress bar, placed under the frame, reaches 100%. The intermediate salt will be created. Then click on "Create the final salt" to generate the final salt to be copied in the "Create a key" tab and the "Create a symmetric key with a CSPRNG" section.
                        </p>`,
                    "intermediateSaltField" : "Intermediate salt",
                    "finalSaltField" : "Final salt",
                    "finalSaltFieldHelp" : `Copy this final salt into the "Final salt" field on the "Create key" tab.`,
                    "createFinalSaltBtn" : "Create the final salt"
                },

                "encryption" : {
                    "title" : "Sym cipher.",
                    "paragraph01" : `Please read the explanations in the "Explanations" first tab if you are not familiar with symmetric encryption.`,
                    "symKeyField" : "Your symmetric key",
                    "symKeyFieldHelp" : "64 hexadecimal characters.",
                    "contentToEncrypt" : "Text content to encrypt",
                    "contentEncrypted" : "Text content after encryption",
                },

                "decryption" : {
                    "title" : "Decryption sym.",
                    "symKeyField" : "Your symmetric key",
                    "symKeyFieldHelp" : "64 hexadecimal characters.",
                    "contentToDecrypt" : "Text content to decrypt",
                    "contentDecrypted" : "Text content after decryption",
                },

                "findBackSymKey" : {
                    "title" : "Key reconstruction",
                    "introduction" : `
                        <p>
                            To reconstitute a symmetrical key created with the CSPRNG, see the "Create a key" tab. You must provide the password or passphrase and the salt that you have stored in safety and perhaps partly memorized. Without these two elements no reconstruction is possible.
                        </p>`,
                    "passwordField" : "Password - passphrase",
                    "saltField" : "Salt",
                    "backKeyField" : "Restored key",
                    "backKeyBtn" : "Restore key"
                },

                "symFile" : {
                    "title" : "File - Encryption–Decryption",
                    "symKeyField" : "Your symmetric key",
                    "symKeyFieldHelp" : "64 hexadecimal characters.",

                    "encryption" : "Encryption",
                    "encryption-introduction" : `Select a file and it will be automatically encrypted in AES-256 - CBC Mode - Random IV and base64 conversion. A window will open and you will be asked where to save the encrypted file. The .enc suffix will be added to the original file name.`,
                    "fileToEncrypt" : "File to encrypt",

                    "decryption" : "Decryption",
                    "decryption-introduction" : `Select an encrypted file and it will be decrypted automatically. A window will open and you will be asked where to save the decrypted file.
                    <br>The file must have been encrypted by XENATX in AES-256, and not by another software, due to the specific characteristics of the implementation in particular the method of inclusion of the IV and the conversion to base64.`,
                    "fileToDecrypt" : "File to decrypt",
                    "fileToDecryptFiedHelp" : "The file to be decrypted must have the suffix .enc"
                }
            },

            "otp" : {
                "introduction": `Symmetric encryption called "perfect", because it is indecipherable, often used in addition to sensitive passages of a message.`,
                "menuExplanations": "Explanations",
                "menuConverter": "Converter",
                "menuEncryption": "Digital encryption",
                "menuDecryption": "Digital decryption",
                "menuHic": "HIC - Hidden In the Crowd",

                "explanations" : {
                    "general" : {
                        "tabTitle" : "General",
                        "title" : "General information on the OTP",
                        "text" : `
                            <p>
                                Throughout this section, titled OTP - One-Time-Use Mask, the following terms are used interchangeably and mean the same thing:
                                <ul>
                                    <li>Disposable mask</li>
                                    <li>Mask</li>
                                    <li>One-time use key</li>
                                    <li>Vernam cipher key</li>
                                    <li>OTP key</li>
                                    <li>Key</li>
                                    <li>OTP</li>
                                    <li>One-Time-Pad</li>
                                    <li>Pad (OTP key stored on paper)</li>
                                </ul>
                            </p>
                            <p>
                                The OTP (One Time Pad), or OTP key, is similar to a symmetric key but where each character of the key would only be used once. The length of the key must therefore be at least equal to the length of the message.
                                <br>The theoretical argument is as follows: all keys being equally probable, then all plaintexts of this length are equally possible and with the same probability. Knowing only the ciphertext, there is no way to distinguish, among these possible plaintexts, the original plaintext. Statistical analysis, such as frequency analysis, is futile.
                            </p>
                            <p>
                                OTP is the only encryption that is theoretically 100% secure, regardless of the computing power in front, even quantum. On the other hand, unlike asymmetric encryption, OTPs must be exchanged in advance.
                                <br>Each OTP can only be used once. The security of these physical exchanges of OTPs and their storage awaiting use is paramount. This requires rigorous organization. Real security has its requirements. It's this or nothing, and nothing is not an option...
                            </p>
                            <p>
                                Another advantage of OTP encryption is that it is based on the "exclusive OR" (XOR) function. Due to its great simplicity, this function is difficult to weaken by malware or other hardware manipulation, without this being visible. So even on infested equipment, of course not connected to avoid any data leakage like the OTP itself, the encryption is done correctly.
                            </p>
                            <p>
                                More info: <a href="https://en.wikipedia.org/wiki/Disposable_mask" target="_blank">https://en.wikipedia.org/wiki/Disposable_mask</a>
                            </p>
                            <p>
                                The unbreakable characteristic of OTP encryption is based on the real entropy, therefore the truly random character, of the OTP key.
                                <br>The more you find a source capable of generating an OTP with a very high entropy, therefore very random, the better your OTP will be. You can therefore generate it elsewhere than on the XENATX and come back to use it here.
                                <br>Unfortunately, no consumer device can easily create OTPs. So you will depend on professional TRNG (True Random Number Generator) maker. In addition to the difficulty of having access to this type of material, will you be sure that this material is reliable and not hijacked by intelligence agencies as was until 2020 the Swiss manufacturer Crypto AG - Hagelin&nbsp;?
                            </p>
                            <p>
                                We only use numeric OTP encryption in the XENATX, not alphabetic OTP encryption which is also possible. The reason: it's just easier to implement and learn.
                            </p>
                            <p>
                                As with other encryption techniques, an OTP must of course only be created and used on a XENATOFF, or equivalent, to avoid software and hardware flaws in a connected device. All the encryption can even be done with just paper and pencil... This is another of the great advantages of OTP encryption.
                            </p>
                            <p>
                                For very high security, it is this last solution with paper and pencil that should be favored. Thus, no trace is left in any computer memory.
                                <br>Using the "Clear" key is not enough. The memory of your computer equipment could be inspected without your knowledge after the theft of your equipment, or during a simple visit to your home, without a break-in, therefore without you necessarily realizing it.
                            </p>
                            <p>
                                You will find the explanations of the generation of an OTP with two precision dice and the manual encryption-decryption techniques with OTP in the respective topics below <span style="color: rgb(11, 110, 253);"> <i class="bi bi-bullseye"></i>&nbsp;Entropy - Creating an OTP</span> and <span style="color: rgb(11, 110, 253);"><i class ="bi bi-key-fill"></i>&nbsp;Encryption - Decryption</span>.
                            </p>`
                    },

                    "entropy" : {
                        "title" : "Entropy - Creating an OTP",
                        "text" : `
                            <div class="alert alert-info">
                                <p>
                                    <i class="bi bi-info-circle"></i> Find, or even make, a truly random number generator (TRNG - True Random Number Generator) or even a very good CSPRNG (Cryptographically Secure Pseudo Random Number Generator) to generate the most entropic OTP possible is not easy.
                                </p>
                                 <p>
                                    In any case, never use an OTP generated remotely even if the exchanges on the network are protected by TLS/SSL.
                                    <br>Any websites offering so-called safe numbers because they are based on random quantum phenomena are therefore to be avoided.
                                </p>
                                <hr>
                                <p>
                                    <i class="bi bi-dice-4"></i> If you're not a crypto expert, stick to generating your OTPs through dice rolls. In terms of simplicity and efficiency, it is incomparable, at the cost of course of the slowness. You will find the essential explanations below.
                                </p>
                                <p>
                                    Generating an OTP takes time per dice roll. So you won't be able to have many keys in advance. Too time consuming to generate. You will therefore only be able to exchange relatively short messages. However, you can also use it as a complementary cipher, to the AES-256 cipher for example, for certain short but sensitive parts of a message such as a meeting place, names, times, etc.
                                </p>
                                <p>
                                    You will be able to check and complete from the sites below <i>(we do not earn any commission...)</i>:
                                </p>
                                <ul>
                                    <li>https://www.ciphermachinesandcryptology.com/en/onetimepad.htm</li>
                                    <li>https://dicekeys.com/ ou https://www.schneier.com/blog/archives/2020/08/dicekeys.html</li>
                                </ul>
                            </div>
                            <p class="h4">
                                How to generate a mask (OTP) with two dice?
                            </p>
                            <p>
                                Ideally, bring the following items:
                                    <ul>
                                    <li>Six-sided precision dice such as casino or backgammon dice.</li>
                                    <li>A leather cup to shake the two dice well, without damaging them, before throwing them. </li>
                                    <li>A green mat to receive the dice.</li>
                                    <li>The "dice table", below, to convert the results into a sequence of numbers from 0 to 9.</li>
                                </ul>
                            </p>
                            <p>
                                The technique for creating a key, composed exclusively of numbers from 0 to 9 inclusive, is to assign a value to each of the 36 possible combinations with two dice of distinct colors, taking into account the order and color of the dice (see section "Dice table").
                                For the example, we will say that we use a black die and a white die, even if transparent and colored dice are preferable because their "good manufacturing" is more verifiable.
                                <br>We will arbitrarily start the reading always with the black die.
                                <br>In the dice table we have three series of values between 0 and 9. This way each combination has a probability of 1/30. The remaining 6 combinations, starting with a 6, do not ensure a new complete series from 0 to 9. As a result, all rolls starting with a 6 (black) are simply ignored. This does not affect the probability of other combinations.
                            </p>

                            <p class="h4">Practice</p>
                            <p>
                                <strong>First throw:</strong>
                                <br>We roll the two dice for the first time and we get the following result:
                                <br>3 for black and 5 for white, so we read 35 (we arbitrarily chose to always start reading the number with black, which therefore becomes the tens digit).
                                <br>In the dice table, the number 35 corresponds to 6. So the first digit of the digital OTP key will be 6.
                            </p>
                            <p>
                                <strong>Second throw:</strong>
                                <br>6 for black and 2 for white. We ignore this roll because the tens digit, therefore of the black die, is a 6 and all numbers starting with a 6 must be ignored.
                            </p>
                            <p>
                                <strong>Third throw:</strong>
                                <br>2 for black and 3 for white. So 23 which corresponds in the table to 8. The second digit of the OTP key will therefore be 8.
                                <br>And so on, until you reach the desired key length. Reminder: the first 5 digits randomly drawn in this way will be the key identifier and not yet the key itself.
                            </p>
                            <p>
                                You will find explanations of directory and storage techniques and encryption-decryption in the respective sections below <span style="color: rgb(11, 110, 253);"><i class="bi bi-book"></i>&nbsp;Directory and Storage</span> et <span style="color: rgb(11, 110, 253);"><i class="bi bi-key-fill"></i>&nbsp;Encryption - Decryption</span>.
                            </p>`,
                    },

                    "diceTable" : {
                        "title" : "Dice table",
                        "introduction" : `
                            <p>
                                Use two dice of different colors. The important thing is to easily differentiate between them: read the explanations for creating a key using dice in the "Entropy - Creating an OTP" section above.
                                <br>We indicate Black and White dice in the table below, however the best is two different colors and at the same time transparent to visually check that no "impurity" comes to unbalance these precision dice (casino or backgammon)
                            </p>
                        `,
                        "image" : `<img class="code__table" src="assets/images/otp/dice_en.png" alt="XENATON">`
                    },

                    "randomOtp" : {
                        "title" : 'Training - Pseudo-random OTP',
                        "alert" : `Please note that these training OTPs are ABSOLUTELY NOT valid for use in a real situation. The entropy, therefore the randomness, is too weak!`,

                        "quantityField" : "Length of the OTP",
                        "otpField" : "OTP",
                        "createOtpField" : "Create the OTP",
                    },

                    "directoryAndStorage" : {
                        "tabTitle": "Directory and storage",
                        "title": "OTP Directory and storage",
                        "subtitle": "Stored for example on microSD card or on paper",
                        "paragraph01": `
                            <p>
                                To perform OTP encryption, we need a key, called a one-time key or OTP. This key must be exchanged beforehand, we will create in advance several keys to be exchanged during a single physical exchange for the sake of saving meetings that are often difficult to organize.
                            </p>
                            <p>
                                A directory/notepad (the Pad in One-Time-Pad) can be a single mini-sheet, several stapled mini-sheets, a microfilm, etc. In any case, a set of OTP directories consists of two identical (but reversed) directories, a directory called OUT and another called IN.
                            </p>
                            <p>
                                A standard OTP directory sheet, or OTP key, contains about 250 digits in groups of five digits, which is enough for a message of about 180 characters. The first group of five digits on each sheet serves as a key reference and must be unique for that particular set of sheets. All numbers in each sheet must be truly random (see Entropy - Creating an OTP). Randomness is an essential part of the security of the encryption process.
                                <br>Since any leaf (or digitally stored key) started must be destroyed, it is better to create shorter keys of hundreds of digits or less. The length depends only on your personal choice.
                                <br>If the message is longer than the key, the next key in the directory will be used (without the first 5 digits, serving as identifier, of this next key) and it is not necessary to specify it.
                                <br>The next key identifier to be used can nevertheless be indicated at the end of the first part of the encrypted message to be as clear as possible.
                            </p>
                            <p class="alert alert-danger">
                                <i class="bi bi-exclamation-octagon"></i> To establish two-way communications, you need two different sets of directories: person A has an OUT directory of which person B has the IN copy, and person B has another OUT directory of which person A has the IN copy. .
                                <br>
                                <br>
                                Never use a single directory to communicate in both directions in order to avoid the risk of simultaneous use of the same OTP key in the case, for example, of a need for people A and B to write in direction l each other at the same time. Encrypting both messages with the same key would potentially allow the decryption of both intercepted messages, even without knowing the key!
                            </p>
                            <p>
                                The use of several IN copies of a directory is possible in order to allow more than one person to receive a message, but this practice is discouraged. Multiple copies pose additional security risks and should only be used in a very strictly controlled environment. This practice also multiplies the risk of undestroyed copies of a directory after decryption, with possible interception afterwards.
                                <br>In any case, never use multiple OUT copies of a directory, as this will inevitably lead to the same OTP key being used at the same time, thus potential decryption by an attacker without knowing the key.
                            </p>
                            <p>
                                OTP directories will be exchanged during physical meetings. Each directory will ideally be encrypted and duplicated on a second backup microSD, stored in a different "cache". Be careful not to lose the AES-256 directory protection key and do not forget the places where all these elements are stored... Organization and rigor...
                            </p>
                            <p class="h4">
                                Example of OTP directory paper sheets.
                            </p>
                            <p>
                                Such sheets comprising 120 digits, 5 of which are identifiers, can easily be obtained by dividing a RHODIA n°12 block sheet into 4 and using a felt-tip pen with a 0.5 lead
                                <br>Then staple the sheets together and you have your "in-house" OTP directories ready for trading.
                            </p>
                            <p>
                                You see below the OUT key correspondence with the IN key. The first OUT 01 key (to write and encrypt) is therefore given to correspondent A and the IN 01 key (to receive and decrypt) is given to correspondent B.
                            </p>
                        `,
                        
                        "paragraph02" : `
                            <i class="bi bi-exclamation-octagon"></i> Please note that the microSD card used for digital OTP storage and which has been regularly inserted into your XenatOFF must ABSOLUTELY NEVER be inserted into any other equipment. After using all the OTPs on this microSD, the card will have to be destroyed (burned) or possibly kept safe for new subsequent decryptions of old messages (not recommended)
                            <hr>
                            <i class="bi bi-exclamation-octagon"></i> This microSD could be "recharged" in OTP from "outside" via your XenatOFF exclusively by using the XenaTx secure channels.
                            <br>Nevertheless, the ideal is that no new OTP passes through your XenatOFF before actual use. Indeed, traces of the OTPs having passed through your XenatOFF when reloading your microSD could persist on the hard disk or even in the memory. Recovery by a qualified and equipped attacker having physical access to your hardware would then be possible by inspecting and copying the memories of your XenatOFF. The latter being more difficult to hide than a microSD.
                            <hr>
                            <i class="bi bi-exclamation-octagon"></i> Store your OTP directories and your XenatOFF in different and safe places with imperatively tips to know if your "caches" would not have been " visited" without authorization...`
                    },

                    "encryptionDecryption" : {
                        "title" : "Encryption - Decryption",
                        "paragraph01" : `
                            <p class="alert alert-danger">
                                <i class="bi bi-exclamation-octagon"></i> Reminder: <strong>an OTP should NEVER be used twice</strong>. To be used only once, really only once, hence its name "single-use mask" or "disposable mask".
                            </p>

                            <p class="h4">Clear Code</p>
                            <p>
                                First, the "plain textual content" or "plain message" must be converted into "plain code" thanks to a code table which allows the conversion (cf. section "Multilingual code tables"). There is a code table for each language. We will use the French code table in the following examples.
                            </p>
                            <p>
                                In a code table, each alphabetic letter (or special character) has a numerical correspondence in the form of digits or numbers. The most used letters in the considered language are assigned to digits to shorten the plaintext code as much as possible. Other letters and special characters are translated to a two-digit number.
                                <br>Tricks and rules of construction avoid misinterpretations at the time of decoding, namely the passage of the plaintext code into a plaintext message, on the recipient side of the message.
                            </p>
                            <p>
                                After encoding, the plain code is grouped into blocks of 5 digits for more readability and to avoid errors. The OTPs themselves are arranged in this way, making it easier to enter, encrypt and decrypt manually.
                            </p>
                            <p>
                                <strong>
                                    Example below of the passage in clear code of the simple message "EXPLANATIONS OF THE OTP" using the French code table <i>(see Multilingual code tables tab below)</i> then encryption by modular arithmetic.
                                </strong>
                            </p>`,

                        "paragraph02" : `
                            <p>
                                In practice, you must always “finish” any “started” block of 5. So by convention we always end the final block of plaintext code by manually adding points (91).
                                <br>The plaintext code should therefore end with 68191 instead of 681 as indicated above. If you have 4 "slots" to complete this gives 9191 (so two points). If you have to fill three slots or just one slot you add just 919 or just 9, in the latter case. By seeing a final 9, we will know that this 9, placed at the end of the message, means the start of 91, therefore a point. We indicate it by the sign (.) or nothing (Ø), if there was already a dot before, at the time of decoding the plain code into a plain message.
                            </p>
                            <p>
                                This final plain code 28781 78371 16380 45997 22997 89380 68191 is now ready to be encrypted with an OTP one-time key.
                            </p>
                            <p>
                                Once the code has been encrypted (not yet produced), it can be "exfiltrated" directly from your XenatOFF via a secure transfer channel (sound or QR Code) or even sent as is by CiBi/PMR by "playing" the corresponding morse code using the TAM (Tools section, TAM tab - Morse Transmitter). Be careful, find out about the legislation in your country concerning encrypted transmission by CiBi/PMR.
                                <br>The encrypted result could also be embedded in plaintext content which itself will be encrypted "classically" in symmetric or asymmetric encryption, or both.
                            </p>
                            
                            <p class="h4">
                                encryption
                            </p>
                            <p>
                                Once the plaintext message is converted to plaintext, encryption, like decryption, is based on modular arithmetic. The process is very easy to assimilate.
                            </p>
                            <p>
                                To encrypt the message, we will write the digits of the plain code converted into groups of five digits and write just below, well aligned vertically, the digits of the OTP. We then operate a modular subtraction digit by digit (top digit minus bottom digit).
                            </p>
                            <p>
                                With the above plaintext "EXPLICATIONS DE L'OTP" converted to plaintext and the OTP 27793 33873 22989 05220 80984 29034 63759 54704 this gives the layout below.
                                <br>Be careful, you have to shift by one block because the first 5 digits of the OTP correspond to its identifier (ID.) and they are therefore not used for encryption.
                            </p>
                            <p>
                                <strong>Plain text code:</strong>
                                <br>ID.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 28781 78371 16380 45997 22997 89380 68191
                            </p>
                            <p>
                                <strong>OTP key:</strong>
                                <br>27793 33873 22989 05220 80984 29034 63759 54704
                            </p>
                            <p>
                                <strong>Numerical result:</strong>
                                <br>27793 95918 56492 11160 65013 03963 26631 14497 (it is very important to remember the identifier at the beginning of the encrypted message)
                            </p>

                            <p>
                                To obtain this numerical result, we subtracted without going negative (for example 5 - 9 = 6 because [1]5 - 9 = 6 but we do not use the [1] of the number on the left, indicated as a kind of restraint!). Never perform a normal subtraction as this will create a biased and completely insecure ciphertext! Always use modular arithmetic and use the encryption and decryption tabs with sequences of fictitious numbers to train and understand the process.
                            </p>
                            <p>
                                If you wish to send "this cipher" by radio, voice or morse, or by telephone, it is recommended to repeat all the groups twice to avoid errors (ex. 27793 27793 95918 95918...).
                            </p>
                            <p>
                                It can be very useful to know how to hide your encrypted message (steganography). We advise you to deepen your research using the keyword "WPS" or Words Per Sentence.
                            </p>
                            <p>
                                Finally, find out if the use of OTPs is legally permitted in your country. Not all countries allow unbreakable encryption. However, it is essential to know this technique accessible to all in case the legitimate should take precedence over the legal...
                            </p>
                            
                            <p class="h4">Decryption</p>
                            <p>
                                To decrypt an OTP-encrypted message, check its first group of 5 digits (which serves as the key ref/id) against the first group of each key in your OTP directory to find the key used. It must be an OTP key noted "IN" in title because if you decrypt it is that you receive the message (see the section "Directory and storage")
                                <br>Reminder: this first group of 5 digits is not part of the message itself, it only serves as an OTP key reference and therefore is not used in decryption.
                            </p>
                            <p>
                                Write the OTP digits below the received ciphertext and add the ciphertext and the OTP, digit by digit vertically. This means a modular addition. For example 9 + 6 = 5 and not 15. Obviously when it exceeds 9 we remove the tens digit and just keep the ones digit.
                                <br>Never use normal addition!
                            </p>
                            <p>
                                <strong>Numerical result:</strong>
                                <br>27793 95918 56492 11160 65013 03963 26631 14497 (it was essential to have recalled, at the start of the encrypted message, the key identifier used in order to know which key to use for decryption)
                            </p>
                            <p>
                                <strong>OTP Key:</strong>
                                <br>27793 33873 22989 05220 80984 29034 63759 54704
                            </p>
                            <p>
                                <strong>Decryption Result:</strong>
                                <br>ID.   28781 78371 16380 45997 22997 89380 68191
                            </p>
                            <p>
                                You just have to go back to the "Converter" tab then "Clear code to plain text converter" to see that you find the plain text message "EXPLANATIONS OF THE OTP."
                            </p>
                            <p class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> Always encrypt each new message with a new key (stored digitally or as a paper mini-leaf). NEVER reuse an OTP key!
                                <br>Always destroy the complete OTP key immediately after completing the encryption, even if it still contains unused groups of digits. Same after decryption.
                                <br>If you're worried that you can't remember the message you sent, re-encrypt it with another OTP (or possibly an AES-256 key) and hide this new key and the saved message in two safe and separate locations.
                                <br>An attacker will thus have two local caches to discover and no longer just one (to find your "first" OTP key) because your first encrypted message will probably have been intercepted during its transmission by internet, voice, morse code, etc. unless you have concealed it properly by steganography.
                            </p>

                            <p class="alert alert-info">
                                <i class="bi bi-info-circle"></i> You will find more explanations on our learning platform. You can also check out this great site:
                                <br>https://www.ciphermachinesandcryptology.com/en/onetimepad.htm
                            </p>
                            `
                    },

                    "codeTables" : {
                        "title" : "Multilingual code tables",
                        "introduction" : `These multilingual code tables are used for manual conversion. There is also the "Converter" tab to do automatic conversion.`
                    },

                    "frenchCodeBook" : {
                        "title" : "French codebook",
                        "text" : `
                            <p>
                                Manual use of a codebook is possible to reduce message size. All you have to do is directly insert the code preceded by a zero in the plain text message to be transformed into plaintext code in the "Converter" tab. Example for the word ACCEPT, insert 0019.
                            </p>
                            <p>
                                The message ANNULER(073) TRANSFERT(875)S, by putting an S in transfers because there are several transfers that we wish to cancel, becomes in plain text to be written manually 0073 0875S which will finally be automatically translated in by 00739 90875 5 which we will end with 9191 to complete the last block. Either in the end 00739 90875 59191
                            </p>
                            <p>
                                You can of course use your own codebook. It is just necessary that the codes do not exceed three characters and only decimal characters.
                                <u>Reminder</u>: the fact that this codebook is public, therefore known, has no effect on the indecipherability of the final message encrypted by OTP.
                            </p>
                        `
                    },

                     "englishCodeBook" : {
                        "title" : "English codebook",
                        "text" : `
                            <p>
                                Manual use of a codebook is possible to reduce message size. All you have to do is directly insert the code preceded by a zero in the plain text message to be transformed into plain code in the "Converter" tab. Example for the word DANGER, insert 0244.
                            </p>
                            <p>
                                The message DANGER (244) AIRPLANE(064)S, by putting an S to AIRPLANES because there are several planes representing a danger, becomes in plain text to be written manually 0244 0064S which will finally be automatically translated into plain code by 02449 90064 83 that we will end with 919 to complete the last block. Either in the end 02449 90064 83919 (note the S is translated into 83 and not into 5 to respect the English code table)
                            </p>
                            <p>
                                You can of course use your own codebook. It is just necessary that the codes do not exceed three characters and only decimal characters.
                                <u>Reminder</u>: the fact that this codebook is public, therefore known, has no effect on the indecipherability of the final message encrypted by OTP.
                            </p>
                        `
                    }
                },

                "converter" : {
                    "title" : "Converter",
                    "paragraph01" : `
                        <p class="alert alert-info">
                            <i class="bi bi-info-circle"></i> This converter uses the <strong>French</strong> code table available in the "Explanations" tab then the section "Multilingual code tables".
                        </p>
                        <p>
                            Convert plain text content to plaintext code for one-time digital mask (OTP) encryption. Reminder: your OTP must be generated by a reliable TRNG (truly random number generator). Since it is very difficult to find, you must have generated it at least by successive throws of precision dice. Only technique really available to the "general public" and explained in the "Explanations" tab then "Entropy - Creating an OTP".
                        </p>
                        <p>
                            Conversely, convert your plaintext code into plaintext content.
                        </p>
                        <p class="h3">
                            Plain text to plaintext code converter
                        </p>

                        <p>
                            If you want to indicate that the following characters are figurative signs or numbers, formulas, etc. use the opening and closing bracket sign [], and the same to indicate the end of this usage.
                        </p>
                        <p>
                            These square brackets [] are an equivalent to the term "FIG." in the code table. These square brackets will therefore be correctly translated by the code 90
                            <br>Example to frame a number, here is the result: []42[]
                            <br>Thus the plaintext content (or plaintext message) in quotes "ANY NUMBER: []42[]." will give the following plaintext code:
                            84 4 99 4 80 79 70 83 2 99 82 84 2 78 71 80 4 82 84 2 99 92 99 90 444 222 90 91
                            <br>Which gives grouped by blocks of 5: 84499 48079 70832 99828 42787 18048 28429 99299 90444 22290 91
                        </p>
                        <p>
                            However, in accordance with the explanations, you must manually end the block with dots (91) and in this case also with a single 9 at the end, which gives:
                            <br>84499 48079 70832 99828 42787 18048 28429 99299 90444 22290 91919
                        </p>
                        <p>
                            The following signs ?(REQ) . ' : + - = can be used directly in your plain text message. On the other hand for parentheses, indicate () for an opening parenthesis and also () for a closing parenthesis. THE ? here replaces the REQ (which means Request) in the French code table (98).
                        </p>
                        <p>
                            Experiment with the code table in front of you to understand. In any case, as soon as you have digits or numbers, for example the number 947, surround them with [] so []947[]
                        </p>
                        <p>
                            The manual use of a codebook is explained in the "English Codebook" section of the "Explanations" tab. A codebook helps shorten messages.
                        </p>
                    `,
                    "paragraph02" : `
                        Convert your plain text content to plaintext code, using UPPERCASE letters only. Then encrypt this plaintext code in the "Digital encryption" tab.
                    `,
                    "clearMessageField" : "Plain text content",
                    "clearCodeField" : "Plaintext code",
                    "convertToClearCodeBtn" : "Convert to plaintext code",

                    "titleConvertFromClearCodeToClearMessage" : "Plaintext code to plain text converter",
                    "deleteBracketsBtn" : "Remove brackets",
                    "convertToClearMessageBtn" : "Convert to plain text",

                },

                "encryption" : {
                    "title" : "Digital encryption",
                    "introduction" : `
                        <p>
                            If you are not familiar with the OTP, please read the explanations in the first tab "Explanations".
                        </p>
                        <p class="alert alert-warning">
                            <i class="bi bi-exclamation-octagon"></i> First convert your plain text content to plaintext code in the "Converter" tab. Do not forget to <strong>remove the identifier</strong>, therefore the first 5 digits of the OTP, in the field below Digital key (mask - OTP) BUT <strong>then replace this identifier in front of the encrypted result</strong> before sending this encrypted message to your correspondent. It is indeed these 5 digits placed in front of the real encrypted content that will allow him to know which OTP key to use to decrypt.
                        </p>
                    `,
                    "clearCode" : "Plaintext code",
                    "digitalKey" : "Digital key (mask - OTP)",
                    "encryptedResult" : "Result: encrypted content",
                    "deleteSpacesHelp" : `The "Remove spaces" button below allows you to shorten the encrypted message in the case, among other things, of transmission in Morse code, via the "TAM" (Tools section)`,
                    "deleteSpaces" : "Remove spaces"
                },

                "decryption" : {
                    "title" : "Digital decryption",
                    "introduction" : `
                        <p>
                            If you are not familiar with the OTP, please read the explanations in the "Explanations" tab.
                        </p>
                        <p class="alert alert-warning">
                            <i class="bi bi-exclamation-octagon"></i> Do not forget to <strong>remove the OTP key identifier</strong>, therefore the first 5 digits of the encrypted content and the OTP, in the two fields below before proceeding with the decryption. It is indeed these 5 digits, placed in front of the encrypted content received, which make it possible to know which OTP key to use to decrypt. Convert the result, your decrypted plaintext code, into plain text content in the "Converter" tab.
                        </p>
                    `,
                    "encryptedContentField" : "Encrypted content",
                    "otpKeyField" : "Digital key (mask - OTP)",
                    "decryptedCCField" : "Plaintext code decrypted",
                    "decryptedCCFieldHelp" : `Go to the "Converter" tab to turn it into plain text content.`
                },

                "hic" : {
                    "title" : "HIC - Hidden In the Crowd",
                    "subtitle" : "OTP message « Hidden In the Crowd »",

                    "explanations" : {
                        "title" : "Explanations",
                        "subtitle" : "",
                        "text" : `
                            <p>
                                If you are not familiar with the OTP, please read the explanations in the "Explanations" tab.
                            </p>
                            <p>
                                HIC, a term invented by Xenaton, designates more of a principle than a system. The underlying logic is known, at least known to a few “specialists” and real security enthusiasts... in this world of illusions...
                                <br>This logic is used by all intelligence services and armies in the world...
                            </p>
                            <p>
                                HIC is a bit like the “king” of secure communication systems, at least for short messages: simplicity, untraceability and indecryptability guaranteed of the exchange…
                            </p>
                            <p>
                                HIC will probably never have an equivalent because it is based on unsurpassable fundamentals such as OTP and “reverse” steganography (“showing” everything to everyone rather than hiding everything).
                            </p>
                            <p>
                                Once the principle is understood and a little learning, anyone can implement an HIC system, without any technological support. This is one of the HIC's greatest strengths.
                                <br>Xenaton has “only” optimized these underlying principles to make them accessible but also suitable for a “modern” public environment.
                            </p>
                            <p>
                                Xenaton has also made learning “easy” for you with explanations and complete, translated documentation.
                            </p>
                            <p>
                                New simulation and acceleration tools have of course also been created. These tools fit naturally with our other innovative systems such as our essential secure transfer channels using QR codes or Morse Acoustic Transmitter.
                                <br>All of which is, for the most part, grouped here in XENATX, a free tool at your service.
                            </p>
                            <p>
                                A complementary part, online, within the XenaTeam service, allows our customers to optimize-accelerate-automate the distribution of their HIC messages on their multiple channels.
                                <br>However everything can be done “manually” without this service, such as copying HIC messages to multiple channels such as messaging groups, newsletters, websites, etc.
                            </p>
                            <p>
                                In the next topic below, "ID Matching", you will find a feature for use on a XenatOFF. It allows you to greatly speed up the verification that an HIC is addressed to you or not.
                            </p>
                        `,
                    },

                    "keysIdentifiers" : {
                        "title" : "ID matching",
                        "introduction" : `
                            <p>
                                This feature allows you to know if the identifiers (first 5 digits) of HIC (Hidden In the Crowd) messages, encrypted by OTP, consulted online can correspond to OTP key identifiers that are specific to you.
                                <br>Reminder: the first five digits of a message encrypted by OTP (HIC or not) correspond to the identifier, therefore also to the first 5 digits, of the OTP key to use for decryption.
                            </p>
                            <p>
                                Indicate your key identifiers in blocks of 5, and only your identifiers, in the "Identifiers of my OTP keys" field, then place yourself in the "OTP-HIC message identifiers" field and flash the first QR Code of the identifiers of the messages.
                                <br>These identifiers are systematically grouped in a first QR Code entitled IDs and are also indicated above the QR Code in case you have nothing to flash or you wish to re-enter the identifiers by hand on paper or directly in this second field "Message identifiers".
                            </p>
                            <p class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> Use this feature only on a XenatOFF to protect yourself from the fact that an attacker, "present" in your connected equipment thanks to malware or a hardware flaw, can see that this or that HIC message was intended for you. 
                            </p>
                        `,
                        "keysIdentifiersField" : "My OTP keys identifiers",
                        "messagesIdentifiersField" : "OTP-HIC messages IDs",
                        "resultIdentifiersField" : "Match result",
                        "checkKeysIdentifiersBtn" : "Check"
                    },
                    "training" : {
                        "title" : "Practice!",
                        "text1" : `
                            <p>
                                Place yourself in the second “Message identifiers” field and scan the first QR Code below which carries the identifiers of the 20 HICs of the day published on partner websites, partner messaging channels, etc.
                            </p>
                            <p>
                                Next, provide several identifiers that you invent and which, in real life, would correspond to your OTP key identifiers in your possession.
                            </p>
                            <p>
                                Example for 40 keys that you would have, therefore 40 identifiers in the form of blocks of 5 digits: 00414 00868 01656 04876 07735 08436 10860 14258 15833 16843 22383 26126 30544 30750 33465 33 504 34468 41004 45818 50844 52152 53187 53440 57368 60508 61553 61884 63547 64088 67703 76481 78007 80736 81505 82531 82578 84422 85518 88687 88715 (copy them all, separated by a space, into the first "Key IDs" field below)
                            </p>
                            <p>
                                If one or more message IDs of the day match one or more IDs of your keys, the system will tell you.
                            </p>
                        `,
                        "text2" : ` 
                            <p>
                               The 4 QR Codes below carry the OTP messages indicated just above. They are separated by a line break (65 characters including the first block of 5 digits serving as identifier).
                            </p>
                            <p>
                                These 20 OTP-HIC messages in total, accompanied by their 4 QR codes facilitating their retrieval, serve here as an example of a daily sending of 20 different messages         duplicated on multiple partner channels.
                                <br>Thanks to the first QR code called OTP IDs which brings together all the identifiers of these 20 messages, it is easier to find the message intended for you. You will need to flash, from a XenatOFF, the correct QR Code containing this message among the five others for its automatic decryption. You can also copy it by hand for manual deciphering with a simple pencil and paper.
                            </p>
                        `
                    }
                }
            },

            "tools" : {
                "title" : "Tools",
                "header" : `Transmit contactless content by QR Code or Morse - Convert - Calculate a fingerprint - Steganography with an image.`,
                "menuQrCode": "QR Code",
                "menuTam": "TAM - Morse",
                "menuConverter": "Converters",
                "menuHash": "Fingerprint - Hash",
                "menuSteganography": "Steganography",

                "qrcode" : {
                    "title" : "QR Code",
                    "subtitle" : "Secure optical transfer",

                    "textualAndVisualHashSubtitle": `Global SHA-1 fingerprint (textual and visual): `,
                    "visualGlobalHashIndication": `Global SHA-1 visual fingerprint`,
                    "finalGlobalHashVerificationText": `Textual and visual SHA-1 fingerprint of final verification of all the contents of each QR Code above combined: `,
                    "hashParenthesis" : "(SHA-1 fingerprint)",
                    "introduction" : `
                        <p>
                            The XENATX uses the QR Code as a secure optical transfer channel. No malware can hide in the QR Code. No significant data leakage is therefore possible. The "transport" of the data contained in the QR Code is also unidirectional. You therefore do not risk contamination in the other direction.
                        </p>
                        <p>
                            You could also use the <i class="bi bi-soundwave"></i> TAM - Emett. morse code in the next tab, for the same purpose of secure content sound transfer. Both solutions have their advantages and disadvantages. The TAM lends itself better to transfer by radio (CiBi/PMR) or in the absence of QR Code reading capability.
                        </p>
                        <p>
                            To "transport" content by QR Codes to another device, use the section below <i class="bi bi-qr-code"></i> QR Code display. Conversely, to "receive" content transported by QR Codes, use the section <i class="bi bi-qr-code-scan"></i> Reading QR Code.
                        </p>
                    `,

                    "displayQrCode" : {
                        "title" : "QR Code Display",
                        "paragraph01" : `
                            <p>
                                Transmit optically without contact, via one or more QR Codes, an encrypted message, a fingerprint, a public key, a signature, etc.
                            </p>
                            <p>
                                The transfer may sometimes require the use of the <i class="bi bi-arrow-clockwise"></i> Converters tab to first convert the content to hexadecimal format. This format allows better preservation of special characters and better automatic splitting into multiple QR Codes. The disadvantage being that the hexadecimal increases the number of characters and therefore the number of QR Codes to be scanned afterwards.
                                <br>Do your own testing.
                            </p>
                            <p>
                                The transport capacity of a single QR Code is defined below to adapt to the detection sensitivities of different materials. Reduce the capacity if the detection of the QR Code is not carried out correctly by your equipment (smartphone, webcam, scanner, etc.)
                            </p>
                            <p>
                                Text content can be up to 100 times the maximum carrying capacity of a single QR Code. The content will be distributed automatically on multiple QR Codes. They will be generated and numbered automatically one below the other.
                                <br>Be careful, the generation time can be long depending on the performance of your equipment.
                            </p>
                            <p>
                                To retrieve the content on your destination equipment, use a XENATX again and go to the "Tools" section then tab <i class="bi bi-qr-code"></i> QR Code and finally in the "Reading QR Codes" section. Then indicate the number of QR Codes to read. The "receipt" fields are generated automatically.
                            </p>
                            <p>
                                Scan your first QR Code and paste the content into the first field. Tap "Verify fingerprint". The textual and visual fingerprint generated must correspond to that generated on your equipment displaying the QR Codes <i>(thanks to the visual fingerprint, the control of the correct execution of the transfer is particularly fast).</i>
                            </p>
                            <p>
                                Once all the QR Codes have been copied one by one, press the blue "REPLENISH AND VERIFY GLOBAL" button. The contents of each field will be reassembled and an overall textual and visual fingerprint will be generated. It must of course correspond to the overall fingerprint indicated on your other "transmitter" equipment.
                            </p>
                        `,
                        "transportCapacitySelect" : `Choice of transport capacity (in number of characters)`,
                        "transportCapacitySelectHelp" : `Between 200 and 900 characters transported by a single QR Code`,

                        "contentField" : "Text content",
                        "visualHashSubtitle" : `SHA-1 visual fingerprint: `,
                        "contentFieldHelp" : "Indicate for example an encrypted message, a fingerprint, a signature, etc.",
                        "createQrCodeBtn" : "Create the QR Code"
                    },

                    "readQrCode" : {
                        "title" : "QR Code reading",
                        "visualHashTitle" : "SHA-1 visual fingerprint",
                        "numberOfQRCodesToReadField" : "Number of QR Codes to read",
                        "checkHashBtn" : `Check fingerprint`,
                        "globalVisualHash" : `Global SHA-1 visual fingerprint`,
                        "globalReconstitutionField" : `Reconstruction of the content transferred by QR Codes`,
                        "globalReconstitutionBtn" : `REPLENISH AND VERIFY GLOBAL`,
                    }
                },

                "tam" : {
                    "title" : "TAM",
                    "subtitle" : "Morse Acoustic Transmitter"
                },

                "tamTx" : {
                    "title" : "TAM Transmitter",
                    "introduction" : `Please read the essential instructions and detailed explanations.`,

                    "essentialInstructions" : {
                        "title" : "Essential instructions",
                        "paragraph01" : `
                            <p class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> To transmit encrypted text content in asymmetric or symmetric, first convert it to hexadecimal with the "Convert before transmission" button. This increases transmission time but preserves special characters.
                            </p>
                            <p>
                                For digital OTP encrypted message, you can transmit it without any conversion. To save some transmission time, you can remove all spaces between encrypted blocks of 5 digits.
                            </p>
                            <p>
                                For a message in clear, therefore not encrypted, you can send it as is also without conversion. However, write in English because the apostrophe ['] as in the sentence [ NOUS T'EMM... AFFECTUEUSEMENT ] is not convertible into Morse code. Neither do the accents. Only alphanumeric characters in UPPERCASE are allowed as well as the single dot sign [.]
                                <br>To avoid English, convert to hexadecimal, with the "Convert before transmission" button, your plain English message including accents and other special characters, as for encrypted textual content.
                            </p>
                            <p>
                                At the end of the transmission, transmit the hash of the message so that the recipient can verify that he has received all the data, without loss or error following decoding. He will carry out the verification under the heading "Fingerprint verification" in the "TAM - Morse recept." tab.
                            </p>  
                        `,
                        "morseFreq" : "Frequency (Hz)",
                    },

                    "detailedExplanations" : {
                        "title" : "Detailed explanations",
                        "paragraph01" : `
                            <p>
                                In the same spirit as the transfer by QR Codes, the TAM allows the secure transmission of data between OFF equipment - See section "Code security" for OFF/ON details - and ON equipment connected to the Internet which, like all connected equipment, is vulnerable to attack due to software and hardware flaws.
                            </p>
                            <p>
                                The TAM therefore makes it possible to transmit any type of message, preferably encrypted. Verification of transmission integrity is done both aurally and visually by spectrogram. All in real time.
                            </p>

                            <div class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i>
                                As a bonus, radio transmission (CiBi/Portable transmitter) becomes possible from equipment OFF1 to OFF2, OFF3, OFF4, OFF<sup>N</sup>...
                                <hr>
                                We draw your attention to the fact that the transmission of encrypted data by radio is prohibited for private radio amateurs in certain countries such as France, although the transmission of encrypted data is authorized via the Internet.
                            </div>
                            <p>
                                The TAM can be used on almost all equipment because sound cards are pre-installed almost everywhere, even on reduced equipment such as the Raspberry Pi <i>(model n°2, or n°4 in modular version, without wifi card , nor bluetooth for more security)</i>.
                            </p>

                            <p class="h4">6 steps procedure</p>
                            <p>
                                Precise description of the procedure for transferring an encrypted message from the secure equipment OFF1 of a sender to the secure equipment OFF2 of the recipient, via the equipments ON1 and ON2 of the sender and the recipient, which are very difficult to secure because they are connected .
                            </p>
                            <p class="alert alert-info">
                                <i class="bi bi-info-circle"></i> Reminder: equipment is said to be OFF if it is never connected to the outside world by standard channels (neither wifi, nor bluetooth, nor USB, nor ethernet, etc.). It then only uses the channels secured by the XENATX to communicate with the outside. The safe environment it provides is linked, almost exclusively, to this inaccessibility through standard channels.
                            </p>
                            <p>
                                Details: 4 XENATX will be installed. One on each equipment in play for this transmission. In the case of radio transmission (CiBi-PMR) which only requires two computer equipment, preferably OFF, only two XENATX will therefore be necessary.
                            </p>
                            <br>
                            <h5>
                                <span class="bg-success badge">1</span>&nbsp; Encryption and transmission from the OFF1 to the sender's ON1
                            </h5>
                            <p>
                                The sender first encrypts the message using XENATX installed on his OFF1 equipment. It uses asymmetric, symmetric, one-time mask encryption, or all three.
                                <br>This encrypted message is then transformed into hexadecimal for easy transmission of special characters and simplified visual and sound verification ("Convert before transmission" button). In the case of encryption only by single-use mask (OTP), the transmission will be done as it is in Morse code without prior conversion to hexadecimal.
                                <br>The encoded message is transmitted in Morse to the ON1, by the sound card with output on speakers ("Transmit" button)
                                <br>At the end of the transmission of the message, a SHA-256 fingerprint of the encrypted message can also be transmitted ("Transmit fingerprint" button). The fingerprint is used to check on the ON1 that the transmission was made without error.
                                <br>If there are errors, it often happens that it is the sound from the speaker that is not loud enough. Simply turn up the volume, the smartphone or computer microphone will pick up the signal better and morse code decoding will be better. <br>Do volume tests on a short message. A low-pass filter may still be necessary before decoding on the ON1 (see the explanation in the "TAM - Morse receiver" tab, then "Decoding parameters" and finally paragraph "Low-pass filter")
                            </p>
                            
                            <p class="alert alert-success">
                                <i class="bi bi-info-circle"></i> At this stage, an alternative is possible, namely the direct transmission of the message from the sender's OFF1 to the recipient's OFF2 by radio (CiBi or transmitter portable PMR type) <em> - In this case, go directly to step 6 - See paragraph RESILIENCE below.</em>
                            </p>
                            <br>
                            <h5>
                                <span class="badge bg-danger">2</span>&nbsp; Reception on ON1
                            </h5>
                            <p>
                                The ON1 equipment records this "score" in Morse code via the microphone using a voice recorder type application on a smartphone or, for example, via the open source software Audacity on a computer (https://www.audacityteam.org
                                <br>The audio file of this morse code recording is uploaded and decoded in the ON1's XENATX on the "TAM - Morse receiver" tab "Upload audio file" button. We then find the hexadecimal content, the same as before the transmission.
                                <br>The ON1 device now saves the new Morse code "partition" corresponding to the SHA-256 hash. As the fingerprint is already in hexadecimal format, there is no need to encode it before transmission.
                                <br>A SHA-256 hash of the message in hexadecimal format received on the ON1 is calculated in the "# Fingerprint - Hash" tab and compared to the transmitted hash. The transmission went well if the two fingerprints match. The process can therefore continue.
                                <br>This hexadecimal content is decoded in text format: initial format following encryption on the OFF1.
                                <br>To save 2 to 3 small sub-steps, it is possible not to decode the hexadecimal and to transfer the message in this format with the means that will be explained in the next step n°3, or even to transmit as is the sound file of the recording by messaging or email.
                            </p>
                            <br>
                            <h5>
                                <span class="badge bg-danger">3</span>&nbsp; Internet transfer from ON1 to ON2
                            </h5>
                            <p>
                                Transmission from the sender's ON1 to the recipient's ON2 by depositing the encrypted message in a digital dead letter box (XENATON online service) or transmission by courier, email, etc. even hand delivery or deposit in a physical dead letter box via a USB key, microSD, paper, etc.
                            </p>
                            <br>
                            <h5><span class="badge bg-danger">4</span>&nbsp; Reception on ON2</h5>
                            <p>
                                If this message has been intercepted by an attacker on this ON2 device (or even on ON1 or even during the previous Morse transmission) it is of no more importance than usual because it is encrypted.
                            </p>
                            <p>
                                The message can now be decrypted BUT since it is still on an ON machine, which may be compromised, this is not a good idea. We are therefore going to re-send the message in Morse code to the OFF2, possibly followed by its SHA-256 fingerprint.
                            </p>

                            <br>
                            <h5>
                                <span class="badge bg-danger">5</span>&nbsp; Re-emission from ON2 to OFF2
                            </h5>
                            <p>
                                The recipient re-sends to his secure equipment OFF2 for decryption and finally reading.
                                <br>The sequence of operations is similar to the previous indications. Reminder: the encrypted message is again encoded in hexadecimal unless it has traveled encoded between ON1 and ON2, or even if it has passed directly in the form of a sound file (in .wav format). The message is then "replayed" acoustically in Morse Code from ON2 to OFF2.
                            </p>
                            <br>
                            <h5><span class="bg-success badge">6</span>&nbsp; Reception on OFF2 from ON2</h5>
                            <p>
                                Recording via the OFF2 microphone in the Audacity software then Morse code decoding - Fingerprint verification - Decryption.
                                <br>Decryption and reading in this secure environment provided by this OFF2.
                                <br>Writing the response.
                                <br>Encryption and hexadecimal encoding.
                                <br>Here we go again for acoustic Morse code transmission in the other direction!
                            </p>
                            <p>
                            Transfer by QR Codes is of course faster for these transfers from OFF1 to ON1 and from ON2 to OFF2 but this is not always possible or compatible with radio transmission.
                            </p>
                            <p class="h4">RESILIENCE</p>
                            <p>
                                Resilience for our customers in a degraded situation - low Internet coverage, white zone, Internet and/or mobile telephone cut-off, conflict zone, mobile surveillance, etc. - is therefore real thanks to the radio transmission of the encrypted message.
                            </p>
                            <p>
                                The transmission is done more easily because directly from an OFF1 device to another OFF2 device.
                                <br>Simply "play" the Morse code of the encrypted message from the loudspeaker of its OFF1 through its CiBi/PMR (Portable Transmitter).
                                <br>On the reception side, it is enough in the same way to stick the radio receiver to the microphone of the OFF2 which will directly record the DIT-DIT-DAH of the morse code.
                            </p>
                            <p>
                                In some extreme cases, two people a few hundred or thousand meters apart can even communicate a short message, always encrypted and always in Morse code, thanks to light signals at night and by deflection of the sun's rays by mirror during the day. Or other tricks such as the semaphore code.
                            </p>
                            <p class="h4">CONCLUSION</p>
                            <p>
                                All of the features of XENATX, including this TAM, were expected by our most exposed customers around the world. They wanted a portable solution, therefore suitable for any field of operation, inexpensive with a XenatOFF or equivalent, software easy to use, with verifiable code and free!
                            </p>
                            <p>
                                At XENATON, we are pleased to offer this global solution to this essential need for secure communication.
                            </p>
                            <p>
                                The broadcast truth liberates. Good enlightened use!
                            </p>
                        `,
                    },

                    "morseCode" : {
                        "title" : "Morse code",
                    },

                    "spectrogram" : "Spectrogram",
                    "txTime" : "Broadcast time:",
                    "contentField" : "Text content",
                    "contentFieldHelp" : "Alphanumeric characters in UPPERCASE plus the period sign [.] only for a message to pass without conversion before transmission.",

                    "hexEncodedContentField" : "Textual content encoded in hexadecimal",
                    "hashField" : "SHA-256 hash of transmitted content",
                    "hashFieldHelp" : "Transmit the fingerprint after the message is transmitted.",

                    "convertBeforeTxBtn" : "Convert before transmission",
                    "originalContentBtn" : "Original content",
                    "morseView" : "View of Morse",
                    "txBtn" : "Convey",
                    "hashTxBtn" : "Submit fingerprint",
                },

                "tamRx" : {
                    "title" : "TAM Receiver",
                    "subtitle" : "Acoustic Morse Transmitter",
                    "introduction" : `
                        <p>
                            If decoding errors occur then a low pass filter is probably needed. This is always the case for a radio transmission (CiBi, etc.) - See the explanation in the "Decoding parameters" section below and in the "Low-pass filter" paragraph.
                        </p>
                    `,

                    "downloadFile" : {
                        "title" : "Audio File Download",
                        "paragraph01" : `
                            <div class="alert alert-info">
                                <i class="bi bi-info-circle"></i> With file playback decoding, <strong>the sound of computer equipment can be muted</strong>. This makes morse code decoding much more comfortable for the ears.
                            </div>
                            <p>
                                The best decoding is ensured by this principle of <strong>downloading audio files in WAV format only</strong> compared to the direct use of the microphone, made possible in the section below.
                                <br>This file reading process is therefore imperative for the decoding of an encrypted message because the slightest error in Morse code decoding would then make decryption by cryptographic key impossible.
                            </p>
                        `,
                        "downloadAudioFileBtn" : "Download an audio file",
                        "decodeBtn" : "Decode",
                        "file" : "File:",
                        "none" : "none",
                        "tooManyFiles" : "You have selected too many files.",
                        "selectOneFile" : "Please try again selecting only one file.",
                        "notAcceptedFile" : "This file type is not supported.",
                        "selectAudioWav" : "Please try again by selecting a WAV audio file.",
                    },

                    "decodingParameters" : {
                        "title" : "Decoding parameters",
                        "paragraph01" : `
                           <p>
                                In the "Audio file download" section, only use a file in WAV format. It is therefore necessary that the recording of the "played" Morse code be made with the selected WAV format.
                                <br>The open source software Audacity (Mac, Windows, Linux) allows computer recording and export in WAV format.
                            </p>
                            <p>
                                Please note that the default dictaphone of an iPhone does not allow the WAV format. It only authorizes a priori the M4A format.
                                <br>Use a free app like AVR voice recorder which allows recording in WAV format.
                            </p>
                            <p class="h5">
                                Radio transmission problem (CiBi/PMR)
                            </p>
                            <p>
                                The "hiss" of X-rays and other alterations of the signal disturb the decoding of Morse code. To solve this problem pass the Morse code audio recording in the low pass filter in the Audacity software before decoding in the XENATX on the "TAM - Morse receiver" tab. The use of the low pass filter is explained in the paragraph below.
                            </p>
                            <p class="h5">
                                Low pass filter
                            </p>
                            <p>
                                To apply a low pass filter to the WAV file of the emitted morse code, follow this procedure in the open source software Audacity&nbsp;: double click on the track to select the whole recording then menu Effects / Low pass filter. Select the frequency at 563 Hz and the Roll-off at 6 db.
                                <br>Finally export the track to a new file by going to the menu File / Export / Export to WAV.
                            </p>
                            <p>
                                <i>In a future version we will try to integrate this low-pass filter into XENATX so that no manipulation in Audacity is necessary.</i>
                            </p>
                        `,
                        "freq" : "Frequency (Hz)"
                    },

                    "microphone" : {
                        "title" : "Microphone",
                        "paragraph01" : `
                            <p class="lead">
                                Using the microphone works to decode morse code live. However, the decoding is less efficient than when downloading an audio file.
                            </p>
                            <p>
                                We therefore recommend the additional step of recording Morse code via a voice recorder on a smartphone or via a computer microphone using, for example, Audacity software. The WAV audio file resulting from this recording will then be downloaded and decoded in the section above "Audio file download".
                            </p>
                        `,
                        "listen" : "Listen",
                        "stop" : "Stop",

                        "paragraph02" : `
                           <p class="lead">
                                You told your browser not to listen with your microphone.
                            </p>
                            <p>
                                To re-enable the microphone, you need to change your web browser settings. For Chrome, click on the video camera icon with a red cross in the address bar. For Firefox, click the microphone icon in the address bar. If it's not there, click the globe icon instead.
                            </p>
                        `,
                    },

                    "hashCheck" : {
                        "title" : "Fingerprint verification",
                        "paragraph01" : `
                            <p>
                                First calculate the SHA-256 hash of the decoded Morse code data by going to the <i class="bi bi-hash"></i>Fingerprint - Hash tab of the this "Tools" section. <span class="text-muted">You will get for example: 9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08</span>
                            </p>
                            <p>
                                Indicate and then compare the two fingerprints below, the one calculated and the one transmitted in Morse code.
                            </p>
                        `,
                        "calculatedHashField" : "Calculated fingerprint",
                        "calculatedHashFieldHelp" : "Paste the calculated SHA-256 hash above.",

                        "hashTxField" : "Fingerprint transmitted",
                        "hashTxFieldHelp" : "Paste the transmitted SHA-256 fingerprint above.",
                        "compareBtn" : "Compare"
                    },

                    "displayDataFurtherDown" : `The decoded data will appear a little further down under the heading "Decoded data".`,
                    "decodedDataTitle" : "Decoded data",
                    "paragraph02" : `
                        If no data appears in the field above, it is often due to too weak sound emitted by the loudspeaker during the recording. Restart the transmission and recording by increasing the volume. The indication of a correct recording in terms of loudness is the dark green color of the bars of the spectrogram, and not pale green, below, during decoding.
                    `,
                    "deleteDataBtn" : "Erase data",

                    "spectrogramTitle" : "Spectrogram",
                    "zoomInBtn" : "Zoom in",
                    "zoomOutBtn" : "Zoom out",
                    "band" : "Band"
                },

                "converters" : {
                    "title" : "Converters",
                    "browser-warning" : `This transformation works well with <strong>Chrome, Chromium, Opera, Brave and Vivaldi</strong> browsers for images up to 2&nbsp;MB (more delicate for larger sizes). Does not work with Firefox, Librewolf and Safari!`,
                    "textToHex" : {
                        "title" : "Text to hexadecimal format",
                        "paragraph01" : `
                            This converter converts encrypted or unencrypted content from text format to hexadecimal (HEX) format. And vice versa.
                            <br>This feature is useful for a safer transfer, by QR Code, of the various characters, especially special characters.
                        `,
                        "contentTextFormatField" : "Text content",
                        "contentConvertedToHexField" : "Converted to hexadecimal format",
                        "convertToHexBtn" : "Convert to HEX"
                    },

                    "hexToText" : {
                        "title" : "Hexadecimal to text format",
                        "contentHexFormatField" : "Content in hexadecimal format",
                        "contentConvertedToTextField" : "Converted to text content",
                        "convertToTextBtn" : "Convert to text"
                    },

                    "imageToText" : {
                        "title" : "Image to text-base64 format",
                        "paragraph01" : `
                            <p>
                                Transform an image (JPEG, PNG or GIF) into text-base64 format. Limit the size of the image ideally to less than 350&nbsp;kb (2&nbsp;MB max) by using strong compression in graphics software. Use hardware acceleration in your browser options. Be patient on low-powered computers.
                                <br>This process can be useful to encrypt the image and/or then transfer it by QR Codes (be careful 5&nbsp;kb for this use already makes around ten QR Codes to flash for the transfer...)
                                <br>To find the image, use the menu below "Text-base64 to image format".
                            </p>
                        `,
                        "transformBtn" : "Transform"
                    },

                    "textToImage" : {
                        "title" : `Text-base64 to image format`,
                        "paragraph01" : "Content in text-base64 format transformable into an image",
                        "revealImageBtn" : `Reveal Image`
                    }
                },

                "hash" : {
                    "title" : "Fingerprint - Hash",
                    "introduction" : `
                        <p class="alert alert-info">
                            <i class="bi bi-info-circle"></i>
                            Calculate the results of the main hash mathematical functions, also called "hash", for a file or a character string. We mainly recommend and use the SHA-512 function (SHA = Secure Hashing Algorithm)
                        </p>
                        <p class="h4">
                            Fingerprint of a file
                        </p>
                        <p class="alert alert-warning">
                            <i class="bi bi-exclamation-octagon"></i>
                            Please wait several seconds for a large file.
                        </p>
                    `,
                    "fileToCheckHash" : "File to calculate fingerprint",
                    "title02" : "Fingerprints of a character string",
                    "charsChainField" : "Character string",
                    "hashToCompareField" : "Fingerprint for comparison",
                    "hashToCompareFieldHelp" : `
                        Optional: indicate a SHA-512 hash for automatic comparison with the one that will be calculated from the character string.
                    `,
                    "calculate" : "Calculate"
                },

                "steganography" : {
                    "title" : "Steganography",
                    "subtitle" : "Hiding textual content in an image", 
                    "introduction" : `<p>
                        The following steganography functionality allows you to:
                    </p>
                    <ul>
                        <li><strong>HIDE</strong> textual content in an image</li>
                        <li><strong>REVEAL</strong> textual content hidden in an image</li>
                    </ul>
                    <p>
                        It is also possible to hide an image within another image. The image must first be converted into textual content in base 64. To do this, go to the « <i class="bi bi-arrow-clockwise"></i> Converters » tab and use the <strong>IMAGE <i class="bi bi-arrow-right-short"></i> TEXT in base 64</strong> functionality. We advise you to encrypt this textual content obtained before concealment. The <strong>TEXT in base 64 <i class="bi bi-arrow-right-short"></i> IMAGE</strong> functionality will allow you the opposite operation, namely to reconstruct the image from the textual content, deciphered beforehand.
                    </p>
                    `,
                    
                    "explanations" : {
                        "title" : "Explanations", 
                        "text" : `
                            <div class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> Only hide encrypted text content in an image. Example: a message, a hash, or other image. All this content must first be asymmetrically encrypted at a minimum.
                                <br>Concealing unencrypted textual content is strongly discouraged.
                            </div>
                            <p>
                               If you transfer the image via a specialized transfer service or post it to an email, the image may be compressed and the hidden content cannot be recovered.
                            </p>
                            <p>
                                Only use images such as landscape photos or plants and flowers taken in macro mode. Avoid images of newspapers or diagrams.
                                <br>Depending on the level of security required, be wary that the image does not allow your identification (known location, sunlight characterizing the time, reflections, etc.).
                                <br>Use open source software like https://exiftool.org/install.html#Unix (for more advanced users) to remove metadata that may contain your geolocation, your camera model or even your name, etc.
                            </p>
                            <p>
                                You can use an online image bank such as Unsplash but the personal photo is preferable, especially taken from an unconnected camera with microSD card storage. Build up a secure personal image bank in advance. This may include not having taken your phone with you... to avoid any overlap between your phone which will have limited itself to these places and the recognizable view, especially with AI, and therefore associable.
                            </p>
                            <p>
                                Obviously, your photo must not have been transmitted over the network at any time. Be careful with iCloud or any sending via the internet by email or file transfer service before inserting the textual content to be concealed.
                                <br>Inserting textual content changes the image fingerprint due to the modification of certain bits. Comparison with the initial print is impossible without having the original photo before insertion. This impossibility of comparing fingerprints provides additional security.
                            </p>
                        `,
                        "training" : {
                            "title": "Train yourself!",
                            "text": `
                            <p>
                               The two images below contain hidden content. The desert image contains a decryptable OTP with the key 27793 33873 22989 05220 80984 29034 63759 54704. The other image contains a photo of a woman in red; to reconstruct the image you will have to use the tab "<i class="bi bi-arrow-clockwise"></i> Converters” and the “Text-base64 to image format” section. 
                            </p>
                            <p>
                                In the context of "professional" use, a hidden image must always be encrypted before being hidden in another image with, for example, an AES-256 symmetric key ("Symmetric encryption" section). This prevents anyone using XENATX from discovering your images hidden in other images.
                                <br>A good exercise to do to train yourself..
                            </p>
                            `
                        }
                    },

                    "hideOrReveal" : {
                        "title" : "Hide or reveal textual content",
                        "imageToHideTitle" : "Image for hiding or revealing textual content",
                        "contentToHideOrReveal" : "Text content to hide or reveal",
                        "imageHidingContent" : "Image concealing textual content",
                        "btnHide" : "Hide",
                        "btnReveal" : "Reveal",
                        "btnDownload" : "Download"
                    }
                }
            }
        }
    },

    es: {
        translation: {
            "error": {
                "otp" : {
                    "trainingRandomOtpQuantityField" : {
                        "minimum" : "Indique una longitud mínima de 10 dígitos.",
                        "maximum" : "Indique una longitud máxima de 250 dígitos.",
                        "empty" : "Indique la longitud de la OTP a crear.",
                        "NaN" : "Indique un número como la longitud de la OTP a crear.",
                        "moduloChecker" : "Indique una longitud que sea múltiplo de 5."
                    }
                },
                "symIndicateBinaryKey" : `Especifique una clave de 256 bits.`,
                "symIndicateSaltShort" : `Por favor indique la sal de su clave.`,
                "symIndicatePasswordShort" : `Por favor ingrese su contraseña o frase de contraseña.`,
                "symProblemReconstitutionSubtitle" : `Error de reconstrucción clave: `,
                "symProblemReconstitution" : `¡Ocurrió un problema al intentar recuperar la clave simétrica!`,
                "symProblemCipherDecrytpSubtitle" : `Error de descifrado simétrico: `,
                "symProblemCipherSubtitle" : `Error de cifrado simétrico: `,
                "symIndicateYourHexKey" : `Indique una clave simétrica en formato hexadecimal y 64 caracteres (256 bits).`,
                "symIndicateYourContentToEncrypt" : `Por favor indique su contenido textual para ser encriptado.`,
                "symIndicateYourContentToDecrypt" : `Por favor indique su contenido textual para ser descifrado.`,
                "symIndicateYourKey" : `Indique su clave simétrica.`,
                "symIndicateSalt" : `Indique una sal después de generarla en la pestaña "Entropía de sal".`,
                "symProblemKeyGenerationSubtitle" : `Error de generación de clave simétrica:`,
                "symProblemKeyGeneration" : `¡Ocurrió un problema durante la generación de claves!`,
                "symIndicatePassword" : `Proporcione una contraseña o frase de contraseña de al menos 40 caracteres para crear su clave simétrica.`,
                "indicateBase64Content" : `Inserte contenido de texto codificado en base64.`,
                "convertToHexMultiQRC": `Este contenido contiene caracteres especiales que no se pueden transportar tal cual mediante códigos QR. Convierta todo o parte de este contenido a hexadecimal de antemano en la pestaña Convertidores.`,
                "asymPasswordEmpty": "La contraseña de su clave privada debe indicarse en la pestaña Claves.",
                "privateKeyEmpty": "La clave privada debe indicarse en la pestaña Claves.",
                "publicKeyEmpty": "La clave pública debe indicarse en la pestaña Claves.",
                "asymMessageToCryptEmpty": "¡Por favor indique un mensaje para cifrar!",
                "asymMessageToUncryptEmpty": "¡Por favor indique un mensaje para descifrar!",
                "asymPasswordMinLength": "Proporcione una contraseña de al menos 16 caracteres para proteger su clave privada.",
                "publicKeyInfoEmpty": "Proporcione una clave pública.",
                "revocationPubKeyfieldEmpty": "Proporcione una clave pública para revocar.",
                "revocationCertificateFieldEmpty": "Por favor indique el certificado de revocación.",
                "hashWellCalculated": "¡La huella digital SHA-512 que se indica a continuación corresponde a la calculada!",
                "hashNotCorresponding": "¡La huella digital SHA-512 no coincide!",
                "hexaToUTF8Empty": "¡Indique el contenido hexadecimal para convertir al formato de texto UTF8!",
                "notInHexa": "Este contenido no está en formato hexadecimal. ¡Vuelve a leer tu entrada!",
                "textToHexaEmpty": "¡Indique el contenido textual para convertir a formato hexadecimal!",
                "hexaToTextEmpty": "¡Indique el contenido hexadecimal para convertir a formato de texto!",
                "textUpperOnlyEmpty": 'Indique el contenido textual en LETRAS MAYÚSCULAS con solo números y letras latinas y el signo de punto único [.], o convierta su contenido a formato hexadecimal con el botón "Convertir antes de la transmisión".',
                "OTPUpperOnlyEmpty": "Por favor indique el contenido textual únicamente en MAYÚSCULAS con únicamente números y letras latinas y únicamente los siguientes signos. : ' + - = ? [] ()",
                "moduloBrackets": "Por favor, compruebe que ha cerrado los corchetes [] que indican el uso de números. Su número es actualmente impar.",
                "moduloParenthesis": "Verifique que haya cerrado los paréntesis () en sus fórmulas. Su número es actualmente impar.",
                "textCipheredEmpty": 'Por favor indique el contenido textual, preferiblemente cifrado por seguridad.',
                "textToMorseEmpty": 'Complete el campo con contenido textual, preferiblemente cifrado por seguridad.',
                "textOrHexaEmpty": 'Indique el contenido o conviértalo a hexadecimal de antemano usando el botón "Convertir antes de la transmisión".',
                "transmissionProblem": '¡Ocurrió un problema durante la transmisión!',
                "transmitMessageBeforeHash": 'Por favor reenvíe el mensaje antes de reenviar su impresión.',
                "hashKO": "¡Las impresiones no son equivalentes!",
                "contentToQRCodeEmpty": "Indique el contenido para transformarlo en un código QR.",
                "numberOfQRCodesToReadEmpty": "Por favor indique un número, en cifras, de Códigos QR a leer.",
                "QRCodeEmpty": "Indique el contenido del código QR.",
                "QRCodesEmpty": "Indique el contenido en todos los campos del Código QR.",
                "tooBigForQRCode1": "El tamaño de la información es demasiado grande para transferirla mediante código QR. Excede ",
                "tooBigForQRCode2": " caracteres. Reduce tu mensaje o córtalo en dos.",
                "passwordPrivKeyEmpty": "Debe indicarse la contraseña de su clave privada.",
                "textToSignEmpty": "Por favor indique el contenido textual para firmar.",
                "privKeyEmpty": "Falta la clave privada o es incorrecta.",
                "signInfoMissing": "Falta información importante o es incorrecta, verifíquela.",
                "signatureKO": 'ATENCIÓN - La firma de este contenido textual no es válida. La clave pública indicada no podrá ser hermana "gemela" de la clave privada utilizada para la firma. Otro motivo: si en los siguientes detalles se indica [El resumen firmado no coincide] significa que el contenido textual no corresponde al original. Es posible que haya sido falsificado o que hayas copiado el original incorrectamente. Detalles : ',
                "xorDifferentLengths": '¡Fallo grave al intentar realizar XOR en flujos de bits de diferentes longitudes! ',
                "OTPDecryptSubtitle": 'Descifrado (OTP): ',
                "OTPEmpty": 'Proporcione una clave digital (máscara de uso único - OTP).',
                "OTPNumericEmpty": 'Proporcione una clave digital (máscara de uso único - OTP).',
                "OTPNumericTextEmpty": 'Indique el contenido de texto claro para convertir.',
                "OTPNumericNotMultiple5Block": 'Indique una clave digital compuesta por una cantidad de dígitos cuya longitud total sea múltiplo de 5. Vuelva a leer las explicaciones para obtener más detalles.',
                "clearMessageEmpty": 'Indique un mensaje claro.',
                "clearCodeEmpty": 'Proporcione un código claro.',
                "encryptedCodeEmpty": 'Indique el contenido cifrado para descifrarlo.',
                "clearCodeNotNumber": 'Indique un código claro únicamente en forma de números. Tómate el tiempo para releer las explicaciones, la seguridad de tus comunicaciones está en juego.',
                "encryptedCodeNotNumber": 'Indique el contenido cifrado únicamente en forma de números. Tómese el tiempo para releer las explicaciones.',
                "OTPNumericNotNumber": 'Proporcione un código simple y una clave digital únicamente en forma de números. Tómese el tiempo para releer las explicaciones.',
                "textToCryptAndOTPIdentical": 'La OTP y el mensaje a cifrar no deben ser en ningún caso idénticos. ¡Vuelve a leer tu entrada!',
                "clearCodeAndOTPIdentical": 'El código en texto plano y la clave OTP digital en ningún caso deben ser idénticos. ¡Vuelve a leer tu entrada!',
                "encryptedCodeAndOTPIdentical": 'El contenido cifrado y la clave digital (máscara - OTP) no deben ser en ningún caso idénticos. ¡Vuelve a leer tu entrada!',
                "textToDecryptAndOTPIdentical": 'La OTP y el mensaje a descifrar no deben ser en ningún caso idénticos. ¡Vuelve a leer tu entrada!',
                "OTPLongEnough": 'Indique una clave digital (máscara de un solo uso - OTP) siempre que el mensaje de texto claro sea encriptado.',
                "OTPNumericLongEnough": 'Indique una clave digital (máscara de un solo uso - OTP) siempre que el código simple se encripte.',
                "OTPNumericEncryptedCodeLongEnough": 'Indique una clave digital (máscara de uso único - OTP) siempre que el contenido cifrado se descifre.',
                "OTPIdentifiersNotNumber": 'Indique los identificadores únicamente en forma de números. Tómese el tiempo para releer las explicaciones.',
                "OTPIdentifiersEmpty": 'Proporcione identificadores.',
                "OTPIdentifiersTooShort": 'Proporcione un identificador de al menos 5 dígitos.',
                "OTPIdentifiersNoMatch": 'Sin coincidencia.',
                "OTPIdentifiersOneMatch": '(este identificador de mensaje corresponde al identificador de una de sus claves OTP)',
                "OTPIdentifiersManyMatch": '(estos identificadores de mensajes separados por una coma corresponden a los identificadores de sus claves OTP)',
                "textToDecryptEmpty": 'Por favor indique un mensaje para descifrar.',
                "convertTextToHexa": "¡Indique el contenido textual para convertir a formato hexadecimal!",
                "bracketsMissing": `Faltan corchetes [] para indicar el uso de números.`,
                "bracketsMissingForSpace": `No puedes utilizar espacios en contenido figurativo que incluya números o signos.`,
                "doubleSpaces": "No puedes utilizar dos espacios seguidos. Guarde su contenido seleccionándolo, luego copiándolo y finalmente presione Cancelar para volver a leer y ajustar su entrada después del carácter ",
                "steganographyHideEmpty" : "Por favor indique el contenido a ocultar.",
                "steganographyRevealEmpty" : "Por favor proporcione una imagen para revelar contenido oculto.",
                "steganographyFileEmpty" : "Por favor proporcione una imagen.",
                "steganographyFileNotImage" : "Indique un tipo de archivo de imagen.",
                "steganographyhidingCapacity" : "personajes ocultables."
            },

            "success": {
                "revocationCertificate": "Esta clave pública ha sido revocada. Cargue esta clave revocada que ahora incluye la información de revocación y deposítela en su repositorio de claves."
            },

            "various": {
                "downloadIllustration" : "Descarga esta ilustración en PDF",
                "keyRevocationStatusYes" : "¡Clave revocada! No utilice más esta clave para cifrar. Puedes usarlo para verificar firmas de mensajes cifrados antiguos.",
                "keyRevocationStatusNo" : "Clave válida (no revocada)",
                "copied" : "¡Copiado!",
                "onlyOneQRCodeGenerated": `Se generó un único Código QR para transportar este texto.`,
                "manyQRCodesGenerated": ` Se han generado códigos QR para contener este texto.`,
                "madeIn" : "Desarrollado en Francia",
                "blockOf4": "Presentación por bloque de 4 personajes.",
                "indicateChainToHash": "Ingrese una cadena de caracteres para calcular la huella digital.",
                "hashName": "Huella",
                "hashOK": "Los estampados son muy similares. La transmisión se realizó correctamente.",
                "indicateHashToCompare": "Indique dos impresiones para comparar.",
                "signatureOK": 'Firma verificada y válida. Este texto ha sido firmado por la clave privada hermana “gemela” de esta clave pública cuyo identificador es',
                "signatureVerification": 'Verificación de firma: ',
                "copy": 'Copiar',
                "validate": 'Validar',
                "cancel": 'Anular',
                "encrypt": 'Cifrar',
                "decrypt": 'Descifrar',
                "chars" : "caract."
            },

            "meta": {
                "title": "XENATON - XENATX - Aplicación independiente",
            },

            "nav": {
                "dashboard": "Panel de control",
                "asymCipher": "Cifrar. Asim.",
                "signature": "Signature",
                "symCipher": "Cifrar. Sim.",
                "otpCipher": "OTP",
                "tools": "Herramientas"
            },

            "dashboard": {
                "title": "Panel de control",
                "subtitle": "",

                "features": {
                    "title": "Características",
                    "introduction": `<p>              
                        XENATX funciona como un sitio web, en un navegador, pero sin necesidad de estar conectado a Internet. Por tanto, XENATX es una aplicación independiente que se puede utilizar con los tres sistemas operativos Windows, MacOS, Linux. Algunas restricciones de uso con iOS en iPhone: el sonido no funcionará y puede ser necesaria además la aplicación gratuita Archivos, que incluye un lector HTML. Para Android, depende de las versiones y modelos, pero en general la mayoría de las funciones están operativas.
                        </p>
                        <p>
                        Para generar claves, cifrar o descifrar, el XENATX debe utilizarse preferentemente en equipos denominados desconectados, por lo tanto no conectados y nunca reconectados.
                    </p>`,

                    "menuCipherAsym": "Cifrado asimétrico y generación de claves.",
                    "menuCipherAsymKeyInfo": "Información de clave pública y revocación",
                    "menuSignature": "Firma y verificación de firma.",
                    "menuCipherSym": "Cifrado simétrico y generación de una clave.",
                    "menuOTP": "Cifrado de máscara de una sola vez: OTP",
                    "menuQRCode": "Código QR - Transferencia óptica segura",
                    "menuTAM": "TAM - Código Morse - Transferencia segura de sonido",
                    "menuHash": "Cálculo de huella - Hash",
                    "menuConverterHexa": "Conversores: hexadecimal e imagen",
                    "menuCodeSecurity": "Seguridad del código",
                    "menuXenaton": "XENATON",
                    "menuSteganography": "Esteganografía",

                    "paragraph01": `<p>
                        Vaya a la pestaña <i class="bi bi-file-code"></i> Seguridad del código para verificar el código XENATX siguiendo las instrucciones proporcionadas. También puede obtener más información sobre nuestros servicios en la pestaña <i class="bi bi-patch-question"></i> XENATON.
                    </p>
                    `
                },

                "codeSecurity": {
                    "title": "Seguridad del código",
                    "quickCheck" : `
                        <p class="h3">
                            Comprobación rápida
                        </p>
                        <p>
                            A continuación puede calcular rápidamente la huella digital SHA-512 de la carpeta comprimida xenatx-*.zip que descargó. Compare el resultado con la huella digital proporcionada en el sitio <a href="https://gitlab.com/xenaton/xenatx/-/wikis/version" target="_blanck">https://gitlab.com/xenaton/ xenatx /-/wikis/version</a> y en nuestro canal de Telegram <a href="https://t.me/xenaton_official" target="_blanck">https://t.me/xenaton_official</a>
                        </p>
                    `,
                    "calculateHashField" : `Calculando la huella digital SHA-512 de la carpeta .zip`,
                    "paragraph" : `
                        Duplique la verificación anterior, por ejemplo utilizando software gratuito como HashCheck (https://code.kliu.org/hashcheck/) para Windows.
                        <br>Con MacOS y Linux, simplemente puede escribir en una terminal la siguiente línea de comando: shasum -a 512 ~/Desktop/xenatx-*.zip suponiendo que colocó la carpeta comprimida en su "Escritorio" después de la descarga. Recuerde reemplazar el asterisco (*) con la versión correcta de XENATX.
                        <hora>
                        También hay muchas herramientas en línea que le permiten realizar fácilmente este cálculo de huella, como https://www.tools4noobs.com/online_tools/hash/ o https://md5file.com/calculator, por ejemplo.
                    `,

                    "general" : {
                        "subtitle": "General",
                        "text" : `
                            <p>
                                Cualquier usuario de XENATX tiene el derecho, incluso el deber, de preguntarse qué garantiza la seguridad e inocuidad del código.
                            </p>
                            <p>
                                Desde nuestro punto de vista, la respuesta más válida es que partes críticas del código provienen de bibliotecas de código abierto. Las utilizamos sin modificarlas y te contamos cómo comprobarlo.
                            </p>
                            <p>
                                Sin embargo, incluso si garantizamos que puede confiar en él, por su seguridad le recomendamos... no confiar en nosotros... <br>En otras palabras, verifique siempre el código XENATX.
                                <br>¿Por qué? Porque aunque no pusimos una puerta trasera, no tienes por qué confiar en nuestra palabra. Además, ataques sofisticados han permitido modificar el código sobre la marcha, a veces incluso sólo para determinados usuarios, basándose en su “huella digital” (huella digital multifactorial de su equipo informático).
                            </p>
                            <p>
                                Por lo tanto, el código XENATX puede ser interceptado y modificado por un tercero, por así decirlo sobre la marcha, durante el trayecto entre nuestro servidor y su equipo, durante la descarga. Todo es posible cuando estás conectado a Internet.
                            </p>
                            </p>
                                Esta aplicación está pensada para ejecutarse, preferentemente, en equipos fuera de línea. Por lo tanto, si había una puerta trasera en su equipo, no existe el XENATX.
                                <br>Nombraremos cualquier equipo fuera de línea con el calificador "OFF" para distinguirlo del equipo "ON", conectado a Internet.
                                <br>Por lo tanto, un OFF no tiene wifi, bluetooth ni conexión ethernet por cable. Por tanto, no existe posibilidad de que se filtren datos sensibles como una clave privada, un mensaje claro antes del cifrado, una contraseña, etc.
                            </p>
                            <p>
                                Después de la descarga, aún tendrás que comprobar que las bibliotecas de código abierto no hayan sido modificadas por nosotros o "sobre la marcha", como se indicó anteriormente, para debilitar, por ejemplo, su calidad criptográfica. Calculará la huella general de XENATX (que se ve en el primer párrafo) para compararla con la que indicamos en el sitio <a href="https://gitlab.com/xenaton/xenatx/-/wikis/version" target. = "_blank">https://gitlab.com/xenaton/xenatx/-/wikis/version</a>, en Telegram <a href="https://t.me/xenaton_official" target="_blank"> https://t.me/xenaton_official</a>, en Twitter, Mastodon y en nuestro sitio (enlaces en los íconos azules en la parte inferior de la página). Lo ideal sería comprobar también las huellas digitales de cada biblioteca de código abierto. Explicamos cómo hacer esto en el párrafo siguiente "Bibliotecas de código abierto".
                            </p>
                            <p>
                                Como habrás comprendido, recomendamos encarecidamente el uso de XENATX en equipos desconectados (como XENATOFF actualmente en desarrollo). Luego se utilizan códigos QR o TAM (Morse) para la transferencia segura y sin contacto de datos desde su equipo OFF a su equipo ON. De este modo podrás transferir todo tipo de datos como mensajes cifrados, claves públicas, firmas, etc.
                            </p>
                            <p>
                                Estos canales seguros evitan que tengas que utilizar una llave USB para transferir datos de un dispositivo a otro. Por no hablar de wifi o bluetooth cuyo flujo bidireccional de datos no es verificable para el público en general, ni siquiera para todos (ver la historia de Crypto AG recordada en la pestaña XENATON).
                                <br>Una memoria USB puede infectarse fácilmente con malware (a través del fallo badUSB, entre otros) escondido en el firmware. El malware podría propagarse de un dispositivo a otro sin que usted pueda darse cuenta. La próxima transferencia a su equipo ON podría filtrar sus datos confidenciales almacenados en su equipo OFF. Todos los esfuerzos de protección de cifrado serían destruidos.
                            </p>
                            <p>
                                El diseño de este conjunto de herramientas de Ciberseguridad Personal, integrando desde el inicio un funcionamiento mitad ON, mitad OFF, con transferencias sin contacto, te garantiza el concepto de “Privacidad por diseño”, sin necesidad de ser un experto en seguridad informática. equipo.
                            </p>
                            <p>
                                Combinar XENATX con los servicios en línea XenaTeam, XenaTrust y XenaTraining no es esencial, pero estos servicios son complementarios.
                                <br>Si forma parte de asociaciones que defienden las libertades, es posible que pueda acceder a estos servicios de forma gratuita; en caso contrario, solicite a su asociación que se ponga en contacto con nosotros para colaborar.
                            </p>
                            <p>
                                Cuando se utiliza nuestro servicio XenaTrust, nuestros servidores no pueden descifrar los mensajes cifrados colocados en el DLB (buzón de letras muertas). En efecto, su clave privada utilizada para el descifrado permanece siempre en su equipo ON, o mejor aún, en su equipo OFF, o mejor aún en una llave USB o microSD, ella misma cifrada, y utilizada exclusivamente en su equipo OFF cuando sea necesario.
                            </p>
                            <p>
                                Por eso nunca almacenamos sus claves privadas ni intentamos acceder a ellas. Físicamente, no podríamos incluso si estás usando el equipo OFF. Lo mismo ocurre con la clave simétrica, la contraseña y los datos de tipo salt utilizados en la sección <i class="bi bi-file-earmark-lock"></i> Cifrado simétrico.
                            </p>
                            <p>
                                Notarás que no existe ninguna función JavaScript AJAX para repatriar discretamente datos confidenciales a ningún servidor <i>(recordatorio: si utilizas un equipo OFF, una fuga ni siquiera es físicamente posible)</i>.
                                <br>Por supuesto, debes tener algunos conocimientos de informática para comprobarlo. Sin embargo, el código es relativamente sencillo. El arduo trabajo que realizan las bibliotecas de código abierto cocreadas y mantenidas por docenas de desarrolladores.
                            </p>`
                    },

                    "license" : {
                        "subtitle" : "Licencia",
                        "text" : `<p>
                                    La licencia se mantiene "propietaria" pero el uso es gratuito y sin limitaciones.
                                </p>
                                <p>
                                    El código es abierto y no está deliberadamente ofuscado, por lo que es perfectamente verificable y mejorable.
                                    <br>También indicamos la metodología detallada para esta verificación y animamos a que se lleve a cabo. Para hacer esto, vaya a la pestaña <i class="bi bi-file-code"></i> Seguridad del código de esta sección.
                                </p>
                                <p>
                                    Llamamos su atención sobre el hecho de que XENATON no ofrece ninguna garantía con respecto al uso de XENATX que se realiza bajo la exclusiva responsabilidad del usuario.
                                    <br>Se proporciona información adicional sobre este tema en el archivo README.txt en la raíz de la carpeta XENATX descargada.
                                </p>`
                    }, 

                    "resilience" : {
                        "subtitle" : "Resiliencia",
                        "text" : `<p>
                                    En cuanto a nuestros servicios en línea, si "cayéramos en quiebra", como mínimo el XENATX seguirá siendo utilizable. Se puede utilizar de forma independiente en su equipo informático.
                                </p>
                                <p>
                                    Además, todos los conceptos aprendidos, gracias a las explicaciones en XENATX y nuestro programa de formación online XenaTraining, seguirán siendo conocimientos válidos y reutilizables para ti en todas partes. Este conocimiento se refiere a protocolos abiertos y técnicas y conocimientos lógicos.
                                </p>
                                <p>
                                    Las claves de cifrado y las firmas creadas con XENATX, que reflejan la lenta construcción de una red de confianza, seguirán siendo totalmente válidas, gratuitas y fácilmente reutilizables en otros sistemas. Estas claves y firmas también se basan en protocolos y estándares abiertos y probados.
                                </p>`     
                    },

                    "library" : {
                        "subtitle" : "Bibliotecas de código abierto",
                        "text" : `<p>
                                    Hemos enumerado todas las bibliotecas de JavaScript utilizadas. Puede encontrarlos en la carpeta activos/javascripts/libs en la carpeta fuente de XENATX que descomprimió. También puede consultar el contenido de nuestros códigos JS en activos/javascripts/módulos. Nuestros códigos JS no se minimizan ni se ofuscan deliberadamente. Te indicamos las huellas dactilares en https://gitlab.com/xenaton/xenatx/-/wikis/version para que puedas comprobar que nada ha sido alterado por MITM (Man In The Middle) u otras técnicas.
                                </p>
                                <p>
                                    Utilice la pestaña <i class="bi bi-hash"></i> Huella digital - Hash para calcular las huellas digitales SHA-512 y compararlas entre sí. Para estar seguro, verifique sus huellas digitales como se indica arriba en el cuadro.
                                    <br>Verifique dos veces antes de considerar contactarnos si las impresiones difieren para cualquiera de las bibliotecas a continuación. Todo lo que se necesita es dejar un comentario o una nueva línea adicional para que las huellas digitales sean diferentes.
                                </p>
                                <div class="alert alert-info">
                                    Hemos implementado y mejorado las siguientes bibliotecas con nuestros propios códigos para crear un sistema completo y original en concepto. Sin embargo, el primer crédito es para los creadores y contribuyentes de estas bibliotecas y, a su vez, para todos aquellos que las inspiraron. Para algunos, estas bibliotecas constituyen componentes esenciales de XENATX.
                                    <hora>
                                    <i class="bi bi-hand-thumbs-up"></i> Un agradecimiento especial a davidshimjs Sgnamin Shim (qrcode.js), Fabian Kurz (jscwlib.js - Biblioteca de códigos Morse JS), Joshua M. David (OTP funciones), Stephen C. Philips (Morse Pro), Julian Fietkau (hash visual mosaico), Peter Eigenschink (Steganography.js) y Guillaume (corrección de errores y lenguaje).
                                </div>
                                <p>
                                    <strong>Audio-decoder-adaptive - Decodificación morse</strong>
                                    <br>Fuente pública: https://morsecode.world/js/audio-decoder-adaptive.96da976088f5fb7c78e537b303a0ebee.js 
                                    <br>Si la siguiente cadena entre corchetes en el enlace de arriba cambia [ 96da976088f5fb7c78e537b303a0ebee ].js, solo copia y pega el contenido https://morsecode.world/js/audio-decoder-adaptive.*.js y péguelo localmente como reemplazo. 
                                    <br>Las diferencias no deben ser grandes y, por lo tanto, se garantiza el uso de una fuente pública que no esté "manipulada". La biblioteca completa está aquí, para su estudio: https://github.com/scp93ch/morse-pro
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/audio-decoder-adaptive-2.0.min.js
                                </p>
                                <p>
                                    <strong>Bootstrap</strong>
                                    <br>Fuente pública: https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.2.0/js/bootstrap.min.js
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/bootstrap-5.2.0.min.js
                                </p>
                                <p>
                                    <strong>Chroma</strong>
                                    <br>Fuente pública: https://raw.githubusercontent.com/gka/chroma.js/main/chroma.min.js
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/chroma-2.4.2.min.js
                                </p>
                                <p>
                                    <strong>Clipboard</strong>
                                    <br>Fuente pública: https://raw.githubusercontent.com/zenorocha/clipboard.js/master/dist/clipboard.min.js
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/clipboard-2.0.10.min.js
                                </p>
                                <p>
                                    <strong>Crypto-js</strong>
                                    <br>Fuente pública: https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.1.1/crypto-js.min.js
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/crypto-js-4.1.1.min.js
                                </p>
                                <p>
                                    <strong>Forge</strong>
                                    <br>Fuente pública: https://cdnjs.cloudflare.com/ajax/libs/forge/1.3.1/forge.min.js
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/forge-1.3.1.min.js
                                </p>
                                <p>
                                    <strong>i18next</strong>
                                    <br>Fuente pública: https://cdnjs.cloudflare.com/ajax/libs/i18next/21.8.14/i18next.min.js
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/i18next-21.8.14.min.js
                                </p>
                                <p>
                                    <strong>jQuery</strong>
                                    <br>Fuente pública: https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/jquery-2.2.4.min.js
                                </p>
                                <p>
                                    <strong>Js-cookie</strong>
                                    <br>Fuente pública: https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js (https://github.com/js-cookie/js-cookie/releases/tag/v2.1.4)
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/js-cookie-2.1.4.min.js
                                </p>
                                <p>
                                    <strong>Jscwlib - Codificación morse</strong>
                                    <br>ATENCIÓN ! Hemos realizado algunos cambios menores en esta biblioteca. Lea el archivo README.txt en la raíz de la carpeta XENATX para ver los cambios y calcular mejor la huella digital para la verificación.
                                    <br>Fuente pública: https://git.fkurz.net/dj1yfk/jscwlib/raw/branch/master/src/jscwlib.js (https://git.fkurz.net/dj1yfk/jscwlib/
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/jscwlib.js
                                </p>
                                <p>
                                    <strong>Modernizr</strong>
                                    <br>Fuente pública: https://modernizr.com/download/?touchevents-setclasses-shiv (custom build)
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/modernizr-3.6.0.min.js
                                </p>
                                <p>
                                    <strong>MosaicVisualHash - Huella digital visual</strong>
                                    <br>Fuente pública: https://raw.githubusercontent.com/jfietkau/Mosaic-Visual-Hash/master/mosaicVisualHash.js
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/mosaicVisualHash-1.0.1.js
                                </p>
                                <p>
                                    <strong>Openpgp</strong>
                                    <br>Fuente pública: https://unpkg.com/openpgp@5.3.1/dist/openpgp.min.js
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/openpgpjs-5.3.1.min.js
                                </p>
                                <p>
                                    <strong>Qrcode</strong>
                                    <br>Fuente pública: https://cdn.jsdelivr.net/gh/davidshimjs/qrcodejs@gh-pages/qrcode.min.js
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/qrcode.min.js
                                </p>
                                <p>
                                    <strong>Underscore</strong>
                                    <br>Fuente pública: https://cdn.jsdelivr.net/npm/underscore@1.13.6/underscore-umd-min.js
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/underscore-1.13.6.min.js
                                </p>
                                <p>
                                    <strong>Steganography</strong>
                                    <br>Fuente pública: https://raw.githubusercontent.com/petereigenschink/steganography.js/master/build/steganography.min.js
                                    <br>Llame al "head" del archivo index.html: assets/javascripts/libs/steganography-1.0.3.min.js
                                </p>`        
                    },

                    "jscodes" : {
                        "subtitle" : "Códigos JavaScript",
                        "text" : `<p>
                                    Visita el sitio <a href="https://gitlab.com/xenaton/xenatx/-/wikis/version" target="_blanck">https://gitlab.com/xenaton/xenatx/-/wikis/version</a> para comparar todos los archivos de los siguientes códigos internos JS:
                                </p>
                                <p>
                                    <strong>Asym. Cipher</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/asym-cipher.js
                                </p>
                                <p>
                                    <strong>Asym. Generate Keys</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/asym-generate-keys.js
                                </p>
                                <p>
                                    <strong>Asym. Info Key</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/asym-info-key.js
                                </p>
                                <p>
                                    <strong>Asym. Revocation Certificate</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/asym-revcert.js
                                </p>
                                <p>
                                    <strong>Core</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/core.js
                                </p>
                                <p>
                                    <strong>Hash File</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/hash-file.js
                                </p>
                                <p>
                                    <strong>Hash</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/hash.js
                                </p>
                                <p>
                                    <strong>Hex converter</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/hex-converter.js
                                </p>
                                <p>
                                    <strong>i18n</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/i18n.js
                                </p>
                                <p>
                                    <strong>Image converter</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/image-converter.js
                                </p>
                                <p>
                                    <strong>Lg</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/lg.js
                                </p>
                                <p>
                                    <strong>Morse</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/morse.js
                                </p>
                                <p>
                                    <strong>OTP numeric</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/otp-numeric.js
                                </p>
                                <p>
                                    <strong>QR Code</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/qrcode.js
                                </p>
                                <p>
                                    <strong>Signature</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/signature.js
                                </p>
                                <p>
                                    <strong>Steganography</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/steganography-image.js
                                </p>
                                <p>
                                    <strong>Sym. cipher</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/sym-cipher.js
                                </p>
                                <p>
                                    <strong>Sym. cipher file</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/sym-cipher-file.js
                                </p>
                                <p>
                                    <strong>Sym. entropy</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/sym-entropy.js
                                </p>
                                <p>
                                    <strong>Various functions</strong>
                                    <br>Llame al final del archivo index.html: assets/javascripts/modules/various-functions.js                               
                                </p>
                            `        
                    },

                    "conclusion" : {
                        "subtitle" : "Conclusión",
                        "text" : `
                            <p>
                               Le agradecemos su confianza, no ciega como debería ser. Tenga la seguridad de que respetamos la privacidad y la soberanía personal. Obtenga más información sobre los servicios de XENATON en la tabla <i class="bi bi-patch-question"></i> XENATON.
                            </p>
                           <p>
                                Para aumentar aún más la seguridad proporcionada por XENATX y superar posibles debilidades de cifrado (falta de entropía clave, IA, ataque de sincronización, calculadora cuántica, etc.), hay varias soluciones disponibles y la lista no es exhaustiva.
                            </p>
                            <p>
                                Para mayor entropía, las claves se pueden generar mediante CSPRNG de línea de comandos en sistemas Linux/Unix. Los generadores de entropía adicionales también pueden ayudar. El más común está relacionado con los movimientos del ratón, como el que utilizamos en el apartado “Cifrado Sym”. y la pestaña “Entropía de la sal”. El objetivo es avanzar hacia TRNG (True Random Number Generator).
                                <br>El software de código abierto como Veracrypt tiene este tipo de solución en su "generador de archivos de claves". Además, también vale la pena descubrir su funcionalidad de volumen oculto. La esteganografía en todas sus formas también es importante.
                            </p>
                            <p>
                                También puede mejorar la resistencia de su contenido cifrado utilizando máscaras de un solo uso (OTP - One-Time-Pad) para los elementos más sensibles de sus mensajes antes del "sobrecifrado clásico". Sin embargo, esto requiere el intercambio previo de directorios OTP, bien generados con la máxima entropía.
                                <br>Para aumentar la entropía, tanto para AES-256 como para máscaras de un solo uso, indicamos soluciones para la creación de claves basadas en dados de precisión... Lento pero fiable. Esta seguirá siendo la solución más accesible y segura para todos durante mucho tiempo.
                                <br>Puede valer la pena en algunas situaciones. Profundizamos en estos temas en nuestro programa XenaTraining pero ya tenéis una muy buena visión general en el apartado "Explicaciones" del apartado "OTP - Mascarilla de un solo uso".
                            </p>
                            <p>
                                En todos los casos, le recomendamos que tenga mucho cuidado con los fallos de hardware y software de sus dispositivos conectados permanentemente a Internet... Considere utilizar XENATX, u otro software de cifrado, en equipos desconectados que estén permanentemente fuera de línea y realice la conexión con su dispositivo ON gracias a los canales de transferencia seguros que ofrece XENATX. Hacer lo contrario es ilusorio en términos de seguridad real.
                            </p>
                            <p>
                                Gracias por su interés, buen uso informado y buena recuperación de autonomía.
                            </p>
                        `        
                    }
                },

                "xenaton" : {
                    "titleOrigin" : "Origen y finalidad",
                    "originParagraph01" : `
                        <p>
                            XENATON es la contracción de Xenophon y Platon en homenaje a estos dos sabios, discípulos directos de Sócrates.
                            <br>Bajo esta bandera, apoyamos a las organizaciones responsables (empresa, asociación, colectivo, etc.) en sus necesidades de Ciberseguridad Personal para sus empleados o afiliados.
                        </p>
                        <p>
                            Actualmente estás utilizando XENATX, nuestra aplicación independiente y gratuita para todos. Esta aplicación combina idealmente con el conjunto de servicios en línea desarrollados por XENATON.
                        <p>
                    `,
                    "titlePresentation" : "Breve presentación de nuestros servicios.",

                    "titleXenaTrust" : "XenaTrust",
                    "xenaTrustParagraph01" : `
                       <p>
                            Aplicación web en modo SaaS (Software as a Service) que ofrece envío de mensajes cifrados en un buzón digital muerto y un servicio descentralizado y anónimo para establecer y propagar la confianza mediante firmas criptográficas autogestionadas. La operación se sitúa a medio camino entre la Autoridad de Certificación (PKI) y la red de confianza descentralizada. XenaTrust, por supuesto, se combina con XENATX y, a menudo, con XenaTraining y XenaTeam.
                        </p>
                    `,

                    "titleXenaTraining" : "XenaTraining",
                    "xenaTrainingParagraph01" : `
                        <p>
                            Aplicación web en modo SaaS que promueve el aprendizaje y la actualización de conocimientos y know-how esenciales en materia de PCS (Ciberseguridad personal). XenaTraining suele combinarse con XenaTrust, XenaTeam y, por supuesto, XenaTx.
                        </p>
                    `,

                    "titleXenaTeam" : "XenaTeam",
                    "xenaTeamParagraph01" : `
                        <p>
                            Aplicación web en modo SaaS para la coordinación segura de un equipo: espacio seguro, gestión de cuentas anónimas, depósito de mensajes cifrados en un buzón muerto digital, mensajes multidifusión, servicio XenatHIC para mensajes HIC (Hidden In the Crowd), etc.
                            <br>XenaTeam se acopla automáticamente a XenaTrust, XenaTraining y XenaTx, por supuesto.
                        </p>
                    `,

                    "titleXenatOFF" : "XenatOFF",
                    "xenatOFFParagraph01" : `
                        <p>
                            Pequeño equipo informático portátil denominado OFF. El principio del equipo OFF es que no se conecta y nunca se vuelve a conectar. Está sin tarjeta wifi, sin tarjeta bluetooth y con sus puertos USB y ethernet “obstruidos”. Una especie de caja negra.
                            <br>XenatOFF está preequipado con una distribución de Linux complementada con herramientas esenciales que incluyen, por supuesto, XENATX. Ofrece todo el potencial del conjunto de herramientas XenaTx, XenaTrust, XenaTraining y XenaTeam.
                        </p>
                        <p>
                            Este equipo informático se puede utilizar de forma independiente sólo con XENATX, u otro software de cifrado, sin necesidad de vinculación con nuestros servicios en línea.
                        </p>
                        <p>
                            Para nuestros colectivos asociados, ofrecemos soluciones para crear usted mismo un XenatOFF con un presupuesto reducido. <br>Si aún no es socio o cliente, contáctenos a través de xenaton.com
                        </p>
                        <p>
                            Existen ideas para equivalentes de bajo costo, como una simple torre de computadora, equipada con XENATX gratuito, y de la cual se han retirado físicamente las tarjetas wifi y bluetooth. Ahora puede crear su cadena de hardware y software segura con un presupuesto muy limitado.
                        </p>
                    `,

                    "titleXenaTx" : "XENATX",
                    "xenaTxParagraph01" : `
                        <p>
                            Presenta una aplicación independiente y gratuita en forma de un sitio web sencillo en JavaScript y HTML utilizable en todos los sistemas operativos como MacOS, Windows, Linux e incluso Android e iOS con algunas limitaciones para los dos últimos.
                            <br><i class="text-muted small">Está prevista una versión más adaptada a los smartphones para poder activar todas las funcionalidades.</i>
                        </p>
                        <p>
                           El XENATX cubre muchas características esenciales:
                            <ul>
                                <li>Cifrado asimétrico (OpenPGP.js también utilizado por Proton Mail).</li>
                                <li>Firma criptográfica.</li>
                                <li>Cifrado simétrico AES-256 en modo CBC con IV aleatorio.</li>
                                <li>OTP - Cifrado de un solo uso y HIC</li>
                                <li>Cálculo de huellas dactilares (hash)</li>
                                <li>Transmisión segura mediante Código QR.</li>
                                <li>Transmisión segura mediante Transmisor Acústico Morse para datos cifrados transmitidos, en particular, mediante CiBi/PMR (transmisor de radio portátil).</li>
                                <li>Esteganografía</li>
                            </ul>
                        </p>
                       <p>
                            El enfoque sin contacto (Código QR y Sonido) de XENATX es fundamental. Evita la contaminación o fuga de datos por medios comunes de transferencia de datos (wifi, bluetooth, ethernet, USB, etc.) hacia o desde un XenatOFF o equipo equivalente.
                        </p>
                        <p>
                            El equipo OFF es la única técnica que ofrece seguridad óptima al proteger contra vulnerabilidades de hardware y otras puertas traseras de software presentes en casi todos los equipos (en particular, vulnerabilidades de "día cero").
                            <br>La fuga de datos, o la inserción maliciosa de datos, a través de Internet o mediante conectividad se vuelve físicamente imposible <em>(excepto en casos extremos de ataque por captura remota del campo electromagnético del equipo: encontrará más información sobre estos ataques con la palabra clave TEMPEST)</em>.
                        </p>
                       <p>
                            Los únicos “canales” autorizados y verificables (código QR y sonido) permiten así importar o exportar de forma segura datos valiosos como mensajes cifrados, claves públicas, firmas e incluso imágenes comprimidas, etc.
                        </p>
                        <p>
                            XenaTx se puede utilizar solo, pero se combina de manera óptima con nuestros tres servicios en línea XenaTrust, XenaTraining, XenaTeam y, por supuesto, nuestro XenatOFF.
                            </p>
                            <p>
                            Este enfoque original y transparente nos aleja de las promesas de marketing que deliberadamente borran todos los defectos antes mencionados.
                            </p>
                            <p>
                            Este tema de las vulnerabilidades de “día cero” y otros puede parecerle complicado. Si desea profundizar más para comprender mejor nuestro punto, mire el IME (Intel Management Engine) y AMD st, donde las fallas/puertas traseras podrían estar directamente en los procesadores...
                        </p>
                        <p>
                            ¿Todavía tienes dudas? Luego busque la empresa suiza Crypto AG (Hagelin) <a href="https://youtu.be/SWFlA248spU" target="_blank">https://youtu.be/SWFlA248spU</a>
                            <br>Sus caros productos suizos de calidad estaban destinados, entre otros, a embajadas de todo el mundo. Sin embargo, estos productos habían sido "de puerta trasera" durante décadas por un gran servicio de 3 letras... La quiebra total de Crypto AG se produjo recientemente en 2020 después de estas revelaciones.
                            <br>Otro fallo más “público en general” que resulta interesante porque está muy extendido: el fallo BadUSB que afecta a los dispositivos USB (llave, ratón, teclado, etc.)
                        </p>
                        <p>
                            Con esta breve descripción general, debería convencerse de que el cifrado, que ya tiene sus posibles debilidades, sólo tiene un interés limitado sin protegerlo desconectando completa y permanentemente el equipo en el que se realiza.
                        </p>
                    `,

                    "titleRH" : "Recursos humanos",
                    "RHParagraph01" : `
                        <p>
                            Buscamos regularmente colaboradores de gran sabiduría, con egos controlados, conscientes del equilibrio sutil y ancestral entre los siguientes tres grupos: los “Productores”, los “Sofistas-Limpiadores” y los “Filósofos-Guardianes”. Matices naturales y superposiciones a un lado. Según conocimientos de la antigua Grecia y nuestra propia tabla de análisis.
                            <br>Este entendimiento nos asegura que nuestros colaboradores tendrán el enfoque óptimo de los componentes y problemas de una situación y un entorno. En definitiva, adoptarán una actitud humanista pero realista.
                        </p>
                        <p>
                            Ser conscientes de las razones del predominio de los sofistas en el mercado actual nos parece fundamental. Al estar desequilibrado el mercado, favorece la existencia y el desarrollo de los sofistas. Se comprende mejor su papel de probadores de sagacidad, de “limpiadores” sin empatía, sin justificar sus excesos.
                            <br>Los términos "productores de imágenes y apariencias o productores de maravillas en los discursos", utilizados por Platón en la conclusión de su obra El Sofista, encuentran hoy, más que nunca, su perfecta ilustración.
                        </p>
                        <p>
                            Nos corresponde a todos limpiar la situación mediante servicios centrados en el equilibrio, la equidad, la lucidez y la soberanía.
                        </p>
                    `,

                    "titleTrainers" : "Red de formadores",
                    "trainersParagraph01" : `
                        <p>
                            Estamos desarrollando nuestra red de formadores independientes para apoyar a nuestros clientes. Los capacitamos gratuitamente en nuestras soluciones TI mientras afinamos su visión estratégica de los equilibrios en los sistemas organizacionales.
                        </p>
                        <p>
                            Cualquier forma de negación, incluido el angelismo, no puede ni debe encontrar eco entre nuestros colaboradores. El enfoque hacia nuestros clientes es humanista pero pragmático.
                            <br>Montesquieu, y sus pares anteriores a él, nos recordaron que "Cualquier hombre, o grupo, empresa, organización, etc., que tiene poder está proclive a abusar de él. Es por tanto necesario que por la disposición de las cosas el poder detenga la fuerza." ...
                        </p>
                        <p>
                            De hecho, XENATON se mantiene lejos del angelismo dichoso. La ciropedia de ese equilibrio realista es su búsqueda, sin refugio en un mundo de fantasía. Anclarse en la realidad es fundamental.
                        </p>
                        <p>
                            Como toda empresa orientada a la enseñanza, querida por los filósofos, y no a la depredación, querida por los sofistas, concentramos nuestros esfuerzos en formar y desarrollar la sagacidad y la responsabilidad de nuestros usuarios y colaboradores con un espíritu virtuoso.
                        </p>
                        <p>
                            Animamos a nuestros formadores y a nuestros clientes a difundir nuestros conocimientos y técnicas para que puedan implicarse en el saneamiento de su entorno profesional e incluso personal.
                        </p>
                        <p>
                            Teniendo esto en cuenta, XENATX es gratuito para todos, sin restricciones.
                        </p>
                    `,

                    "titlePartnerships" : "Asociaciones",
                    "partnershipsParagraph01" : `
                       <p>
                            Todos nuestros servicios en línea son accesibles de forma gratuita a numerosos grupos, de jure o de facto, que trabajan en la defensa de las libertades individuales y colectivas, previa cooptación, estudio y establecimiento de una asociación.
                        </p>
                        <p>
                            El respeto por el Hombre y su soberanía personal - incluyendo su vida privada y el secreto de sus comunicaciones - está en la fuente de XENATON.
                        </p>
                        <p>
                            La hiperred centralizada de vigilancia y control, que elimina toda autonomía y, por tanto, toda vida, es un callejón sin salida. Cualquier concentración excesiva, en cualquier ámbito, crea un peligroso punto único de fragilidad. Un disparate en términos de resiliencia y un sofisma que niega a los seres vivos, sus ecosistemas y su necesaria biodiversidad.
                        </p>
                        <p>
                            Si se reconoce en estos valores corporativos, sería un honor para nosotros trabajar junto a usted en la consecución de estos objetivos comunes. No dude en contactarnos en xenaton.com
                        </p>
                    `,
                }
            },

            "menus": {
                "menuCipherAsym": "Cifrado asimétrico",
                "menuSign": "Firma criptográfica",
                "menuCipherSym": "Cifrado simétrico",
                "menuCipherOTP": "OTP - Mascarilla de un solo uso",
                "menuTools": "Herramientas - Transferencia segura",
            },

            "cipherAsym": {
                "introduction": "El cifrado asimétrico, por tanto con claves públicas y privadas, permite evitar el intercambio previo de una clave secreta.",
                "menuExplanations": "Explicaciones",
                "menuCreateKeys": "Crear un par de claves",
                "menuKeys": "Claves",
                "menuEncrypt": "Cifrado",
                "menuDecrypt": "Descifrado",
                "menuKeyInfo": "Info. en una clave pública",
                "menuRevocationKey": "Revocación de una clave pública",

                "explanations": {
                    "title" : "Explicaciones",
                    "paragraph01" : `
                        <p>
                            <strong>La característica excepcional del cifrado asimétrico es evitar el intercambio secreto, de antemano, de una clave común de cifrado y descifrado entre corresponsales como en el caso del cifrado simétrico</strong> (ver la sección "Cifrado simétrico" más abajo).
                        </p>
                        <p>
                            Este tipo de cifrado se inventó recién en la década de 1970. Para conocer el historial completo, busque con la palabra clave RSA (Rivest-Shamir-Adleman).
                        </p>
                        
                        <p>
                            En el intercambio de un mensaje cifrado asimétricamente entre dos personas (o dos máquinas), cada una de las dos personas tiene un par de claves:
                            <ul>
                                <li>Una clave pública, públicamente divulgable, por lo tanto conocida por todos, que permite cifrar pero no descifrar <em>(al menos no con fines de cifrado-descifrado de un mensaje)</em></li>
                                <li>Una clave privada, a veces denominada "clave secreta", para descifrar y firmar. <u>Esta clave privada es estrictamente personal</u>.</li>
                            </ul>
                        </p>
                        <p>
                            No existe posibilidad de recrear la clave privada a partir de la clave pública. Por lo tanto, todos pueden conocer la clave pública, incluido el atacante.
                        </p>
                        <p>
                            El cifrado asimétrico proporciona tres funciones esenciales:
                            <ul>
                                <li>confidencialidad (secreto del mensaje), </li>
                                <li>integridad (no alteración del mensaje), </li>
                                <li>autenticidad (prueba de que el autor del mensaje es quien dice ser).</li>
                            </ul>
                        </p>

                        <p>
                            Para estos dos últimos puntos, integridad y autenticidad, se integra al proceso un mecanismo de firma. Este mecanismo utiliza cifrado y una función hash que establece una huella digital. La firma de un mensaje es una huella digital de este mensaje, cifrada con la clave privada del autor del mensaje.
                        </p>
                        <p class="alert alert-warning">
                            <i class="bi bi-exclamation-octagon"></i> XENATX permite cifrar textos de forma asimétrica pero no un archivo completo.
                            <br>Si aún desea enviar un archivo cifrado sin haber intercambiado primero una clave simétrica con su interlocutor, cifrelo simétricamente en AES-256 (sección "Cifrado simétrico" y luego pestaña "Cifrado - Descifrado de archivos") e indique el cifrado simétrico. clave en texto cifrado asimétricamente.
                            <br>Ejemplo de texto para indicar debajo de su mensaje principal antes del cifrado asimétrico:
                            <br>Archivo adjunto "test.png.enc" cifrado simétricamente con esta clave 55BCCF0E4BA8426C779D62A534EA183EED3FEEAB4C08615712593E646D737EBF
                        </p>
                        <p class="alert alert-info">
                            <i class="bi bi-exclamation-octagon"></i> XENATX proporciona herramientas esenciales para el cifrado, pero no puede reemplazar un programa integral de capacitación en criptografía. Encontrarás muchas otras explicaciones y referencias en nuestro programa de aprendizaje online XenaTraining (gratuito para colectivos asociados).
                            <br>Ya recomendamos este vídeo compuesto por buenos diagramas para comprender completamente el cifrado asimétrico https://youtu.be/AQDCe585Lnc (activa los subtítulos en español si es necesario). Este sitio también explica bien el principio: https://wikilibriste.fr/fr/intermediaire/chiffrement
                        </p>`
                },

                "creatingAKeyPair": {
                    "title" : "Crear un par de claves",
                    "paragraph01": `Cree su par de claves pública y privada (ECC-25519) y luego:
                    <ul>
                        <li>Guárdelas de forma segura en un administrador de contraseñas como Keepass colocado en una llave USB o microSD.</li>
                        <li>Guarde allí también su contraseña de clave privada, si es posible, no almacene la contraseña completa y conserve parte.</li>
                        <li>Guarde el certificado de revocación en un archivo Keepass diferente y en otro medio, si es posible.</li>
                        <li>La información no crítica, como el identificador de clave pública, se puede encontrar en la pestaña "Información de clave pública".</li>
                    </ul>`,

                    "paragraph02": `Para lograr un buen nivel de seguridad, su par de claves debe generarse en un equipo llamado “OFF” que no está ni estará conectado a Internet nunca más.
                    <br>Este equipo OFF debe tener las siguientes características: ni tarjeta wifi, ni tarjeta bluetooth, ni tarjeta wifi-4G externa, ni cable ethernet conectado, ni puertos USB accesibles a terceros, etc. En definitiva, ninguna capacidad de conexión por ondas ni por conectores.
                    <br>Sólo se puede utilizar un teclado y un ratón con cable, nunca reconectados a otros equipos: de hecho, las tomas USB tienen un firmware fácilmente infectable que permite la fuga de datos. Su clave privada u otros datos confidenciales podrían recuperarse más fácilmente de lo que cree.`,
                    "passphraseLabel": `Contraseña que protege su clave privada`,
                    "passphraseHelp": `Al menos 16 caracteres, pero más bien 40 o más, incluidos los especiales y, por supuesto, los números, tanto en mayúsculas como en minúsculas. Tenga en cuenta que no hay forma de encontrar su contraseña si la olvida.`,

                    "keyPairIntroduction": "Para crear una clave pública anónima, le recomendamos que deje este nombre ficticio y correo electrónico a continuación. El hecho de que exista un nombre y un correo electrónico aumenta la compatibilidad con determinados repositorios de claves públicas.",
                    "keyPairName": "Apellido",
                    "keyPairEmail": "Email",

                    "keyValidity": {
                        "label" : "Período de validez",
                        "infinity" : "Infinito",
                        "oneWeek" : "Una semana",
                        "14Days" : "14 dias",
                        "30Days" : "30 dias",
                        "oneYear" : "1 año",
                        "twoYears" : "2 años",
                        "fiveYears" : "5 años",
                        "tenYears" : "10 años"
                    },

                    "keyPairPubKey": "Nueva clave pública",
                    "keyPairPubKeyCopy": "Copiar",
                    "keyPairPrivKey": "Nueva clave privada",
                    "keyPairPrivKeyCopy": "Copiar",
                    "keyPairRevCert": "Certificat de révocation",
                    "keyPairRevCertCopy": "Copiar",
                    "keyPairID": "Identificador de clave pública",
                    "keyPairIDHelp": "Una vez generada la identificación, anótala. Sin embargo, puedes encontrarlo en la pestaña",
                    "keyPairIDInfoKey": "Información. en una clave pública.",
                    "keyPairCreateYourKeys": "Crea tus claves",
                    "keyPairCancel": "Anular",
                },

                "keys": {
                    "title" : "Claves",
                    "introduction": `Envíe su clave privada y su clave pública a continuación para poder cifrar, descifrar, firmar o verificar una firma.
                    <br>Debes tener tu propio par de claves o generarlo en la pestaña`,
                    "createKeysTab": "Crear un par de claves",
                    "password": "Contraseña que protege su clave privada",
                    "pubKey": "Clave pública",
                    "pubKeyHelp": `Para el cifrado, proporcione la clave pública del destinatario (o su propia clave pública para un mensaje cifrado con su propia dirección). Para descifrar, proporcione la clave del remitente para verificar su firma (o su propia clave para verificar su propia firma).`,
                    "privKeyHelp": `Se necesita su clave privada para cifrar (firmar) y descifrar. Normalmente, una clave pública es suficiente para el cifrado. Sin embargo, se inserta automáticamente una firma en el momento del cifrado. Sin embargo, una firma sólo se realiza con su clave privada.`,
                    "pubKeyLoad": `Cargue la clave pública desde un archivo.`,
                    "privKey": "Mi clave privada",
                    "privKeyLoad": `Cargue su clave privada desde un archivo.`
                },

                "encrypt": {
                    "title": "Cifrado asim.",
                    "subtitle": "Firma incluida",
                    "introduction": `<p>
                                        Previamente indique las claves en la pestaña <i class="bi bi-key-fill"></i> Claves.
                                    </p>

                                    <p class="alert alert-warning">
                                        <i class="bi bi-exclamation-octagon"></i> Una clave pública suele ser suficiente para cifrar contenido textual. Sin embargo, se inserta automáticamente una firma en el momento del cifrado. Sin embargo, una firma sólo se realiza con su clave privada. Es por esto que también debes indicar tu clave privada en la pestaña "Claves".
                                    </p>`,
                    "contentToCrypt": "Contenido de texto a cifrar",
                    "contentDecrypted": "Contenido del texto después del cifrado",
                },

                "decrypt": {
                    "title": "Descifrado asimétrico",
                    "subtitle": `Verificación de firma incluida`,
                    "introduction": `<p>
                                        Previamente indique las claves en la pestaña <i class="bi bi-key-fill"></i> Claves.
                                    </p>
                                    <p class="alert alert-warning">
                                        <i class="bi bi-exclamation-octagon"></i> Una clave privada suele ser suficiente para descifrar contenido textual. Sin embargo, se insertó automáticamente una firma en el momento del cifrado. Sin embargo, una firma sólo se verifica con la clave pública del firmante, es decir, la de la persona que le envió este contenido cifrado o la suya si se trata de un contenido con su propia dirección. Es por esto que también debes indicar una clave pública en la pestaña "Claves" para descifrar este contenido.
                                    </p>`,
                    "contentToDecrypt": "Contenido del texto para descifrar",
                    "contentAfterBeingDecrypted": "Contenido del texto después del descifrado",
                    "verifySignature": "Verificación de firma",

                },

                "infoKey": {
                    "title": "Información sobre una clave pública",
                    "introduction": `Especifique una clave pública y descubra la información de esa clave.`,
                    "pubKey": "Clave pública",
                    "infoKeyField": "Información clave",
                    "discover": "Descubrir"
                },

                "revocationKey": {
                    "title": "Revocación de una clave pública",
                    "introduction": `
                    <p>
                        Indique la clave pública a revocar y el certificado de revocación que había conservado cuidadosamente.
                    </p>
                    <p>
                        Obtendrá una nueva clave pública revocada conservando el mismo identificador pero incorporando la información de revocación. Cárguelo en sus repositorios de claves públicas o privadas para reemplazar la versión anterior e informar a sus corresponsales que ya no deben usarlo. Si alguna vez volviera a hacer esto para cifrar un mensaje, se impedirían y aparecería una indicación indicando que esta clave pública está revocada. Sin embargo, podrán seguir utilizando esta versión revocada para verificar su firma en mensajes o documentos antiguos.
                    </p>`,
                    "pubKeyfield": "Clave pública para revocar",
                    "revocationCertificateField": "Certificado de revocación",
                    "newPubKeyRevokedField": "Clave pública revocada",
                    "revoke": "Revocar"
                }

            },

            "signature": {
                "header": "Establecer, propagar y verificar la confianza entre los miembros de una organización o entre organizaciones.",
                "menuExplanations": "Explicaciones",
                "menuSign": "Firmar",
                "menuVerifySignature": "Verificar una firma",
                "explanations": {
                    "title": "Explicaciones de firma criptográfica",
                    "introduction": `
                        <p>
                            La finalidad de una firma criptográfica es asegurar la identidad del autor de un documento (autenticidad) y que éste no ha sido modificado (integridad). Por otro lado, la firma no cuida la confidencialidad que es el negocio del cifrado.
                        </p>
                        <p>
                            Es posible firmar múltiples documentos digitales (foto, contrato, mensaje, clave pública, etc.)
                        </p>
                        <p>
                            Las firmas de clave pública se utilizan para dar fe de las relaciones de confianza. Estas firmas constituyen entonces una cadena o red de confianza.
                        </p>
                        <p>
                            De hecho, en una clave pública no hay necesariamente información como nombre, nombre, correo electrónico o incluso número de seguridad social que pueda vincular una clave pública con su verdadero "propietario". Técnicamente, esta información, por supuesto, se puede incluir y, a veces, se incluye. XENATX le permite hacer precisamente eso.
                        </p>
                        <p>
                            Sin embargo, incluso si esta información está integrada en la clave pública, no garantiza la identidad real del emisor de esta clave sin una verificación directa con esta persona cuyo nombre, nombre, etc. han sido insertados en esta clave pública. Cualquiera puede falsificar un nombre, apellido o incluso un correo electrónico y crear una clave pública con esta identidad.
                        </p>
                       <p>
                            Por lo tanto, firmar la clave pública de un individuo le permite certificar que el individuo que dice ser el propietario de esta clave es efectivamente el propietario. De hecho, lo habrá conocido y firmado su clave pública con su clave privada. Al firmar la clave pública de un individuo, uno se convierte en una especie de autoridad de certificación, como un sello en un pasaporte. De esta manera transmites confianza.
                        </p>

                        <p class="h4">
                            ¿Cómo funciona esto en la práctica?
                        </p>
                        <p class="alert alert-info">
                            <i class="bi bi-info-círculo"></i>
                            Tenga en cuenta que en los siguientes párrafos utilizaremos los términos pubKey (clave pública, clave pública), privKey (clave privada, clave privada) e IDPubKey (identificador de clave pública).
                        </p>
                        <p>
                            Durante una reunión, preferentemente física, dos miembros de una misma organización (o una familia, un club, un grupo de amigos, etc.) llamados M1 y M2 deciden santiguarse. En concreto, la clave privada de uno firmará la clave pública del otro y viceversa. Esta operación se realiza sin mostrar su clave privada al otro porque este último debe permanecer siempre secreto.
                        </p>
                        <p>
                            Lo ideal es que cada uno repatrie, sin contacto por seguridad (gracias a la generación de Códigos QR en XENATX - sección "Herramientas"), la clave pública del otro en su propio equipo, preferiblemente no conectado, y finalmente firme esta clave pública con su privado. llave.
                            <br>La firma realizada se "devuelve" al otro mediante una nueva generación de Códigos QR.
                        </p>
                        <p>
                            Los miembros M1 y M2 se demuestran así mutuamente como los verdaderos poseedores de su respectivo par de claves, compuestas por dos claves “gemelas”, la pública y la privada.
                        </p>
                        <p>
                            La firma de la pubKey1 del miembro M1 realizada con la privKey2 del firmante M2 demuestra así que este último conoce bien al M1 titular de esta pubKey1. E inversamente.
                            M1 y M2 forman entonces una cadena (o red) de confianza con dos eslabones. El vínculo entre estos dos vínculos corresponde a estos dos pares de claves firmadas juntas.
                            <br>Para otro miembro de la organización, ahora se debe confiar en estas dos firmas. Usted (M3) es otro miembro y no estuvo presente durante la reunión física de M1 con M2. Entonces no sabes si estas firmas son válidas. Debe verificarlos para asegurarse de que exista una cadena de confianza entre M1 y M2.
                        </p>
                        <p>
                            Por lo tanto, usted (M3) recuperará la pubKey1 de M1 porque conoce su IDPubKey1 gracias, por ejemplo, a un repositorio público o privado de claves públicas o simplemente gracias a un amigo en común, es decir, M2.
                            <br><i>XENATON proporciona un repositorio de claves públicas con su servicio en línea XenaTrust, pero hay muchos repositorios públicos mantenidos, por ejemplo, por universidades</i>.
                        </p>
                        <p>
                            Tú (M3) y M2 ya forman una cadena de confianza. Sin embargo, ha visto el nombre de M2 ​​o su IDPubKey2, en la lista de firmas de M1.
                        </p>
                        <p>
                            Copiando tanto la pubKey1 de M1, la firma como la pubKey2 de M2 ​​(si aún no la tienes dado que conoces M2) podrás verificar criptográficamente la validez de esta firma que tu amigo M2 hizo en la pubKey1 de M1.
                        </p>
                        <p>
                            El punto esencial a comprender es el siguiente: sólo pubKey2, hermana gemela de privKey2, habiendo realizado la firma de pubKey1, puede descifrar y por tanto verificar la validez de la firma establecida con esta llamada privKey2.
                        </p>
                        <p>
                            Técnicamente, una firma corresponde a la huella digital cifrada SHA-256 del documento. Esta huella digital SHA-256 del documento se cifra con la clave privada de la persona que firma.
                        </p>
                        <p>
                            Encontrará muchos recursos en Internet o en nuestra plataforma de aprendizaje XenaTraining para profundizar en el funcionamiento de la firma criptográfica basada en cifrado asimétrico y funciones hash matemáticas.
                        </p>
                        <p class="h4">
                            Ejemplo práctico
                        </p>
                        <p>
                            Aquí está la firma (que comienza con -----BEGIN PGP SIGNATURE-----) de la foto de abajo de una joven morena con un suéter rojo.
                            <br>A continuación encontrará la clave pública, la hermana "gemela" de la clave privada utilizada para realizar esta firma. Esta clave pública te permitirá verificar la validez de la firma de esta foto en la pestaña “Verificar una firma”.
                        </p>
                        <p>
                            <label class="control-label" for="signatureGirlRed">Firmando la foto de la chica de rojo</label>
                            <textarea id="signatureGirlRed" class="form-control" rows="7" cols="100" type="text">-----BEGIN PGP SIGNATURE-----

wnUEARYKAAYFAmQF5GIAIQkQInG4+5oONHwWIQSy1IOvxQwveTazar4icbj7
mg40fA0RAP9WeFvOrnDxE393rDuZzfuu37nSYcfm9cyReq3SMX0btgEAm/Hb
Ub62qULmGxaDj3TAg2/hq5mhGgXnqUTDcRKmNQw=
=HIKk
-----END PGP SIGNATURE-----</textarea>
                        </p>
                        <p class="h4">Foto - Propósito de la firma</p>
                        <p>
                            <img src="assets/images/signature/girl_red.jpg" alt="XENATON">
                        </p>

                        <br>
                        <p>
                            <label class="control-label" for="pubKeyGirlRed">Clave pública del firmante para verificación</label>
                            <textarea id="pubKeyGirlRed" class="form-control" rows="14" cols="100" type="text">
-----BEGIN PGP PUBLIC KEY BLOCK-----

xjMEZAXhvxYJKwYBBAHaRw8BAQdAekpBliuUeh/zxH5LdtEZm7dL9CkZ7SaP
7c7jJZ1K8+zNG0pvaG4gRG9lIDxqb2huQGV4YW1wbGUuY29tPsKMBBAWCgAd
BQJkBeG/BAsJBwgDFQgKBBYAAgECGQECGwMCHgEAIQkQInG4+5oONHwWIQSy
1IOvxQwveTazar4icbj7mg40fMUHAQDARa54M0QPcZLhtjYHn9ULamTvg2uL
XNBJ1ampVnNfXgD/U/FCsquv34Oth2mv6dkqJ4ViXiVK91vBUIpx/0Zc6QXO
OARkBeG/EgorBgEEAZdVAQUBAQdA6lkmut2gpU48IKSSqHrq+rbXjnO8Btfn
i7/ogWxMUAoDAQgHwngEGBYIAAkFAmQF4b8CGwwAIQkQInG4+5oONHwWIQSy
1IOvxQwveTazar4icbj7mg40fClgAP9GOMLfSS4XhHgbWRwKE3FfRjOlbBhE
m3eeKhcO8Uwe/gEAkqUCUb14GdqqqEMSg4/o6ybM/hkD8kTZ7jhnx08FYQ8=
=rhKn
-----END PGP PUBLIC KEY BLOCK-----</textarea>

                    </p>
                    <p class="alert alert-info">
                        <i class="bi bi-info-circle"></i>
                        Para verificar la firma con fines educativos, encontrará debajo la foto transformada al formato base64. De hecho, una fotografía, como cualquier archivo, es sólo una serie de 0 y 1 y, por lo tanto, puede adoptar esta forma textual después de la conversión.
                        <br>Para comprobarlo usted mismo, puede copiar todo el contenido textual a continuación y reconstruir la foto yendo a la sección "Herramientas", luego a la pestaña "Convertidores" y finalmente a "Formato de texto a imagen".
                    </p>
                    <p>
                        <label class="control-label" for="girlRed">Foto convertida a contenido de texto en formato base64.</label>
                        <textarea class="form-control" rows="10" cols="100" id="girlRed" type="text">/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAAAAAD/4QOBaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA3LjItYzAwMCA3OS4xYjY1YTc5YjQsIDIwMjIvMDYvMTMtMjI6MDE6MDEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9IkQ3RTRCMkY4RkIzRDEwQjUwMEI3MjEzN0Q0MjNFQjBEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkFCMzRCNzlEMTgzMzExRUQ5RTU0QTNFNkEwOTFFODUwIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkFCMzRCNzlDMTgzMzExRUQ5RTU0QTNFNkEwOTFFODUwIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCAyMDIyIE1hY2ludG9zaCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjY4NmVhMjEwLTVjMGItNDAxYS1iODgzLWY5ZGM4ZDk5NTBhYSIgc3RSZWY6ZG9jdW1lbnRJRD0iYWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOjFhZTUyMTAwLWE3NzktNWM0OS1iZTZhLWUxNTdlMTBlYzJmMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uACFBZG9iZQBkwAAAAAEDABADAgMGAAAMkwAAFXYAABlg/9sAhAAbGhopHSlBJiZBQi8vL0JHPz4+P0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHAR0pKTQmND8oKD9HPzU/R0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0dHR0f/wgARCAFdAWMDASIAAhEBAxEB/8QAqgAAAgMBAQAAAAAAAAAAAAAAAAECAwQFBgEBAQEBAQEAAAAAAAAAAAAAAAECAwQFEAACAgEEAgEEAwEBAQAAAAAAARECAxAhEgQgEzEwQCIUUDIFQRVgEQABAgQEBAQGAgMBAAAAAAABABEQITECIEFREjBAYXFQgaEi8JGx0TIDwWLhghOSEgAABQIFBQAAAAAAAAAAAAAAQAERIRBwIFBgMUHwUXGhMv/aAAwDAQACEQMRAAAA5ks0pdLzBreQNjxs1vGza8TN7wyNzws2wzVmyXLF6dvNDqPlzOjHmRTqHPsNqyBrWVGpZQ1LKGlZkaI0IvVCNCoRpMwYW3ESSqI0AmMQSIsk4MnbTpHVZnptIcYonKhGkomO/NoJwlTLMpaWlIXFLLCtFhWiwrRYVotKg6MuxKOJHt0HEp7PPMrnYtb02Lhj0HGGW5mNN7zCjRSRLAhXoZmjfWRkgtspuL6boGdWqWBbKKTQzMbtJyF3mnn13MhzVoqqBMPaObSmnVXLzed1ubLgvq01MmpqV0bc2NGjNqY3W94LL+lnXHu68zj19uqXg0dzPXGXVoTC7HZPRlssnGwlVq1S1z2TzYaZW6zCU3c5svSqXh4fRY864x1SX0LDpzjXZXm5Ob1uZnXKuplua40KNluK+XXytfNqWzL2jVeyWMZxWMZKIRmlgrBefzfRYE5Mq7N83Mmt/Rx9Ll0nN2EZs1zGG8qM0U06oZuY0EugH0xGuyvNq5XW5GdcqUH0zOIotspsVZNuIv8AQcLvS64yjLEZLGMkRUhYjSibTz2DrcrWNF+C+zp9Dg9Dn07F2PRjVxB6xMiayxGoIBARax7zGFkZaOP2+Ri8NkemZEXLbOuxbMtwWeh5HazpYNta5NWPDL3DBulcY886FfKvs6N2LQcvl9/z1zHXk0azosjKas2YFm9yzj6Ma6RinZqMs7m8hKxiE1g+mUpIq5XW5eNcGF0NyDk4cpJYQrZ6HbTfjfJwdrOvN1wrOnszbIoxdDkrBZ1Zv38PcunzPq/MXNFtM9Y26cWuVqSEBYnGEX7+N2JdVkLEYytTi9ZaIxDm7+XnXKU3qQLBQlAwyTl9Xbn0Z0UaY1kNBKWMuac+quaxzvlLXY2kPNek89WAa3zu2YNK7a51kFXBm+NYS7HD6MvYsy2y6Cgudssspb66s5Ll2ZqhIlqJTQqrqYy21a5vt6sunOpQUAz13Z10YZXrOjJJSxu52yW4iWV+c9DwTCk+nKV1NpvefSuSrRVcwJJI21h1NPDuzewcgOiZHWiuDsEywkpANkc+vKuCVbxv0HR856HOlj083HS/Zwuhq74Y6l6T5Tl0GKcz0p1WJX57sed3iDDfK2yu1qevFpFG+NxQr1ZQrkUq1FRYGlxZJpjknTZIGSRZtWWa5ZKvOrPU+W6uddbHsq59OZo6PK3rrauTXefW5leCVdKyzOmymOVzSHXi2nZOyqwtsqkvQKNmsUx0KzNHTBKI3ooLQjJTzokSobkKTkkXJrXl1VS4MurHLYAvp7uP2Oe7seyNvBr71WfRxOtdYw4WQcquBv4WohvfKIkWSgLfZmmujTGlOsY9+s1xtilMbolRMKpOUqm2DchNyESZTn05V5dVsZSKkvV35dXPe583WWpMTjBZ4ms6x8Xu8PpzacNYiSjZMGRYS7rcm1aujivTapRuYRsjUBhXJTGyQSGDbAbjPi6tC8XRZQsc2urPTr7uN3MbzRvUmSN9OdKamCnAycXtcbpzrB75pSSaad9C5LYWhoxbpbLst9bbHXcyjOKRAIyjOnJSG1IbTG0wKME7aeaq53koOWfU5cJ09Q/P9LPn2VtZiIqWVcsdlHLtq6ISia4TUomrfxNVlUWyG3NbNbE4nQWDal0QuYgEZxnTkpDlGldJyKZ26/Pxxd7IRbowITBJRFNiBi2/ETO6vKmbKxUkxIRsjeSTHOIK5slXKWyVYaL82hbI3Zk6jpuuUARnGVSlGuafNdD3KKAEDAlbUmgBRNCUlMxGMxJIQxIjSJMYipFzBTTnEExKdTLp0WL1aYC7boO4kIQnCYcy/E9pAU7JAwAwabTaGmAoCQTJENIhgkxEMSKmkipCRJJIRtV51qxMxsi0nZTcurVPLeWowidCUKzn1xU+g0JGJyDBoYKxNWIVoAAQTBAAAgAIaBMRAIhiJSQkxknF3XV1crq3zUFo5V5dPPdM8Wp6QCRtNptNsABoVgKAAgRgCGgAAAAEQAAIhgk4sghhuLWztcPq3F4i+Wvl9Tku0E1Ow05ZIGpSjJtAAArEDEAADENAAAAIJoAAAQGljGUXMAZGgl0OfsroCL4//9oACAECAAEFAP5OSft1pJJJJPjPhJOj1ggQ/JPRjei1ZGj8n5rRrSfrTrtotWpIfm/LY2EhohaLxga+nBGjIIF5PWdEvB6STJJImSLzjSCPB6PV+CX1no9Z0eiX2rKr7Jk6yV+yfjX7KCNULb7SCF/9Mvuf/9oACAEDAAEFAP5KCCPt3pBBBxOL8II8Y0XixeTU6ISGPwnReS83omNEfWjXfS2skoj6m5uNiZLPkt4yT9OSXoiSR/HitY8lpBEHESgaIHq/CdJJ8FotxH4iVTij40s/rLRCI5riKurcvV/WTdWsha86Wfk19REawW+xVSyh+Fvr1xNnBVEpdq8k6tC1gf00nZrEytVXRqRKNYIRAxqR/RrV2aoqLVawQQRq0OuqGo8MVeKb+nBBBBapxIgiR6JS5gn7CNEZFFjGt39i9EZVKMfgvrPW/wDU/9oACAEBAAEFAFYVjmczmczmczmczmK4rnM5nM5lsjK5YH2IF2JFmFmk9hzPYczmczmczmczmczmch2HY5HI5HI5HIVjkcjmczmew9h7D2HsFkPYew9h7D2Fsg8jYrJCzHtFnQ89Uv2LWPdYV5FZHOD2HsPYew9h7D2HsOZzOZzOZzOZz1kkkkk5HI5HI5HI5nMqmLYs5bbIhczmhZKnJEzo7Mx2bHceQ9h7D2HsPYczmczmczmczmczmQQQQQQR5pCUuYUwSULOXEj1UITGnZ44RasDX2H65+ufrjwFsUDUECqKknqHjg4MWORYmKqRcXwyuwvg4y2tFIkKqKwhPkWTGiCCCCCCDiziyCCCCBYUelFsJbEZMZkpAkVqVocThJ6RYoLUG95bGNCqOm3GU8Zeg1BuxIUFYK2VS1lZuu/EVRVOB6ymCSvWF1UPqot1kX65fHBBAqnEdS1TLUzVKoqiCCtStRqDJsnJDOLbWEWFi67ar19r4mWqWqOrOLIE2imRlWkNSJCQqioLHvjxlaCqcS1DJiMuIvjaOLEiBosjKjKipQjSghfm+y4OQnJhwOxTALCkcEOiLYky/Xkt1JLdQv1mi1bVE0zhBTINpiaKox4iuMWMrUSEiB1LUL4pMmA9GrLGRSdhQLYrc5juVucpK2hdiztdVk6+Hm8WJJRo9WiDiPGmZuvtfHxa2HXaraFuYaS8dStTiJEEaQNDqWoevVjLI7S3WkklWUsZcqorX3qoXTx7JD0fm6ydjAX2eLINQVUmLZ13VREeTQ0RqxljtfK8EVZm+KLlbG+duqvxHqyB+CL1ldvHDTgplKWRS0vHYo9l9CCNWMsdv5RJImIRk3rb8Vipxr1v6j+ihrbt/N0VY8bRjvepgzox5EyrORJJJJJJJOrGiyO2tySRCZVlvjHT2XxTkvirCyZK41fv8Sv+ljZXNW5y0kkeRIfZxox5qXGju1dXb8ivzibRx3hMqrVMXYTFcVjkcjkciSSfCCyO2iNUIWw90vxr/nY5Ezgk73Rlx0yD698bwZbMRZwZsthdfJkKdLGivTxlHah3qTS27+Sj5Kr5JSiYHaTHnaK50LMmew9hW8iYn5WO0tmhogSEtMtoVn+PQpGO2y7PfWJ/vOwu4jHl5GKsnHa62vFW82/7MFO2kVyK6zV5UyLSjh1sRySGhLdo5NCztH7BhtySF5WO18NDRAkJFlDvabP56yjG1JfrY279TEy/RohdSH16Oqfxb47Fbn6l8i/86x/590dTr3xGRbZlD+BGO2+LYezsyd2xsenWX4oRHgyzOzuoIIEhIv8AFVNlvbF/UaLY5HgTFgSK1gfxZF6SekVGKkCUFzsqLNFWVZjtJfcsx2ORyGyTq2/FMTJE9JGzJY7F9hIggRlf44/mm9uu5otWKBKSxA0QQRpY7O7tsyrMbhvdWLEkkknWybVuK5zEySR2M2QzXnRIjXO4rVRXrU536yaoiRstaBWeW6SRcqjt0tRUyKyT0Zdwsjbtk/sITK2mrUq6a8cd+Lx5hZT2CyHsHlRkzpGTJI9xIgggg7C/Fs6CnLg2UklrQZMsmCvEyPKh5ZVstjLkdqUfB1sSSZv63XFWctaVML2fxc4wQQRoruos7R+wfsj7DY8zHds+fGCDs/1kV3R9Dse5FnBmy8TDj5FcuNHtoNOwmzjJmxtGK8iemb+vZyKBCI2q4c8lZEHE4kEEEEaLxWsEHYU1/wCLc/zcnDNJY7Uox887xf5yY+gy/Sy1f6OWf1s1U+1mxnXyex1JO/k4YolPRblGLZ43KaHUdTicTiQQR9CCCCDP/RlXvydLY8iyVZkrN+z13jMH+nahTt43RpS8lFe/+nhVXkv2rYMaoJEn+lmm1Xoiu2jXJY7bpSOpxOI6nE4nE46L6Wfd3rGllK/y88odfzzYvZThxFixZU/86w+nCdKnV6yxVpXf4M+VY65Lu9tFpVlHDiHgy8hI4nEdR1OI6nEQhISEhIgSIIG4Epv2KxX5KsrZ4r4cqy1alo7OBt3lP2QJKz63Xgs5EoLODvdn2uCI0kkTG9uUrLTiYM+yh6NDQ0NECQkJCQkJEEEEGRbJHYtLmBraeS/zbPjS3LRlh0qyqgRBZwd3M+LIExvwrYrbbDlXG+P02w5OJ8jQ0NDRAkJCQkJEEEEEF1tf8TK98iiuJyo3/wA5vjW5S6sho4kCRe6qr5Hc7FfwYlKew9EfGiZiY6bUnHbBaU0NDQ0QJCQkJCRBBBBBkaqZE7J/nbJV2eJRY/y6xTLil0yXo6ditzkmSci2VIs3Zwdn+r+cbh2e730S2tpX5o4K2My/DC+Np0Y9EJCEtI8bUh9vI2lSFhok82J0txP83+lkOpasjdqntuLJZi3ILGfdX2bIk+D/ALhx801KktXgJlXtltOLA+T3oLcY9EIQvODN11kL9J1G7Ud7Ow1C6Wb13aIHUtUdRI4HwWMvxk/tMCGI6Kk7OPhka3zW5FWY9q5NqdVjUquzY9EIQvoNpGbsotbflJZy2dTuqJkksh1I1sdrJCsSKqZwIOlfhbu0V6D+K1Me67FIrgaRW+19tHohCF5TBfsKplztlskjciZXdvTD3L4jF3aZCTkNkjukZ+yql7uzZZCIGTBj7LVRqSqKWgpnTVsFjH2Ibc1W60QhaLWUi/YrUy9tstlbJ0qyyKuNHrTPegu9dH7zLdyzLZrWG51aPg5wOxI2Ji2JFucSnOhe1cy6+Vp1crRCEIRn7HqLdzIy2e9h2b+i/qNEHHRoXwnGtLGO0ldzs14Xw2laIQhGTMqma/JyT9y/BOBEmK8Wwnb3uqw50QhGXLwVryX+tH1YI0jROBWE0zDl4vLblmqLYkQi1uKyZOTbHo/sZ0gggggjSBogjT5EtlaBzZ07XErlrdSIR2sg2N/ZQR9CCNYIII0Rj2daVyVydbifssRMLJflaf4KCCNEdO8qx6VKM9+NGySf4VHWvxu9Edu238G/BFXDraaiOzabfwb8Uda00EZXNvJfwSOnbT/mT+3ihC/gUdP5P//aAAgBAgIGPwC1M4Y1m92f/9oACAEDAgY/ALUxhnQMuIUfXqrEo3HYQVdBsIo2ezARE5DdMhJkEiCjIILPypNqsV8l3opVaf/aAAgBAQEGPwDkawqpKcK+EaCM1VOvaPmmJUypeCME5w18F7B4EopsbeBHtDzRT9IMKRp4CykFMQfAyYRqqxcDnHRKlJV4rFU5x8gmUhB1PgSg1wT2pjypxNAkmS+PVdOLL5J8k3KHHKDZlPxHEGKeE6j6J+ZnQLpx2K+J/wCfqpIIshy7pszVB8+MU6nROJhSmEz+SbmHP42r6IBPcvwuTF7V7S+GZU7gmtIMHUkxW01yTiRXuHmE9pcJjXjntjcr4qt5huMzrBiE9hcJjWLBPcWU5pwGOq23z0KfRaFTquoU6iPVMeMe2JkAgEIbc9F9g6q3cL4Y4Oq9of6fNZf+l7gbfp84EdE6nDcE44D8E4ngFb2g5tD6pmHkpO3dPbdt7zCnOPtm/kvdd6SHkjO31XtuXvLwkukSOAOCcRgEO3CcYT3MeyBQOMcE4yUFb2454jci3RAGocYRYKZ4P+luVR0T4DBovzDJv6lF9cDBOntYjQoZJrQtuqbAeydPFk3MbrZEIvIiPVbim3BSuCcEEKo+cHwHstowOnCccxtNLw3nGVULbrmB+Sm9NUCLrgDrNMLnCE6p7b82kmv9w6pxQxOpknwMU3LGIuFQXQvFLg8AERbIXTHdN+0OJ+4VOkvQ/NWkkWkt7dCfiqDpiQCA89EGck7pCoaj90zNuoNAmGURYMplNgcJxVd+VFsXR/Ue9v8AMAmFRRFxutNdRq2h+qP/ADvHS25OGJ7r33WWDWpW2yZzu+2gW4/kR6ImBuNAjcanExTapjXlHOa84MULraiaF4oU8N/6/wAsxqvdayYKQcrdeG/r9/tg22/jb6nG6dC8UKa6mvJuiOqdOF1RGWGYHyUg2Ai3zON00Nt9CnE7bqfYr+p9OTJ1QC2wNwDjPVbrZjG5XRHG0WTVtNRotl1D8OmOXITTmQT5BSTLzRPVbrfbdr91MeY/kKRnphcwIxvhF2dlzeS6cg5mtohPNPAjBIwnwiMR6kLyXTkZFMVPKDGh4ssB0M06BGkHQGpTwbjzTWrca5JkBDZ+zyKccBs8FYMc1vFRBoNogRkmTJ+LNMFMqSeDxaoU5FSwsE+PbdO0+mBltuT/AKz5fZbb5FeXDniYqWKRU1SFeNNe0prwx1R/XdXLgtUqUlM8xLGxTZhOFbcOAwqnPgTwtClibPwWaGJyn8GFwqmvCe04No8HdOt1kiqZN/tB0T4Rth/s/pA+Gt4S6B8QPhRET4UYf//Z</textarea>
                    </p>`
                },
                
                "sign": {
                    "title": "Firmar",
                    "introduction": `
                       <p>
                            Proporcione su contraseña y clave privada a continuación antes de firmar cualquier clave pública, contenido de texto, huella digital de contenido, etc.
                            <br>Solo su clave privada puede firmar.
                        </p>
                    `,
                    "passwordPrivKey": `Contraseña que protege su clave privada`,
                    "yourPrivKey": "Tu clave privada",
                    "yourPrivKeyHelp": "Pegue su clave privada arriba o cárguela desde un archivo.",
                    "yourPrivKeyFromFile": "Su clave privada de un archivo.",
                    "contentToSign": "Contenido de texto para firmar",
                    "contentToSignHelp": "Pegue encima su contenido textual para firmar (texto, clave pública, huella digital, etc.)",
                },

                "verifySignature": {
                    "title": "Verificar una firma",
                    "introduction": `
                       <p>
                            Deberá indicar a continuación la clave pública del firmante, el contenido textual original que fue objeto de la firma y la firma a verificar.
                            <br>Recordatorio: el contenido textual original puede ser contenido textual simple o cifrado, una huella digital de contenido, una clave pública, una huella digital de clave pública, etc.
                        </p>
                        <p class="alert alert-info">
                            <i class="bi bi-info-círculo"></i>
                            Siempre es la clave pública del firmante la que se utiliza para verificar su firma realizada con su clave privada. De hecho, sólo la clave pública puede descifrar, así que verifique la firma realizada con su hermana “gemela”, la clave privada.
                        </p>
                    `,
                    "pubKeyField": `Clave pública del firmante`,
                    "pubKeyFieldHelp": `Pegue la clave pública del firmante arriba o cárguela desde un archivo.`,
                    "pubKeyFile": `Clave pública del firmante de un archivo.`,
                    "originalContent": "Contenido del texto original",
                    "originalContentHelp": "Pegue encima el contenido textual original que fue objeto de la firma (texto, clave pública, pie de imprenta, etc.)",
                    "signatureToCheck": `Firma para verificar`,
                    "signatureToCheckHelp": `Pegue la firma que desea verificar arriba.`,
                    "verifyBtn": `Controlar`
                }
            },

            "symCipher": {
                "header": "Cifrado seguro, aunque requiere intercambio de claves u operación en combinación con cifrado asimétrico.",
                "menuExplanations": "Explicaciones",
                "menuCreateKey": "Crear una clave",
                "menuEntropy": "Entropía de la sal",
                "menuCrypt": "Cifrado",
                "menuDecrypt": "Descifrado",
                "menuRecreateKey": "Recuperación de clave",

                "explanations": {
                    "introduction": `
                        <p>
                            Desde hace varios milenios, en diferentes formas, el llamado cifrado "simétrico" permite hacer incomprensible una cadena de caracteres o un archivo completo gracias a una clave de seguridad o una clave de cifrado (una especie de supercontraseña para resumir trivialmente).
                            <br>Un mensaje claro se convierte así en un mensaje cifrado, garantizando así el secreto durante la transmisión.
                            <br>En el cifrado simétrico, se utiliza la misma clave para cifrar y descifrar el mensaje, de ahí el nombre "simétrico". Esto requiere intercambiarlo previamente entre corresponsales. Esta es una limitación importante del cifrado simétrico porque este intercambio físico previo de la clave plantea problemas logísticos y de seguridad.
                        </p>
                        <p>
                            Existe otro tipo de cifrado más reciente: el cifrado simétrico, tema de una sección anterior, que suele utilizarse además del cifrado simétrico.
                        </p>
                        <p>
                            Entonces comience creando una clave usando un CSPRNG en la pestaña <mark><i class="bi bi-filetype-key"></i> Crear clave</mark>.
                            <br>Guarde la contraseña y el salt en un Keepass (software de código abierto de tipo seguro) o incluso en dos keepass diferentes, uno para las contraseñas y el otro para los salts. Lo ideal es no mantener escrita toda la contraseña, sino memorizar parte de ella.
                            <br>Conservar la contraseña y Salt es esencial. Sin estos dos datos, no podrá recrear la clave simétrica.
                        </p>
                        <p>
                            A menos que la clave se haya creado mediante un método manual, mantener la clave directamente, en lugar de la contraseña y el salt, es, por supuesto, posible, pero no recomendado. Su posible robo se facilitará y, por motivos técnicos relacionados con la función PBKDF2, será más difícil recrear la clave con un solo elemento como la contraseña o el salt.
                        </p>
                        <p>
                            El cifrado simétrico utilizado es AES en modo CBC (Cipher Block Chaining) con una clave de 256 bits y un vector de inicialización único de 128 bits para cada cifrado. Puede generar claves utilizando la función de derivación de claves PBKDF2 con una sal basada en parte en los movimientos del mouse. En todos los casos, tienes la opción de utilizar tus propias claves generadas con otro software y equipo o tirar los dados.
                        </p>
                        <p>
                            Ejemplo de mensaje para cifrar: « Tout organisme vivant subit des attaques. L’Homme aura toujours sa stratégie de défense à cacher. » (Traducción: Todo organismo vivo está sujeto a ataques. El hombre siempre tendrá su estrategia de defensa que esconder.)
                            <br>Después del cifrado con la clave 1E5A4797BCECAEB0926FEDDE2A10F28E6D6082BE1411E90D66AA79946908D1F7 obtendrá la siguiente cadena de caracteres:
                            <br>2uZ4je6Nnfd0t6aCwhhk7w==:fj5+3D0mITDRWKgD2xVUSQRmsXFlXZ7VryYVbCQRx6fqzQtKHrt+Xtx18Su6lSdB0mAPB81C7VPWhTpsn6gYmVM/Lx9AANz8xQaJ1OpMzmYv+X0BlT9/atjTDhkqzdWujYoQgsmTXAiedbz5pyU2fQ==
                        </p>
                        <p>
                            Si vuelve a cifrar el mismo mensaje con la misma clave, obtendrá un "cifrado" diferente, lo cual es normal y deseado:
                            <br>qB2+XhwK6AKT8lMd1JtGzA==:vG/xNCUFfhd3mXNQ7OCUuq1hWEbb9UsQkT/oT5rF7J0hpAJmmASXW4Atu680c1yQ9XWFkFgYPIbKMFKsox3a1SnM0B34XLym5A6N9I3ZCWc/Ja8ApjVdQoBH3Yow53YZD7ciVZPntwdfXsTfwr7hew==
                        </p>
                        <p>
                            Análisis de este "cifrado": los primeros caracteres hasta el signo de puntuación "dos puntos" [:] constituyen el Vector de Inicialización (IV - Vector de Inicialización) simplemente codificado en base64, es decir, esta cadena qB2+XhwK6AKT8lMd1JtGzA==
                            <br>Después del signo "dos puntos" que actúa como separador, encontrará el mensaje real cifrado y también codificado en base64, es decir, esta cadena vG/xNCUFfhd3mXNQ7OCUuq1hWEbb9UsQkT/oT5rF7J0hpAJmmASXW4Atu680c1yQ9XWFkFgYPIbKMFKsox3a1SnM0B34XLym5A6N9I3ZCWc/Ja8ApjVdQoBH3Yow53YZD7ciVZPntwdfXsTfwr7hew==
                            <br>El IV es necesario para decodificar este mensaje cifrado ubicado después de los "dos puntos". Por este motivo, se "incrusta" colocándolo antes del mensaje cifrado real. El IV cambia aleatoriamente con cada cifrado, lo que también hace que el mensaje cifrado cambie con cada cifrado. Esta técnica limita los ataques por texto elegido.
                        </p>

                        <p class="h4">Cifrado, transferencia de clave y texto cifrado</p>
                        <p>
                            Cifre su contenido textual con su clave simétrica. Vuelva a cifrar este contenido ya cifrado con la clave pública de su interlocutor (cifrado sim. junto con cifrado asim.). Recuerda guardar en un lugar seguro tu contenido cifrado simétricamente o mejor eliminarlo de tu equipo si lo tienes en mente. Opcionalmente, mantenga un hash SHA-512 del mensaje en texto claro.
                        </p>
                        <p>
                            Proporcione a su corresponsal la siguiente información a través de al menos tres servicios de mensajería cifrada diferentes, como Signal, Briar, Wire, Session, Olvid o Telegram<sup>*</sup>:
                            <ul>
                                <li>El código de mensaje para su recuperación en el DLB. <span class="text-muted">Después de haber depositado el mensaje en el DLB (Dead Letter Box) digital del servicio online de XenaTrust (o para configurarlo usted mismo) o incluso en un DLB físico.</span></li >
                                <li>La mitad de la contraseña. <span class="text-muted">La otra mitad se puede colocar encima del contenido de texto cifrado simétricamente antes del nuevo cifrado asimétrico.</span></li>
                                <li>Sal cortada por la mitad. <span class="text-muted">Utilice mensajes diferentes para cada mitad de la sal.</span></li>
                            </ul>
                            <i><sup>*</sup> Evite Telegram que no esté cifrado de extremo a extremo ni utilice sus mensajes efímeros. En cualquier caso, recuerda que ninguna de estas aplicaciones es realmente segura, de lo contrario no habríamos creado XENATX...</i>
                        </p>

                        <p>
                            Su corresponsal volverá aquí para reconstruir la clave simétrica a partir del salt y la contraseña. Luego recuperará el contenido textual que se descifrará en el DLB utilizando el código del mensaje. Primero lo descifrará asimétricamente por primera vez con su clave privada y luego una segunda vez simétricamente con la clave simétrica reconstituida.
                        </p>
                        <p class="h4">Directorio físico de claves simétricas</p>
                        <p>
                            El intercambio de la clave simétrica, simplemente transmitiendo el salt y la contraseña para luego reconstituir la clave, se puede realizar de forma relativamente segura utilizando tres sistemas de mensajería, como vimos anteriormente. Sin embargo, esto todavía conlleva muchos riesgos de interceptación, en particular debido a las vulnerabilidades de día cero en sus equipos conectados, incluso antes de la transmisión por mensajería.
                        </p>
                        <p>
                            Otra solución más segura es crear un directorio de claves compuesto por numerosas claves creadas previamente, idealmente mediante tiradas de dados, e intercambiar este directorio durante una reunión física o a través de un intermediario muy fiable. Las claves se colocan en microSD, llave USB o incluso se imprimen en hojas con códigos QR para una fácil recuperación digital. También puedes escribirlos a mano en mini hojas de papel.
                        </p> 
                        <p>
                            Con un directorio de claves, sólo la referencia de la clave simétrica n°A2, A3, A4, etc. debe ser comunicado a su corresponsal que tiene doble este directorio. Por lo tanto, un atacante no tiene idea de la clave utilizada porque sólo su referencia pasa a través de la red, y aun así no siempre, porque la mayoría de las veces colocará esta referencia encima del contenido textual cifrado simétricamente. Luego todo se vuelve a cifrar de forma asimétrica.
                            <br>Solución alternativa: utilice las claves del directorio en orden. Una clave que sólo se utiliza una vez (al menos es la más segura) o por un tiempo limitado. Así que no hay nada especial que comunicarle a su destinatario.
                        </p>
                        <p>
                            Para proteger aún más este directorio y para las claves creadas mediante el generador de claves tipo XENATX CSPRNG, solo puede indicar las contraseñas en lugar de las claves. Luego se podría comunicar, siempre de forma segura, idealmente en mano, una sal común (o varias) almacenada en otro soporte, o incluso en papel con su posible traducción a un Código QR para su fácil recuperación.
                            <br>Para encontrar la clave de referencia A3, por ejemplo, bastará con recrearla a través de la pestaña "Reconstitución de clave" utilizando la contraseña n°A3 y la sal común (o si la sal no es común, con la sal también indicada como A3).
                        </p>
                        <p>
                            Por razones de seguridad, la creación de estos directorios de claves debe realizarse en equipos denominados OFF (desconectados), es decir, no conectados a Internet y nunca reconectados.
                        </p>

                        <p class="h4">Código de depósito y mensaje</p>
                        <p>
                            Cuando utilice el servicio en línea de XenaTrust, simplemente coloque su mensaje cifrado en el DLB digital. El mensaje también se puede depositar en un DLB físico. A continuación, aplica los medios discretos y originales, acordados de antemano en el directorio de esta clave específica, para comunicar discretamente a su interlocutor que debe tomar nota de un mensaje.
                        </p>
                        <p>
                            También se puede utilizar un código de mensaje, acordado de antemano como la palabra común "tren", en lugar de, por ejemplo, X4V2Z, para introducirlo más discretamente en la conversación para indicar que se debe enviar un mensaje con este código, u otro indicado en el directorio. anotado. La forma más discreta es la que tú inventas...
                        </p>`
                },

                "createKey": {
                    "title": "Crear una clave simétrica",
                    "introduction": `
                        <p>
                            Hay varias soluciones, que se detallan a continuación, disponibles para crear una clave AES de 256 bits.
                        </p>
                        <ul>
                            <li>
                                Para crear una clave computacionalmente usando un CSPRNG (generador de números pseudoaleatorios criptográficos seguros), consulte "Creación de una clave con un CSPRNG" a continuación. TENGA EN CUENTA que el método más seguro es la creación tirando los dados, con la ventaja de que su creación no deja rastro en la memoria de su equipo... De hecho, los rastros pueden persistir incluso después de transferirlos a la microSD y borrarlos. Entonces, con el método de tirar los dados, siempre que no uses la clave de un equipo para cifrar o descifrar, esa clave no tiene existencia en la computadora. Su robo se vuelve difícil para un atacante, a menos que encuentre su caché de claves "en papel" en casa o en otro lugar.
                            </li>
                            <li>
                                Para crear una clave verdaderamente aleatoria con dados (de precisión), consulte la explicación completa "Creación de una clave con dados" a continuación.
                            </li>
                            <li>
                                Para convertir automáticamente su clave de formato binario a hexadecimal, utilice el "Convertidor de binario a hexadecimal" que aparece a continuación.
                            </li>
                            <li>
                                Para convertir manualmente su clave de formato binario a hexadecimal, utilice la "Tabla de conversión de binario a hexadecimal" a continuación.
                            </li>
                        </ul>
                    `,

                    "csprng": {
                        "title": `Creando una clave simétrica con un CSPRNG`,
                        "paragraph01": `Cree una sal de antemano en la pestaña <strong><i class="bi bi-infinity"></i> Entropía de sal</strong>. También puedes suministrar tu propia sal o incluso utilizar tu propia llave. En cualquier caso, el salt se integrará con su contraseña en la función de derivación de clave PBKDF2 con 100.000 iteraciones. Ojo, en un ordenador de bajo consumo como un Raspberry Pi 2 por ejemplo, generar la clave puede tardar varios minutos...
                        <br>Por supuesto, para una clave generada por tiradas de dados, por ejemplo, u otro TRNG (True Random Number Generator), no es necesario crear una sal.`,

                        "passwordField": "Contraseña: frase de contraseña para crear su clave",
                        "passwordFieldHelp": "personaje. - Al menos 40 caracteres incluidos los especiales. Tenga en cuenta que esta contraseña - frase secreta no le permitirá encontrar la clave simétrica si no ha memorizado también la sal final.",
                        "saltField": `Sal final`,
                        "saltFieldHelp": `Tenga en cuenta esta sal que generó en la pestaña "Entropía de sal" o usted mismo. Es tan esencial como su contraseña: frase secreta para reconstituir su clave en caso de pérdida.`,
                        "symKeyField": "Clave simétrica",
                        "symKeyFieldHelp": "Recordatorio: una clave AES de 256 bits corresponde a 64 caracteres hexadecimales.",
                        "createKeyBtn": "Crea la clave"
                    },

                    "keyWithDices" : {
                        "title" : `Explicaciones para crear una clave con dados.`,
                        "text" : `
                            <p>
                                Puedes crear una clave mediante lanzamientos sucesivos de dados, pero ten cuidado de utilizar dados equilibrados y, por tanto, precisos (dados de casino o de backgammon).
                            </p>
                            <p>
                                Creará una clave binaria de 256 bits, es decir, 256 ceros o unos. Después de agitarlo bien en la taza o en la mano, lanza el dado y observa el resultado. Cualquier tirada menor o igual a 3 se puntuará con 0 y cualquier tirada mayor o igual a 4 se puntuará con 1.
                            </p>
                            <p>
                                Para acelerar la creación de una clave y reducir a la mitad el número de tiradas necesarias (128 en lugar de 256), puedes utilizar dos dados de diferentes colores. Tira ambos dados al mismo tiempo y anota siempre los números IMPERATIVAMENTE en el mismo orden de colores. Entonces, si ambos dados son azules y rojos, entonces elige un orden de lectura y respétalo escrupulosamente. Por ejemplo leyendo azul y luego rojo.
                            </p>
                            <p>
                                Ejemplo:
                                <br>1<sup>er</sup> tira dos dados: 5 para el azul y 2 para el rojo por lo que notaremos: 10
                                <br>2<sup>e</sup> tira dos dados: 6 para el azul y 4 para el rojo por lo que notaremos: 11
                                <br>Los primeros 4 bits de la clave serán, por tanto, 1011 (estos 4 bits se traducirán posteriormente al hexadecimal por la letra B)
                                <br>Continúa tus lanzamientos hasta obtener 256 bits.
                                <br>Luego convierta estos 256 bits en hexadecimales de 4 bits por 4 bits usando la "tabla de conversión de binario a hexadecimal" o use el convertidor automático. El objetivo de esta conversión es poder utilizar su clave en las pestañas Cifrado o Descifrado que solo aceptan claves en formato hexadecimal.
                            </p>
                            <p>
                                Nada muy complicado pero intentalo mucho para entenderlo.
                            </p>
                        `
                    },
                    
                    "autoConvertBinToHex" : {
                        "title" : "Convertidor automático de binario a hexadecimal",
                        "binKeyField" : "Clave en formato binario (01001..)",
                        "keyConvertedToHex" : "Clave convertida a hexadecimal",
                        "keyConvertedToHexHelp" : "Su clave hexadecimal tendrá 64 caracteres.",
                        "convertToHexBtn" : "Convertir a hexadecimal",
                    },
                    "manualConversionBinToHex" : {
                        "title" : "Tabla de conversión de binario a hexadecimal",
                        "introduction" : `Para una mejor comprensión, consulte el tema anterior "Explicaciones sobre la creación de una clave con dados" antes de utilizar esta tabla.`,
                        "binary" : "Binario",
                        "hexadecimal" : "Hexadecimal",
                    } 
                },

                "entropy" : {
                    "title" : "Entropía de la sal",
                    "text" : `
                        <p>
                            El objetivo de esta característica es crear una sal final más aleatoria mezclando dos sales: una sal intermedia generada por los movimientos del mouse y otra sal, no mostrada, generada por una función matemática. Esta funcionalidad sólo es útil en el caso de crear una clave simétrica con un CSPRNG. Es inútil si creas tu clave simétrica con tiradas de dados.
                        </p>
                        <p class="alert alert-info">
                            <i class="bi bi-info-círculo"></i>
                            Mueva el mouse lo más aleatoriamente posible dentro del marco hasta que la barra de progreso, ubicada debajo del marco, alcance el 100%. Se creará la sal intermedia. Luego haga clic en "Crear sal final" para generar la sal final para copiar en la pestaña "Crear una clave" y la sección "Creación de una clave simétrica con un CSPRNG".
                        </p>`,
                    "intermediateSaltField" : "Sal intermedia",
                    "finalSaltField" : "Sal final",
                    "finalSaltFieldHelp" : `Copie esta sal final en el campo "Sal final" en la pestaña "Crear clave".`,
                    "createFinalSaltBtn" : "Crea la sal final"
                },

                "encryption" : {
                    "title" : "Cifrado sim",
                    "paragraph01" : `Lea las explicaciones en la primera pestaña "Explicaciones" si no está familiarizado con el cifrado simétrico.`,
                    "symKeyField" : "Tu clave simétrica",
                    "symKeyFieldHelp" : "64 caracteres hexadecimales.",
                    "contentToEncrypt" : "Contenido de texto a cifrar",
                    "contentEncrypted" : "Contenido del texto después del cifrado",
                },

                "decryption" : {
                    "title" : "Descifrado de símbolos",
                    "symKeyField" : "Tu clave simétrica",
                    "symKeyFieldHelp" : "64 caracteres hexadecimales.",
                    "contentToDecrypt" : "Contenido del texto para descifrar",
                    "contentDecrypted" : "Contenido del texto después del descifrado",
                },

                "findBackSymKey" : {
                    "title" : "Recuperación de claves",
                    "introduction" : `
                        <p>
                            Para reconstruir una clave simétrica creada con CSPRNG, consulte la pestaña "Crear una clave". Debe proporcionar la contraseña o frase de contraseña y el salt que haya almacenado de forma segura y quizás memorizado parcialmente. Sin estos dos elementos no es posible la reconstrucción.
                        </p>`,
                    "passwordField" : "Contraseña - frase de contraseña",
                    "saltField" : "Sal",
                    "backKeyField" : "Clave restaurada",
                    "backKeyBtn" : "Restaurar la clave"
                },

                "symFile" : {
                    "title" : "Archivo - Cifrado-Descifrado",
                    "symKeyField" : "Tu clave simétrica",
                    "symKeyFieldHelp" : "64 caracteres hexadecimales.",

                    "encryption" : "Cifrado",
                    "encryption-introduction" : `Seleccione un archivo y se cifrará automáticamente en AES-256 - Modo CBC - Conversión aleatoria IV y base64. Se abrirá una ventana y se le preguntará dónde guardar el archivo cifrado. El sufijo .enc se agregará al nombre del archivo original.`,
                    "fileToEncrypt" : "Archivo para cifrar",

                    "decryption" : "Descifrado",
                    "decryption-introduction" : `Seleccione un archivo cifrado y se descifrará automáticamente. Se abrirá una ventana y se le preguntará dónde guardar el archivo descifrado.
                    <br>El archivo debe haber sido cifrado por XENATX en AES-256, y no por otro software, debido a las características específicas de la implementación, en particular el método de inclusión del IV y la conversión a base64.`,
                    "fileToDecrypt" : "Archivo para descifrar",
                    "fileToDecryptFiedHelp" : "El archivo a descifrar debe tener el sufijo .enc"
                }
            },

            "otp" : {
                "introduction": `El cifrado simétrico, conocido como "perfecto", porque es indescifrable, suele utilizarse además para pasajes sensibles de un mensaje.`,
                "menuExplanations": "Explicaciones",
                "menuConverter": "Convertidor",
                "menuEncryption": "Cifrado digital",
                "menuDecryption": "Descifrado digital",
                "menuHic" : "HIC - Hidden In the Crowd",

                "explanations" : {
                    "general" : {
                        "tabTitle" : "General",
                        "title" : "Información general sobre la OTP",
                        "text" : `
                            <p>
                                A lo largo de esta sección, titulada “OTP – Máscara de un solo uso”, los siguientes términos significan lo mismo y, por lo tanto, se usan indistintamente:
                                <ul>
                                    <li>Máscara desechable</li>
                                    <li>Máscara</li>
                                    <li>Clave o libreta de un solo uso</li>
                                    <li>Clave de cifrado Vernam</li>
                                    <li>Clave OTP</li>
                                    <li>Clave</li>
                                    <li>OTP</li>
                                    <li>Bloc de notas (clave OTP almacenado en papel)</li>
                                </ul>
                            </p>
                            <p>
                                La OTP (One Time Pad), o clave OTP, se puede comparar con una clave simétrica en la que cada carácter de la clave solo se usaría una vez. La longitud de la clave es al menos igual a la longitud del mensaje.
                                <br>El argumento teórico es el siguiente: siendo todas las claves igualmente probables, todos los textos claro de esta longitud son igualmente posibles y con la misma probabilidad. Conociendo sólo el texto cifrado, no hay forma de distinguir, entre estos posibles textos claros, el texto claro original. Los análisis estadísticos, como el análisis de frecuencia, son inútiles.
                            </p>
                            <p>
                                OTP es el único cifrado que, en teoría, es 100% seguro, independientemente de la potencia informática involucrada, incluso cuántica. Por otro lado, a diferencia del cifrado asimétrico, las OTP deben intercambiarse con antelación.
                                <br>Cada OTP solo debe usarse una vez. La seguridad de estos intercambios físicos de OTP y su almacenamiento en espera de su uso es esencial. Esto requiere una organización rigurosa. La verdadera seguridad tiene sus requisitos. Es esto o nada, y nada no es una opción...
                            </p>
                            <p>
                                Otra ventaja del cifrado OTP es que se basa en la función "OR exclusivo" (XOR). Debido a su gran simplicidad, esta función es difícil de debilitar mediante malware u otra manipulación del hardware, sin que sea visible. Por lo tanto, incluso en equipos infestados, por supuesto no conectados para evitar fugas de datos como la propia OTP, el cifrado se realiza correctamente.
                            </p>
                            <p>
                                Más información: <a href="https://es.wikipedia.org/wiki/Libreta_de_un_solo_uso" target="_blank">https://es.wikipedia.org/wiki/Libreta_de_un_solo_uso</a>
                            </p>
                           <p>
                                La característica inquebrantable del cifrado OTP se basa en la entropía real, y por lo tanto en la naturaleza verdaderamente aleatoria, de la clave OTP.
                                <br>Cuanto más encuentre una fuente capaz de generar una OTP con una entropía muy alta y, por lo tanto, muy aleatoria, mejor será su OTP. Por lo tanto, puede generarlo en otro lugar que no sea XENATX y volver a usarlo aquí.
                                <br>Desafortunadamente, ningún dispositivo de consumo puede crear OTP fácilmente. Por lo tanto, dependerá del fabricante profesional TRNG (True Random Number Generator). Además de la dificultad de tener acceso a este tipo de material, ¿estará seguro de que este material sea confiable y no sea mal utilizado por las agencias de inteligencia como lo fue el fabricante suizo Crypto AG - Hagelin hasta 2020?
                            </p>
                            <p>
                                Solo utilizamos cifrado OTP digital en XENATX, no cifrado OTP alfabético, que también es posible. La razón: es más fácil de implementar y aprender.
                            </p>
                            <p>
                                Al igual que con otras técnicas de cifrado, una OTP, por supuesto, solo debe crearse y usarse en un XENATOFF, o equivalente, para evitar vulnerabilidades de software y hardware de un dispositivo conectado. Todo el cifrado se puede realizar incluso con sólo papel y lápiz… Esta es otra de las grandes ventajas del cifrado OTP.
                            </p>
                            <p>
                                Para una seguridad muy alta, se debe preferir esta última solución con papel y lápiz. Por lo tanto, no queda ningún rastro en la memoria de ninguna computadora.
                                <br>Usar la tecla "Borrar" no es suficiente. La memoria de su equipo informático podría ser inspeccionada sin su conocimiento después del robo de su equipo, o durante una simple visita a su domicilio, sin robo, por lo tanto sin que usted necesariamente se dé cuenta.
                            </p>
                            <p>
                                Las explicaciones sobre cómo generar una OTP con dos dados de precisión y las técnicas de cifrado y descifrado manual con OTP se pueden encontrar en las secciones respectivas a continuación <span style="color: rgb(11, 110, 253);" ><i class="bi bi -bullseye"></i>&nbsp;Entropía: creación de una OTP</span> y <span style="color: rgb(11, 110, 253);"><i class="bi bi-key-fill" ></i>&nbsp;Cifrado - Descifrado</span>.
                            </p>
                            `,
                    },

                    "entropy" : {
                        "title" : "Entropía: creación de una OTP",
                        "text" : `
                            <div class="alert alert-info">
                               <p>
                                    <i class="bi bi-info-circle"></i> Encuentre, o incluso cree, un verdadero generador de números aleatorios (TRNG - True Random Number Generator) o incluso un muy buen CSPRNG (Generador de números pseudoaleatorios criptográficamente seguro) Generar una OTP que sea lo más entrópica posible no es algo fácil.
                                </p>
                                <p>
                                    En cualquier caso, nunca utilice una OTP generada de forma remota, incluso si los intercambios en la red están protegidos por TLS/SSL. Por lo tanto, se deben evitar todos los sitios web que ofrecen los llamados números seguros porque se basan en fenómenos cuánticos aleatorios.
                                </p>
                                <hora>
                                <p>
                                    <i class="bi bi-dice-4"></i> Si no es un experto en criptografía, limítese a generar sus OTP mediante tiradas de dados. En términos de simplicidad y eficiencia, no tiene rival, a costa, por supuesto, de lentitud. A continuación encontrará las explicaciones esenciales.
                                </p>
                                <p>
                                    Generar una OTP requiere tiempo mediante tiradas de dados. Por lo que no podrás tener muchas claves por adelantado. Generar lleva demasiado tiempo. Por lo tanto, sólo podrá intercambiar mensajes relativamente cortos. Sin embargo, también puedes utilizarlo como cifrado complementario, al cifrado AES-256 por ejemplo, para determinadas partes breves pero sensibles de un mensaje como el lugar de reunión, los nombres, las horas, etc.
                                </p>
                               <p>
                                    Puedes comprobar y completar desde los siguientes sitios <i>(no recibimos ninguna comisión...)</i>:
                                </p>
                                <ul>
                                    <li>https://www.ciphermachinesandcryptology.com/en/onetimepad.htm</li>
                                    <li>https://dicekeys.com/ or https://www.schneier.com/blog/archives/2020/08/dicekeys.html</li>
                                </ul>
                            </div>
                            <p class="h4">
                                ¿Cómo generar una máscara (OTP) con dos dados?
                            </p>
                            <p>
                                Lo ideal es llevar los siguientes artículos:
                                <ul>
                                    <li>Dados de precisión de seis caras, como los dados de casino o de backgammon.</li>
                                    <li>Un vaso de cuero para agitar bien los dos dados, sin dañarlos, antes de tirarlos. </li>
                                    <li>Una alfombra verde para recibir los dados.</li>
                                    <li>La "tabla de dados", a continuación, para convertir los resultados en una serie de números del 0 al 9.</li>
                                </ul>
                            </p>

                            <p>
                                La técnica para crear una clave, compuesta exclusivamente por números del 0 al 9 inclusive, consiste en asignar un valor a cada una de las 36 combinaciones posibles con dos dados de distintos colores, teniendo en cuenta el orden y color de los dados (ver apartado " Tabla de dados").
                                Para el ejemplo diremos que utilizamos un dado negro y un dado blanco, aunque son preferibles los dados transparentes y de colores porque es más comprobable su "buena fabricación".
                                <br>Arbitrariamente iniciaremos la lectura siempre con el dado negro.
                                <br>En la tabla de dados tenemos tres series de valores entre 0 y 9. De esta forma cada combinación tiene una probabilidad de 1/30. Las 6 combinaciones restantes, que comienzan con un 6, no garantizan una nueva serie completa del 0 al 9. Como resultado, todas las tiradas que comienzan con un 6 (negro) simplemente se ignoran. Esto no afecta la probabilidad de otras combinaciones.
                            </p>

                            <p class="h4">Poner en práctica</p>
                            <p>
                                <strong>Primer lanzamiento:</strong>
                                <br>Lanzamos los dos dados por primera vez y obtenemos el siguiente resultado:
                                <br>3 para el negro y 5 para el blanco, entonces leemos 35 (recordatorio: hemos elegido arbitrariamente comenzar siempre a leer el número con negro, que por lo tanto se convierte en la cifra de las decenas).
                                <br>En la tabla de dados, el número 35 corresponde a 6. Por tanto, el primer dígito de la clave OTP digital será 6.
                            </p>
                           <p>
                                <strong>Segundo lanzamiento:</strong>
                                <br>6 para negro y 2 para blanco. Ignoramos esta tirada porque el número de decenas, por lo tanto en el dado negro, es un 6 y todos los números que comienzan con un 6 deben ignorarse.
                            </p>
                            <p>
                                <strong>Tercer lanzamiento:</strong>
                                <br>2 para negro y 3 para blanco. Entonces 23, que corresponde en la tabla a 8. Por lo tanto, el segundo dígito de la clave OTP será 8.
                                <br>Y así sucesivamente, hasta alcanzar la longitud de clave deseada. Recordatorio: los primeros 5 dígitos extraídos al azar serán el identificador de la clave y aún no la clave en sí.
                            </p>
                            <p>
                                Encontrará explicaciones sobre el directorio y las técnicas de almacenamiento y cifrado-descifrado en las secciones respectivas a continuación <span style="color: rgb(11, 110, 253);"><i class="bi bi-book"></ i>&nbsp;Directorio y almacenamiento</span> y <span style="color: rgb(11, 110, 253);"><i class="bi bi-key-fill"></ i>&nbsp;Cifrado - Descifrado</span>.
                            </p>`,
                    },

                    "diceTable" : {
                        "title" : "Mesa de dados",
                        "introduction" : `
                            <p>
                                Usa dos dados de diferentes colores. Lo importante es diferenciarlos fácilmente: lee las explicaciones para crear una clave usando dados en la sección "Entropía - Creación de una OTP" más arriba.
                                <br>Indicamos dados blancos y negros en la siguiente tabla, sin embargo lo mejor son dos colores diferentes y al mismo tiempo transparentes para comprobar visualmente que ninguna "impureza" desequilibra estos dados de precisión (casino o backgammon)
                            </p>
                        `,
                        "image" : `<img class="code__table" src="assets/images/otp/dice_fr.png" alt="XENATON">`
                    },

                    "randomOtp" : {
                        "title" : 'Capacitación: OTP pseudoaleatoria',
                        "alert" : `Tenga en cuenta que estas OTP de capacitación NO SON ABSOLUTAMENTE válidas para su uso en una situación real. ¡La entropía, y por tanto la aleatoriedad, es demasiado baja!`,

                        "quantityField" : "Duración de la OTP",
                        "otpField" : "OTP",
                        "createOtpField" : "Crear la OTP"
                    },

                    "directoryAndStorage" : {
                        "tabTitle": "Directorio y almacenamiento",
                        "title": "Directorio OTP",
                        "subtitle": "Almacenado, por ejemplo, en una tarjeta microSD o en papel",
                        "paragraph01": `<p>
                                    Para realizar el cifrado OTP, necesitamos una clave, llamada clave de un solo uso u OTP. Esta clave debe intercambiarse previamente, crearemos de antemano varias claves que se intercambiarán durante un único intercambio físico para ahorrar en reuniones que muchas veces son difíciles de organizar.
                                </p>
                                <p>
                                    Un directorio/bloc de notas (el Bloc de notas en One-Time-Pad) puede ser una minihoja única, varias minihojas grapadas, un microfilm, etc. En todos los casos, un conjunto de directorios OTP consta de dos directorios idénticos (pero invertidos), un directorio llamado OUT y otro llamado IN.
                                </p>
                                <p>
                                    Una hoja de directorio OTP estándar, o clave OTP, contiene aproximadamente 250 dígitos en grupos de cinco dígitos, lo que es suficiente para un mensaje de aproximadamente 180 caracteres. El primer grupo de cinco números de cada hoja sirve como referencia clave y debe ser único para ese conjunto de hojas en particular. Todos los números en cada hoja deben ser verdaderamente aleatorios (consulte Entropía - Creación de una OTP). La aleatoriedad es una parte esencial de la seguridad del proceso de cifrado.
                                    <br>Dado que cualquier hoja (o clave almacenada digitalmente) iniciada debe destruirse, es mejor crear claves más cortas de cien dígitos o menos. La duración depende únicamente de su elección personal.
                                    <br>Si el mensaje es más largo que la clave, se utilizará la siguiente clave del directorio (sin los primeros 5 dígitos, que sirven como identificadores, de esta siguiente clave) y no es necesario especificarla.
                                    <br>No obstante, el siguiente identificador de clave a utilizar se puede indicar al final de la primera parte del mensaje cifrado para que sea lo más claro posible.
                                </p>
                                <p class="alert alert-danger">
                                    <i class="bi bi-exclamation-octagon"></i> Para establecer comunicaciones bidireccionales, necesita dos conjuntos diferentes de directorios: la persona A tiene un directorio OUT del cual la persona B tiene la copia IN y la persona B tiene otro directorio FUERA del cual la persona A tiene la copia ENTRADA.
                                    <br>
                                    <br>
                                    Nunca utilice un solo directorio para comunicarse en ambas direcciones para evitar el riesgo de uso simultáneo de la misma clave OTP en el caso, por ejemplo, de que las personas A y B necesiten escribir en la dirección l 'entre sí en el Mismo tiempo. Cifrar ambos mensajes con la misma clave permitiría potencialmente descifrar los dos mensajes interceptados, ¡incluso sin conocer la clave!
                                </p>
                                <p>
                                    Es posible utilizar varias copias IN de un directorio para permitir que más de una persona reciba un mensaje, pero no se recomienda esta práctica. Las copias múltiples plantean riesgos de seguridad adicionales y sólo deben usarse en un entorno muy estrictamente controlado. Esta práctica también aumenta el riesgo de que se produzcan copias no destruidas de un directorio después del descifrado, con posible interceptación posterior.
                                    <br>En cualquier caso, nunca utilice varias copias OUT de un directorio, ya que esto inevitablemente dará como resultado el uso simultáneo de la misma clave OTP y, por lo tanto, un posible descifrado por parte de un atacante sin conocer la clave.
                                </p>
                                <p>
                                    Los directorios de OTP se intercambiarán durante las reuniones físicas. Lo ideal es que cada directorio esté cifrado y duplicado en una segunda microSD de respaldo, almacenado en un "caché" diferente. Ojo con perder la clave de protección de directorios AES-256 y no olvidar las ubicaciones de almacenamiento de todos estos elementos... Organización y rigor...
                                </p>
                                <p class="h4">
                                    Ejemplo de hojas de directorio OTP en papel
                                </p>
                                <p>
                                    Estas hojas de 120 dígitos, incluidos 5 identificadores, se pueden obtener fácilmente dividiendo una hoja de bloque RHODIA n°12 en 4 y utilizando un rotulador de 0,5 minas.
                                    <br>Luego, engrape las hojas y tendrá sus directorios OTP "de inicio" listos para intercambiar.
                                </p>
                                <p>
                                    A continuación se ve la correspondencia entre la tecla OUT y la tecla IN. Por lo tanto, la primera clave OUT 01 (para escribir y cifrar) se entrega al corresponsal A y la clave IN 01 (para recibir y descifrar) se entrega al corresponsal B.
                                </p>
                        `,

                        "paragraph02" : `
                            <i class="bi bi-exclamation-octagon"></i> Tenga en cuenta que la tarjeta microSD utilizada para el almacenamiento digital de OTP y que se habrá insertado periódicamente en su XenatOFF, NUNCA debe insertarse en otro equipo. Después de usar todas las OTP en esta microSD, será necesario destruir (quemar) la tarjeta o posiblemente mantenerla a salvo para nuevos descifrados posteriores de mensajes antiguos (no recomendado).
                            <hora>
                            <i class="bi bi-exclamation-octagon"></i> Esta microSD podrá "recargarse" en OTP desde "externamente" a través de su XenatOFF utilizando exclusivamente los canales seguros de XENATX.
                            <br>Sin embargo, lo ideal es que ninguna OTP nueva pase por su XenatOFF antes de su uso real. De hecho, los rastros de OTP que pasaron por su XenatOFF al recargar su microSD podrían persistir en el disco duro o incluso en la memoria. La recuperación por parte de un atacante calificado y equipado con acceso físico a su hardware sería posible inspeccionando y copiando las memorias de su XenatOFF. Siendo este último más difícil de ocultar que una microSD.
                            <hr>
                            <i class="bi bi-exclamation-octagon"></i> Almacena tus directorios OTP y su XenatOFF en lugares diferentes y seguros con consejos para saber si tus "caches" no han sido "visitados" sin autorización...`
                    },

                    "encryptionDecryption" : {
                        "title" : "Cifrado - Descifrado",
                        "paragraph01" : `
                            <p class="alert alert-danger">
                                <i class="bi bi-exclamation-octagon"></i> Recordatorio: <strong>una OTP NUNCA debe usarse dos veces</strong>. Para usar una sola vez, realmente una sola vez, de ahí su nombre “mascarilla de un solo uso” o “mascarilla desechable”.
                            </p>

                            <p class="h4">Borrar código</p>
                            <p>
                                En primer lugar, el “contenido de texto plano” o “mensaje plano” debe convertirse en “código plano” utilizando una tabla de códigos que permita la conversión (ver sección “Tablas de códigos multilingües”). Hay una tabla de códigos para cada idioma. Usaremos la tabla de códigos francesa en los siguientes ejemplos.
                            </p>
                            <p>
                                En una tabla de códigos, cada letra alfabética (o carácter especial) tiene una correspondencia numérica en forma de dígitos o números. Las letras más utilizadas en el idioma en cuestión se asignan a números para acortar al máximo el código de texto plano. Otras letras y caracteres especiales se traducen como un número de dos dígitos.
                                <br>Los consejos y reglas de construcción evitan malas interpretaciones en el momento de la decodificación, es decir, el paso del código claro a un mensaje claro, en el lado del destinatario del mensaje.
                            </p>
                            <p>
                                Después de la codificación, el código plano se agrupa en bloques de 5 dígitos para una mayor legibilidad y evitar errores. Las propias OTP están organizadas de tal manera que facilitan la entrada, el cifrado y el descifrado manualmente.
                            </p>
                            <p>
                                <fuerte>
                                    Ejemplo a continuación del pasaje en código claro del mensaje simple “EXPLICACIONES DE LA OTP” utilizando la tabla de códigos en francés <i>(consulte la pestaña Tablas de códigos multilingües a continuación)</i> y luego cifrado mediante aritmética modular.
                                </strong>
                            </p>`,

                        "paragraph02" : `
                            <p>
                                En la práctica, siempre debes “terminar” cualquier bloque de 5 “iniciado”. Entonces, por convención, siempre finalizamos el bloque final de código de texto sin formato agregando puntos manualmente (91).
                                <br>Por lo tanto, el código de texto sin formato debería terminar con 68191 en lugar de 681 como se indicó anteriormente. Si tiene 4 "espacios" para completar, esto le dará 9191 (es decir, dos puntos). Si tienes que llenar tres espacios o solo uno sumas solo 919 o solo 9, en el último caso. Al ver un 9 final, sabremos que este 9, colocado al final del mensaje, significa el inicio del 91, por tanto un punto. Lo indicamos con el signo (.) o nada (Ø), si ya había un punto antes, al momento de decodificar el código simple en un mensaje simple.
                            </p>
                            <p>
                                Este código final de texto plano 28781 78371 16380 45997 22997 89380 68191 ahora está listo para cifrarse con una clave OTP de un solo uso.
                            </p>
                            <p>
                                Una vez que el código se ha cifrado (aún no se ha hecho), se puede "exfiltrar" directamente desde su TAM (sección Herramientas, pestaña TAM - transmisión de código Morse). Tenga en cuenta que infórmese sobre la legislación de su país relativa a la transmisión cifrada mediante CiBi/PMR.
                                <br>El resultado cifrado también podría integrarse en contenido de texto claro que a su vez se cifrará "clásicamente" mediante cifrado simétrico o asimétrico, o ambos.
                            </p>
                            
                            <p class="h4">
                                Cifrado
                            </p>
                            <p>
                                Una vez que el mensaje de texto claro se convierte en código de texto claro, el cifrado, al igual que el descifrado, se basa en aritmética modular. El proceso es muy sencillo de asimilar.
                            </p>
                            <p>
                                Para cifrar el mensaje escribiremos los dígitos del código en texto plano convertidos en grupos de cinco dígitos y escribiremos justo debajo, bien alineados verticalmente, los dígitos de la OTP. Luego realizamos una resta modular dígito por dígito (dígito superior menos dígito inferior).
                            </p>
                            <p>
                                Con el mensaje de texto claro anterior "EXPLICACIONES DE OTP" convertido a código de texto claro y la OTP 27793 33873 22989 05220 80984 29034 63759 54704, se obtiene el siguiente diseño.
                                <br>Atención, debes desplazarte un bloque porque los primeros 5 dígitos de la OTP corresponden a su identificador (ID.) y por lo tanto no se utilizan para el cifrado.
                            </p>
                            <p>
                                <strong>Borrar código:</strong>
                                <br>ID.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 28781 78371 16380 45997 22997 89380 68191
                            </p>
                            <p>
                                <strong>Clave OTP:</strong>
                                <br>27793 33873 22989 05220 80984 29034 63759 54704
                            </p>
                            <p>
                                <strong>Resultado numérico: </strong>
                                <br>27793 95918 56492 11160 65013 03963 26631 14497 (era imprescindible haber recordado, al inicio del mensaje cifrado, el identificador de clave utilizado para saber qué clave utilizar para el descifrado)
                            </p>
                            <p>
                                Para obtener este resultado numérico, restamos sin volvernos negativos (por ejemplo 5 - 9 = 6 porque [1]5 - 9 = 6 pero no usamos el [1] del número de la izquierda, indicado como una especie de restricción. ¡Nunca realices una resta normal ya que esto creará un texto cifrado sesgado y completamente inseguro! Utilice siempre aritmética modular y utilice las pestañas de cifrado y descifrado con secuencias de números ficticios para practicar y comprender el proceso.
                            </p>
                            <p>
                                Si desea enviar "este cifrado" por radio, por voz o en código Morse, o por teléfono, se recomienda repetir todos los grupos dos veces para evitar errores (por ejemplo, 27793 27793 95918 95918...).
                            </p>
                            <p>
                                Puede resultar muy útil saber cómo ocultar su mensaje cifrado (esteganografía). Le recomendamos que continúe su investigación utilizando la palabra clave "WPS" o Palabras por oración.
                            </p>
                            <p>
                                Finalmente, averigüe si el uso de OTP está permitido legalmente en su país. No todos los países permiten el cifrado inquebrantable. Sin embargo, es fundamental conocer esta técnica accesible a todos en caso de que lo legítimo prevalezca sobre lo legal...
                            </p>
                            
                            <p class="h4">Descifrado</p>
                            <p>
                                Para descifrar un mensaje cifrado con OTP, verifique su primer grupo de 5 dígitos (que sirve como clave de referencia/identificación) con el primer grupo de cada clave en su directorio OTP para encontrar la clave utilizada. Debe ser una clave OTP marcada "IN" en el título porque si la descifra, recibirá el mensaje (consulte la sección "Directorio y almacenamiento").
                                <br>Recordatorio: este primer grupo de 5 dígitos no forma parte del mensaje en sí, solo sirve como referencia de clave OTP y, por lo tanto, no se utiliza en el descifrado.
                            </p>
                            <p>
                                Escriba los dígitos de la OTP debajo del texto cifrado recibido y agregue el texto cifrado y la OTP, dígito por dígito verticalmente. Esto significa una adición modular. Por ejemplo, 9 + 6 = 5 y no 15. Obviamente, cuando excede 9, eliminamos el dígito de las decenas y solo mantenemos el dígito de las unidades.
                                <br>¡Nunca uses la suma normal!
                            </p>
                            <p>
                                <strong>Resultado numérico: </strong>
                                <br>27793 95918 56492 11160 65013 03963 26631 14497 (es muy importante recordar el identificador al inicio del mensaje cifrado)
                            </p>
                            <p>
                                <strong>Clave OTP:</strong>
                                <br>27793 33873 22989 05220 80984 29034 63759 54704
                            </p>
                            <p>
                                <strong>Resultado del descifrado: </strong>
                                <br>ID.   28781 78371 16380 45997 22997 89380 68191
                            </p>
                            <p>
                                Solo debes regresar a la pestaña “Convertidor” y luego a “Borrar conversor de código a texto plano” para ver que encuentras el mensaje claro “EXPLICACIONES DE LA OTP”.
                            </p>
                           <p class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> Cifre siempre cada mensaje nuevo con una nueva clave (almacenada digitalmente o como un mini-resguardo de papel). ¡NUNCA reutilice una clave OTP!
                                <br>Destruya siempre toda la clave OTP inmediatamente después de completar el cifrado, incluso si todavía contiene grupos de dígitos no utilizados. Lo mismo después del descifrado.
                                <br>Si le preocupa no recordar el mensaje que envió, vuelva a cifrarlo con otra OTP (o posiblemente con una clave AES-256) y oculte esta nueva clave y el mensaje almacenado en dos ubicaciones separadas y seguras.
                                <br>Por lo tanto, un atacante tendrá que descubrir dos cachés locales y ya no solo uno (para encontrar su "primera" clave OTP), porque su primer mensaje cifrado probablemente habrá sido interceptado durante su transmisión por Internet, voz, código Morse, etc. . a menos que lo haya ocultado adecuadamente mediante esteganografía.
                            </p>

                            <p class="alert alert-info">
                                <i class="bi bi-info-circle"></i> Encontrarás más explicaciones en nuestra plataforma de aprendizaje. También puedes consultar este muy buen sitio: <br>https://www.ciphermachinesandcryptology.com/en/onetimepad.htm
                            </p>
                            `
                    },

                    "codeTables" : {
                        "title" : "Tablas de códigos multilingües",
                        "introduction" : `Estas tablas de códigos multilingües se utilizan para la conversión manual. También existe la pestaña "Convertidor" para realizar la conversión automática.`
                    },

                    "frenchCodeBook" : {
                        "title" : "Libro de códigos francés",
                        "text" : `
                            <p>
                                Es posible utilizar un libro de códigos manualmente para reducir el tamaño del mensaje. Simplemente inserte el código precedido por un cero directamente en el mensaje de texto claro para transformarlo en código de texto claro en la pestaña "Convertidor". Ejemplo de la palabra ACEPTAR, inserte 0019.
                            </p>
                            <p>
                                El mensaje CANCELAR(073) TRANSFERENCIA(875)S, poniendo una S en transferencias porque hay varias transferencias que deseamos cancelar, se convierte en un mensaje claro que se escribirá manualmente 0073 0875S que finalmente será traducido automáticamente a código claro por 00739 90875 5 que finalizaremos con 9191 para completar el último bloque. O al final 00739 90875 59191
                            </p>
                            <p>
                                Por supuesto, puedes utilizar tu propio libro de códigos. Los códigos simplemente no deben exceder los tres caracteres y solo los decimales.
                                <u>Recordatorio</u>: el hecho de que este libro de códigos sea público y, por lo tanto, conocido, no tiene ningún impacto en la indescifrabilidad del mensaje final cifrado por OTP.
                            </p>
                        `
                    },

                    "englishCodeBook" : {
                        "title" : "Libro de códigos en inglés",
                        "text" : `
                            <p>
                                Es posible utilizar un libro de códigos manualmente para reducir el tamaño del mensaje. Simplemente inserte el código precedido por un cero directamente en el mensaje de texto claro para transformarlo en código de texto claro en la pestaña "Convertidor". Ejemplo de la palabra PELIGRO, inserte 0244.
                            </p>
                            <p>
                                El mensaje PELIGRO (244) AVIÓN(064)S, al poner una S en AVIONES porque hay varios aviones que representan un peligro, se convierte en un mensaje claro a escribir manualmente 0244 0064S que finalmente será traducido automáticamente a código claro por 02449 90064 83 el cual finalizaremos con 919 para completar el último bloque. O al final 02449 90064 83919 (tenga en cuenta que la S se traduce a 83 y no a 5 para respetar la tabla de códigos en inglés)
                            </p>
                            <p>
                                Por supuesto, puedes utilizar tu propio libro de códigos. Los códigos simplemente no deben exceder los tres caracteres y solo los decimales.
                                <u>Recordatorio</u>: el hecho de que este libro de códigos sea público y, por lo tanto, conocido, no tiene ningún impacto en la indescifrabilidad del mensaje final cifrado por OTP.
                            </p>
                        `
                    }
                },

                "converter" : {
                    "title" : "Convertidor",
                    "paragraph01" : `
                        <p class="alert alert-info">
                            <i class="bi bi-info-circle"></i> Este conversor utiliza la tabla de códigos <strong>francés</strong> disponible en la pestaña "Explicaciones" y luego la sección "Tablas de códigos multilingües".
                        </p>
                        <p>
                            Convierta contenido de texto claro en código de texto claro para cifrado de máscara digital de un solo uso (OTP). Recordatorio: su OTP debe ser generada por un TRNG (generador de números verdaderamente aleatorios) confiable. Como es muy difícil de encontrar, deberás haberlo generado al menos mediante tiradas sucesivas de dados de precisión. La única técnica realmente disponible para el "público en general" y explicada en la pestaña "Explicaciones" y luego en "Entropía - Creación de una OTP".
                        </p>
                        <p>
                            Por el contrario, convierta su código de texto claro en contenido de texto claro.
                        </p>
                        <p class="h3">
                            Conversor de texto claro a código claro
                        </p>

                        <p>
                            Si desea indicar que los siguientes caracteres son signos figurativos o números, fórmulas, etc. utilice el signo de corchete de apertura y cierre [], y lo mismo para indicar el final de este uso.
                        </p>
                        <p>
                            Estos corchetes [] son ​​equivalentes al término "FIG". en la tabla de códigos. Por lo tanto, estos corchetes se traducirán correctamente por el código 90
                            <br>Ejemplo de enmarcar un número, aquí está el resultado: []42[]
                            <br>Por lo tanto, el contenido de texto claro (o mensaje claro) entre comillas "CUALQUIER NÚMERO: []42[]". dará el siguiente código simple:
                            84 4 99 4 80 79 70 83 2 99 82 84 2 78 71 80 4 82 84 2 99 92 99 90 444 222 90 91
                            <br>Que da agrupados en bloques de 5: 84499 48079 70832 99828 42787 18048 28429 99299 90444 22290 91
                        </p>
                        <p>
                            Sin embargo, de acuerdo con las explicaciones, hay que terminar manualmente el bloque con puntos (91) y en este caso también con un solo 9 al final lo que da:
                            <br>84499 48079 70832 99828 42787 18048 28429 99299 90444 22290 91919
                        </p>
                        <p>
                            ¿Las siguientes señales? (REQUISITO). ' : + - = se puede utilizar directamente en su mensaje de texto claro. Por otro lado, para los paréntesis, indique () para un paréntesis de apertura y también () para un paréntesis de cierre. EL ? aquí reemplaza el REQ (que significa Solicitud) en la tabla de códigos francesa (98).
                        </p>
                        <p>
                            Realice algunas pruebas con la tabla de códigos frente a usted para comprenderla completamente. En cualquier caso, en cuanto tengas cifras o números, como el número 947, rodéalos con [] para que dé []947[]
                        </p>

                        <p>
                            El uso manual de un libro de códigos se explica en la sección “Libro de códigos en francés” de la pestaña “Explicaciones”. Un libro de códigos ayuda a acortar los mensajes.
                        </p>
                    `,
                    "paragraph02" : `
                        Convierta su contenido de texto claro a código de texto claro, utilizando únicamente TODO EN MAYÚSCULAS. Luego cifre este código en texto claro en la pestaña "Cifrado digital".
                    `,
                    "clearMessageField" : "Contenido de texto claro",
                    "clearCodeField" : "Borrar código",
                    "convertToClearCodeBtn" : "Convertir a texto claro",

                    "titleConvertFromClearCodeToClearMessage" : "Conversor de codigo claro a texto claro",
                    "deleteBracketsBtn" : "Quitar corchetes",
                    "convertToClearMessageBtn" : "Convertir a mensaje de texto claro",

                },

                "encryption" : {
                    "title" : "Cifrado digital",
                    "introduction" : `
                        <p>
                            Si no está familiarizado con la OTP, lea las explicaciones en la primera pestaña "Explicaciones".
                        </p>
                        <p class="alert alert-warning">
                            <i class="bi bi-exclamation-octagon"></i> Primero convierta su contenido de texto claro en código claro en la pestaña "Convertidor". No olvide <strong>eliminar el identificador</strong>, es decir, los primeros 5 dígitos de la OTP, en el campo debajo de Clave numérica (máscara - OTP) PERO <strong>luego vuelva a colocar este identificador delante del cifrado resultado</strong>antes de transmitir este mensaje cifrado a su interlocutor. De hecho, son estos 5 dígitos colocados delante del contenido cifrado real los que le permitirán saber qué clave OTP utilizar para descifrar.
                        </p>
                    `,
                    "clearCode" : "Borrar código",
                    "digitalKey" : "Clave digital (máscara - OTP)",
                    "encryptedResult" : "Resultado: contenido cifrado",
                    "deleteSpacesHelp" : `El botón "Eliminar espacios" que aparece a continuación le permite acortar el mensaje cifrado en el caso, entre otros, de una transmisión en código Morse, a través del "TAM" (sección Herramientas)`,
                    "deleteSpaces" : "Quitar espacios"
                },

                "decryption" : {
                    "title" : "Descifrado digital",
                    "introduction" : `
                        <p>
                            Si no está familiarizado con la OTP, lea las explicaciones en la pestaña "Explicaciones".
                        </p>
                        <p class="alert alert-warning">
                            <i class="bi bi-exclamation-octagon"></i> No olvides <strong>eliminar el identificador de clave OTP</strong>, de modo que los primeros 5 dígitos del contenido cifrado y la OTP, en el dos campos a continuación antes de continuar con el descifrado. De hecho, son estos 5 dígitos colocados delante del contenido cifrado recibido los que le permiten saber qué clave OTP utilizar para descifrar. Convierta el resultado obtenido, es decir, su código de texto claro descifrado, en contenido de texto claro en la pestaña "Convertidor".
                        </p>
                    `,
                    "encryptedContentField" : "Contenido cifrado",
                    "otpKeyField" : "Clave digital (máscara - OTP)",
                    "decryptedCCField" : "Código de texto plano descifrado",
                    "decryptedCCFieldHelp" : `Vaya a la pestaña "Convertidor" para transformarlo en un mensaje simple.`
                },

                "hic" : {
                    "title" : "HIC - Hidden In the Crowd",
                    "subtitle" : `Mensaje OTP « Escondido entre la multitud »`,
                    "explanations" : {
                        "title" : "Explicaciones",
                        "subtitle" : "",
                        "text" : `
                            <p>
                                Si no está familiarizado con la OTP, lea las explicaciones en la pestaña "Explicaciones".
                            </p>
                            <p>
                                HIC, término inventado por Xenaton, designa más un principio que un sistema. La lógica subyacente es conocida, al menos conocida por algunos “especialistas” y verdaderos entusiastas de la seguridad... en este mundo de ilusiones...
                                <br>Esta lógica es utilizada por todos los servicios de inteligencia y ejércitos del mundo...
                            </p>
                            <p>
                                HIC es un poco como el “rey” de los sistemas de comunicación seguros, al menos para los mensajes cortos: simplicidad, imposibilidad de seguimiento e indescifrabilidad garantizadas del intercambio…
                            </p>
                            <p>
                                Probablemente HIC nunca tendrá un equivalente porque se basa en fundamentos insuperables como OTP y esteganografía “inversa” (“mostrar” todo a todos en lugar de ocultarlo todo).
                            </p>
                            <p>
                                Una vez entendido el principio y aprendiendo un poco, cualquiera puede implementar un sistema HIC, sin ningún soporte tecnológico. Ésta es una de las mayores fortalezas del HIC.
                                <br>Xenaton “sólo” ha optimizado estos principios subyacentes para hacerlos accesibles pero también adecuados para un entorno público “moderno”.
                            </p>
                            <p>
                                Xenaton también ha hecho que el aprendizaje sea “fácil” para usted con explicaciones y documentación traducida completa.
                            </p>
                            <p>
                                Por supuesto, también se han creado nuevas herramientas de simulación y aceleración. Estas herramientas encajan naturalmente con nuestros otros sistemas innovadores, como nuestros canales de transferencia segura esenciales que utilizan códigos QR o el transmisor acústico Morse.
                                <br>Todo lo cual está, en su mayor parte, agrupado aquí en XENATX, una herramienta gratuita a tu servicio.
                            </p>
                            <p>
                                Una parte complementaria, online, dentro del servicio XenaTeam, permite a nuestros clientes optimizar-acelerar-automatizar la distribución de sus mensajes HIC a través de sus múltiples canales.
                                <br>Sin embargo, todo se puede hacer "manualmente" sin este servicio, como copiar mensajes de HIC a múltiples canales como grupos de mensajería, boletines, sitios web, etc.
                            </p>
                            <p>
                                En el siguiente tema, "Coincidencia de identificación", encontrará una función para usar en XenatOFF. Te permite agilizar enormemente la verificación de que un HIC está dirigido a ti o no.
                            </p>
                        `,
                    },

                    "keysIdentifiers" : {
                        "title" : "Coincidencia de identificación",
                        "introduction" : `
                            <p>
                                Esta función le permite saber si los identificadores (primeros 5 dígitos) de los mensajes HIC, cifrados por OTP, consultados en línea en los canales asociados, pueden corresponder a identificadores de claves OTP que son específicos para usted.
                                <br>Recordatorio: los primeros cinco dígitos de un mensaje cifrado por OTP (HIC o no) corresponden al identificador, por lo tanto también a los primeros 5 dígitos, de la clave OTP que se utilizará para el descifrado.
                            </p>
                            <p>
                                Indique sus identificadores de claves en bloques de 5, y solo sus identificadores, en el campo "Identificadores de mis claves OTP", luego ubíquese en el campo "Identificadores de mensajes OTP-HIC" y muestre el primer Código QR de los identificadores de los mensajes. .
                                <br>Estos identificadores se agrupan sistemáticamente en un primer Código QR titulado ID y también se indican encima del Código QR en caso de que no tenga nada que flashear o desee volver a introducir los identificadores a mano en papel o directamente en este segundo campo " Identificadores de mensajes".
                            </p>
                            <p class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> Utilice esta función sólo en un XenatOFF para protegerse del hecho de que un atacante, "presente" en su equipo conectado gracias a un malware o a un defecto de hardware, pueda ver que tal o cual mensaje HIC estaba destinado a usted.
                            </p>
                        `,
                        "keysIdentifiersField" : "Mi identificadores claves OTP",
                        "messagesIdentifiersField" : "IDs de mensajes OTP-HIC",
                        "resultIdentifiersField" : "Resultado del partido",
                        "checkKeysIdentifiersBtn" : "Controlar"
                    },
                    "training" : {
                        "title" : "¡Práctica!",
                        "text1" : `
                            <p>
                                Vaya al segundo campo "Identificadores de mensajes" y escanee el primer código QR que aparece a continuación, que contiene los identificadores de los 20 HIC del día publicados en sitios web de socios, canales de mensajería de socios, etc. 
                            </p>
                            <p>
                                A continuación, introduzca varios identificadores que usted invente y que, en la vida real, corresponderían a sus identificadores de clave OTP.
                            </p>
                            <p>
                                Por ejemplo, tendrías 40 claves, así que 40 identificadores en forma de bloques de 5 dígitos: 00414 00868 01656 04876 07735 08436 10860 14258 15833 16843 22383 26126 30544 30750 33465 33504 34468 41004 45818 50844 52152 53187 53440 57368 60508 61553 61884 63547 64088 67703 76481 78007 80736 81505 82531 82578 84422 85518 88687 88715 (cópialos todos, separados por un espacio, en el primer campo "Key Identifiers" que aparece a continuación)
                            </p>
                            <p>
                                Si uno o varios de los identificadores de mensajes de hoy coinciden con uno o varios de sus identificadores de clave, el sistema se lo hará saber.
                            </p>`
                        ,
                        "text2" : `
                            <p>
                                Los 4 códigos QR a continuación llevan los mensajes OTP indicados justo arriba. Están separados por un salto de línea (65 caracteres incluido el primer bloque de 5 dígitos que sirve como identificador).
                            </p>
                            <p>
                                Estos 20 mensajes OTP-HIC en total, acompañados de sus 4 códigos QR que facilitan su recuperación, sirven aquí como ejemplo de un envío diario de 20 mensajes diferentes duplicados en múltiples canales asociados.
                                <br>Gracias al primer código QR llamado OTP ID que reúne todos los identificadores de estos 20 mensajes, es más fácil encontrar el mensaje destinado a usted. Será necesario flashear, desde un XenatOFF, el Código QR correcto que contiene este mensaje entre los otros cinco para su descifrado automático. También puedes copiarlo a mano para descifrarlo manualmente con un simple lápiz y papel.
                            </p>
                        `
                    }
                }
            },

            "tools" : {
                "title" : "Herramientas",
                "header" : `Transmita contenido sin contacto mediante código QR o Morse - Convertir - Calcular una huella digital - Esteganografía con una imagen.`,
                "menuQrCode": "Código QR",
                "menuTam": "TAM - Morse",
                "menuConverter": "Convertidores",
                "menuHash": "Huella - Hash",
                "menuSteganography": "Esteganografía",

                "qrcode" : {
                    "title" : "Código QR",
                    "subtitle" : "Transferencia óptica segura",

                    "textualAndVisualHashSubtitle": `Huella digital SHA-1 global (textual y visual): `,
                    "visualGlobalHashIndication": `Huella digital visual global SHA-1`,
                    "finalGlobalHashVerificationText": `Huella digital textual y visual SHA-1 para la verificación final de todo el contenido de cada Código QR anterior combinado: `,
                    "hashParenthesis" : "(huella digital SHA-1)",
                    "introduction" : `
                        <p>
                            El XENATX utiliza el Código QR como canal de transferencia óptica segura. Ningún malware puede esconderse en el código QR. Por tanto, no es posible una fuga de datos significativa. El “transporte” de los datos contenidos en el Código QR también es unidireccional. Por lo tanto, no corre el riesgo de contaminarse en la otra dirección.
                        </p>
                        <p>
                            También puedes utilizar el<i class="bi bi-soundwave"></i>TAM - Emett. Código morse en la siguiente pestaña, con el mismo objetivo de transferencia sonora segura de contenidos. Ambas soluciones tienen sus ventajas y desventajas. El TAM es más adecuado para realizar transferencias por radio (CiBi/PMR) o en ausencia de capacidad de lectura de códigos QR.
                        </p>
                        <p>
                            Para "transportar" contenido mediante códigos QR a otro dispositivo, utilice la sección siguiente <i class="bi bi-qr-code"></i> Visualización del código QR. Por el contrario, para "recibir" contenido transportado mediante códigos QR, utilice la sección <i class="bi bi-qr-code-scan"></i> Lectura de códigos QR.
                        </p>
                    `,

                    "displayQrCode" : {
                        "title" : "Visualización del código QR",
                        "paragraph01" : `
                            <p>
                                Transmitir ópticamente sin contacto, mediante uno o más Códigos QR, un mensaje cifrado, una huella digital, una clave pública, una firma, etc.
                            </p>
                            <p>
                                En ocasiones, la transferencia puede requerir el uso de la pestaña Convertidores <i class="bi bi-arrow-clock"></i> para convertir primero el contenido al formato hexadecimal. Este formato permite una mejor conservación de los caracteres especiales y una mejor división automática en múltiples códigos QR. La desventaja es que el hexadecimal aumenta la cantidad de caracteres y, por lo tanto, la cantidad de códigos QR para escanear posteriormente.
                                <br>Haz tus propias pruebas.
                            </p>
                            <p>
                                La capacidad de transporte de un único Código QR se define a continuación para adaptarse a las sensibilidades de detección de diferentes materiales. Reduzca la capacidad si la detección del Código QR no se realiza correctamente por su equipo (smartphone, webcam, escáner, etc.)
                            </p>
                            <p>
                                El contenido de texto puede ser hasta 100 veces la capacidad máxima de carga de un único código QR. El contenido se distribuirá automáticamente en varios códigos QR. Se generarán y numerarán automáticamente uno debajo del otro.
                                <br>Tenga en cuenta que el tiempo de generación puede ser largo dependiendo del rendimiento de su equipo.
                            </p>
                            <p>
                                Para recuperar el contenido en su dispositivo de destino, use un XENATX nuevamente y vaya a la sección "Herramientas", luego a la pestaña Código QR <i class="bi bi-qr-code"></i> y finalmente a "Lectura QR". Códigos. Luego indique la cantidad de Códigos QR a leer. Los campos de “recibo” se generan automáticamente.
                            </p>
                            <p>
                                Escanea tu primer código QR y pega el contenido en el primer campo. Toca "Verificar huella digital". La impresión textual y visual generada debe corresponder a la generada en su equipo que muestra los Códigos QR <i>(gracias a la impresión visual, comprobar la correcta ejecución de la transferencia es particularmente rápido).</i>
                            </p>
                            <p>
                                Una vez copiados uno a uno todos los Códigos QR, presiona el botón azul “REPRODUCIR Y VERIFICAR GLOBALMENTE”. El contenido de cada campo se volverá a ensamblar y se generará una huella textual y visual general. Por supuesto, debe corresponder a la huella total indicada en su otro equipo "transmisor".
                            </p>
                        `,
                        "transportCapacitySelect" : `Elección de la capacidad de transporte (en número de caracteres)`,
                        "transportCapacitySelectHelp" : `Entre 200 y 900 caracteres contenidos en un único Código QR`,

                        "contentField" : "Contenido del texto",
                        "visualHashSubtitle" : "Huella visual SHA-1: ",
                        "contentFieldHelp" : "Indique por ejemplo un mensaje cifrado, una huella digital, una firma, etc.",
                        "createQrCodeBtn" : "Crear el código QR"
                    },

                    "readQrCode" : {
                        "title" : "Lectura de código QR",
                        "visualHashTitle" : "Huella visual SHA-1",
                        "numberOfQRCodesToReadField" : "Número de códigos QR para leer",
                        "checkHashBtn" : `Comprobar huella digital  `,
                        "globalVisualHash" : `Huella digital visual global SHA-1`,
                        "globalReconstitutionField" : `Reconstrucción de contenidos transferidos mediante Códigos QR`,
                        "globalReconstitutionBtn" : `RECONSTITUIR Y VERIFICAR GLOBALMENTE`
                    }
                },

                "tam" : {
                    "title" : "TAM",
                    "subtitle" : "Transmisor Acústico Morse"
                },

                "tamTx" : {
                    "title" : "Transmisor TAM",
                    "introduction" : `Lea las instrucciones esenciales y las explicaciones detalladas.`,

                    "essentialInstructions" : {
                        "title" : "Instrucciones esenciales",
                        "paragraph01" : `
                            <p class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> Para transmitir contenido de texto cifrado asimétrico o simétrico, primero conviértalo a hexadecimal con el botón "Convertir antes de la transmisión". Esto aumenta el tiempo de transmisión pero conserva los caracteres especiales.
                            </p>
                            <p>
                                Para un mensaje cifrado OTP digital, puede transmitirlo sin ninguna conversión. Para ahorrar algo de tiempo de transmisión, puede eliminar todos los espacios entre bloques cifrados de 5 dígitos.
                            </p>
                            <p>
                                Para obtener un mensaje claro y, por tanto, no cifrado, puede transmitirlo tal cual también sin conversión. Sin embargo, escribe en inglés porque el apóstrofo ['] como en la oración [NOUS T'EMM... AFFECTUEUSEMENT] no es convertible a código Morse. Los acentos tampoco. Sólo se permiten caracteres alfanuméricos en MAYÚSCULAS así como el signo de punto único [.]
                                <br>Para evitar el inglés, convierta a hexadecimal, con el botón "Convertir antes de la transmisión", su mensaje en francés simple, incluidos acentos y otros caracteres especiales, como ocurre con el contenido textual cifrado.
                            </p>
                            <p>
                                Al final de la transmisión, transmitir la huella digital del mensaje para que el destinatario pueda verificar que ha recibido todos los datos, sin pérdida ni error tras la decodificación. Realizará la verificación en el apartado “Verificación de huellas dactilares” en la pestaña “TAM – Recepción de código Morse”.
                            </p> 
                        `,
                        "morseFreq" : "Frecuencia (Hz)",
                    },

                    "detailedExplanations" : {
                        "title" : "Explicaciones detalladas",
                        "paragraph01" : `
                            <p>
                               En el mismo espíritu que la transferencia mediante Códigos QR, el TAM permite la transmisión segura de datos entre equipos OFF - Ver sección "Código de seguridad" para detalles OFF/ON - y equipos ON conectados a Internet que, como todos los equipos conectados, están vulnerable a ataques debido a fallas de software y hardware.
                            </p>
                            <p>
                                Por tanto, el TAM permite transmitir cualquier tipo de mensaje, preferiblemente cifrado. La verificación de la integridad de la transmisión se realiza tanto de oído como visualmente mediante espectrograma. Todo en tiempo real.
                            </p>

                            <div class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> 
                                Como beneficio adicional, la transmisión por radio (CiBi/transmisor portátil) es posible desde el equipo OFF1 a OFF2, OFF3, OFF4, OFF<sup>N</sup>...
                                <hora>
                                Llamamos su atención sobre el hecho de que la transmisión de datos cifrados por radio está prohibida para los radioaficionados privados en determinados países como Francia, aunque la transmisión de datos cifrados está autorizada a través de Internet.
                            </div>
                           <p>
                                El TAM se puede utilizar en casi todos los equipos porque las tarjetas de sonido están preinstaladas en casi todas partes, incluso en equipos pequeños como la Raspberry Pi <i>(modelo nº 2, o nº 4 en versión modular, sin tarjeta wifi). ni bluetooth para mayor seguridad)</i>.
                            </p>

                            <p class="h4">Procedimiento de 6 pasos</p>
                            <p>
                                Descripción precisa del procedimiento para transferir un mensaje cifrado desde el equipo seguro OFF1 de un remitente al equipo seguro OFF2 del destinatario, a través de los equipos ON1 y ON2 del remitente y del destinatario, que son muy difíciles de asegurar porque están conectados.
                            </p>
                            <p class="alert alert-info">
                                <i class="bi bi-info-circle"></i>Recordatorio: se dice que el equipo está OFF si nunca está conectado al exterior mediante canales estándar (ni wifi, ni bluetooth, ni USB, ni ethernet, etc.). Luego sólo utiliza los canales asegurados por XENATX para comunicarse con el mundo exterior. El entorno seguro que proporciona está vinculado, casi exclusivamente, a esta inaccesibilidad por rutas estándar.
                            </p>
                            </p>
                               Detalles: Se instalarán 4 XENATX. Uno en cada equipo en juego para esta transmisión. En el caso de transmisión por radio (CiBi-PMR) que sólo utiliza dos equipos informáticos, preferentemente OFF (desconectados), serán por tanto necesarios sólo dos XENATX.
                            </p>
                            <br>
                            <h5>
                                <span class="badge bg-success">1</span>&nbsp; Cifrado y transmisión de OFF1 a ON1 del remitente
                            </h5>
                            <p>
                                El remitente primero cifra el mensaje utilizando el XENATX instalado en su equipo OFF1. Utiliza cifrado de máscara asimétrico, simétrico, de un solo uso o los tres.
                                <br>Este mensaje cifrado luego se transforma a hexadecimal para facilitar la transmisión de caracteres especiales y una verificación visual y auditiva simplificada (botón "Convertir antes de la transmisión"). En el caso de cifrado únicamente mediante máscara de un solo uso, la transmisión se realizará tal cual en código Morse sin conversión previa a hexadecimal.
                                <br>El mensaje codificado se transmite en código Morse a ON1, por la tarjeta de sonido con salida de altavoz (botón “Transmitir”)
                                <br>Al final de la transmisión del mensaje, también se puede transmitir una huella digital SHA-256 del mensaje cifrado (botón "Transmitir huella digital"). La huella dactilar se utiliza para verificar en el ON1 que la transmisión se realizó sin errores.
                                <br>Si hay errores, a menudo sucede que el sonido emitido por el altavoz no es lo suficientemente alto. Simplemente aumente el volumen, el micrófono del teléfono inteligente o de la computadora grabará mejor la señal y la decodificación del código Morse será mejor. <br>Realice pruebas de volumen en un mensaje corto. Es posible que aún sea necesario un filtro de paso bajo antes de decodificar en el ON1 (consulte la explicación en la pestaña "TAM - Recepción Morse", luego "Parámetros de decodificación" y finalmente el párrafo "Filtro de paso bajo")
                            </p>
                            
                            <p class="alert alert-success">
                                <i class="bi bi-info-circle"></i> En esta etapa es posible una alternativa, es decir, la transmisión directa del mensaje desde el OFF1 del remitente al OFF2 del destinatario por radio (CiBi o transmisor portátil tipo PMR). ) <em> - En este caso, vaya directamente al paso número 6 - Consulte el párrafo RESILIENCIA a continuación.</em>
                            </p>
                            <br>
                            <h5>
                                <span class="badge bg-danger">2</span>&nbsp; Recepción en ON1
                            </h5>
                            <p>
                                El equipo ON1 registra esta “puntuación” en código Morse a través del micrófono utilizando una aplicación tipo grabadora de voz en un teléfono inteligente o por ejemplo a través del software gratuito y de código abierto Audacity en una computadora (https://www.audacityteam.org
                                <br>El archivo de audio de esta grabación en código morse se descarga y decodifica en el XENATX del ON1 en la pestaña "TAM - Receptor Morse" botón "Descargar un archivo de audio". Luego encontramos el contenido hexadecimal, igual que antes de la transmisión.
                                <br>El dispositivo ON1 ahora registra la nueva “partición” del código Morse correspondiente a la huella digital SHA-256. Como la huella digital ya está en formato hexadecimal, no es necesario codificarla antes de la transmisión.
                                <br>Una huella digital SHA-256 del mensaje en formato hexadecimal recibido en el ON1 se calcula en la pestaña “# Huella digital - Hash” y se compara con la huella digital transmitida. La transmisión se realizó correctamente si las dos huellas dactilares coinciden. Por tanto, el proceso puede continuar.
                                <br>Este contenido hexadecimal se decodifica en formato de texto: formato inicial después del cifrado en OFF1.
                                <br>Para ahorrar de 2 a 3 pequeños subpasos, es posible no decodificar el hexadecimal y transferir el mensaje en este formato con los medios que se explicarán en el siguiente paso n°3, o incluso transmitir tal cual el archivo de sonido de la grabación por mensajería o correo electrónico.
                            </p>
                            <br>
                            <h5>
                                <span class="badge bg-danger">3</span>&nbsp; Transferencia de Internet de ON1 a ON2
                            </h5>
                            <p>
                                Transmisión desde el ON1 del remitente al ON2 del destinatario mediante depósito del mensaje cifrado en un buzón digital (servicio online XENATON) o transmisión por mensajería, correo electrónico, etc. o incluso entregarse en mano o depositarse en un buzón físico mediante memoria USB, microSD, papel, etc.
                            </p>
                            <br>
                            <h5><span class="badge bg-danger">4</span>&nbsp; Recepción en ON2</h5>
                            <p>
                                Si este mensaje fue interceptado por un atacante en este dispositivo ON2 (o incluso en ON1 o incluso durante la transmisión anterior en código Morse), esto no es más importante de lo habitual porque está cifrado.
                            </p>
                            <p>
                                El mensaje ahora se puede descifrar, PERO como todavía está en una máquina ENCENDIDA, que puede estar comprometida, esto no es prudente. Por tanto, retransmitiremos el mensaje en código Morse a OFF2, posiblemente seguido de su huella digital SHA-256.
                            </p>

                            <br>
                            <h5>
                                <span class="badge bg-danger">5</span>&nbsp; Retransmisión de ON2 a OFF2
                            </h5>
                            <p>
                                El destinatario retransmite a su equipo seguro OFF2 para descifrarlo y finalmente leerlo.
                                <br>El resto de operaciones son similares a las instrucciones anteriores. Recordatorio: el mensaje cifrado vuelve a estar codificado en hexadecimal a menos que haya viajado codificado entre ON1 y ON2, o incluso si ha pasado directamente en forma de archivo de sonido (en formato .wav). A continuación, el mensaje se “reproduce” acústicamente en código Morse desde ON2 hasta OFF2.
                            </p>
                            <br>
                            <h5><span class="badge bg-success">6</span>&nbsp; Recepción en OFF2 desde ON2</h5>
                            <p>
                                Grabación a través del micrófono OFF2 en el software Audacity y luego decodificación Morse - Verificación de la huella dactilar - Descifrado.
                                <br>Descifrado y lectura en este entorno seguro proporcionado por este OFF2.
                                <br>Escribiendo la respuesta.
                                <br>Cifrado y codificación hexadecimal.
                                <br>¡Y aquí vamos de nuevo para una transmisión acústica en código Morse en la otra dirección!
                            </p>
                            <p>
                                La transferencia mediante códigos QR es, por supuesto, más rápida para estas transferencias de OFF1 a ON1 y de ON2 a OFF2, pero no siempre es posible o compatible con la transmisión por radio.
                            </p>
                            <p class="h4">RESILIENCIA</p>
                            <p>
                                Resiliencia para nuestros clientes en situaciones degradadas: baja cobertura de Internet, zona blanca, corte de Internet y/o telefonía móvil, zona de conflicto, vigilancia móvil, etc. - es, por tanto, real gracias a la transmisión por radio del mensaje cifrado.
                            </p>
                            <p>
                                La transmisión se realiza más fácilmente porque directamente desde un dispositivo OFF1 a otro dispositivo OFF2.
                                <br>Simplemente “reproduzca” el código Morse del mensaje cifrado desde el altavoz de sonido OFF1 a través de su CiBi/PMR (transmisor portátil).
                                <br>En el lado de la recepción, simplemente conecte el receptor de radio al micrófono OFF2 que grabará directamente el DIT-DIT-DAH del código Morse.
                            </p>
                            <p>
                                En ciertos casos extremos, dos personas separadas por algunos cientos o miles de metros pueden incluso comunicar un mensaje breve, siempre cifrado y siempre en código Morse, utilizando señales luminosas durante la noche y mediante la desviación de los rayos del sol en un espejo durante el día. U otros consejos como el código de semáforo.
                            </p>

                            <p class="h4">CONCLUSIÓN</p>
                            <p>
                                Todas las funciones de XENATX, incluido este TAM, eran esperadas por nuestros clientes más expuestos en todo el mundo. Querían una solución portátil, por lo tanto adecuada para cualquier campo de operación, económica con XenatOFF o equivalente, un software fácil de usar, con código verificable y gratuito.
                            </p>
                            <p>
                                En XENATON, nos complace ofrecer esta solución integral a esta necesidad esencial de comunicación segura.
                            </p>
                            <p>
                                La transmisión de la verdad libera. ¡Feliz uso iluminado!
                            </p>
                        `,
                    },

                    "morseCode" : {
                        "title" : "Código Morse",
                    },

                    "emission" : {
                        "title" : "Emisión",
                    },
                    
                    "spectrogram" : "Espectrograma",
                    "txTime" : "Hora de emisión:",
                    "contentField" : "Contenido del texto",
                    "contentFieldHelp" : "Caracteres alfanuméricos en MAYÚSCULAS más el signo de punto [.] solo para que un mensaje se envíe sin conversión antes de la transmisión.",

                    "hexEncodedContentField" : "Contenido de texto codificado en hexadecimal.",
                    "hashField" : "Huella digital SHA-256 del contenido transmitido",
                    "hashFieldHelp" : "Transmitir la huella digital después de transmitir el mensaje.",

                    "convertBeforeTxBtn" : "Convertir antes de la transmisión",
                    "originalContentBtn" : "Contenido original",
                    "morseView" : "Vista del código Morse",
                    "txBtn" : "Transmitir",
                    "hashTxBtn" : "Transmitir la huella digital",
                },

                "tamRx" : {
                    "title" : "Receptor TAM",
                    "subtitle" : "Transmisor Acústico Morse",
                    "introduction" : `
                        <p>
                            Si se producen errores de decodificación, probablemente sea necesario un filtro de paso bajo. Este es siempre el caso para una transmisión de radio (CiBi, etc.) - Consulte la explicación en la sección « <i class="bi bi-gear"></i> Configuración de decodificación » más abajo y en el párrafo « Filtro paso bajo ».
                        </p>
                    `,

                    "downloadFile" : {
                        "title" : "Descarga de archivos de audio",
                        "paragraph01" : `
                            <div class="alert alert-info">
                                <i class="bi bi-info-circle"></i> Con la decodificación de reproducción de archivos, <strong>el sonido del equipo informático se puede silenciar</strong>. Esto hace que decodificar el código Morse sea mucho más cómodo para los oídos.
                            </div>
                            <p>
                                La mejor decodificación está garantizada por este principio de <strong>descargar archivos de audio sólo en formato WAV</strong> en comparación con el uso directo del micrófono, posible en la sección siguiente.
                                <br>Por lo tanto, este proceso de lectura de archivos es imperativo para decodificar un mensaje cifrado porque el más mínimo error en la decodificación del código Morse haría imposible el descifrado mediante clave criptográfica.
                            </p>
                        `,
                        "downloadAudioFileBtn" : "Sube un archivo de audio",
                        "decodeBtn" : "Descodificar",
                        "file" : "Archivo:",
                        "none" : "ninguno",
                        "tooManyFiles" : "Ha seleccionado demasiados archivos.",
                        "selectOneFile" : "Inténtelo de nuevo seleccionando solo un archivo.",
                        "notAcceptedFile" : "Este tipo de archivo no es compatible.",
                        "selectAudioWav" : "Inténtelo de nuevo seleccionando un archivo de audio WAV.",
                    },

                    "decodingParameters" : {
                        "title" : "Configuración de decodificación",
                        "paragraph01" : `
                            <p>
                                En la sección “Descarga de archivos de audio”, utilice únicamente un archivo en formato WAV. Por tanto, es necesario que la grabación del código Morse “reproducido” se realice con el formato WAV seleccionado.
                                <br>El software gratuito de código abierto Audacity (Mac, Windows, Linux) permite grabar en una computadora y exportar en formato WAV.
                            </p>
                            <p>
                                Tenga en cuenta que el dictáfono predeterminado de un iPhone no permite el formato WAV. Sólo autoriza el formato M4A.
                                <br>Utilice una aplicación gratuita como la grabadora de voz AVR que permite grabar en formato WAV.
                            </p>
                            <p class="h5">
                                Problema de transmisión de radio (CiBi/PMR)
                            </p>
                           <p>
                                El “silbido” de las radios y otras alteraciones de la señal interrumpen la decodificación del código Morse. Para resolver este problema, pase la grabación de audio Morse a través del filtro de paso bajo en el software Audacity antes de decodificarla en el XENATX en la pestaña "TAM - Receptor Morse". El uso del filtro de paso bajo se explica en el párrafo siguiente.
                            </p>
                            <p class="h5">
                                Filtro de paso bajo
                            </p>
                            <p>
                                Para aplicar un filtro de paso bajo al archivo WAV de la grabación del código Morse transmitido, siga este procedimiento en el software de código abierto Audacity: haga doble clic en la pista para seleccionar la grabación completa y luego en el menú Efectos / Filtro de paso bajo. Seleccione la frecuencia a 563 Hz y el Roll-off a 6 db.
                                <br>Finalmente exporte la pista a un nuevo archivo yendo al menú Archivo / Exportar / Exportar a WAV.
                            </p>
                            <p>
                                <i>En una versión futura intentaremos integrar este filtro de paso bajo en XENATX para que no sea necesaria ninguna manipulación en Audacity.</i>
                            </p>
                        `,
                        "freq" : "Frecuencia (Hz)"
                    },

                    "microphone" : {
                        "title" : "Micrófono",
                        "paragraph01" : `
                            <p class="lead">
                               El uso del micrófono sirve para decodificar el código Morse en vivo. Sin embargo, la decodificación es menos eficiente que descargar un archivo de audio.
                            </p>
                            <p>
                                Por lo tanto, recomendamos el paso adicional de grabar el código Morse mediante una grabadora de voz en un teléfono inteligente o mediante un micrófono de computadora usando, por ejemplo, el software Audacity. El archivo de audio WAV resultante de esta grabación se descargará y decodificará en la sección "Descarga de archivos de audio" anterior.
                            </p>
                        `,
                        "listen" : "Ecouter",
                        "stop" : "Stop",

                        "paragraph02" : `
                            <p class="lead">
                                Le dijiste a tu navegador que no escuchara con tu micrófono.
                            </p>
                            <p>
                                Para volver a habilitar el micrófono, debe cambiar la configuración de su navegador web. Para Chrome, haga clic en el ícono de la cámara de video con una cruz roja en la barra de direcciones. Para Firefox, haga clic en el icono del micrófono en la barra de direcciones. Si no está allí, haz clic en el ícono del globo terráqueo.
                            </p>
                        `,
                    },

                    "hashCheck" : {
                        "title" : "Verificación de huellas dactilares",
                        "paragraph01" : `
                            <p>
                                Primero calcule la huella digital SHA-256 de los datos decodificados en Morse yendo a la pestaña <i class="bi bi-hash"></i> Huella digital - Hash de esta sección <i class=" bi bi-tools"></i> Herramientas. <span class="text-muted">Obtendrá, por ejemplo: 9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08</span>
                            </p>
                            <p>
                                Luego indique y compare las dos impresiones siguientes, la calculada y la transmitida en código Morse.
                            </p>
                        `,
                        "calculatedHashField" : "Huella calculada",
                        "calculatedHashFieldHelp" : "Pegue la huella digital SHA-256 calculada arriba.",

                        "hashTxField" : "Huella digital transmitida",
                        "hashTxFieldHelp" : "Pegue la huella digital SHA-256 transmitida arriba.",
                        "compareBtn" : "Comparar"
                    },

                    "displayDataFurtherDown" : `Los datos decodificados aparecerán un poco más abajo bajo el título "Datos decodificados".`,
                    "decodedDataTitle" : "Datos decodificados",
                    "paragraph02" : `
                        Si no aparece ningún dato en el campo de arriba, a menudo se debe a que el sonido emitido por el altavoz es demasiado débil durante la grabación. Comience a transmitir y grabar nuevamente aumentando el volumen. La indicación de una grabación correcta en términos de volumen es el color verde oscuro de las barras del espectrograma, no el verde claro que aparece debajo, durante la decodificación.
                    `,
                    "deleteDataBtn" : "Borrar datos",

                    "spectrogramTitle" : "Espectrograma",
                    "zoomInBtn" : "Ampliar el zoom",
                    "zoomOutBtn" : "Disminuir el zoom",
                    "band" : "Playa"
                },

                "converters" : {
                    "title" : "Convertidores",
                    "browser-warning" : `Esta transformación funciona bien con los navegadores <strong>Chrome, Chromium, Opera, Brave y Vivaldi</strong> para imágenes de hasta 2 MB (más delicada para tamaños más grandes). ¡No funciona con Firefox, Librewolf y Safari!`,
                    "textToHex" : {
                        "title" : "Contenido de texto en formato hexadecimal.",
                        "paragraph01" : `
                            Este conversor convierte contenido textual, cifrado o no, al formato hexadecimal (HEX). E inversamente.
                            <br>Esta funcionalidad es útil para una transferencia más segura, mediante Código QR, de diferentes caracteres, particularmente los especiales.
                        `,
                        "contentTextFormatField" : "Contenido del texto",
                        "contentConvertedToHexField" : "Convertido a formato hexadecimal",
                        "convertToHexBtn" : "Convertir a hexadecimal"
                    },

                    "hexToText" : {
                        "title" : "Formato hexadecimal a texto",
                        "contentHexFormatField" : "Contenido en formato hexadecimal",
                        "contentConvertedToTextField" : "Convertido a contenido de texto",
                        "convertToTextBtn" : "Convertir a texto"
                    },

                    "imageToText" : {
                        "title" : "Imagen a formato text-base64",
                        "paragraph01" : `
                            <p>
                                Transforme una imagen (JPEG, PNG o GIF) al formato text-base64. Lo ideal es limitar el tamaño de la imagen a menos de 350 KB (2 MB como máximo) utilizando una fuerte compresión en el software de gráficos. Utilice la aceleración de hardware en las opciones de su navegador. Tenga paciencia con las computadoras de baja potencia.
                                <br>Este proceso puede ser útil para cifrar la imagen y/o luego transferirla mediante códigos QR (cuidado, 5 KB para este uso ya equivalen a unos diez códigos QR que parpadearán para la transferencia...)
                                <br>Para encontrar la imagen, utilice el menú debajo de "Formato de texto-base64 a imagen".
                            </p>
                        `,
                        "transformBtn" : "Transformar"
                    },

                    "textToImage" : {
                        "title" : `Text-base64 a formato de imagen`,
                        "paragraph01" : "Contenido en formato text-base64 que se puede transformar en una imagen.",
                        "revealImageBtn" : `Revelar la imagen`
                    }
                },

                "hash" : {
                    "title" : "Huella - Hash",
                    "introduction" : `
                        <p class="alert alert-info">
                            <i class="bi bi-info-circle"></i>
                            Calcula los resultados de las principales funciones hash matemáticas, también llamadas "hashes", para un archivo o una cadena de caracteres. Recomendamos y utilizamos principalmente la función SHA-512 (SHA = Secure Hashing Algorithm)
                        </p>
                        <p class="h4">
                            Huella digital de un archivo
                        </p>
                        <p class="alert alert-warning">
                            <i class="bi bi-exclamation-octagon"></i>
                            Espere varios segundos para ver un archivo grande.
                        </p>
                    `,
                    "fileToCheckHash" : "Archivo para calcular tu huella",
                    "title02" : "Huellas dactilares de una cuerda",
                    "charsChainField" : "Cadena de caracteres",
                    "hashToCompareField" : "Huella para comparar",
                    "hashToCompareFieldHelp" : `
                        Opcional: indique una huella digital SHA-512 para compararla automáticamente con la que se calculará a partir de la cadena de caracteres
                    `,
                    "calculate" : "Calcular"
                },

                "steganography" : {
                    "title" : "Esteganografía",
                    "subtitle" : "Ocultar contenido textual en una imagen", 
                    "introduction" : `La siguiente funcionalidad de esteganografía le permite:
                    <br>
                    <ul>
                        <li><strong>ESCONDER</strong> contenido textual en una imagen</li>
                        <li><strong>REVELAR</strong> contenido textual oculto en una imagen</li>
                    </ul>
                    <p>
                        También es posible ocultar una imagen dentro de otra imagen. Primero se debe convertir la imagen a contenido textual en base 64. Para ello, vaya a la pestaña « <i class="bi bi-arrow-clockwise"></i> Convertidores » y utilice la herramienta <strong>IMAGEN <i class="bi bi-arrow-right-short"></i> TEXTO en base 64</strong>. Recomendamos cifrar el contenido textual resultante antes de ofuscarlo. La funcionalidad <strong>base 64 TEXT <i class="bi bi-arrow-right-short"></i> IMAGEN</strong> te permitirá la operación contraria, es decir la reconstrucción de la imagen a partir del texto. contenido. descifrado de antemano.
                    </p>
                    `,
                    
                    "explanations" : {
                        "title" : "Explicaciones", 
                        "text" : `
                            <div class="alert alert-warning">
                                <i class="bi bi-exclamation-octagon"></i> Oculte únicamente el contenido de texto cifrado en una imagen. Ejemplo: un mensaje, un huella-hash u otra imagen. Todo este contenido debe primero estar cifrado asimétricamente como mínimo.
                                <br>Se desaconseja encarecidamente ocultar contenido textual no cifrado.
                            </div>
                            <p>
                                Si transfiere la imagen a través de un servicio de transferencia especializado o la publica en un correo electrónico, es posible que la imagen se comprima y el contenido oculto no se pueda recuperar.
                            </p>
                            <p>
                                Utilice únicamente imágenes como fotografías de paisajes o plantas y flores tomadas en modo macro. Evite imágenes de periódicos o diagramas.
                                <br>Dependiendo del nivel de seguridad requerido, tenga cuidado de que la imagen no permita su identificación (ubicación conocida, luz solar que caracteriza la hora, reflejos, etc.).
                                <br>Utilice software de código abierto como https://exiftool.org/install.html#Unix (para usuarios más avanzados) para eliminar metadatos que puedan contener su geolocalización, su modelo de cámara o incluso su nombre, etc.
                            </p>
                            <p>
                                Puedes utilizar un banco de imágenes online como Unsplash pero es preferible la foto personal, especialmente tomada desde una cámara desconectada con almacenamiento en tarjeta microSD. Cree un banco de imágenes personales seguro con antelación. Esto puede incluir no haber llevado su teléfono consigo... para evitar cualquier superposición entre su teléfono que se habrá limitado a estos lugares y la vista reconocible, especialmente con IA, y por lo tanto asociable.
                            </p>
                            <p>
                                Evidentemente, tu foto no debe haber sido transmitida por la red en ningún momento. Tenga cuidado con iCloud o cualquier envío a través de Internet por correo electrónico o servicio de transferencia de archivos antes de insertar el contenido textual a ocultar.
                                <br>La inserción de contenido textual cambia la huella digital de la imagen debido a la modificación de ciertos bits. La comparación con la impresión inicial es imposible sin tener la foto original antes de la inserción. Esta imposibilidad de comparar huellas dactilares proporciona una seguridad adicional.
                            </p>
                        `,
                        "training" : {
                            "title": "Entrenarte !",
                            "text": `
                            <p>
                                Las dos imágenes siguientes contienen contenido oculto. La imagen del desierto contiene una OTP descifrable con la clave 27793 33873 22989 05220 80984 29034 63759 54704. La otra imagen contiene una foto de una mujer vestida de rojo; para reconstruir la imagen tendrás que usar la pestaña "<i class="bi bi-arrow-clockwise"></i> Convertidores” y la sección “Text-base64 a formato de imagen”.
                            </p>
                            <p>
                                En el contexto de un uso "profesional", una imagen oculta siempre debe cifrarse antes de ocultarse en otra imagen con, por ejemplo, una clave simétrica AES-256 (sección "Cifrado simétrico"). Esto evita que cualquier persona que utilice XENATX descubra sus imágenes ocultas en otras imágenes.
                                <br>Un buen ejercicio para entrenarte..
                            </p>
                            `
                        }
                    },

                    "hideOrReveal" : {
                        "title" : "Ocultar o revelar contenido textual",
                        "imageToHideTitle" : "Imagen para ocultar o revelar contenido textual",
                        "contentToHideOrReveal" : "Contenido de texto para ocultar o revelar",
                        "imageHidingContent" : "Imagen que oculta texto",
                        "btnHide" : "Esconder",
                        "btnReveal" : "Revelar",
                        "btnDownload" : "Descargar"
                    }
                }
            }
        }
    }
};