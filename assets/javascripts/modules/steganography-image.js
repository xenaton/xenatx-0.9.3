// **************
// STEGANOGRPAHY
// **************

(function() {

    "use strict";

    let base64String = "";

    function imageUploaded() {

        var fileSteganography = document.getElementById("hideFile").files[0];

        var readerSteganography = new FileReader();
        
            // console.log('test');
        
        readerSteganography.onload = function () {
            base64String = readerSteganography.result.replace("data:", "")
                .replace(/^.+,/, "");
    
            imageBase64Stringsep = base64String;
            read(imageBase64Stringsep);
        }
        if(fileSteganography) {
            readerSteganography.readAsDataURL(fileSteganography);
            removeDisabled('cancelHideOrReveal');
        }
    }
    
    function read(imageBase64Stringsep) {
        
        var texta = document.createElement("textarea");
        texta.setAttribute('id', 'texta');
        texta.setAttribute('class', 'form-control');
        texta.setAttribute('rows', '8');
        
        textaParent = document.getElementById("textaLocation");
        textaParent.innerHTML = ''; // empty the eventual precedent textarea

        textaParent.appendChild(texta);
        textarea = document.getElementById("texta");
        textarea.value = imageBase64Stringsep;
    }

    function base64ToImage() {
        var imageBase64Stringsep = document.getElementById("texta2").value;
        if(imageBase64Stringsep) {
            var image = document.createElement("img"),
                imageParent = document.getElementById("imageLocation");

            imageParent.innerHTML = ''; // empty the eventual precedent image
            image.id = "insertedImage";
            image.className = "insertedImage";
            // image.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==";
            image.src = 'data:image/png;base64,' + imageBase64Stringsep;
            imageParent.appendChild(image);
        } 
        else {
            alert("Merci d'insérer un contenu textuel encodé en base64.");
        }
    }

    function handleFileSelect(evt) {

        // remove potential notification message
        var errorSteganography = document.getElementById("errorSteganography");
            errorSteganography.innerHTML = '';

        var original = document.getElementById("original"),
            stego = document.getElementById("stego"),
            img = document.getElementById("img"),
            cover = document.getElementById("cover");

        if(!original || !stego) return;

        var files = evt.target.files; // FileList object

        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                errorSteganography.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.steganographyFileNotImage') + '</p>';
                removeDisabled('cancelHideOrReveal');
                continue;
            }
            removeDisabled('cancelHideOrReveal');

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    img.src = e.target.result;
                    img.title = escape(theFile.name);
                    stego.className = "half invisible";
                    cover.src = "";
                    updateCapacity();
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    function hide() {
        var stego = document.getElementById("stego"),
            img = document.getElementById("img"),
            cover = document.getElementById("cover"),
            textarea = document.getElementById("text"),
            download = document.getElementById("download"),
            encrypted = document.getElementById("encrypted");

        if(img && textarea) {
            cover.src = steg.encode(textarea.value, img);
            stego.className = "half";
            encrypted.className = "well";
            download.href = cover.src.replace("image/png", "image/octet-stream");

            // var element = document.getElementById("imgLoaded");
            // var imgLoaded = element.getBoundingClientRect();
            
            var anchorBottom = document.querySelector('#anchorBottom');

            removeDisabled('cancelHideOrReveal');

            setTimeout(function () {
                anchorBottom.scrollIntoView();
            }, 300);

            window.scrollTo({
                behavior: "smooth"
            });
        }
    }

    function reveal() {
        var img = document.getElementById("img"),
            cover = document.getElementById("cover"),
            textarea = document.getElementById("text");
        if(img && textarea) {
            var decryptedMessage = steg.decode(img);
            if(decryptedMessage !== "") {
                textarea.value = decryptedMessage;
                updateCapacity();
                removeDisabled('cancelHideOrReveal');
            }
        }
    }

    function updateCapacity() {
        var img = document.getElementById('img'),
            textarea = document.getElementById('text');
        if(img)
            if(!textarea) {
                var textareaLength = "0";
            }else {
                var textareaLength = textarea.value.length;
            }
            document.getElementById('capacity').innerHTML = textareaLength + ' / ' + steg.getHidingCapacity(img) +' '+ i18next.t('error.steganographyhidingCapacity');
    }

    window.onload = function(){
        document.getElementById('transformBtn').addEventListener('click', imageUploaded, false);
        document.getElementById('base64ToImageBtn').addEventListener('click', base64ToImage, false);
        document.getElementById('hideFile').addEventListener('change', handleFileSelect, false);

        document.getElementById('text').addEventListener('keyup', updateCapacity, false);
        document.getElementById('cancelHideOrReveal').addEventListener('click', reload, false);
        document.getElementById('cancelDownload').addEventListener('click', reload, false);

        // window.onload for morce placed here in steganography-image.js to not redefine window.onload
        document.getElementById('btnMorseStop').addEventListener('click', reload, false);
        updateCapacity();
    };

    //******
    // Hide
    //******
    var steganographyHideFeature = document.getElementById('hide');
    if(steganographyHideFeature){
        steganographyHideFeature.addEventListener('click', function(e){
            e.preventDefault();
            var errorSteganography = document.getElementById("errorSteganography");
                errorSteganography.innerHTML = '';

            if( document.getElementById("hideFile").files.length == 0 ){                        
                errorSteganography.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.steganographyFileEmpty') + '</p>'
                return;
            }

            var steganographyTextField = document.getElementById('text').value;
            if('' == steganographyTextField) {                           
                errorSteganography.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.steganographyHideEmpty') + '</p>'
                return;
            }
            hide();
        })
    }

    //******
    // Reveal
    //******
    var steganographyRevealFeature = document.getElementById('reveal');
    if(steganographyRevealFeature){
        steganographyRevealFeature.addEventListener('click', function(e){
            e.preventDefault();
            var errorSteganography = document.getElementById("errorSteganography");
                errorSteganography.innerHTML = '';
               
            if( document.getElementById("hideFile").files.length == 0 ){                        
                errorSteganography.innerHTML = '<p class="alert alert-danger"><i class="bi bi-exclamation-circle"></i> ' + i18next.t('error.steganographyRevealEmpty') + '</p>'
                return;
            }
            reveal();
        })
    }
})();
