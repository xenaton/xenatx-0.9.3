//*******************
// VARIOUS FUNCTIONS
//*******************

// STOP
//******
// Used in morse and steganography
// For morse feature, do a real web api stop button one day as this is just a reload button which reload the entire page so no possibility to go on after a temporary stop.
function reload() {
    location.reload(true); // the "true" option means cache will be removed: what we need.
}

function clearInputFile(f){
    f.value = null
}

function moduloChecker(inputField, number){
    if(inputField % number == 0) {
        return true;
    }
    return false;
}

function addText(content, status) {
    document.getElementById(status).value = content;
}

function removeDisabled(elementID) {
    document.getElementById(elementID).removeAttribute('disabled', '');
}

function setDisabled(elementID) {
    document.getElementById(elementID).setAttribute('disabled', '');
}

function addHTMLContent(content, status) {
    document.getElementById(status).innerHTML = content;
}

function removeSpaces(str) {
    return str.replace(/\s/g,'');
}

function removeBrackets(str) {
    return str.split('[').join("").split(']').join("");
}

function visualHash(canvas, input, width = 128, height = 128) {
    canvas.setAttribute("width", width);
    canvas.setAttribute("height", height);
    mosaicVisualHash(input, canvas,
        { jitter: parseInt(3) }
    );
}

// check key OTP is a multiple of 5 blocks to be able to end the bloc with points..
function checkMultiple5Block(id) {
    var textareaValue = document.getElementById(id).value.replace(/\s/g,''); // delete space before testing modulo
    if(textareaValue.length % 5 === 0) {
        return true;
    }
    return false;
}

function completeOTPNumericBlockOf5(id) {
    var textareaValue = document.getElementById(id).value.replace(/\s/g,''); // delete space before testing modulo
    var remainder = textareaValue.length % 5;
    var newEntry = document.getElementById(id);

    if (remainder == 1) {
        newEntry.value += '9191';
    } 
    else if (remainder == 2) {
        newEntry.value += '919';
    } 
    else if (remainder == 3) {
        newEntry.value += '91';
    } 
    else if (remainder == 4) {
        newEntry.value += '9';
    }

    // Clear Code counter
    var charCounterCryptClearCode = document.getElementById("charCounterCryptClearCode");   
    var OTPClearCodeTextarea = document.querySelector('#OTPClearCode');
    const countCharactersClearCode = () => {
        let numOfEnteredChars = OTPClearCodeTextarea.value.replace(/\s/g,'').length;
        charCounterCryptClearCode.textContent = numOfEnteredChars;
    };
    countCharactersClearCode();
}

// equivalent to OTPSeparatorEveryNChar below but longer...
function separatorOTPNumericEveryNChar(str, separator, nChar) {
    var chain = '';
    var lastNumbers = '';
    var resultTab = [];

    for (var i=0; i < str.length; i++) {
        chain += str[i];
        lastNumbers += str[i];

        if(chain.length % nChar === 0) {
            var temp = chain.slice(chain.length - nChar);
            resultTab.push(temp);
            lastNumbers = '';
        }
    }
    resultTab.push(lastNumbers);

    return resultTab.join(separator);
}

// space each five caracters
function OTPSeparatorEveryNChar(str, separator) {
    const result = str.match(/.{1,5}/g) ?? [];
    return result.join(separator);
}

function separatorEveryNChar(str, separator, nChar) {
    var chain = '';
    var resultTab = [];
    for (var i=0; i < str.length; i++) {
        chain += str[i];

        if(chain.length % nChar === 0) {
            var temp = chain.slice(chain.length - nChar);
            resultTab.push(temp);
        }
    }
    return resultTab.join(separator);
}

function hasNumber(str) {
    return /\d/.test(str);
}


// check if string 128 charcters long (SHA-512 length)
function is128chars(val) {
    if(val.length == 128) {
        return true;
    }
    return false;
}

function isBase64(str) {
    var base64regex = /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;
    return base64regex.test(str);
}

// if you want "23" to be a number, then
function isNumber(val) {
    return !isNaN(val);
}

function isAlphaUpper(str) {
    return /^[A-Z()\s]+$/.test(str);
}

// only alphanum upper plus DOT only
function isAlphaNumUpper(str) {
    var regEx  = /^[A-Z\d\s\.]+$/;
    return regEx.test(str);
}

// only alphanum upper plus . : ' + - = ?
function isAlphaNumUpperOTPNumeric(str) {
    var regEx  = /^[A-Z\d\s\.\:\'\+\-\=\?\(\)\[\]]+$/;
    return regEx.test(str);
}

function isHex(str) {
    var regEx = /^[0-9a-fA-F]+$/;
    return regEx.test(str);
}


function xenStrToHex(str) {
    try {
        var words = CryptoJS.enc.Utf8.parse(str);
        var hex = CryptoJS.enc.Hex.stringify(words);

        return hex;
    } 
    catch (e) {
        if(e) {
            return "Erreur. Impossible d'encoder ces caractères vers le format hexadécimal !"; 
        }  
    }
}

function xenHexToStr(hex) {
    try {
        var words = CryptoJS.enc.Hex.parse(hex);
        var str = CryptoJS.enc.Utf8.stringify(words);

        return str;       
    } 
    catch (e) {
        if(e) {
            return "Erreur. Impossible de décoder ces caractères vers le format texte !"; 
        }  
    }
}

// https://developer.mozilla.org/fr/docs/Glossary/Base64
function utf8_to_b64(str) {
  return window.btoa(unescape(encodeURIComponent( str )));
}

function b64_to_utf8(str) {
  return decodeURIComponent(escape(window.atob( str )));
}

function sleep(ms) {
    const date = Date.now()
    let currentDate = null
    do currentDate = Date.now()
    while (currentDate - date < ms)
}

/****************************************
 * Various common functions (jericho-OTP)
 ****************************************/
var common = {

    /**
     * Left pad a string with a certain character to a total number of characters
     * @param {String} inputString The string to be padded
     * @param {String} padCharacter The character/s that the string should be padded with
     * @param {Number} totalCharacters The length of string that's required
     * @returns {String} A string with characters appended to the front of it
     */
    leftPadding: function(inputString, padCharacter, totalCharacters)
    {
        // Convert to string first, or it starts adding numbers instead of concatenating
        inputString = inputString.toString();

        // If the string is already the right length, just return it
        if (!padCharacter || (inputString.length >= totalCharacters))
        {
            return inputString;
        }

        // Work out how many extra characters we need to add to the string
        var charsToAdd = (totalCharacters - inputString.length) / padCharacter.length;

        // Add padding onto the string
        for (var i = 0; i < charsToAdd; i++)
        {
            inputString = padCharacter + inputString;
        }

        return inputString;
    },

    /**
     * Converts binary code to hexadecimal string. All hexadecimal is lowercase for consistency with the hash functions
     * These are used as the export format and compatibility before sending via JSON or storing in the database
     * @param {String} binaryString A string containing binary numbers e.g. '01001101'
     * @returns {String} A string containing the hexadecimal numbers
     */
    convertBinaryToHexadecimal: function(binaryString)
    {
        var output = '';

        // For every 4 bits in the binary string
        for (var i = 0; i < binaryString.length; i += 4)
        {
            // Grab a chunk of 4 bits
            var bytes = binaryString.substr(i, 4);

            // Convert to decimal then hexadecimal
            var decimal = parseInt(bytes, 2);
            var hex = decimal.toString(16);

            // Append to output
            output += hex;
        }

        return output;
    },

    /**
     * Converts hexadecimal code to binary code
     * @param {String} hexString A string containing single digit hexadecimal numbers
     * @returns {String} A string containing binary numbers
     */
    convertHexadecimalToBinary: function(hexString)
    {
        var output = '';

        // For each hexadecimal character
        for (var i = 0; i < hexString.length; i++)
        {
            // Convert to decimal
            var decimal = parseInt(hexString.charAt(i), 16);

            // Convert to binary and add 0s onto the left as necessary to make up to 4 bits
            var binary = this.leftPadding(decimal.toString(2), '0', 4);

            // Append to string
            output += binary;
        }

        return output;
    },

    /**
     * This function does a bitwise exclusive or (XOR) operation on two bitstreams. This can be used to encrypt or
     * decrypt data by doing a XOR on the key and the plaintext. The two bitstreams should be of the same length.
     * @param {String} bitsA The first stream of bits e.g.  '01010101'
     * @param {String} bitsB The second stream of bits e.g. '00001111'
     * @returns {String} A binary string containing the XOR of the first and second bitstreams e.g. '01011010'
     */
    xorBits: function(bitsA, bitsB)
    {
        // Get the lengths of the two bitstreams
        var lengthBitsA = bitsA.length;
        var lengthBitsB = bitsB.length;

        // If the lengths of each stream of bits is different then this could be a serious problem e.g. the whole
        // message does not get encrypted properly. This is added as a basic defense against possible coding error.
        if (lengthBitsA !== lengthBitsB)
        {
            throw new Error(i18next.t('error.xorDifferentLengths') + ' ' + new Error().stack);
        }

        var output = '';

        // For each binary character in the message
        for (var i = 0; i < lengthBitsA; i++)
        {
            // Get binary number of the two bitstreams at the same position
            var binaryDigitA = bitsA.charAt(i);
            var binaryDigitB = bitsB.charAt(i);

            // XOR the binary character of the pad and binary text character together and append to output
            output += (binaryDigitA ^ binaryDigitB);
        }

        return output;
    },

    /**
     * A function to XOR two hexadecimal strings together
     * @param {String} hexStringA The first string of hexadecimal symbols e.g. 'a7d9'
     * @param {String} hexStringB The second string of hexadecimal symbols e.g. 'c72a'
     * @returns {String} The result of the strings XORed together e.g. '60f3'
     */
    xorHex: function(hexStringA, hexStringB)
    {
        // Convert the hexadecimal to binary
        var bitsA = common.convertHexadecimalToBinary(hexStringA);
        var bitsB = common.convertHexadecimalToBinary(hexStringB);

        // XOR the bit strings together and convert back to hexadecimal
        var xoredBits = common.xorBits(bitsA, bitsB);
        var xoredBitsHex = common.convertBinaryToHexadecimal(xoredBits);

        return xoredBitsHex;
    }
};
